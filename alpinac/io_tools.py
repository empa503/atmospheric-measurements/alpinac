"""IO tools package. 

Helpers for various files io operations.
"""

import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path

import numpy as np
import pandas as pd
from numpy.typing import ArrayLike

from alpinac.mass_calibration.utils import number_of_params
from alpinac.periodic_table import formula_to_mass
from alpinac.utils_data_extraction import f_tofidx_to_mass

logger = logging.getLogger("alpinac.io_tools")


# format of fragment file to read from:
ic_rt = 0  # rt, seconds
ic_m = 1  # mass, average of peak
# uncertainty of mass, ppm (measurement noise only) 201001 to add cal u as well
ic_m_u = 2
ic_m_cal_u = 3
ic_area = 4  # area of peak
ic_area_u = 5  # uncertainty of area
ic_m_sigma = 6
ic_alpha = 7
ic_comp_bin = 8  # index of group of co-eluting fragments
ic_LOD = 9  # LOD value
ic_SN = 10  # signal to noise ratio
ic_ionisation = 11  # ionisation type, support EI and CI
ic_adduct = 12
ic_run_bin = 13  # run index, to treat replicate measurements together
# or various runs of presumably the same compound together


# indices of mass and comp_bin in each tuple of output:
idx_rt = 0
idx_m = 1
idx_m_u = 2
idx_m_cal_u = 3
idx_area = 4
idx_area_u = 5
idx_peak_width = 6
idx_peak_alpha = 7
idx_comp_bin = 8
idx_LOD = 9
idx_ionisation = 10
idx_adduct = 11
idx_run_bin = 12
idx_SN = None


# format of formula file to read from:
iform_percent_norm_I = 0
iform_rt = 1
iform_total_I = 2
iform_total_I_percent = 3
iform_mass_meas = 4
iform_mass_exact = 5
iform_mass_u_ppm = 6
iform_mass_diff_ppm = 7
iform_formula = 8
iform_DBE = 9
iform_I = 10
iform_I_f = 11
iform_ionisation = 12

# fmt: on


def read_gcwerks_chromatogram_file(filename: Path) -> dict[str, pd.Series]:
    """Read data from a GCWerks chromatogram file."""

    filename = Path(filename)

    with filename.open("r") as f:
        rts = {}
        intensities = {}
        current_chromatogram = None
        for i, line in enumerate(f.readlines()):
            if line.startswith("#"):
                continue
            raw = line.split()
            if len(raw) < 2:
                raise ValueError(f"Line {i} has less than 2 columns: {line}")
            rt, intensity = raw
            if rt == "mass":
                # Gcwerks will round to 4 decimals
                current_chromatogram = f"{float(intensity):.4f}"
                rts[current_chromatogram] = []
                intensities[current_chromatogram] = []
            else:
                rts[current_chromatogram].append(float(rt))
                intensities[current_chromatogram].append(float(intensity))

    # Convert to pandas Series
    return {chrom: pd.Series(intensities[chrom], index=rts[chrom]) for chrom in rts}


# fmt: off

  
    
def get_data_from_frag_file(filename: Path) -> dict:
    """Read data (mass, etc) and return a structured dict.
    
    Read data from a file obtained after calibration, adjust mass: add
    the electron mass to compensate the EI (electron ionisation), return
    a dict indexed by bin number.

    INPUT:
    - ``filename`` -- Path object, can be opened with .open('r')
    - ``electron_mass`` -- float, mass of the electron (0.00054858)
    
    OUTPUT: a dict indexed by bin number (integer value of column `ic_comp_bin`)
    Data are in this order:
    RT	mass	mass_u_ppm	mass_cal_u_ppm	area	area_u	peak_width	peak_alpha	compound_bin	LOD
    """
    logger.debug(f"get_data_from_frag_file({filename=})")
    # open filename with Path syntax
    f = filename.open('r')
    # 2. Read file
    frag_data_batch = {}
    for line in f:
        raw = line.split()
        logger.debug(f"line splitted = {raw}")
        if (
            not raw  # Empty lines
            or raw[0] == 'RT' # header
         ): 
            continue
        
        #insure backward compatibility: 
            #if ionisation column does not exist, assume EI
            #if adduct column does not exist, assume None
            #if run_bin column dies not exist, assume one unique run and assigne index 0 to all lines.
        if len(raw)-1 < ic_ionisation:
            row = [float(raw[i]) for i in range(ic_alpha+1)] + [int(raw[ic_comp_bin])] + [float(raw[ic_LOD])] + ["EI"] + [""] + [int(0)]
        elif len(raw)-1 == ic_ionisation:
            row = [float(raw[i]) for i in range(ic_alpha+1)] + [int(raw[ic_comp_bin])] + [float(raw[ic_LOD])] + [raw[ic_ionisation]] + [""] + [int(0)]
        else:
            adduct = raw[ic_adduct]
            if adduct == "None":
                adduct = ""
            if len(raw)-1 == ic_adduct:
                row = [float(raw[i]) for i in range(ic_alpha+1)] + [int(raw[ic_comp_bin])] + [float(raw[ic_LOD])] + [raw[ic_ionisation]] + [adduct] + [int(0)]
            elif len(raw)-1 == ic_run_bin:
                row = [float(raw[i]) for i in range(ic_alpha+1)] + [int(raw[ic_comp_bin])] + [float(raw[ic_LOD])] + [raw[ic_ionisation]] + [adduct] + [str(raw[ic_run_bin])]
            else:
                raise ValueError(f"Too many columns in line {raw}")
            

        row[ic_m_u] = row[ic_m_u]/10**6 * row[ic_m] #convert u mass in ppm in m/z
        row[ic_m_cal_u] = row[ic_m_cal_u]/10**6 * row[ic_m] #convert mass_cal_u from ppm to m/z
        logger.debug(f"parsed row = {row}")
        comp_bin = row[idx_comp_bin]
        #logging.info(comp_bin)
        if comp_bin in frag_data_batch:
            frag_data_batch[comp_bin].append(tuple(row))
        else:
            frag_data_batch[comp_bin] = [tuple(row)]
    f.close()
    #logging.info(frag_data_batch)
    
    
    return frag_data_batch

@dataclass
class NISTMetadata():
    chem_name: str
    target_formula: str
    mol_mass_exact: str
    CAS: str

    

def get_data_from_jdx_file(filename: Path) -> tuple[dict[int, list[tuple]], NISTMetadata]:
    """load unit mass resolution EI mass spectra from NIST database

    Read data from NIST file. This is unit mass resolution. Format the 
    data so that it matches the format of the function 
    `get_data_from_frag_file`.

    INPUT:
    - ``filename`` -- Path object, can be opened with .open('r')
    
    OUTPUT: a dictionary indexed by the bin number (usually 0 for NIST)
    """
    # Jcamp is not a mandatory import for us
    from jcamp import jcamp_read  # to read NIST spectra

    with open(filename, 'r') as f:
        dict_target = jcamp_read(f)

    frag_data_batch = {0:[]}


    target_formula = ''.join(dict_target['molform'].split())
    frag_metadata = NISTMetadata(
        chem_name = dict_target['title'],
        target_formula = target_formula,
        mol_mass_exact = formula_to_mass(target_formula),
        CAS = dict_target['cas registry no']  if 'cas registry no' in dict_target else 'no_CAS_' + dict_target['title']
    )
    
    #logging.info('CAS number: ' + str(self.CAS))
    
    LOD = min(dict_target['y'])*0.1


    for i in range(len(dict_target['x'])):
        frag_data_batch[0].append(tuple([
                0.0, #rt
                float(dict_target['x'][i]),#meas mass
                0.0, #meas mass u [m/z]
                0.0, #m_cal_u
                float(dict_target['y'][i]), #area
                0.05*dict_target['y'][i], #area u
                0.0, #ic_width
                0.0, #ic_alpha
                0,#comp_bin
                LOD,
                "EI",
                "", # adduct 
                0                
            ]))
    return frag_data_batch, frag_metadata




# =============================================================================
# #format of formula file to read from:
# iform_percent_norm_I = 0
# iform_rt = 1
# iform_total_I = 2
# iform_total_I_percent = 3
# iform_mass_meas = 4
# iform_mass_exact = 5
# iform_mass_u_ppm = 6
# iform_mass_diff_ppm = 7
# iform_formula = 8
# iform_DBE = 9
# iform_I = 10 #intensity computed for this formula only
# iform_I_f = 11
# iform_ionisation = 12
# =============================================================================

def get_data_from_formula_file(filename: Path) -> dict:
    """Read data (mass, etc) and return a structured dict.
    
    Read data from a file obtained after identification.

    INPUT:
    - ``filename`` -- Path object, can be opened with .open('r')

    
    OUTPUT: a dict indexed by bin number (integer value of column `ic_comp_bin`)
    important will be colun 11, fraction that this formula represent for total signal at this mass peak.
    
    """
    # open filename with Path syntax
    f = filename.open('r')
    # 2. Read file
    frag_data_batch = {}
    extract_data = False
    for line in f:
        raw = line.split()
        #logging.info(raw[0])
        #while extract_data == False:
        if extract_data == False and raw[0] != '%': # header
            continue
        else:
            extract_data = True
            #logging.info("extract_data = True")
                
        if extract_data == True and raw[0] != '%' :
            #logging.info("extract")
            #logging.info(raw)
            row = [float(raw[i]) for i in range(iform_formula)] + \
            [raw[iform_formula]] + \
            [float(raw[iform_DBE])] + \
            [float(raw[iform_I])] + \
            [float(raw[iform_I_f])] + \
            [raw[iform_ionisation]]
            #logging.info(row)
            comp_bin = 0
            #logging.info(comp_bin)
            if comp_bin in frag_data_batch:
                frag_data_batch[comp_bin].append(tuple(row))
            else:
                frag_data_batch[comp_bin] = [tuple(row)]
    f.close()
    #logging.info(frag_data_batch)
    
    
    return frag_data_batch

def export_mc(file,
              rt_idx_centre_list, 
              params_optimised, 
              sigma_tofidx_slope, 
              sigma_tofidx_b, 
              sigma_m_slope, 
              sigma_m_b, 
              alpha_pseudo_voigt,
              masscal_list_exact_export, 
              mass_cal_u, 
              masscal_list_exact,
              s_per_bin,
              mass_cal_mode: int,
):
    
    mass_cal_lines: list[str] = []

    for idx_rt in range(len(params_optimised)):
        rt_index = rt_idx_centre_list[idx_rt]
        line_values = [rt_index]
        this_rt_params = params_optimised[idx_rt]
        if this_rt_params is None:
            logger.error(f"Could not fit a mass calibration for RT = {rt_index * s_per_bin}")
            continue
        nb_params = number_of_params(mass_cal_mode)
        for param in range(1, nb_params + 1):
            line_values.append(this_rt_params[f'p{param}'])
        
        mass_cal_lines.append('\t'.join([str(v) for v in line_values]))
    
    if len(mass_cal_lines) == 0:
        raise ValueError("The mass calibration could not be fitted for any of the given RTs")

    lines = [
        (len(mass_cal_lines), 'Number of points, mass cal. vs RT'),
        (len(mass_cal_u), 'Number of points, mass cal. u vs mass'),
        (mass_cal_mode, 'Mass cal. mode'),
        (sigma_tofidx_slope, 'Peak shape, sigma_slope vs TOF index'),
        (sigma_tofidx_b, 'Peak shape, sigma_b vs TOF index'),
        (sigma_m_slope, 'Peak shape, sigma_slope vs mass'),
        (sigma_m_b, 'Peak shape, sigma_b vs mass'),
        (alpha_pseudo_voigt, 'alpha, for tofidx Pseudo-Voigt fit'),
        (s_per_bin, 's_per_bin'),
    ]
    with file.open('w') as outfile:
        for value, description in lines:
            outfile.write(f"{value}\t{description}\n")

        for line_str in mass_cal_lines:
            outfile.write(line_str + '\n')

        for idx_m in range(len(mass_cal_u)):
            outfile.write('{:.7f}'.format(masscal_list_exact_export[idx_m]) + '\t' + '{:.2f}'.format(mass_cal_u[idx_m]) + '\n')
            
        #20200319 new: write detailed list of masses used for mc per time bin
        #to be used later for re-calibration during identification of mass peaks.
        
    return None


MassCalibrationParameters = dict[str, float | int]
# Abstract class for mass calibration

class MassCalibrationData(ABC):
    @abstractmethod
    def get_parameters_for_rt(self, rt: float)-> MassCalibrationParameters:
        """Get parameters for a given retention time."""
        ...
    
    @abstractmethod
    def _calibrate(self, rt: np.ndarray, tofidx: np.ndarray):
        """Internal method to do mass calibration for the points at the given coordinates."""

        ...

    def calibrate(self, rt: ArrayLike, tofidx: ArrayLike):
        """Do mass calibration for the points at the given coordinates.

        args:
        - rt: retention time in seconds
        - tofidx: time of flight indexes

        returns:
        - mass_meas_list: list of float, masses in m/z
        """
        rt = np.array(rt)
        tofidx = np.array(tofidx)

        if len(rt) != len(tofidx):
            raise ValueError(f"rt and tofidx must have the same length. Got {len(rt)=} and {len(tofidx)=}")
        
        return self._calibrate(rt, tofidx)

class MassCalibrationH5(MassCalibrationData):
    """Default mass calibration from the TOF data."""


    # Default values which should usually be calculated from the data
    # Peak shape, sigma_slope vs TOF index
    sigma_tofidx_slope: float = 3.6e-05 
    # Peak shape, sigma_b vs TOF index
    sigma_tofidx_b: float = 0.51  
    # Peak shape, sigma_slope vs mass
    sigma_m_slope: float = 8.36e-05  
    # Peak shape, sigma_b vs mass
    sigma_m_b: float = 0.0015  
    # alpha, for tofidx Pseudo-Voigt fit
    alpha_pseudo_voigt: float = 0.48  

    # Defaults for mass calibration
    mass_cal_u_exact_mass = [0, 400]
    mass_cal_u = [50, 50]

    def __init__(self, filepath: Path)-> None:

        import h5py

        tof_file = h5py.File(filepath, 'r')

        self.params = {}
        mode = tof_file["FullSpectra"].attrs.get("MassCalibMode")

        self.params["mode"] = mode
        # all calibration modes have at least 2 parameters
        for param in range(1,5):
            if param == 3:
                if mode in [0, 1]:
                    continue
            elif param == 4:
                if mode in [0, 1, 2]:
                    continue
            elif param == 5:
                if mode in [0, 1, 2, 3]:
                    continue

            self.params[f"p{param}"] = float(
                tof_file["FullSpectra"].attrs.get(f"MassCalibration p{param}")
            )
    
    def get_parameters_for_rt(self, rt: float)-> dict[str, float]:
        """Every RT has the same mass calibration parameters."""

        return self.params
    
    def _calibrate(self, rt: np.ndarray, tofidx: np.ndarray):
        mass_calib_params = self.params
        if mass_calib_params['mode'] != 2:
            raise NotImplementedError(
                "Only modes 2 is currently supported. Please implement. "
                # The problem is mostly that higher modes need fancy methods to 
                # find the zero of a function.
            )
            
        return f_tofidx_to_mass(tofidx, mass_calib_params)
            
class MassCalibrationDataGCMS(MassCalibrationData):
    """Read the mass calibration for GCMS data.
    
    The GCMS mass calibration must be done at different times to correct for 
    the mass drift.
    """
    def __init__(self, filename: Path)-> None:
        """Read data from mass calibration file
        
        INPUT:
        - ``filename`` -- Path object, can be opened with .open('r')
        """
        if filename.suffix == ".h5":
            filename = filename.with_suffix(".mc.txt")
            # warning: this is the single_mode file, specify if EI/CI device
            logger.warning(f"Mass calibration file {filename} is a general mass calibration, not specified for EI/CI, give specific file if EI/CI device")
        mass_cal_file = filename.open('r')
        nb_mc_params = 9 #number of parameters saved in the mass cal file
        self.mass_cal_u = []
        self.mass_cal_u_exact_mass = []
        self.mass_cal_tof_values = []
        
        # line 1:
        line = mass_cal_file.readline(); raw=line.split()
        mass_cal_len = int(raw[0])
        # line 2
        line = mass_cal_file.readline(); raw=line.split()
        mass_cal_u_len = int(raw[0])
        # line 3
        line = mass_cal_file.readline(); raw=line.split()
        self.mass_cal_mode = int(raw[0])
        # line 4
        line = mass_cal_file.readline(); raw=line.split()
        self.sigma_tofidx_slope = float(raw[0])
        # line 5
        line = mass_cal_file.readline(); raw=line.split()
        self.sigma_tofidx_b = float(raw[0])
        # line 6
        line = mass_cal_file.readline(); raw=line.split()
        self.sigma_m_slope = float(raw[0])
        # line 7
        line = mass_cal_file.readline(); raw=line.split()
        self.sigma_m_b = float(raw[0])
        # line 8
        line = mass_cal_file.readline(); raw=line.split()
        self.alpha_pseudo_voigt = float(raw[0])
        # line 9
        line = mass_cal_file.readline(); raw=line.split()
        self.s_per_bin = float(raw[0])
        
        #load mass calibration parameters: rt, param1, param2, param3 (using mode 2)
        self.mass_cal_parameters = [None]*mass_cal_len
        for i in range(mass_cal_len):
            line = mass_cal_file.readline(); raw=line.split()
            # test if raw[3] is empty list
            rt = float(raw[0]) * self.s_per_bin
            if raw[3] == '[]':
                # fallback to 2-parameter fit with p3 = 0.5
                raw[3] = 0.5 
                logger.warning(f"Mass calibration parameter 3 is empty for RT = {rt}, set to 0.5")
            
            self.mass_cal_parameters[i] = [rt, float(raw[1]), float(raw[2]), float(raw[3])]
        #remove None lines
        #self.mass_cal_parameters = [x for x in self.mass_cal_parameters if x is not None]

        #extract mass calibration uncertainty, varying over the mass domain:
        if mass_cal_u_len > 0:
            self.mass_cal_u_exact_mass = [None]*mass_cal_u_len
            self.mass_cal_u = [None]*mass_cal_u_len
            for i in range(mass_cal_u_len):
                line = mass_cal_file.readline(); raw=line.split()
                self.mass_cal_u_exact_mass[i] = float(raw[0])
                self.mass_cal_u[i] = float(raw[1])
        else:
            self.mass_cal_u_exact_mass = [0, 400]
            self.mass_cal_u = [50, 50]
            
        mass_cal_file.close()

    def get_parameters_for_rt(self, rt: float)-> dict[str, float]:
        """Get parameters for a given retention time.

        Uses simple interpolation (lever rule) between the previous and next time 
        registerd in the data.

        INPUT:
        - ``rt`` -- float, retention time in seconds
        """

        out_dict = {}
        out_dict['mode'] = self.mass_cal_mode
        # find the two closest times
        rts = np.array([params[0] for params in self.mass_cal_parameters ])
        idx = np.searchsorted(rts, rt)

        if idx == 0 or idx == len(self.mass_cal_parameters):
            if idx == 0:
                # rt is before the first time
                params = self.mass_cal_parameters[0]
            else:
                # rt is after the last time
                params = self.mass_cal_parameters[-1]

            for i in range(1, len(params)):
                out_dict[f'p{i}'] = params[i]
            
        else:
            # rt is between two times
            # interpolate
            t_before = self.mass_cal_parameters[idx-1][0]
            t_after = self.mass_cal_parameters[idx][0]
            params_before = self.mass_cal_parameters[idx-1][1:]
            params_after = self.mass_cal_parameters[idx][1:]
            for i in range(len(params_before)):
                out_dict[f'p{i + 1}'] = (
                    params_before[i]
                    + (params_after[i] - params_before[i])
                    * (rt - t_before) / (t_after - t_before)
                    )

        return out_dict
    


    def _calibrate(self, rt: np.ndarray, tofidx: np.ndarray):
        # for each time, calculate corresponding mass calibration parameters at this time.
        # Then apply this set of parameters to the given measured time of flight.
        rt_cal_list = [line[0] for line in self.mass_cal_parameters]
        p1_cal_list = [line[1] for line in self.mass_cal_parameters]
        p2_cal_list = [line[2] for line in self.mass_cal_parameters]
        p3_cal_list = [line[3] for line in self.mass_cal_parameters]
        
        #calculate values of parameters for each time in rt_list:
        p1 = np.interp(rt, rt_cal_list,  p1_cal_list)
        p2 = np.interp(rt, rt_cal_list,  p2_cal_list)
        p3 = np.interp(rt, rt_cal_list,  p3_cal_list)
        
        mass_calib_params = {}
        mass_calib_params['mode'] = self.mass_cal_mode
        mass_calib_params['p1'] = p1
        mass_calib_params['p2'] = p2
        mass_calib_params['p3'] = p3
            
        if self.mass_cal_mode != 2:
            raise NotImplementedError(
                "Only modes 2 is currently supported. Please implement. "
                # The problem is mostly that higher modes need fancy methods to 
                # find the zero of a function.
            )
            
        return f_tofidx_to_mass(tofidx, mass_calib_params)
