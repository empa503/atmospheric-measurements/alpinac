"""Molecules to use for validation.

The data is stored in a csv file.
This module loads the data into a list of dictionaries.

The table contains some unkown compounds, with names starting with Mrs names 
of the form: 'Ms_Nane', 'Mr_Name'

Table contains columns:

* CAS number
* Compound
* Chemical name
* boiling point
* Sum formula
* SMILES code
* aliases

All chemical names, SMILES codes, and boiling points are from ChemSpider: http://www.chemspider.com/
and PubChem: https://pubchem.ncbi.nlm.nih.gov/



"""
from pathlib import Path

import pandas as pd

labels = (
    "CAS",
    "Compound",
    "Chemical name",
    "Boiling point",
    "Sum formula",
    "SMILES code",
    "Aliases",
)



SUBSTANCES_CSV = Path(__file__).parent / "substances.csv"


def get_molecules_for_validation():
    """
    Generate dictionaries of information about molecules for validation.

    For each molecule, the dictionary consists of the following information (and keys):
    CAS number, Compound, Chemical name, Boiling point, Sum formula,
    SMILES code, Aliases.
    """
    
    

    df = pd.read_csv(SUBSTANCES_CSV, index_col=0)
    df = df.replace(pd.NA, "")
    mol_validation_tmp = []

    # Iterate over each row
    for rows in df.itertuples(False):
        # Create tuple for the current row
        processed_row = tuple(rows)
        # the last column is saved as a string of a list, therefore we need to remove the brackets and split it
        processed_row = processed_row[:-1] + (
            processed_row[-1].strip("]['").split("', '"),
        )
        processed_row = dict(zip(labels,processed_row))
        if processed_row["CAS"].isnumeric():
            processed_row["CAS"] = int(processed_row["CAS"])
        # append the list to the final list
        if not processed_row["Compound"] in {"Xe", "unknown_compound"}:
            mol_validation_tmp.append(processed_row)
    return mol_validation_tmp




mol_validation = get_molecules_for_validation()



if __name__ == "__main__":
    import pandas as pd

    substances_df = pd.DataFrame(mol_validation, columns=labels)
    substances_df
    substances_df.to_excel("substances.xlsx")
