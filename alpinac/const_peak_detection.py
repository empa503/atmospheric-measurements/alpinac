# -*- coding: utf-8 -*-
#def make_dict_k():
#dictionnary with all needed constants
#dict_k = {}

#*************************
#contants from the hdf5 file:
#not sure it's a good idea to mix it with constants not coming from the hdf5 file
#s_per_bin = s_per_bin

#*************************
#PEAK DETECTION IN TOF/MASS DOMAIN: FIXED PARAMETERS

#to prepare extraction from peak ID file:
peak_fit_min_rt_window = 50.0 #seconds


tofidx_domain_range = int(40) #we will look within a range of xx points on the tofidx axis

tof_per_bin = 1.0
PTtof_mass_cal = 9.0 #9 [V/tofidx], slope threshold for peak detection in tofidx domain
PTtof = 1.25 #1.25
#TO DO: check if PTtof is constant over tofidx domain
#as sigma in TOF domain increase with mass, maybe PTtof should decrease with increasing mass
#Define min peak area on tofidx axis:
nb_bin_min_tofidx = 6 # 6   a peak in TOF domain is defined by minimum 6 points
#nb_bin_min_tofidx_mass_cal = 2
i_min_peak_tofidx = 2 #9  min peak intensity value [V]
WTtofidx = nb_bin_min_tofidx/2 #width threshold, nb of index covering half a peak
#from these two parameters we can calculate threshold for peak acceptance:
A_min_tofidx = i_min_peak_tofidx * WTtofidx/3.0 #20190715, divited by 2




#Fixed parameters to makes histograms of extracted tofidx/masses:
#width of a histogram bin:
#delta_mass_discrete = float(0.005) #no too small to account for mass drift over rt!
mc_avrg_min = 6.0 #averaging time, minutes, for mass calibration
mc_spacing_min = 2.0 #time interval, minutes, in between each mc point.
mc_alpha_guess = 0.6


delta_tofidx_discrete_mass_cal = float(0.3) #20191113 changed from 0.2 to 0.23 #20200811 changed to 0.3
delta_tofidx_discrete = float(0.4)


"""We want to allow for drift of up tp 3 TOFindex for a complete set.
A complete set (fragment found for each average) is one point per average.
"""
PWh_tofidx = 10
PTh_tofidx = 5/3
h_nb_per_bin = 1
#making buckets of masses
#for the mass calbration,
#we want to allow for 100 ppm drift over 2000 seconds

mass_cal_mode2_p3_guess = float(0.5)
mass_cal_mode2_p3_min = float(0.49985)
mass_cal_mode2_p3_max = float(0.49995)

#std_dev_p_tofidx_start = 5.0
std_dev_p_tofidx_max_mass_cal = 1.0
std_dev_p_tofidx_max = 3.0 #20190909, increased to #1.5 #should be set according to expected mass drift and potential overlapping peaks



#*************************
#PEAK DETECTION IN TIME DOMAIN: FIXED PARAMETERS
#cf. GCWerks manual Chapter 15 p. 56, 'The integration parameters'

#values of PWt used over RT:
#12 at RT = 1960, sigma close to 1.2, max 5, min 1. noisy data
#10 at RT = 2330, sigma close to 3.
#13 at RT = 2660, sigma 3 to 3.5
#15 at RT = 2790, sigma close to 4 or 5 (tailing).


#PWt = 3 #3 peak width at half height in second should be function of RT as first guess (increase with RT)
#PTt = 60.0 / PWt  #20.0 #peak slope detection threshold in time domain, with areas in TOF-indexes
A_min_t = float(20.0) #20.0   #400.0 * s_per_bin
fit_chromato_ratio_to_linear_fit = 0.9 #0.78
area_opt_u_threshold = 0.33

#prediction of sigma in rt domain:
#note: they should be one set per chromatographic method!
#meaning, the info about which chromatographic method is used should be registered
#(in the hdf5 file).
sigma_guess_slope = 0.05452 #*1.3 #add this for jfj-Medusa
sigma_guess_exp_k = 0.00193

sigma_guess_slope_bafu = 0.108979
sigma_guess_exp_k_bafu = 0.001538

suspect_mass_bin_u = 100.0 #ppm

#split search time window into smaller time windows:
rt_window = 120.0 #seconds
rt_window_edge = 2.5 #2.5 seconds added on each side, fitted but not saved


#uncertainty to take into account to group co-eluting peaks:
#all peaks at rt +/- rt_peak_U will be considered as co-eluting
rt_peak_U = 2.0
nb_bin_min_peak_RT = 6 #min number of point around the apex of one peak in RT domain
nb_bin_min_RT = 2 #at least 3 masses peaking at the same time to indicate presence of a substance
#detection RT domain on the histogram:
PTh_t = 0.7

fit_chromato_nb_params = 4
conf_int = 0.90


#********************************
#import GCWerks peak list
GCWerks_min_rt_window = 30
GCWerks_rt_window_max_spacing = 10
GCWerks_u_ppm = 100
    
    
    
    
#return dict_k
