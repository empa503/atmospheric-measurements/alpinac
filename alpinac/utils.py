from enum import Enum
import numpy as np


class IonizationMethod(Enum):
    EI = 0
    CI = 1
    EI_CI = 2


def normalize(array: np.ndarray) -> np.ndarray:
    """Normalize an array to the range [0, 1]."""
    min, max = np.min(array), np.max(array)
    return (array - min) / (max - min)
