"""Small config based on yaml file."""

import logging
from pydantic import BaseModel
import os
from pathlib import Path
from threading import Lock
import platform


def get_app_data_path():
    if platform.system() == "Windows":
        app_data = os.getenv("APPDATA")
        return app_data
    else:
        # Use XDG Base Directory on Linux/macOS
        return os.getenv(
            "XDG_CONFIG_HOME", os.path.join(os.path.expanduser("~"), ".config")
        )


# Json file in the appdata directory OS independent
path_to_json = Path(get_app_data_path()) / "alpinac" / "mode_extraction_config.json"

logger = logging.getLogger(__name__)


save_lock = Lock()


class FileSelectorConfig(BaseModel):
    data_directory: Path = Path("")
    selected_file: Path | None = None


class SpectralMatchingConfig(BaseModel):
    cutoff_mass: float = 19.0
    databases: dict[str, Path] = {}
    ignored_masses: list[float] = []


# General config class
class Config(BaseModel):
    file_selector: FileSelectorConfig = FileSelectorConfig()
    spectral_matching: SpectralMatchingConfig = SpectralMatchingConfig()

    def save(self):
        """Save the config to the json file."""
        _save_config(self)


def load_config() -> Config:
    """Load the config from the json file."""
    if not path_to_json.is_file():
        return Config()
    # Load the config from the json file
    try:
        with open(path_to_json, "r") as file:
            config = Config.model_validate_json(file.read())
    except Exception as e:
        logger.error(f"Error loading config: {e}")
        return Config()
    return config


def _save_config(config: Config):
    """Save the config to the json file."""
    with save_lock:
        logger.info(f"Saving config to {path_to_json}")
        path_to_json.parent.mkdir(exist_ok=True, parents=True)
        with open(path_to_json, "w") as file:
            file.write(config.model_dump_json())


CONFIG = load_config()
