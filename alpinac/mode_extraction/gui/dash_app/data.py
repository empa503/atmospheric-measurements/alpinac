from __future__ import annotations


from pathlib import Path
from typing import TYPE_CHECKING
from pydantic import BaseModel

if TYPE_CHECKING:
    import pandas as pd
    import xarray as xr
    from alpinac.io_tools_hdf5 import hdf5Metadata
    from matchms.filtering.SpectrumProcessor import SpectrumProcessor
    from matchms import Spectrum, Scores


class AppSettings(BaseModel):

    h5_path: Path = Path("")

    databases_path: dict[str, Path] = {}


class AppData:

    h5_helper: hdf5Metadata = None

    work_dir: Path = Path(".")

    databases: dict[str, dict[str, Spectrum]] = {}
    processing: SpectrumProcessor = None

    references_processed_by_id: dict[str, Spectrum] = {}
    query_processed_by_id: dict[str, Spectrum] = {}
    scores: Scores | None = None
