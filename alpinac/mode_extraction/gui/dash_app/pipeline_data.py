import argparse
import itertools
import logging
import threading
from typing import Any

import numpy as np
import pandas as pd
import xarray as xr
from dash import Dash, Input, Output, State, callback, dash_table, dcc, html
import dash_bootstrap_components as dbc
from dash.dash_table.Format import Format, Scheme
from plotly.subplots import make_subplots
import plotly.express as px
import plotly.graph_objects as go

from alpinac.mode_extraction.gui.dash_app.data import AppData

from alpinac.mode_extraction.gui.dash_app import file_selector
from alpinac.mode_extraction.gui.dash_app import run_pipeline
from alpinac.mode_extraction.gui.dash_app import spectral_matching

from alpinac.mode_extraction.mode_extraction_nontarget import nontarget_peak_extraction
from alpinac.mode_identification.main_from_file import make_identification_from_file

logger = logging.getLogger("alpinac.pipeline_data")

between = lambda x, a, b: (a <= x) & (x <= b)

rt_format = {
    "type": "numeric",
    "format": Format(precision=2, scheme=Scheme.fixed),
}

# Create a color cycler for the chromatograms
_c_iter = itertools.cycle(px.colors.qualitative.Plotly)
masses_colors = {}

columns = [
    dict(id="id", name=("Compound", "id"), type="numeric"),
    dict(id="peak_rt_mean", name=("Compound", "RT"), **rt_format),
    # dict(id="peak_rt_std", name=("Compound", "std"), **rt_format),
    dict(id="peak_rt_count", name=("Compound", "number of peaks"), type="numeric"),
    dict(
        id="retention_index",
        name=("Compound", "RI"),
        type="numeric",
        format=Format(precision=0, scheme=Scheme.fixed),
    ),
    dict(
        id="prominences_sum",
        name=("Compound", "prominence"),
        type="numeric",
        format=Format(precision=0),
    ),
    # dict(id="start_ip_mean", name=("start_ip", "mean"), **rt_format),
    # dict(id="stopp_ip_mean", name=("stopp_ip", "mean"), **rt_format),
    dict(id="mai_", name=("Compound", "mai"), type="numeric"),
    dict(
        id="compound_name",
        name=("Best Candidate", "compound"),
    ),
    dict(
        id="score",
        name=("Best Candidate", "score"),
        type="numeric",
        format=Format(precision=3, scheme=Scheme.fixed),
    ),
    # dict(
    #    id="fraction_explained",
    #    name=("Best Candidate", "match_frac"),
    #    type="numeric",
    #    format=Format(precision=2, scheme=Scheme.fixed),
    # ),
    dict(
        id="best_candidate_ri_diff",
        name=("Best Candidate", "δri"),
        type="numeric",
        format=Format(precision=0, scheme=Scheme.fixed),
    ),
    dict(id="valid_candidate", name=("Best Candidate", "valid"), type="text"),
    # dict(
    #    id="best_candidate_ri",
    #    name=("Best Candidate", "ri"),
    #    **rt_format,
    # ),
    # dict(
    #    id="best_candidate_bp",
    #    name=("Best Candidate", "bp"),
    #    type="numeric",
    #    format=Format(precision=0),
    # ),
]


def div_line(children: list[any], equal_spacing: bool = False, **kwargs):
    """Create a div to put all the given elements in a line."""
    if equal_spacing:
        children = [
            html.Div(child, style={"flex": 1, "marginRight": "10px"})
            for child in children
        ]
    return html.Div([*children], style={"display": "flex", **kwargs})


def highlight_condition_dicts(
    column_id: str,
    colors_of_thresholds: dict[float, str],
    base_color: str = "red",
    absolute_values: bool = False,
) -> list[dict[str, Any]]:

    ret_list = [
        {
            "if": {"column_id": column_id},
            "backgroundColor": base_color,
            "color": "black",
        }
    ]
    for threshold_value in sorted(colors_of_thresholds.keys()):
        filter_query = f"{{{column_id}}} > {threshold_value}"
        if absolute_values:
            filter_query = f"{filter_query} || {{{column_id}}} < -{threshold_value}"
        ret_list.append(
            {
                "if": {
                    "column_id": column_id,
                    "filter_query": filter_query,
                },
                "backgroundColor": colors_of_thresholds[threshold_value],
            }
        )
    return ret_list


empty_data_table = [
    {
        "id": 0,
        "best_candidate_compound": "No data loaded",
    }
    | {col["id"]: np.nan for col in columns if col["id"] != "id"}
]

table_compounds = dash_table.DataTable(
    data=empty_data_table,
    id="table-compounds",
    columns=columns,
    merge_duplicate_headers=True,
    page_size=20,
    sort_action="native",
    style_data_conditional=highlight_condition_dicts(
        "score",
        {0.8: "yellow", 0.98: "lightgreen"},
    )
    + highlight_condition_dicts(
        "fraction_explained",
        {0.8: "yellow", 0.95: "lightgreen"},
    )
    + highlight_condition_dicts(
        "best_candidate_ri_diff",
        {5: "yellow", 15: "red"},
        base_color="lightgreen",
        absolute_values=True,
    )
    + highlight_condition_dicts(
        "peak_rt_count",
        {3: None},
        base_color="red",
    )
    + [
        {
            "if": {"column_id": "compound_name"},
            # "backgroundColor": "orange",
            "maxWidth": "90px",
            "textOverflow": "ellipsis",
        },
    ],
    # style_cell={
    #    "textAlign": "left",
    # },
    style_header={
        # Crop the header text if longer than the content
        "textOverflow": "ellipsis",
        "overflow": "hidden",
        "maxWidth": 0,
        "fontWeight": "bold",
    },
    export_format="csv",
)


table_bestmatches = dash_table.DataTable(
    id="table-bestmatches",
    data=[],
    columns=[
        dict(id="id", name="id"),
        dict(
            id="score",
            name="score",
            type="numeric",
            format=Format(precision=3, scheme=Scheme.fixed),
        ),
    ],
)


layout = html.Div(
    children=[
        html.Div(
            [
                dcc.Checklist(
                    id="checkbox-table_params",
                    options=[
                        {
                            "label": "Only neighboring compounds",
                            "value": "show_only_neighbors",
                        },
                        {
                            "label": "Only Retention Index calibrants",
                            "value": "show_only_ri_calibrants",
                        },
                    ],
                ),
                # table for the ri calibrants
                html.Div(
                    dash_table.DataTable(
                        id="table-ri-calibrants",
                        columns=[
                            dict(id="compound", name="compound"),
                            dict(id="ri", name="Reference RI", type="numeric"),
                        ],
                        data=spectral_matching.df_ri_refs[["compound", "ri"]].to_dict(
                            "records"
                        ),
                    ),
                    id="div-table-ri-calibrants",
                ),
                table_compounds,
            ],
            style={"width": "40%"},
        ),
        html.Div(
            children=[
                div_line(
                    [
                        html.Div(
                            children=[
                                div_line(
                                    [
                                        html.P(
                                            "Selected Compound",
                                            style={"marginRight": "20px"},
                                        ),
                                        html.P("", id="text-selected-compound"),
                                    ]
                                ),
                                # Edit to rename the compound
                                dbc.Input(
                                    id="input-edit-compound-name",
                                    type="text",
                                    placeholder="Compound name",
                                ),
                                dbc.Button(
                                    id="button-edit-compound-name",
                                    children="Rename",
                                    color="primary",
                                ),
                                html.Label(
                                    "",
                                    id="label-rename-successful",
                                    style={"color": "green"},
                                ),
                                html.Label("", id="label-export-valid"),
                                html.Label("", id="label-export-valid2"),
                                # Delete button
                                dbc.Button(
                                    "Delete",
                                    id="button-delete-compound",
                                    # info when hovering over the button
                                    title="Delete the selected compound.",
                                    color="danger",
                                ),
                            ]
                        ),
                        html.Div(
                            children=[
                                html.P("Compounds to include"),
                                dcc.Dropdown([], multi=True, id="dropdown-compounds"),
                                dbc.Button(
                                    "Merge",
                                    id="button-merge-compounds",
                                    # info when hovering over the button
                                    title="Merge the selected compounds into one."
                                    "\nThe new compound will have the number of the first selected compound.",
                                ),
                            ]
                        ),
                        html.Div(
                            children=[
                                html.P("Alpinac Extraction"),
                                dcc.Loading(
                                    id="loading-extract-alpinac",
                                    type="default",
                                    children=dbc.Button(
                                        "Extract",
                                        id="button-extract-alpinac",
                                        title="Extract the selected compound via Alpinac.",
                                    ),
                                ),
                                # Label to show the output
                                html.Label("", id="label-loadoutput-alpinac"),
                            ]
                        ),
                        dcc.Checklist(
                            id="checkbox-chromato_params",
                            options=[
                                {
                                    "label": "All masses with peaks in interval",
                                    "value": "show_all_peaks",
                                },
                                {
                                    "label": "Whole time interval",
                                    "value": "show_whole_chromato",
                                },
                                {
                                    "label": "All masses",
                                    "value": "show_all_masses",
                                },
                                {
                                    "label": "Total ion current",
                                    "value": "show_tic",
                                },
                            ],
                        ),
                        html.Div(
                            children=[
                                html.P("Comparison spectrum"),
                                dcc.Dropdown(
                                    id="dropdown-mass_spectrum_reference",
                                ),
                                # Label to show the score
                                html.Div(
                                    [
                                        "Score",
                                        html.Label(
                                            "",
                                            id="label-spectral-matching-output-score",
                                        ),
                                    ]
                                ),
                            ],
                            style={"flex": 1},
                        ),
                    ],
                    equal_spacing=True,
                    flexDirection="row",
                    alignItems="center",
                    justifyContent="space-between",
                ),
                dcc.Graph(id="graph-chromatogram"),
                dcc.Graph(id="graph-mass_spectrum"),
                table_bestmatches,
                # dcc.Graph(id="graph-mass_spectrum"),
            ],
            style={"width": "60%", "margin": "10px"},
        ),
    ],
    # Set children on the same line
    style={"display": "flex", "flexDirection": "row", "width": "100%"},
)


@callback(
    Output("div-table-ri-calibrants", "style"),
    Input("checkbox-table_params", "value"),
)
def show_ri_calibrants(table_params: list[str] | None):
    if table_params is None or "show_only_ri_calibrants" not in table_params:
        return {"display": "none"}
    return {"display": "block"}


@callback(
    [
        Output("table-compounds", "data"),
        Output("table-compounds", "columns"),
    ],
    [
        Input("label-pipeline-info", "children"),
        Input("label-export-valid", "children"),
        Input("checkbox-table_params", "value"),
        State("dropdown-compounds", "options"),
        Input("dropdown-compounds", "value"),
        Input("text-selected-compound", "children"),
        Input("label-spectral-matching-output", "children"),
        Input("label-ri-comparison-output", "children"),
        Input("label-rename-successful", "children"),
    ],
)
def update_table(
    pipeline_info: str,
    export_valid: str,
    table_params: list[str],
    compounds: list[dict[str, str]],
    compounds_selected: list[int],
    compound_selected_text: str,
    spectral_matching_output: str,
    ri_comparison_output: str,
    rename_successful: str,
):
    """Update the table with the selected compounds."""
    logger.info(
        f"Updating the table {table_params=}, {compounds=}, {compounds_selected=}, {compound_selected_text=}"
    )

    if table_params is None:
        table_params = []

    if (
        AppData.h5_helper is None
        or (ds := AppData.h5_helper.extraction_pipeline) is None
    ):
        return empty_data_table, columns

    mask = xr.ones_like(ds["compound"], dtype=bool)

    if "show_only_neighbors" in table_params:
        mask &= ds["compound"].isin([c["value"] for c in compounds])
    if "show_only_ri_calibrants" in table_params:
        mask &= ds["is_ref_ri"]

    this_ds = ds.loc[{"compound": mask}]

    columns_to_show = [c for c in columns if c["id"] in this_ds or c["id"] == "id"]

    return (
        pd.DataFrame(
            {"id": this_ds["compound"]}
            | {
                col["id"]: this_ds[col["id"]]
                for col in columns_to_show
                if col["id"] != "id"
            }
        ).to_dict("records"),
        columns_to_show,
    )


@callback(
    [
        Output("text-selected-compound", "children"),
        Output("label-export-valid", "children"),
    ],
    Input("table-compounds", "active_cell"),
    State("text-selected-compound", "children"),
)
def change_selected_compound(active_cell, current_value: str):
    """Changes the currently selected compound from the table."""
    logger.info(f"active cell changed {active_cell=}. Updating compound options.")
    out_text = ""
    if active_cell is None:
        return current_value, out_text
    selected_cmp = int(active_cell["row_id"])
    if active_cell["column_id"] == "valid_candidate":
        # Change in the data
        ds = AppData.h5_helper.extraction_pipeline
        if ds is None:
            return current_value, out_text
        ds["valid_candidate"].loc[dict(compound=selected_cmp)] = not ds[
            "valid_candidate"
        ].loc[dict(compound=selected_cmp)]
        # Save the data
        AppData.h5_helper.extraction_pipeline = ds
        out_text = f"Validity changed {selected_cmp}"
    return f"{selected_cmp}", out_text


@callback(
    [
        Output("dropdown-compounds", "options"),
        Output("dropdown-compounds", "value"),
        Output("input-edit-compound-name", "value"),
    ],
    Input("text-selected-compound", "children"),
    State("dropdown-compounds", "options"),
)
def add_compound(selected_compound: str, current_options):
    logger.info(f"active cell changed {selected_compound=}. Updating compound options.")

    try:
        selected_cmp = int(selected_compound)
    except ValueError:
        return current_options, [], None

    if (
        AppData.h5_helper is None
        or (ds := AppData.h5_helper.extraction_pipeline) is None
    ):
        return current_options, [], None

    # Get the compounds in range
    selected_ds = ds.sel(compound=selected_cmp)
    left_ip, right_ip = (
        selected_ds["start_ip_mean"],
        selected_ds["stopp_ip_mean"],
    )
    mask_in_range = (left_ip <= ds["peak_rt_mean"]) & (ds["peak_rt_mean"] <= right_ip)
    compounds_in_range = (
        ds["compound"].where(mask_in_range, drop=True).values.astype(int)
    )
    return (
        [{"label": f"{cmp}", "value": cmp} for cmp in compounds_in_range],
        [selected_cmp],
        (
            selected_ds["compound_name"].values.item()
            if "compound_name" in selected_ds
            else None
        ),
    )


@callback(
    Output("label-rename-successful", "children"),
    Input("button-edit-compound-name", "n_clicks"),
    State("input-edit-compound-name", "value"),
    State("text-selected-compound", "children"),
    prevent_initial_call=True,
)
def rename_compound(rename_nclicks: int, new_name: str, selected_compound: str):
    if rename_nclicks is None:
        return
    logger.info(f"Renaming {selected_compound=} to {new_name=}.")
    if selected_compound is None:
        logger.warning("No compound selected to rename.")
        return

    ds = AppData.h5_helper.extraction_pipeline
    if ds is None:
        logger.error("No extraction pipeline found. Compound renaming failed.")
        return

    # Assign all peaks to this compound
    try:
        compound_int = int(selected_compound)
    except ValueError:
        logger.error(f"Invalid compound selected: {selected_compound}")
        return
    ds["compound_name"].loc[dict(compound=compound_int)] = new_name

    # Update the data
    AppData.h5_helper.extraction_pipeline = ds

    return f"Renamed compound {compound_int} to {new_name}"


@callback(
    Output("label-loadoutput", "children", allow_duplicate=True),
    Input("button-merge-compounds", "n_clicks"),
    State("dropdown-compounds", "value"),
    prevent_initial_call=True,
)
def merge_compounds(
    merge_nclicks: int, compounds_to_merge: list[str]
):
    if merge_nclicks is None:
        return
    logger.info(f"Merging {compounds_to_merge=}.")
    if len(compounds_to_merge) < 2:
        logger.warning("Need at least two compounds to merge.")
        return

    # Set the compound id of the other compounds to the new compound id
    ds = AppData.h5_helper.extraction_pipeline
    if ds is None:
        logger.error("No extraction pipeline found. Compound merging failed.")
        return
    # Assign all peaks to this compound
    compounds_int = [int(c) for c in compounds_to_merge]
    out_compound = min(compounds_to_merge)
    compounds_to_remove = [c for c in compounds_int if c != out_compound]
    ds["compound_id"].loc[dict(peak=ds["compound_id"].isin(compounds_int))] = (
        out_compound
    )
    ds["is_max_of_cmp"].loc[dict(peak=ds["compound_id"].isin(compounds_to_remove))] = (
        False
    )

    # Select the compounds to merge
    ds_to_group = ds.sel(compound=compounds_int)
    logger.debug(f"{ds_to_group=}")
    ds = ds.sel(compound=~ds["compound"].isin(compounds_to_remove))

    vars_to_sum = ["prominences_sum", "peak_rt_count"]
    for var in vars_to_sum:
        if var in ds:
            ds[var].loc[dict(compound=out_compound)] = (
                ds_to_group[var].sum(dim="compound").values
            )

    # Update the table
    AppData.h5_helper.extraction_pipeline = ds
    return f"Merged compounds {compounds_to_merge} into {out_compound}"


@callback(
    Output("label-loadoutput-alpinac", "children"),
    Output("button-extract-alpinac", "children", allow_duplicate=True),
    Input("button-extract-alpinac", "n_clicks"),
    State("text-selected-compound", "children"),
    prevent_initial_call=True,
)
def extract_alpinac(nclicks: int, compound: str):
    """Extract the compound via alpinac."""
    if nclicks is None:
        return "", "Extract"

    ds = AppData.h5_helper.extraction_pipeline

    if ds is None:
        logger.error("No extraction pipeline found. Compound extraction failed.")
        return "Extraction failed: No extraction pipeline found.", "Extract"

    selected_ds = ds.loc[dict(compound=int(compound))]

    rt_start = selected_ds["start_ip_mean"].data - 1.0
    rt_stop = selected_ds["stopp_ip_mean"].data + 2.0
    rt_mean = selected_ds["peak_rt_mean"].data

    def run_extraction():
        try:
            out_file = nontarget_peak_extraction(
                path_file=AppData.h5_helper.file_path,
                rt_start_extract=rt_start,
                rt_stop_extract=rt_stop,
                outfile_path=AppData.h5_helper.out_dir,
                outfile_name=f"cmp_{compound}_peak{rt_mean:.0f}s_{rt_start:.0f}s{rt_stop:.0f}s.txt",
            )

            make_identification_from_file(out_file)
        except Exception as e:
            logger.error(f"Extraction failed: for compound {compound}. {e}")

    thread = threading.Thread(target=run_extraction)
    thread.start()

    return f"Extraction of compound {compound} started.", "Extract"


@callback(
    Output("label-loadoutput", "children", allow_duplicate=True),
    Input("button-delete-compound", "n_clicks"),
    State("text-selected-compound", "children"),
    prevent_initial_call=True,
)
def delete_compound(delete_nclicks: int, selected_compound: str | None):
    if delete_nclicks is None:
        return
    logger.info(f"Deleting {selected_compound=}.")
    if selected_compound is None:
        logger.warning("No compound selected to delete.")
        return

    ds = AppData.h5_helper.extraction_pipeline
    if ds is None:
        logger.error("No extraction pipeline found. Compound deletion failed.")
        return

    # Assign all peaks to this compound
    compound_int = int(selected_compound)
    ds = ds.sel(dict(compound=ds["compound"] != compound_int))
    # Remove the peaks from the compound
    ds = ds.sel(dict(peak=ds["compound_id"] != compound_int))

    # Update the table
    AppData.h5_helper.extraction_pipeline = ds

    return f"Deleted compound {compound_int}"


@callback(
    Output("graph-chromatogram", "figure"),
    Input("dropdown-compounds", "value"),
    Input("checkbox-chromato_params", "value"),
)
def update_chromatograms(
    compounds: list[str] | None, chromato_params: list[str] | None
):
    logger.info(f"Updating chromatograms {compounds=}, {chromato_params=}")
    if compounds is None:
        return {}
    if chromato_params is None:
        chromato_params = []
    if len(compounds) == 0 and "show_all_masses" not in chromato_params:
        return {}

    if (
        AppData.h5_helper is None
        or (ds := AppData.h5_helper.extraction_pipeline) is None
        or (df := AppData.h5_helper.get_unit_mass_chromatos(with_tic=True)) is None
    ):
        return {}

    mask_peakofcompounds = ds["compound_id"].isin(compounds)

    if "show_whole_chromato" in chromato_params:
        time_range = (df.index.min(), df.index.max())
    else:
        left_ip, right_ip = (
            ds["start_ip"].where(mask_peakofcompounds).mean().values,
            ds["stopp_ip"].where(mask_peakofcompounds).mean().values,
        )
        duration = right_ip - left_ip
        time_range = (left_ip - duration, right_ip + duration)

    if "show_all_masses" in chromato_params:
        masses = ds["mass_of_peak"]
    elif "show_all_peaks" in chromato_params:
        # Get all peaks in an interval around the IP
        mask_in_range = between(ds["peak_rt"], *time_range)
        masses = ds["mass_of_peak"].loc[mask_in_range]
    else:
        # Get the peaks for the selected compounds
        masses = ds["mass_of_peak"].loc[mask_peakofcompounds]
    masses = np.unique(masses)
    fig = make_subplots(
        rows=2,
        cols=1,
        shared_xaxes=True,
    )

    logger.debug(f"{df.columns=}")
    chroms = df.loc[slice(*time_range), masses]
    chroms_norm = chroms / chroms.max(axis="index")

    for mass in masses:
        chrom = chroms[int(mass)]
        chrom_norm = chroms_norm[int(mass)]

        if mass not in masses_colors:
            masses_colors[mass] = next(_c_iter)
        color = masses_colors[mass]
        trace_kwargs = dict(
            name=f"{mass}", legendgroup=f"{mass}", line=dict(color=color)
        )
        fig.add_trace(
            go.Scatter(x=chrom.index, y=chrom.values, **trace_kwargs),
            row=1,
            col=1,
        )
        # Add the normalized chromatogram
        fig.add_trace(
            go.Scatter(
                x=chrom_norm.index,
                y=chrom_norm.values,
                showlegend=False,
                opacity=0.6,
                **trace_kwargs,
            ),
            row=2,
            col=1,
        )

    if "show_tic" in chromato_params:
        tic = df["tic"].loc[slice(*time_range)]
        tic_norm = tic / tic.max()
        fig.add_trace(
            go.Scatter(x=tic.index, y=tic.values, name="TIC", line=dict(color="black")),
            row=1,
            col=1,
        )
        fig.add_trace(
            go.Scatter(
                x=tic_norm.index,
                y=tic_norm,
                showlegend=False,
                opacity=0.6,
                line=dict(color="black"),
            ),
            row=2,
            col=1,
        )

    return fig


def main():

    parser = argparse.ArgumentParser(description="Run the TOF pipeline visualization.")

    parser.add_argument(
        "--port",
        "-p",
        help="Port to run the server on.",
        default=8050,
        type=int,
        dest="port",
    )
    parser.add_argument(
        "--debug",
        "-d",
        help="Dash debug mode",
        action="store_true",
        dest="debug",
    )
    args = parser.parse_args()
    logging.basicConfig()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    werkzeug_logger = logging.getLogger("werkzeug")
    werkzeug_logger.setLevel(logging.WARNING)

    header_cmps = [
        file_selector.layout,
        run_pipeline.layout,
        spectral_matching.layout,
    ]
    app = Dash(
        __name__, external_stylesheets=[dbc.themes.BOOTSTRAP], title="TOF pipeline"
    )
    app.layout = [
        html.H1("TOF pipeline visualization", style={"textAlign": "center"}),
        html.Div(
            header_cmps,
            style={
                "width": "100%",
                "display": "flex",
                "flexDirection": "row",
                "justifyContent": "space-between",
            },
        ),
        layout,
    ]
    app.run_server(debug=args.debug, port=args.port)


if __name__ == "__main__":

    main()
