import logging
from pathlib import Path
from time import sleep
from dash import Dash, html, dash_table, Input, Output, dcc, State, callback
from alpinac.mode_extraction.gui.config import CONFIG

logger = logging.getLogger(__name__)

config = CONFIG.file_selector


def path_from_str(x: str) -> Path:
    # Remove any leading or trailing whitespace or ' or " chars
    return Path(x.strip().strip("'").strip('"'))


layout = html.Div(
    # Dropdown for the input file
    [
        html.H3("File Selector"),
        html.Label("Data directory: "),
        dcc.Input(
            id="input-directory",
            type="text",
            value=str(config.data_directory),
            placeholder="Input directory",
        ),
        html.Label("", id="label-invalid-directory", style={"color": "red"}),
        # Combobox
        html.Label("Select file: "),
        html.Label("", id="label-loadoutput"),
        dcc.Dropdown(
            id="dropdown-file",
            options=[],
            value=None,
            placeholder="Select a file",
            style={"width": "100%"},
        ),
        dcc.Loading(
            [load_button := html.Button("Load", id="button-loadfile")],
            id="loading-file",
            type="circle",
        ),
    ],
    style={
        "marginBottom": 10,
        "marginTop": 10,
        "display": "flex",
        "flexDirection": "column",
        "alignItems": "center",
    },
)


@callback(
    [
        Output("dropdown-file", "options"),
        Output("dropdown-file", "value"),
        Output("label-invalid-directory", "children"),
    ],
    [Input("input-directory", "value")],
)
def load_files(directory_str: str):

    directory = path_from_str(directory_str)

    if not directory.exists():
        logger.error(f"Directory does not exist: {directory}")
        return [], None, f"Directory {directory} does not exist"

    # save the directory to the config
    config.data_directory = directory
    CONFIG.save()

    files = sorted(directory.rglob("*.h5"))
    # Load files
    options = [{"label": file.name, "value": str(file)} for file in files]

    if len(options) == 0:
        selected_file = None
    elif config.selected_file in files:
        selected_file = str(config.selected_file)
    else:
        selected_file = options[0]["value"]

    CONFIG.file_selector.selected_file = selected_file
    CONFIG.save()

    return options, selected_file, ""
