import itertools
import logging
from pathlib import Path
from typing import Any

import numpy as np
import pandas as pd
import xarray as xr
from dash import Dash, Input, Output, State, callback, dash_table, dcc, html
from dash.dash_table.Format import Format, Scheme
from plotly.subplots import make_subplots
import dash_bootstrap_components as dbc
import plotly.express as px
import plotly.graph_objects as go


from alpinac.mode_extraction.gui.dash_app.data import AppData
from alpinac.mode_extraction.gui.config import CONFIG

from db_reader import AlpinacFragmentsReader, JdxReader, AbstractReader
from matchms import Spectrum, SpectrumProcessor, calculate_scores
from matchms.filtering import select_by_mz
from matchms.similarity import CosineGreedy

from alpinac.mode_extraction.pipeline import get_matchms_spectrum
from alpinac.mode_extraction.rt_index_finder import (
    calculate_retention_index,
    gcbase_get_retention_indexes,
    df_ri_refs,
)


logger = logging.getLogger(__name__)

config = CONFIG.spectral_matching


def merge_spectras(compounds: list[int]) -> Spectrum:
    """Merge spectrums into a single one"""

    ds = AppData.h5_helper.extraction_pipeline
    mask = ds["compound_id"].isin(compounds)
    mz = ds["mass_of_peak"].loc[mask].to_numpy()
    intensities = ds["peak_heights"].loc[mask].to_numpy()
    arg_sort = np.argsort(mz)
    mz = mz[arg_sort]
    intensities = intensities[arg_sort]
    unique_mz, unique_indices = np.unique(mz, return_inverse=True)
    mz = np.array(unique_mz, dtype=float)
    new_intensities = np.zeros_like(mz)
    np.add.at(new_intensities, unique_indices, intensities)
    # Normalize the intensities
    new_intensities = new_intensities / new_intensities.max()

    return Spectrum(
        mz=mz,
        intensities=new_intensities,
        metadata={
            "id": f"Merged from {compounds}",
            "precursor_mz": 0,
        },
    )


def read_library(path: Path) -> dict[str, Spectrum]:
    """Read the library from the path."""
    # Add the library
    # This is a placeholder

    if not path.is_dir():
        raise ValueError(f"Path {path} is not a directory")

    jdxs = JdxReader(path).read_all()
    alpinacs = AlpinacFragmentsReader(path, integer_masses=True).read_all()

    return {**jdxs, **alpinacs}


modal_add_library = dbc.Modal(
    [
        dbc.ModalHeader("Add library"),
        dbc.ModalBody(
            [
                html.Label("Library name"),
                dcc.Input(id="input-library-name", type="text"),
                html.Label("Library path (folder containing the mass spec files)"),
                dcc.Input(id="input-library-path", type="text"),
                html.Label(
                    id="label-library-error", children="", style={"color": "red"}
                ),
            ],
            style={"display": "flex", "flexDirection": "column"},
        ),
        dbc.ModalFooter(
            [
                dbc.Button("Add", id="add-modal-add-library"),
                dbc.Button("Close", id="close-modal-add-library"),
            ]
        ),
    ],
    id="modal-add-library",
    is_open=False,
)


def get_dropdown_db_options():
    return [
        {"label": db_name, "value": str(db_path)}
        for db_name, db_path in config.databases.items()
    ]


layout = html.Div(
    children=[
        html.H3("Spectral data base"),
        html.Div(
            [
                html.Label("Select databases"),
                dcc.Dropdown(
                    id="dropdown-databases",
                    options=get_dropdown_db_options(),
                    value=list(map(str, config.databases.values())),
                    multi=True,
                ),
                html.Button("Add library", id="button-add-library"),
                modal_add_library,
            ],
            style={"display": "flex", "flexDirection": "row"},
        ),
        html.Div(
            [
                html.Label("Cutoff mass [Da]:"),
                dcc.Input(
                    id="input-cutoffmass",
                    type="number",
                    value=config.cutoff_mass,
                ),
            ],
            style={"display": "flex", "flexDirection": "row"},
        ),
        html.Div(
            [
                html.Label("Ignored masses [Da]:"),
                dcc.Dropdown(
                    id="dropdown-ignored-masses",
                    options=[
                        {"label": str(i), "value": i} for i in config.ignored_masses
                    ],
                    value=config.ignored_masses,
                    multi=True,
                ),
            ]
        ),
        html.Label(id="label-spectral-matching-output"),
        dcc.Loading(
            [
                run_spectral_matching_button := html.Button(
                    "Run spectral matching ( + RI)", id="button-run-spectral-matching"
                )
            ],
            id="loading-spectral-matching",
            type="circle",
        ),
        html.Label(id="label-ri-comparison-output"),
        dcc.Loading(
            [
                run_ri_comparison_button := html.Button(
                    "Run RI comparison (only)", id="button-run-ri-comparison"
                )
            ],
            id="loading-ri-comparison",
            type="circle",
        ),
    ],
    style={
        "display": "flex",
        "flexDirection": "column",
        "alignItems": "center",
    },
)


@callback(
    Output("modal-add-library", "is_open", allow_duplicate=True),
    Input("button-add-library", "n_clicks"),
    Input("close-modal-add-library", "n_clicks"),
    State("modal-add-library", "is_open"),
    prevent_initial_call=True,
)
def toggle_modal(n1, n2, is_open):
    logger.info(f"Toggle modal with {n1=}, {n2=}, {is_open=}")
    if n1 or n2:
        return not is_open
    return is_open


@callback(
    [
        Output("label-library-error", "children"),
        Output("dropdown-databases", "options"),
        Output("modal-add-library", "is_open", allow_duplicate=True),
    ],
    Input("add-modal-add-library", "n_clicks"),
    State("input-library-name", "value"),
    State("input-library-path", "value"),
    prevent_initial_call=True,
)
def add_library(n_clicks: int, name: str, path: str):
    """Register a new library."""
    logger.info(f"Adding library with {n_clicks=}, {name=}, {path=}")

    name = name.replace(" ", "_").strip()
    if n_clicks is None:
        error_msg = ""
    elif not name:
        error_msg = "Name is required"
    elif not path:
        error_msg = "Path is required"
    elif name in AppData.databases:
        error_msg = f"Name {name} already registered"
    else:
        error_msg = None

    path = path.strip().replace('"', "")
    path = Path(path)
    if not path.is_dir():
        error_msg = f"Path {path} is not a directory"

    if error_msg is not None:
        return error_msg, get_dropdown_db_options(), True

    try:
        AppData.databases[name] = read_library(path)
    except Exception as e:
        logger.error(f"Could not read the library at {path}: {e}")
        return f"Could not read the library at {path}", get_dropdown_db_options(), True

    config.databases[name] = path
    CONFIG.save()

    return "", get_dropdown_db_options(), False


@callback(
        Input("input-cutoffmass", "value"),
)
def save_cutoff_mass(value: float):
    """Save the cutoff mass to the config."""
    config.cutoff_mass = value
    CONFIG.save()

@callback(
    [
        Output("loading-spectral-matching", "children"),
        Output("loading-spectral-matching", "style"),
        Output("label-spectral-matching-output", "children"),
    ],
    Input("button-run-spectral-matching", "n_clicks"),
    [
        State("input-cutoffmass", "value"),
        State("dropdown-ignored-masses", "value"),
    ],
)
def run_spectral_matching(
    n_clicks: int, cutoff_mass: float, masses_to_ignore: list[float]
):
    """Run the spectral matching."""
    if n_clicks is None:
        return run_spectral_matching_button, {}, ""

    if AppData.h5_helper is None or AppData.h5_helper.extraction_pipeline is None:
        return run_spectral_matching_button, {}, "No data loaded"

    ds_pipeline = AppData.h5_helper.extraction_pipeline
    my_spectrums = get_matchms_spectrum(ds_pipeline)

    logger.info(f"Running spectral matching with {cutoff_mass=}")

    def remove_cutoff(spec: Spectrum, cutoff: float = cutoff_mass) -> Spectrum:
        """Remove all peaks below a cutoff"""
        if len(spec.peaks.mz) == 0 or max(spec.peaks.mz) < cutoff:
            # Avoid emtpy spectra bugs
            return spec
        return select_by_mz(spec, cutoff, max((max(spec.peaks.mz) + 1), cutoff))

    def remove_ignored(
        spec: Spectrum, masses_to_ignore: list[float] = masses_to_ignore
    ) -> Spectrum:
        """Remove all peaks below a cutoff"""
        mask_valid = np.isin(spec.peaks.mz, masses_to_ignore, invert=True)
        spec = Spectrum(
            mz=spec.peaks.mz[mask_valid],
            intensities=spec.peaks.intensities[mask_valid],
            metadata=spec.metadata,
        )
        return spec

    processing = SpectrumProcessor(
        [
            remove_cutoff,
            # lambda spec: select_by_mz(
            #    spec, LOW_MASS_CUTOFF, max((max(spec.peaks.mz) + 1), LOW_MASS_CUTOFF)
            # ),
            remove_ignored,
            "normalize_intensities",
        ]
    )
    AppData.processing = processing

    # Prepare db spectras

    # Load the missing databases
    for db_name, db_path in config.databases.items():
        if db_name not in AppData.databases:
            logger.info(f"Loading database {db_name} from {db_path}")
            AppData.databases[db_name] = read_library(Path(db_path))
            logger.info(f"Loaded {len(AppData.databases[db_name])} spectra")

    # assign some identifiers
    def process_spec(spec, db_name, cmp):
        spec.set("id", f"{db_name}_{cmp}")
        spec.set("compound_name", cmp)
        return spec

    ref_spectrums = [
        process_spec(spec, db_name, cmp)
        for db_name, spectrums in AppData.databases.items()
        for cmp, spec in spectrums.items()
    ]
    if len(ref_spectrums) == 0:
        return run_spectral_matching_button, {}, "No references given"

    # Process the references
    references_processed, _ = processing.process_spectrums(ref_spectrums)
    my_processed, _ = processing.process_spectrums(my_spectrums.values())

    logger.info(f"Running spectral matching with {len(my_processed)} spectra")
    logger.info(
        f"Running spectral matching with {len(references_processed)} references"
    )

    scores = calculate_scores(
        references_processed,
        queries=my_processed,
        similarity_function=CosineGreedy(),
    )

    logger.info(f"Calculated scores")

    def query_output(spec: Spectrum) -> tuple[str | float]:
        """Return parameters for the best match of a query spectrum."""
        scores_of_query = scores.scores_by_query(
            spec, sort=True, name="CosineGreedy_score"
        )
        if len(scores_of_query) == 0:
            logger.warning(f"No match for {spec.get('id')}")
            return (spec.get("id"), None, 0, None, 0, 0, 0)
        best_match, (similarity, matched_peaks) = scores_of_query[0]
        return (
            spec.get("id"),
            best_match.get("id"),
            similarity,
            best_match.get("compound_name"),
            matched_peaks,
            matched_peaks / len(spec.peaks.mz),
            matched_peaks / len(best_match.peaks.mz),
        )

    df = pd.DataFrame(
        [query_output(spec) for spec in my_processed],
        columns=[
            "id",
            "best_candidate",
            "score",
            "compound_name",
            "matched_peaks",
            "matched_fraction",
            "fraction_explained",
        ],
    )

    AppData.references_processed_by_id = {
        spec.get("id"): spec for spec in references_processed
    }
    AppData.query_processed_by_id = {spec.get("id"): spec for spec in my_processed}
    AppData.scores = scores

    if "valid_candidate" in ds_pipeline:
        # Only assign the scores to the not validated 
        ds = ds_pipeline.copy()
        mask = ~ds["valid_candidate"].data
        for col in df.columns:
            ds[col].loc[{"compound": mask}] = df[col].values[mask]
    
    else:
        # Simply assign the scores
        ds = ds_pipeline.assign({col: (["compound"], df[col].values) for col in df.columns})
    
    ds = ri_comparison(ds)

    AppData.h5_helper.extraction_pipeline = ds
    logger.info(f"Finished spectral matching with {len(my_processed)} spectra")

    return run_spectral_matching_button, {}, f"Matching sucessful"

def ri_comparison(ds: xr.Dataset) -> xr.Dataset:
    """Compare the retention indexes with the references."""
    # Calculate the retention indexes
    ds = calculate_retention_index(ds)
    try:
        ri_of_compound = gcbase_get_retention_indexes(
            np.unique([v for v in ds["compound_name"].values if v])
        )
        ri_of_compound_func = np.vectorize(lambda x: ri_of_compound.get(x, np.nan))
        # Map the dict to the compound_name
        ds["best_candidate_ri"] = (
            ["compound"],
            ri_of_compound_func(ds["compound_name"]),
        )
        ds["best_candidate_ri_diff"] = (
            ["compound"],
            (ds["retention_index"] - ds["best_candidate_ri"]).data,
        )
        valid_candidate = (
            (np.abs(ds["best_candidate_ri_diff"]) < 10.0) & (ds["score"] > 0.97)
        ).data

    except Exception as e:
        logger.error(f"Could not compare retention indexes with references: {e}")
        valid_candidate = (ds["score"] > 0.98).data

    if 'valid_candidate' in ds:
        valid_candidate |= ds['valid_candidate'].data

    ds["valid_candidate"] = (["compound"], valid_candidate)

    return ds

@callback(
    [
        Output("loading-ri-comparison", "children"),
        Output("loading-ri-comparison", "style"),
        Output("label-ri-comparison-output", "children"),
    ],
    Input("button-run-ri-comparison", "n_clicks"),
)
def run_ri_comparison(n_clicks: int):

    if n_clicks is None:
        return run_ri_comparison_button, {}, ""
    
    if AppData.h5_helper is None or AppData.h5_helper.extraction_pipeline is None:
        return run_ri_comparison_button, {}, "No data loaded"
    
    ds = AppData.h5_helper.extraction_pipeline

    ds = ri_comparison(ds)

    AppData.h5_helper.extraction_pipeline = ds

    return run_ri_comparison_button, {}, "RI comparison successful"

@callback(
    Output("dropdown-mass_spectrum_reference", "options"),
    Input("label-spectral-matching-output", "children"),
)
def update_dropdown_mass_spectrum_options(output: str):
    """After the spectral matching is run, update the dropdown options."""

    # Check the spectras loaded
    if len(AppData.references_processed_by_id) == 0:
        return []

    return [
        {"label": id, "value": id} for id in AppData.references_processed_by_id.keys()
    ]


@callback(
    [
        Output("dropdown-mass_spectrum_reference", "value", allow_duplicate=True),
        Output(
            "label-spectral-matching-output-score", "children", allow_duplicate=True
        ),
    ],
    Input("table-bestmatches", "active_cell"),
    prevent_initial_call=True,
)
def update_selected_reference_from_table_click(active_cell: dict[str, Any] | None):
    """Update the selected reference from the table click."""
    logger.info(f"active cell changed {active_cell=}. Updating dropdown mass spec ref.")
    if active_cell is None:
        return None, None
    return active_cell.get("row_id"), active_cell.get("score")


@callback(
    [
        Output("dropdown-mass_spectrum_reference", "value", allow_duplicate=True),
        Output(
            "label-spectral-matching-output-score", "children", allow_duplicate=True
        ),
    ],
    Input("text-selected-compound", "children"),
    State("dropdown-mass_spectrum_reference", "value"),
    State("label-spectral-matching-output-score", "children"),
    prevent_initial_call=True,
)
def update_dropdown_mass_spectrum(
    selected_compound: str, current_value: str, current_score: str
):
    """Update the dropdown for the mass spectrum reference."""
    if selected_compound is None:
        return current_value, current_score
    try:
        selected_cmp = int(selected_compound)
    except ValueError:
        return current_value, current_score

    # Get the best match for that selected compound
    if (
        AppData.h5_helper is None
        or (ds := AppData.h5_helper.extraction_pipeline) is None
        or "best_candidate" not in ds
    ):
        return current_value, current_score
    best_candidate = ds["best_candidate"].loc[{"compound": selected_cmp}].values.item()
    score = ds["score"].loc[{"compound": selected_cmp}].values.item()

    return best_candidate, f"{score:.2f}"


@callback(
    Output("graph-mass_spectrum", "figure"),
    Input("dropdown-compounds", "value"),
    Input("dropdown-mass_spectrum_reference", "value"),
)
def update_massspec(compounds, reference_spec: str):
    logger.info(f"Updating mass spec with {compounds=}, {reference_spec=}")
    if compounds is None or len(compounds) == 0:
        return {}

    # Merge the spectra
    spec = merge_spectras(compounds)
    name = f"merged_from_{compounds}"

    fig = go.Figure(layout=go.Layout(barmode="overlay"))
    fig.add_trace(
        go.Bar(
            x=spec.mz,
            y=spec.intensities,
            name=name,
            # Widht
            width=0.8,
        )
    )

    spec_ref = AppData.references_processed_by_id.get(reference_spec, None)
    if spec_ref is not None:
        fig.add_trace(
            go.Bar(
                x=spec_ref.mz,
                y=-spec_ref.intensities,
                name=reference_spec,
                width=0.8,
            )
        )

    return fig


@callback(
    Output("table-bestmatches", "data"),
    Input("dropdown-compounds", "value"),
    # Ignore calls on start
    prevent_initial_call=True,
)
def update_best_matches_table(compounds: list[int]):
    logger = logging.getLogger(__name__)
    logger.info(f"{compounds=}")
    data = []

    if AppData.scores is None:
        logger.warning("No scores calculated")
        return [{"id": "No data loaded", "score": "Run Spectral Matching first"}]

    if len(compounds) == 0:
        return []
    elif len(compounds) == 1:
        (compound_id,) = compounds
        query_spec = AppData.query_processed_by_id[compound_id]
        scores = AppData.scores

    else:
        merged_processed = AppData.processing.process_spectrum(
            merge_spectras(compounds)
        )
        query_spec = merged_processed
        scores = calculate_scores(
            list(AppData.references_processed_by_id.values()),
            [merged_processed],
            CosineGreedy(),
        )

    for spec, (score, _) in scores.scores_by_query(
        query_spec,
        sort=True,
        name="CosineGreedy_score",
    ):
        data.append({"id": spec.get("id"), "score": score})
        if len(data) > 10 or score < 0.5:
            break
    return data


if __name__ == "__main__":

    # Set to terminal logging
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    logger.setLevel(logging.INFO)
    app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
    app.layout = layout
    app.run_server(debug=True)
