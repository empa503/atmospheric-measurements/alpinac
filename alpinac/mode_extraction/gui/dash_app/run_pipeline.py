"""Module to run the pipeline.

Once the pipeline is loaded, it will update the `label-pipeline-info` with a text.
It will also update the data in `AppData.h5_helper.extraction_pipeline` once the pipeline is run.
"""

import logging

from dash import Dash, html, Input, Output, State, dcc, callback
from pydantic import BaseModel

from alpinac.mode_extraction.gui.dash_app import file_selector
from alpinac.mode_extraction.gui.dash_app.data import AppData
from alpinac.mode_extraction.gui.config import CONFIG


from alpinac.io_tools_hdf5 import hdf5Metadata
from alpinac.mode_extraction.pipeline import run_extraction_pipeline


layout = html.Div(
    children=[
        html.H3("Data Extraction"),
        html.Label("", id="label-pipeline-info"),
        dcc.Loading(
            [
                run_pipeline_button := html.Button(
                    "Run pipeline", id="button-run-pipeline"
                )
            ],
            id="loading-pipeline",
            type="circle",
        ),
        html.Label("", id="label-pipeline-output"),
    ],
    style={
        "marginBottom": 10,
        "marginTop": 10,
        "display": "flex",
        "flexDirection": "column",
        "alignItems": "center",
    },
    id="pipeline-layout",
)


# Example callback of on the file loading
@callback(
    [
        Output("loading-file", "children"),
        Output("label-loadoutput", "children", allow_duplicate=True),
        Output("label-loadoutput", "style"),
    ],
    [Input("button-loadfile", "n_clicks")],
    [Input("dropdown-file", "value")],
    prevent_initial_call=True,
)
def load_file(nclicks: int, file: str | None):
    """Callback of what happens when the button is clicked.

    It returns the loading button, such that it is made unavailable during the loading.
    """
    if file is None:
        return file_selector.load_button, "No file selected", {"color": "red"}

    # Here load the file and do anything you want with it
    try:
        helper = hdf5Metadata(file)
    except Exception as e:
        return file_selector.load_button, f"Error loading file: {e}", {"color": "red"}

    AppData.h5_helper = helper
    CONFIG.file_selector.selected_file = file

    return file_selector.load_button, f"File loaded: {file}", {"color": "green"}


@callback(
    Output("label-pipeline-info", "children"),
    Input("label-loadoutput", "children"),
)
def update_label_info(text: str):
    h5_helper = AppData.h5_helper

    if h5_helper is None:
        return "No file loaded"

    ds_pipeline = h5_helper.extraction_pipeline
    if ds_pipeline is None:
        return "No extraction pipeline found"

    description = ds_pipeline.attrs.get(
        "description", "No description found in pipeline"
    )

    return description


@callback(
    [Output("loading-pipeline", "children"), Output("loading-pipeline", "style")],
    Input("button-run-pipeline", "n_clicks"),
    [State("dropdown-file", "value")],
)
def run_pipeline(n_clicks: int, file: str | None):

    if file is None:
        return run_pipeline_button, {"color": "red"}

    if AppData.h5_helper is None:
        return run_pipeline_button, {"color": "red"}
    # MAke sure the file selected is the same as what is in the data
    if file != str(AppData.h5_helper.file_path):
        # Should not happen
        return [
            "File selected is not the same as in the data. please try to load the file again"
        ], {"color": "red"}

    # Here run the pipeline
    ds_pipeline = run_extraction_pipeline(AppData.h5_helper)

    # Set the pipeline in the data
    try:
        AppData.h5_helper.extraction_pipeline = ds_pipeline
    except Exception as e:
        return [f"Error setting the extraction pipeline in the file: {e}"], {
            "color": "red"
        }

    return run_pipeline_button, {}


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    app = Dash(__name__)
    app.layout = [
        file_selector.layout,
        layout,
    ]
    app.run_server(debug=True)
