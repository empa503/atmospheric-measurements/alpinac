from __future__ import annotations
import logging
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import xarray as xr
from math import ceil
from alpinac.io_tools_hdf5 import hdf5Metadata
from alpinac.mode_extraction.peaks import (
    extract_peaks_from_chromatograms,
    group_peaks_into_compounds,
)
from alpinac.utils_data_extraction import extract_chomatograms

if TYPE_CHECKING:
    from matchms import Spectrum

logger = logging.getLogger(__name__)


def run_extraction_pipeline(
    h5_helper: hdf5Metadata,
    rt_start: float = 0.0,
    rt_stopp: float | None = None,
    # The delta mass to use to find the peaks
    d_m: float = 0.5,
    # The width (in time bins) of the peak to use for peak detection
    width: int = 9,
    # The minimum peak height to use for peak detection
    min_peak_height: int = 40,
    # Masses to ignore the chromatograms
    masses_to_ignore: list[int] = [],
    # Cutoff low masses below this value. Does not extract chromatograms for masses below this value
    low_mz_cutoff: float = 0.0,
    # Miminum prominence of the peak
    min_prominence: float = 120,
    # Whether to recomputer the chromatograms if present
    recompute_chromatograms: bool = False,
) -> xr.Dataset:
    """Run the extraction pipeline on the data.

    The pipeline follows the following steps:

        * Import the data
        * Extract the chromatograms
        * Extract the peaks from the chromatograms
        * Group the peaks into compounds


    """

    if rt_stopp is None:
        rt_stopp = h5_helper.time_axis[-1]

    tof_data, time_axis = h5_helper.import_hdf5_data(
        rt_start,
        rt_stopp,
        return_timeaxis=True,
    )
    mass_axis = h5_helper.mass_axis_h5

    # Small function to convert from bin to rt
    time_bin_to_time = lambda x: np.interp(x, np.arange(len(time_axis)), time_axis)

    logger.info(f"Width used for peak detection: {width * h5_helper.s_per_bin:.2f} s")

    df_chromatos = h5_helper.get_unit_mass_chromatos()
    if recompute_chromatograms or df_chromatos is None:
        chromato_dict = extract_chomatograms(
            tof_data,
            mass_axis,
            # compounds
            masses=[
                i
                for i in range(int(round(mass_axis[0])), ceil(mass_axis[-1]))
                if i > low_mz_cutoff and i not in masses_to_ignore
            ],
            d_m=d_m,
            smoothing_kernel=np.array([0.25, 0.5, 0.25]),
            baseline_method="arpls",
        )

        df_chromatos = pd.DataFrame(chromato_dict, index=time_axis)
        df_chromatos["tic"] =  df_chromatos.sum(axis=1)
        h5_helper.save_unit_mass_chromatos(df_chromatos)
    else:
        logger.info("Using the chromatograms already extracted")
        chromato_dict = df_chromatos.to_dict(orient="list")

    df_peaks = extract_peaks_from_chromatograms(
        chromato_dict,
        find_peaks_kwargs={
            "width": width,
            "height": min_peak_height,
            "prominence": min_prominence,
        },
        time_bin_to_time=time_bin_to_time,
    )
    df_compounds, df_peaks = group_peaks_into_compounds(df_peaks)

    df_peaks = df_peaks.rename(columns={"mass": "mass_of_peak"})
    # Put the data in an xarray dataset
    ds = xr.Dataset(
        {col: (("peak"), df_peaks[col]) for col in df_peaks.columns}
        | {
            "_".join(col): (("compound"), df_compounds[col])
            for col in df_compounds.columns
        },
        coords={
            "peak": df_peaks.index,
            "compound": df_compounds.index.values,
        },
        attrs={
            "rt_start": rt_start,
            "rt_stopp": rt_stopp,
        },
    )

    return ds


def get_matchms_spectrum(ds: xr.Dataset) -> dict[any, Spectrum]:
    """Convert the xarray dataset to a dictionary of matchms spectrum."""

    from matchms import Spectrum

    my_spectrums = {}
    for cmp_id in ds["compound"].values:
        mask = ds["compound_id"] == cmp_id
        peaks_ds = ds.loc[{"peak": mask.values}]
        # Some masses could be duplicate if they are overlapping
        u_mz, inv_inds = np.unique(peaks_ds["mass_of_peak"].values, return_inverse=True)
        u_intensities = np.zeros_like(u_mz, dtype=float)
        np.add.at(u_intensities, inv_inds, peaks_ds["peak_heights"].values)
        mass_spec = Spectrum(
            mz=u_mz.astype(float),
            intensities=u_intensities,
            metadata={
                "id": cmp_id,
                "compound_name": f"cmp_{cmp_id}",
                "precursor_mz": 0,
            },
        )
        my_spectrums[cmp_id] = mass_spec

    return my_spectrums
