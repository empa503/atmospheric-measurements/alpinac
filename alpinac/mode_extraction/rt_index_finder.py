"""Script that can be used to find the compounds used for calculating the RT index."""

import logging
import pandas as pd
from pathlib import Path
import xarray as xr
import numpy as np

_rt_ref_filpath = Path(__file__).parent / "rt_refs.csv"
df_ri_refs: pd.DataFrame = pd.read_csv(_rt_ref_filpath, comment="#")

logger = logging.getLogger(__name__)

aliases: dict[str, str] = {
    "ethane28nudged": "ethane",
    "C3H8-Propane": "propane",
    "n-butane": "butane",
    "C4H10-Butane": "butane",
    "n-pentane": "pentane",
    "C5H12-Pentane": "pentane",
    "n-hexane": "hexane",
    "n-heptane": "heptane",
    "ethylbenz": "ethylbenzene",
    "m-xylene": "xylene",
    "p-xylene": "xylene",
    "o-xylene": "xylene",
    "mp-xylene": "xylene",
}


def calculate_retention_index(
    ds_pipeline: xr.Dataset,
    aliases: dict[str, str] = aliases,
    df_ri_refs: pd.DataFrame = df_ri_refs,
    min_matched_peaks: int = 4,
    min_score: float = 0.95,
) -> xr.Dataset:
    """Calculate the retention index of the given compounds."""

    valid_compound_names = list(aliases.keys()) + df_ri_refs["compound"].to_list()

    mask = ds_pipeline["compound_name"].isin(valid_compound_names)
    df_references = (
        ds_pipeline.loc[dict(compound=mask)][
            [
                "compound_name",
                "matched_fraction",
                "matched_peaks",
                "peak_rt_mean",
                "score",
            ]
        ]
        .to_pandas()
    )

    if "valid_candidate" in ds_pipeline and (ds_pipeline["valid_candidate"].sum() > 0):
        df_references['valid_candidate'] = ds_pipeline['valid_candidate'].loc[dict(compound=mask)].values
        df_references = df_references.loc[df_references['valid_candidate']]
    
    df_references = df_references.sort_values("peak_rt_mean")

    logger.debug(f"{df_references=}")

    # apply a selection of the references
    mask = (df_references["matched_peaks"] >= min_matched_peaks) & (
        df_references["score"] > min_score
    )
    df_references = df_references.loc[mask]
    df_references["reference_name"] = df_references["compound_name"].apply(
        lambda x: aliases.get(x, x)
    )

    # Attempt to match the references with the extracted compounds
    current_rt = 0

    df_ri_refs['rt'] = np.nan
    for row_id, row in df_ri_refs.iterrows():
        mask = (df_references["reference_name"] == row["compound"]) & (
            df_references["peak_rt_mean"] > current_rt
        )
        if mask.sum() == 0:
            # assignements_rt[row_id] = None
            continue

        # Take the next match
        rt = df_references.loc[mask, "peak_rt_mean"].values[0]
        df_ri_refs.loc[row_id, 'rt'] = rt
        current_rt = rt


    ds_pipeline["is_ref_ri"] = xr.zeros_like(ds_pipeline["compound_name"], dtype=bool)
    ds_pipeline["is_ref_ri"].loc[dict(compound=df_references.index)] = True


    df_valid_ri = df_ri_refs.dropna(subset=['rt'])

    rt_to_ri = np.interp(
        x=ds_pipeline["peak_rt_mean"].values,
        xp=df_valid_ri["rt"].values,
        fp=df_valid_ri["ri"].values,
    )
    ds_pipeline["retention_index"] = ("compound", rt_to_ri)

    return ds_pipeline


def gcbase_get_retention_indexes(compounds: list[str]) -> dict[str, float]:
    """Get the retention index of the given compounds.

    using gcbase
    """
    from sqlalchemy.orm import Session
    from gcbase.engine import get_engine
    from gcbase.queries import retention_index as ri_queries

    engine = get_engine()
    with Session(engine) as session:
        ri_df = ri_queries.get_retention_index_all(session)
    logger.debug(f"{ri_df=}")
    return {
        cmp: (ri_df.loc[cmp, "mean"] if cmp in ri_df.index else np.nan)
        for cmp in compounds
    }
