"""Functions for peak extraction."""
from __future__ import annotations

from typing import Any

import numpy as np
import pandas as pd
from scipy.signal import find_peaks



def extract_peaks_from_chromatograms(
    chromato_dict: dict[int | float | str, np.ndarray],
    find_peaks_kwargs: dict[str, Any] = {},
    time_bin_to_time: callable | None = None,
) -> pd.DataFrame:
    """Extract peaks from a set of chromatograms.

    Use the scipy.signal.find_peaks function to find peaks in the chromatograms.
    Return a DataFrame where each row is a peak and the columns are the peak parameters.

    Parameters
    ----------
    chromato_dict:
        A dictionary where the keys are the masses and the values are the chromatograms.
    find_peaks_kwargs:
        A dictionary with the keyword arguments to pass to the find
        peaks function. The default is an empty dictionary.
    time_bin_to_time:
        A function to convert from time bin to time. The default is None, which
        will use the indices as the time.

    Returns
    -------
    dataframe_peaks:
        Each row is a peak and the columns are the peak parameters.
        Most of the columns are the direct output of `find_peaks`.

        Columns:

        * `peak_heights`: The height of the peak.
        * `widths`: The width of the peak.
        * `prominences`: The prominence of the peak.
        * `start_ip`: The start inflexion point in retention time.
        * `stopp_ip`: The stop inflexion point in retention time.
        * `peak_rt`: The retention time of the top of the peak.
        * `mass`: The mass of the peak

    See Also
    --------
    scipy.signal.find_peaks : Function to find peaks in a signal. Link to the scipy
        documentation
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html
    """

    peaks_timebins = {}
    all_peak_params = {}
    for mass, chromato in chromato_dict.items():
        # Find the peaks on each chomatorogram
        peaks_timebin, peaks_params = find_peaks(chromato, **find_peaks_kwargs)
        peaks_timebins[mass] = peaks_timebin
        all_peak_params[mass] = peaks_params

    # Now that we found all peaks, the following steps will
    # put all the peaks together in a data frame
    df_peaks = pd.DataFrame({
        key: np.concatenate([all_peak_params[mass][key] for mass in peaks_timebins.keys()])
        for key in peaks_params.keys()
    })
    # Convert bins to RTs
    # Left is where the peak starts and ends on the right
    if time_bin_to_time is None:
        time_bin_to_time = lambda x: x
    df_peaks["start_ip"] = time_bin_to_time(df_peaks["left_ips"])
    df_peaks["stopp_ip"] = time_bin_to_time(df_peaks["right_ips"])
    df_peaks["peak_rt"] = time_bin_to_time(
        np.concatenate([peaks_timebins[mass] for mass in peaks_timebins.keys()])
    )

    # get the mass for which the peak was found
    df_peaks["mass"] = np.concatenate(
        [np.full(len(peaks), mass) for mass, peaks in peaks_timebins.items()]
    )

    # Add some custom metrics
    df_peaks["flatness"] = df_peaks["peak_heights"] / df_peaks["widths"]
    # How the peak area is spread around the peak top
    df_peaks["tailing_ratio"] = (
        (df_peaks["stopp_ip"] - df_peaks["peak_rt"])
        - (df_peaks["peak_rt"] - df_peaks["start_ip"])
    ) / (df_peaks["stopp_ip"] - df_peaks["start_ip"])

    df_peaks.drop(columns=["left_ips", "right_ips"], inplace=True)

    return df_peaks



def group_peaks_into_compounds(
    df_peaks: pd.DataFrame,
    rt_threshold: float = 0.2,
    rt_threshold_start_ip: float = 0.25,
    rt_threshold_stopp_ip: float = 0.25,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Group peaks into compounds.

    Group peaks that are close to each other in retention time and mass.
    The grouping is done by finding the peaks that are close to each other in retention time
    and mass and then merging them into a single compound.

    Parameters
    ----------
    df_peaks:
        A DataFrame with the peaks.
        This can be the output of the :py:func:`extract_peaks_from_chromatograms`
    rt_threshold:
        Threshold to consider two peaks as part of the same compound.
        The threshold is a fraction of the retention time range of the peak.
        e.g. if the peak has a retention time range of 0.5, and the threshold is 0.2,
        then the threshold is 0.5 * 0.2 = 0.1.
    rt_threshold_start_ip:
        Same as `rt_threshold`, but for the start inflexion point.
    rt_threshold_stopp_ip:
        Same as `rt_threshold`, but for the stop inflexion point.

    Returns
    -------
    dataframe_compounds, dataframe_peaks:
        A tuple with two DataFrames. The first DataFrame is the compounds.
        Each row is a compound with the following columns:
        * `peak_rt`: The retention time of the compound.
        * `peak_rt_std`: The standard deviation of the retention time of the compound.
        * `peak_rt_count`: The number of peaks in the compound.
        * `prominences`: The sum of the prominences of the peaks in the compound.
        * `start_ip`: The start inflexion point in retention time.
        * `start_ip_std`: The standard deviation of the start inflexion point.
        * `stopp_ip`: The stop inflexion point in retention time.
        * `stopp_ip_std`: The standard deviation of the stop inflexion point.
        * `mai`: The mass of the most
        * `compound_id`: The id of the compound.
        The second DataFrame is the peaks dataframe.
        It is the same as the input DataFrame, but with the following columns added:
        * `compound_id`: The id of the compound to which the peak was assigned.
        * `is_max_of_cmp`: A boolean indicating if the peak is the largest peak of the compound.
        
    """

    # Make a copy to avoid modifying the original DataFrame
    df_peaks = df_peaks.copy()

    cmp_id = 0

    df_peaks["assigned"] = False
    df_peaks["compound_id"] = -np.ones(len(df_peaks), dtype=int)
    df_peaks["is_max_of_cmp"] = False

    while not df_peaks["assigned"].all():
        # Take the first not assigned peak
        mask_not_assigned = ~df_peaks["assigned"]
        df_not_assigned = df_peaks.loc[mask_not_assigned]
        # Take the largest peak so that compounds will be assigned based on the largest found peak first
        arg_max = np.argmax(df_not_assigned["prominences"])
        peak_index = df_not_assigned.index[arg_max]
        # print(f"Peak {peak_index} is assigned to {cmp_id=}")

        peak_rt = df_peaks["peak_rt"][peak_index]
        start_ip = df_peaks["start_ip"][peak_index]
        stopp_ip = df_peaks["stopp_ip"][peak_index]

        duration = stopp_ip - start_ip

        mask_in_range = np.logical_and.reduce(
            [
                np.abs(df_peaks["peak_rt"] - peak_rt) < rt_threshold * duration,
                np.abs(df_peaks["start_ip"] - start_ip)
                < rt_threshold_start_ip * duration,
                np.abs(df_peaks["stopp_ip"] - stopp_ip)
                < rt_threshold_stopp_ip * duration,
            ]
        )
        mask_this_compound = mask_in_range & mask_not_assigned

        df_peaks.loc[mask_this_compound, "compound_id"] = cmp_id
        df_peaks.loc[peak_index, "is_max_of_cmp"] = True
        cmp_id += 1

        df_peaks.loc[mask_this_compound, "assigned"] = True

    df_compounds_interim = df_peaks.groupby("compound_id").agg(
        {
            "peak_rt": ["mean", "std", "count"],
            "start_ip": ["mean", "std"],
            "stopp_ip": ["mean", "std"],
        }
    )

    mergable = df_compounds_interim.loc[
        df_compounds_interim[("peak_rt", "count")] <= 4
    ].index

    # Code for mergin compounds, but currently it is not doing anything
    for cmp_to_merge in mergable:
        # Take the compounds close, for which this cmp could include as part of a coelution
        this_rt = df_compounds_interim.loc[cmp_to_merge, ("peak_rt", "mean")]
        this_start_ip = df_compounds_interim.loc[cmp_to_merge, ("start_ip", "mean")]
        this_stopp_ip = df_compounds_interim.loc[cmp_to_merge, ("stopp_ip", "mean")]
        duration = this_stopp_ip - this_start_ip
        mask_compounds_includable = (
            # Must be included in the cmp_to_merge
            (df_compounds_interim[("peak_rt", "mean")] < this_stopp_ip)
            & (df_compounds_interim[("peak_rt", "mean")] > this_start_ip)
            & (
                (
                    np.abs(df_compounds_interim[("start_ip", "mean")] - this_start_ip)
                    < rt_threshold_start_ip * duration
                )
                | (
                    np.abs(df_compounds_interim[("stopp_ip", "mean")] - this_stopp_ip)
                    < rt_threshold_stopp_ip * duration
                )
            )
        )
        mergable_to = df_compounds_interim[mask_compounds_includable].index
        if mergable_to[0] == cmp_to_merge:
            # Highly unlikely that is is a coeluting of less intensitive compounds
            continue
        # print(f"Merging {cmp_to_merge} : ({this_start_ip:.2f}-{this_stopp_ip:.2f}) to {mergable_to}")

    df_compounds = df_peaks.groupby("compound_id").agg(
        {
            "peak_rt": ["mean", "std", "count"],
            "prominences": ["sum"],
            "start_ip": ["mean", "std"],
            "stopp_ip": ["mean", "std"],
        }
    )

    # Most abundant ion
    df_compounds["mai"] = df_peaks.loc[
        df_peaks["is_max_of_cmp"], ["mass", "compound_id"]
    ].set_index("compound_id")

    df_peaks.drop(columns=["assigned"], inplace=True)

    return df_compounds, df_peaks
