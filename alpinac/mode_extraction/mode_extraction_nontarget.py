from __future__ import annotations

import logging
import math
from os import PathLike
import time
from enum import Enum, auto
from pathlib import Path
from typing import TYPE_CHECKING, Any

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import alpinac.const_identification as const_id
import alpinac.const_peak_detection as const_pd
from alpinac.io_tools_hdf5 import hdf5Metadata
from alpinac.utils import IonizationMethod
from alpinac.utils_data_extraction import (
    MakeBinsModes,
    compute_rt_sigma_guess,
    f_mass_to_tofidx,
    f_tofidx_to_mass,
    find_bins_np,
    fit_chromato,
    fit_pseudoVoigt,
    make_bins_np,
    peak_detect,
    split_into_windows,
)
from alpinac.utils_user_def_params import make_colors

if TYPE_CHECKING:
    from alpinac.io_tools import MassCalibrationData

# Organisation of the results in a np.array with the folowing columns:
ic_rt = 0  # retention time, seconds
ic_tof = 1  # time of flight index value
ic_tof_u = 2  # uncertainty of time of flight index value
ic_area = 3
ic_area_u = 4
ic_sigma = 5
ic_alpha = 6
ic_m = 7
ic_m_u = 8
ic_LOD = 9
ic_SN = 10
ic_comp_bin = 11
ic_ionisation = 12
ic_m_u_ppm = 13
ic_m_u_cal_ppm = 14
ic_width = 15


color1_names, color2_names, color3_names = make_colors()

logger = logging.getLogger("alpinac.peak_extraction")

# Size of the colmns in the files
COLSIZE = 15


hdf5_frag_extraction = 0
hdf5_mass_cal = 1
hdf5_import_mode = hdf5_frag_extraction


SIGMA_GUESS_SLOPE = 0.05452
SIGMA_GUESS_EXP_K = 0.00193


class ExctractionMode(Enum):
    """Extraction mode.

    .. warning:: MANY_RUNFILES is currently not working
    """

    SINGLE_RUNFILE = 0
    MANY_RUNFILES = 1


class Plots(Enum):
    """Avaliable plots."""

    CHROMATOGRAPHY_FITTING = auto()
    COELUTION_PLOT = auto()
    MASS_PEAK_DETECTION = auto()
    MZ_FITTING = auto()
    RT_PEAK_DETECTION = auto()
    EXTRACTED_DATA = auto()
    MASS_PEAKS_BASELINE = auto()
    MASS_GROUPPING = auto()


def extract_all_peaks(
    cl_run: hdf5Metadata,
    dict_extraction_m_rt: dict[str, list[tuple[float, float]]],
    ionization_segments: dict[IonizationMethod, tuple[int, int]],
    mz_domain_range: float | None = None,
    tofidx_domain_range: int = 40,
    meas_mass_u_cal_min: float = 0.001,
    mass_u_ppm_min: float = 0.0,
    save_plots: list[Plots] = [],
    show_plots: list[Plots] = [],
    plots_dir: Path | None = None,
    **kwargs,
) -> list[list[Any]]:
    """Extract all peaks from the data."""

    all_peaks = []
    # Loop over ionisation types
    for ionisation_type, (
        seg_start,
        seg_stop,
    ) in ionization_segments.items():
        peak_list = []
        mass_calibration_data = cl_run.mass_calibration_data[ionisation_type]

        for key_mass, rt_target_list in dict_extraction_m_rt.items():
            mass_suspect = float(key_mass)

            logger.info(f"Candidate unit mass: {mass_suspect:.1f}")
            logger.info(f"{rt_target_list=}")

            # Loop over the time intervals to extract from that mass
            for idx_target_rt in range(len(dict_extraction_m_rt[key_mass])):
                interval_rt = [
                    rt_target_list[idx_target_rt][0],
                    rt_target_list[idx_target_rt][1],
                ]

                logger.info(
                    f"Looking for fragments m/z={mass_suspect} in {interval_rt[0]:.1f} to {interval_rt[1]:.1f} s"
                )

                # getting mass axis for given mass calibration parameters
                # calculate mass cal parameters for the target mass, at centre of rt window
                # using the mass calibration parameters from the hdf5 may not be precise enough

                mass_calibration_params = mass_calibration_data.get_parameters_for_rt(
                    (interval_rt[0] + interval_rt[1]) / 2.0
                )
                logger.debug(f"{mass_calibration_params=}")

                # convert target mass to target time of flight, integer value.
                tofidx_int = int(
                    f_mass_to_tofidx(mass_suspect, mass_calibration_params)
                )

                # Get the domain of the mass
                if mz_domain_range is None:
                    assert isinstance(
                        tofidx_domain_range, int
                    ), "tofidx_domain_range is not int."
                    tofidx_int_start = tofidx_int - tofidx_domain_range
                    tofidx_int_stopp = tofidx_int + tofidx_domain_range
                else:
                    tofidx_int_start = int(
                        f_mass_to_tofidx(
                            mass_suspect - mz_domain_range, mass_calibration_params
                        )
                    )
                    tofidx_int_stopp = math.ceil(
                        f_mass_to_tofidx(
                            mass_suspect + mz_domain_range, mass_calibration_params
                        )
                    )

                # we make sure we are not outside tofidx domain.
                tofidx_int_start = max(0, tofidx_int_start)
                tofidx_int_stopp = min(tofidx_int_stopp, cl_run.len_mass_axis - 1)

                tofidx_series = np.arange(
                    tofidx_int_start, tofidx_int_stopp + 1, dtype=float
                )
                mass_axis = f_tofidx_to_mass(tofidx_series, mass_calibration_params)

                m_start = f_tofidx_to_mass(tofidx_int_start, mass_calibration_params)
                m_stopp = f_tofidx_to_mass(tofidx_int_stopp, mass_calibration_params)
                logger.debug(
                    f"Candidate unit mass: {mass_suspect:.1f}, "
                    f"TOF: [{tofidx_int_start:.2f},{tofidx_int_stopp:.2f}[, "
                    f"Mass interval: [{m_start:.2f},{m_stopp:.2f}[, "
                )

                # load selected tof data here
                tof_data, rt_axis = cl_run.import_hdf5_data(
                    *interval_rt,  # rt time, seconds
                    seg_start,
                    seg_stop,
                    tofidx_int_start,
                    tofidx_int_stopp + 1,
                    return_timeaxis=True,
                )
                # Plot the extract Chromato/MS data
                if Plots.EXTRACTED_DATA in save_plots + show_plots:
                    fig, axes = plt.subplots(2, 2, gridspec_kw=dict(wspace=0, hspace=0))
                    # Carful this plot assumes no local mass drift !
                    # which is locally true
                    axes[0, 0].imshow(tof_data[::-1, :], aspect="auto")
                    axes[0, 0].set_xticks([])
                    axes[0, 0].set_yticks([])
                    axes[1, 1].set_xticks([])
                    axes[1, 1].set_yticks([])
                    # Plot the sum along the first axis in the bottom left subplot
                    axes[1, 0].plot(mass_axis, np.sum(tof_data, axis=0))
                    axes[1, 0].set_title("Mass spectra (sum)", y=0)
                    # Plot the sum along the second axis in the top right subplot
                    axes[0, 1].plot(np.sum(tof_data, axis=1), rt_axis)
                    axes[0, 1].set_title("Chromato (sum)")
                    axes[0, 1].yaxis.tick_right()
                    if Plots.EXTRACTED_DATA in save_plots:
                        fig.savefig(
                            plots_dir
                            / f"tofdata_{ionisation_type}_mz_{m_start}to{m_stopp}_rt_{interval_rt[0]}_{interval_rt[1]}.png"
                        )
                    if Plots.EXTRACTED_DATA in show_plots:
                        plt.show()

                peaks = extract_peaks(
                    tof_data,
                    rt_axis,
                    tofidx_series,
                    cl_run=cl_run,
                    mass_calibration_data=mass_calibration_data,
                    save_plots=save_plots,
                    show_plots=show_plots,
                    plots_dir=plots_dir,
                    **kwargs,
                )

                peak_list += peaks

        # Add other data to the peaks

        # Add the inonication type to the peaks
        for peak in peak_list:
            peak.append(ionisation_type.value)

        # Add uncertainty to the peaks
        peaks_masses = np.array([peak[ic_m] for peak in peak_list])
        mass_cal_u_exact_mass = np.array(mass_calibration_data.mass_cal_u_exact_mass)
        mass_cal_u_ppm = np.array(mass_calibration_data.mass_cal_u)
        # mass_cal_u_mz = mass_cal_u_ppm * peak_list * 1e-6

        # Interpolate the calibration uncertainty
        meas_mass_u_cal_ppm = np.interp(
            peaks_masses, mass_cal_u_exact_mass, mass_cal_u_ppm
        )

        # Apply a minimum calibration uncertainty for low masses ? Really needed ?
        meas_mass_u_cal_ppm = np.where(
            peaks_masses < min(65.0, mass_cal_u_exact_mass[0]),
            np.maximum(meas_mass_u_cal_min / peaks_masses * 1e6, meas_mass_u_cal_ppm),
            meas_mass_u_cal_ppm,
        )

        # Apply a minimum uncertainty for the mass
        mass_u = np.array([peak[ic_m_u] for peak in peak_list])
        mass_u_ppm = np.maximum(mass_u / peaks_masses * 1e6, mass_u_ppm_min)

        rts = np.array([peak[ic_rt] for peak in peak_list])
        sigmas = np.array([peak[ic_sigma] for peak in peak_list])
        tofs = np.array([peak[ic_tof] for peak in peak_list])
        peak_width = mass_calibration_data.calibrate(
            rts, tofs + sigmas
        ) - mass_calibration_data.calibrate(rts, tofs - sigmas)

        for peak, mass_u_cal_ppm, mass_u_ppm, width in zip(
            peak_list, meas_mass_u_cal_ppm, mass_u_ppm, peak_width
        ):
            peak.append(mass_u_ppm)
            peak.append(mass_u_cal_ppm)
            peak.append(width)

        all_peaks += peak_list

    return all_peaks


def extract_peaks(
    tof_data: np.ndarray,
    rt_axis: np.ndarray,
    tofidx_series: np.ndarray,
    mass_calibration_data: MassCalibrationData,
    cl_run: hdf5Metadata,
    mass_bin_plot_mode: bool = False,
    rt_interval_sec: float | None = None,
    rt_sigma_guess_scale: float = 1.0,
    mass_peaks_over_median_thresholds: float = 1.3,
    mass_peaks_median_proportion: float = 1.0 / 3.0,
    area_test_threshold_pseudovoigt: float | None = None,
    fit_chromato_nb_params: int = 4,
    i_min_peak_tofidx: float = const_pd.i_min_peak_tofidx,  # min peak intensity value [V]
    sigma_guess_slope: float = SIGMA_GUESS_SLOPE,
    sigma_guess_exp_k: float = SIGMA_GUESS_EXP_K,
    ratio_to_linear_fit: float = const_pd.fit_chromato_ratio_to_linear_fit,
    save_plots: list[Plots] = [],
    show_plots: list[Plots] = [],
    plots_dir: Path | None = None,
    rt_peaks_plot_dir: Path | None = None,
    lod: float = 0.0,
) -> list[list[float]]:
    """Detect peaks from the data.

    Steps:
    * Detect the mass peaks at each time
    * fit them with a pseudo-voigt function
    * group the peaks with similar time of flight
    * fit the chromatography

    TODO: at some point we should make this function not depend on the hdf5 file (cl_run argument)
    Simply anything related to GCMS data should be passed here.

    # Data required for the extraction
    :param tof_data: The TOF data.
    :param rt_axis: The retention time axis.
    :param tofidx_series: The TOF index series.
    :param mass_calibration_data: The mass calibration data.
    :param cl_run: The run metadata.

    # Parameters for controlling the outputs
    :param mass_bin_plot_mode: Plot the mass bin plot.
    ...

    # Parameters for the peak detection/fittings
    :param lod: The limit of detection.
        This will not consider data with a signal below this value.
    :param ratio_to_linear_fit: A ratio that compares if the chromatographic fit
        is better than a linear fit.
        The ratio is the amount of imporovement in the residuals compared to a linear fit.
    """

    max_signal = np.max(tof_data)
    logger.debug(f"{max_signal=}")
    if max_signal <= lod:
        # No signal
        logger.info("No peaks found. Signal is below the LOD")
        return []

    logger.debug(f"{tof_data.shape=}")
    logger.debug(f"{rt_axis.shape=}")
    logger.debug(f"{rt_axis=}")

    # Get the average tofidx
    index = len(tofidx_series) // 2
    tofidx_int = int(tofidx_series[index])

    # Whether the plot should be done
    do_plots = save_plots + show_plots

    if save_plots:
        if plots_dir is None:
            raise ValueError("Plots dir is not given.")
        plots_dir.mkdir(exist_ok=True)
        if Plots.CHROMATOGRAPHY_FITTING in save_plots:
            if rt_peaks_plot_dir is None:
                rt_peaks_plot_dir = plots_dir / f"rt_peaks"
            rt_peaks_plot_dir.mkdir(exist_ok=True)

    results_list = []

    # Calculate constants for mass peak detection
    WTtofidx_local = (
        float(tofidx_int) * mass_calibration_data.sigma_tofidx_slope
        + mass_calibration_data.sigma_tofidx_b
    )
    A_min_tofidx_local = i_min_peak_tofidx * WTtofidx_local / 3.0
    logger.debug(f"{WTtofidx_local=}")
    logger.debug(f"{A_min_tofidx_local=}")

    for idx_rt in range(len(rt_axis)):
        # take the spectra at that time
        mass_spectra = tof_data[idx_rt, :]
        this_ms_rt = rt_axis[idx_rt]
        logger.debug(f"{this_ms_rt=}")

        peaks_detected = peak_detect(
            x=tofidx_series,
            y=mass_spectra,
            PW=WTtofidx_local,
            PT=const_pd.PTtof,
            n_per_bin=const_pd.tof_per_bin,
            graph=Plots.MASS_PEAK_DETECTION in show_plots,
            mode="tofidx_peak_detect",
            additional_plot_info=f"RT={this_ms_rt:.2f}",
        )

        logger.debug(f"{len(peaks_detected)=} \n {peaks_detected}")

        if len(peaks_detected) == 0:
            # No peaks detected
            continue

        peak_positions = peaks_detected[:, 0]
        peak_intensities = peaks_detected[:, 1]

        if len(peaks_detected) > 1:
            # Keep only peaks standing out of the noise
            n_peaks_for_median_intensity = int(
                len(mass_spectra) * mass_peaks_median_proportion
            )
            # Take the n smallest peaks
            median_intensity = np.median(
                np.sort(mass_spectra)[:n_peaks_for_median_intensity]
            )
            logger.debug(f"{median_intensity}")
            # Accept only peaks above the threshold
            mask_valid_peak = (
                peak_intensities >= mass_peaks_over_median_thresholds * median_intensity
            )
            n_peaks_removed = sum(~mask_valid_peak)
            logger.debug(f"{n_peaks_removed=}")

            peak_positions = peak_positions[mask_valid_peak]
            peak_intensities = peak_intensities[mask_valid_peak]

        if len(peak_positions) == 0:
            # No peaks valid
            continue

        # fit mass spectra data with a peudo-voigt fit
        (
            mu_opt,
            A_opt,
            sigma_opt,
            alpha_opt,
            baseline_opt_pv,
        ) = fit_pseudoVoigt(
            tofidx_series,
            mass_spectra,
            peak_positions,
            peak_intensities,
            mass_calibration_data.alpha_pseudo_voigt,
            const_pd.tof_per_bin,
            mass_calibration_data.sigma_tofidx_slope,
            mass_calibration_data.sigma_tofidx_b,
            graph=Plots.MZ_FITTING in show_plots,
            mode="extraction",
            area_test_threshold=area_test_threshold_pseudovoigt,
        )

        if len(mu_opt) == 0:
            logger.debug(f"No mass peak could be fitted")
            continue
        # Store the peaks for the mass spectrum at the current RT
        # write all found peaks at integer mass at the given time index:
        results_list.extend(
            [
                [
                    rt_axis[idx_rt],
                    mu_opt[idx_peak],
                    A_opt[idx_peak],
                    sigma_opt[idx_peak],
                    alpha_opt[idx_peak],
                    baseline_opt_pv,
                    None,
                    None,
                    None,
                ]
                for idx_peak in range(len(mu_opt))
            ]
        )

    # for one given integer mass,
    # now we have extracted all area on TOF-index domain,
    # for each rt index of the list of rt indexes to look at.

    if len(results_list) < fit_chromato_nb_params:
        # Not enough peaks to fit the chromatography
        return []

    # ============================================================
    # GROUP TOF (MASS) PEAKS LIKELY BELONGING TO SAME EXACT TOF
    # Now separate results in groups with similar TOF-index.
    #
    # ============================================================
    # create numpy array with all values:
    results_list_np = np.asarray(results_list, dtype=float)

    # do mass calibration here to then force slope of zero
    results_list_np[:, cl_run.i_mass_exact] = mass_calibration_data.calibrate(
        results_list_np[:, cl_run.i_rt],
        results_list_np[:, cl_run.i_tof],
    )

    if Plots.MASS_PEAKS_BASELINE in do_plots:
        fig, ax1 = plt.subplots(figsize=(8, 4))
        fig.suptitle("Baseline of peaks that were extracted in the mass domain")
        ax1.set_title("Colors correspond to m/z values")
        ax1.set_xlabel("Retention time [s]")
        ax1.set_ylabel("Baseline ", color="b")
        ax1.tick_params("y", colors="b")
        ax1.scatter(
            results_list_np[:, cl_run.i_rt],
            results_list_np[:, cl_run.i_baseline],
            c=results_list_np[:, cl_run.i_mass_exact],
        )
        if Plots.MASS_PEAKS_BASELINE in save_plots:
            plt.savefig(
                plots_dir
                / f"baseline_tofid_{tofidx_series[0]}to{tofidx_series[1]}_rt_{rt_axis[0]}_{rt_axis[-1]}.png"
            )
        if Plots.MASS_PEAKS_BASELINE in show_plots:
            plt.show()

    # Sort by increasing time of flight:

    results_list_np = results_list_np[results_list_np[:, cl_run.i_tof].argsort()]

    # Now we wnat to separate results in groups with similar TOF-index, by assigning index of group
    # ================================

    # bin-width depends on average signal per peak.
    # the aim is to have wider bins where the signal is small
    # because the spread of peak position on the mass axis increased with decreasing signal.
    mean_peak_area = np.mean(results_list_np[:, cl_run.i_A])
    const_pd_delta_mass_discrete_local = max(
        0.003, 0.005 - 0.001 / 200.0 * mean_peak_area
    )
    logger.debug(f"{const_pd_delta_mass_discrete_local=}")

    mass_calibration_params = mass_calibration_data.get_parameters_for_rt(
        rt_axis.mean()
    )

    PTh_mass_local = f_tofidx_to_mass(
        tofidx_int + const_pd.PTh_tofidx, mass_calibration_params
    ) - f_tofidx_to_mass(tofidx_int, mass_calibration_params)

    # Find the masses by groupping the peaks of the mass spectra at each RT
    mass_bin_centres = find_bins_np(
        results_list_np[:, cl_run.i_mass_exact],
        const_pd_delta_mass_discrete_local,
        const_pd.PWh_tofidx,
        PTh_mass_local,
        const_pd.h_nb_per_bin,
        graph=Plots.MASS_GROUPPING in show_plots,
        mode="histo_tofidx",
    )
    logger.debug(f"{mass_bin_centres=}")

    # tofidx_bin_centres is now a list of best guess centre
    # (on x axis, that is tof-index) of identified peaks
    # now we assign each peak result to the closest centre
    # and we clean up the buckets
    if len(mass_bin_centres) == 0:
        return []

    max_mass_diff = 0.02
    bin_value_list, no_bins, bin_list = make_bins_np(
        results_list_np[:, cl_run.i_rt] / cl_run.s_per_bin,
        results_list_np[:, cl_run.i_mass_exact],
        mass_bin_centres,
        max_mass_diff,
        fit_chromato_nb_params + 1,
        cl_run.s_per_bin,
        graph=mass_bin_plot_mode,
        mode=MakeBinsModes.NO_SLOPE,
        plot_mode="mass_domain",
    )

    if no_bins == 0:
        return []

    chromato_list_all = []

    # Loop over each tof mass bin found
    for ii_bin in range(len(bin_list)):
        indexes_bin = np.flatnonzero(bin_value_list == bin_list[ii_bin])
        if len(indexes_bin) < fit_chromato_nb_params:
            continue
        # sort by time
        bin_sorting = results_list_np[indexes_bin, cl_run.i_rt].argsort()
        results_list_np_bin = results_list_np[indexes_bin[bin_sorting], :]
        rt_bins = results_list_np_bin[:, cl_run.i_rt] / cl_run.s_per_bin

        windows_start_stop = split_into_windows(
            rt_bins[0], rt_bins[-1], cl_run.s_per_bin, cl_run.len_time_axis
        )

        # Loop over each time domain checking for rt peaks
        for i_w in range(len(windows_start_stop)):
            # Get the peaks matching this time window
            this_window_start, this_window_stop = windows_start_stop[i_w]
            logger.debug(f"{this_window_start=:.2f} {this_window_stop=:.2f}")
            rt_window_indexes = np.flatnonzero(
                (rt_bins >= this_window_start) & (rt_bins < this_window_stop)
            )
            results_list_np_w = results_list_np_bin[rt_window_indexes, :]

            if len(rt_window_indexes) <= fit_chromato_nb_params + 1:
                # Too few params
                continue
            rt_sigma_guess = compute_rt_sigma_guess(
                sigma_guess_slope,
                sigma_guess_exp_k,
                (this_window_start + this_window_stop) / 2 * cl_run.s_per_bin,
            )

            # Optional scale from user
            rt_sigma_guess = rt_sigma_guess * rt_sigma_guess_scale

            const_pd_PWt_local = max(3, 3.0 * rt_sigma_guess)
            const_pd_PTt_local = 40.0 / const_pd_PWt_local
            peak_rt_detected = []
            # detect peak only if number of points > minimum value,
            rt_chromato = np.arange(
                int(
                    (results_list_np_w[0, cl_run.i_rt] - const_pd_PWt_local)
                    / cl_run.s_per_bin
                ),
                int(
                    (results_list_np_w[-1, cl_run.i_rt] + const_pd_PWt_local)
                    / cl_run.s_per_bin
                ),
            )
            i_chromato = np.interp(
                rt_chromato,
                results_list_np_w[:, cl_run.i_rt] / cl_run.s_per_bin,
                results_list_np_w[:, cl_run.i_A],
            )

            peak_rt_detected = peak_detect(
                x=rt_chromato * cl_run.s_per_bin,
                y=i_chromato,
                PW=const_pd_PWt_local,
                PT=const_pd_PTt_local,
                n_per_bin=cl_run.s_per_bin,
                graph=Plots.RT_PEAK_DETECTION in show_plots,
                mode="rt_peak_detect",
            )
            logger.debug(
                f"{const_pd_PWt_local=},{const_pd_PTt_local=},{cl_run.s_per_bin=},{peak_rt_detected=}"
            )

            # 20190822 we need to make sure that nb of parameters < nb of points
            # otherwise an error in lmfit is returned:
            # raise TypeError('Improper input: N=%s must not exceed M=%s' % (n, m))
            # if len(x) > nb_params, we are fine
            # otherwise, we remove the peak with the smallest expected area, so with min(apex_guess)
            # (baseline is the same for all peaks so need to take it into account here)
            indexes_len = len(rt_window_indexes)
            nb_peaks = len(peak_rt_detected)
            nb_params = 1 + nb_peaks * fit_chromato_nb_params
            while nb_peaks > 0 and indexes_len < nb_params:
                peak_rt_detected = peak_rt_detected[
                    peak_rt_detected[:, 1] > min(peak_rt_detected[:, 1]), :
                ]
                nb_peaks = len(peak_rt_detected)
                nb_params = 1 + nb_peaks * fit_chromato_nb_params

            if len(peak_rt_detected) == 0:
                continue
            # check that at least one peak is within the time window looked at:
            nb_peak_rt_detected_in_window = len(
                np.flatnonzero(
                    (peak_rt_detected[:, 0] >= this_window_start * cl_run.s_per_bin)
                    & (peak_rt_detected[:, 0] < this_window_stop * cl_run.s_per_bin)
                )
            )
            if nb_peak_rt_detected_in_window == 0:
                continue
            logger.debug(f"{nb_peak_rt_detected_in_window=}")

            # ***20190819 try to locate the baseline: make histogram of I vs RT***
            baseline_bin_centres = find_bins_np(
                np.sort(results_list_np_w[:, cl_run.i_A]),
                15,
                5,
                0.01,
                1,
                graph=False,
                mode="histo_tofidx",
            )
            if len(baseline_bin_centres) >= 1:
                baseline_guess = min(baseline_bin_centres)
            else:
                # Take minimum intensity
                baseline_guess = min(results_list_np_w[:, cl_run.i_A])

            # 20190731: do not use extended values here as it will perturb the fit
            (
                rt_mu_opt,
                rt_mu_opt_stderr,
                rt_area_opt,
                rt_area_opt_u,
                rt_sigma_opt,
                rt_alpha_opt,
                rt_LOD,
                rt_SN,
                tofidx_centre_opt,
                tofidx_centre_opt_u,
                mass_centre_opt,
                mass_centre_opt_u,
            ) = fit_chromato(
                x=results_list_np_w[:, cl_run.i_rt],
                y=results_list_np_w[:, cl_run.i_A],
                mu_guess=peak_rt_detected[:, 0],
                apex_guess=peak_rt_detected[:, 1],
                baseline_guess=baseline_guess,
                n_per_bin=cl_run.s_per_bin,
                conf_int=const_pd.conf_int,
                broadness=const_pd.rt_peak_U,
                sigma_guess_value=rt_sigma_guess,
                rt_window_min=windows_start_stop[i_w][0] * cl_run.s_per_bin,
                rt_window_max=windows_start_stop[i_w][1] * cl_run.s_per_bin,
                tof_series=results_list_np_w[:, cl_run.i_tof],
                mass_series=results_list_np_w[:, cl_run.i_mass_exact],
                ratio_to_linear_fit=ratio_to_linear_fit,
                rt_interval_sec=rt_interval_sec,
                graph_show=Plots.CHROMATOGRAPHY_FITTING in show_plots,
                graph_savefile=(
                    rt_peaks_plot_dir
                    if Plots.CHROMATOGRAPHY_FITTING in save_plots
                    else None
                ),
            )

            logger.debug(f"Opt sigma: {rt_sigma_opt}")

            if len(rt_mu_opt) == 0:
                # Nothing found
                continue

            # We now remove peaks found outside the rt window,
            # they are very likely not precisely positionned anyway
            # and should be in RT domained already indentified as not containing many fragments.
            # Note that we need to integrate them beforehand anyway
            # to make sure the deconvolution in RT domain is correct.
            chromato_indexes = np.flatnonzero(
                (rt_mu_opt >= this_window_start * cl_run.s_per_bin)
                & (rt_mu_opt < this_window_stop * cl_run.s_per_bin)
            )

            if len(chromato_indexes) == 0:
                continue

            logger.debug(
                f"no res indexes: {chromato_indexes}; total no peaks: {len(chromato_indexes)}"
            )

            chromato_list_interim = [
                [
                    rt_mu_opt[index],
                    tofidx_centre_opt[index],
                    tofidx_centre_opt_u[index],
                    rt_area_opt[index],
                    rt_area_opt_u[index],
                    rt_sigma_opt[index],
                    rt_alpha_opt[index],
                    mass_centre_opt[index],
                    mass_centre_opt_u[index],
                    rt_LOD[index],
                    rt_SN[index],
                    None,
                ]
                for index in chromato_indexes
            ]
            chromato_list_all += chromato_list_interim

    return chromato_list_all


def nontarget_peak_extraction(
    path_file: PathLike | list[Path],
    rt_start_extract: float = 1,  # TODO: fix a bug when this start with 0
    rt_stop_extract: float = None,
    mass_start: float = None,
    mass_stop: float = None,
    mass_list: list = None,
    ionization_segments: dict[IonizationMethod, tuple[int, int]] | None = None,
    outfile_path: Path = None,
    outfile_name: str | None = None,
    sigma_guess_slope: float = SIGMA_GUESS_SLOPE,
    sigma_guess_exp_k: float = SIGMA_GUESS_EXP_K,
    save_plots: list[Plots] = [],
    show_plots: list[Plots] = [],
    adduct: str = "None",
    df_peaks: pd.DataFrame | None = None,
    **kwargs,
):
    """Extract peaks from the h5 files.

    depending on the used GC method, the expected peak-width is not the same
    it is basically wider when the GC is warmer.
    For that reason there are parameters for tweeaking the width of the chomato
    peaks. these are sigma_guess_slope, sigma_guess_exp_k
    and they are described in
    :py:func:`alpinac.utils_data_extraction.compute_rt_sigma_guess`

    The extraction script will look around the given masses to check for peaks.

    :param path_file: Path to the file containing the data or list of path.
    :param rt_start_extract: start of the retention time window.
    :param rt_stop_extract: stop of the retention time window.
    :param mass_start: start of the mass window.
    :param mass_stop: stop of the mass window.
    :param mass_list: list of masses to extract. (used instead of mass_start and mass_stop)
    :param ionization_segments: dict of ionization segments.
        Maps the ionization method to the start and stop indices.
    :param rt_interval_sec: A custom value to define the rt interval
        for the rt peak fitting part.
    :param rt_sigma_guess_scale: A custom value for how to scale the
        estimated sigma peak value.
        This parameter can be changed if one realizes the fitting
        is not working because the sigma is out of range.
    :param tofidx_domain_range: The range of tof point to look around the
        unit masses.
    :param mz_domain_range: Alternatively ot the `tofidx_domain_range`
        you can give a mz range. this will calculate how many tofids have
        to be extracted arond a mass based on the mass calibration.
        If this is specified, `tofidx_domain_range` is ignored
    :param mass_peaks_over_median_thresholds: Threshold to select which
        mass peaks stand sufficently well out of the noise.
    :param mass_peaks_median_proportion: The proportion of smallest peaks
        used to compute the median proportion.
    :param area_test_threshold_pseudovoigt:
        Not clear what this exactly represents, but
        more mass peaks are found for smaller values of this.
        If not set, a default value of 2.6 is used.
    :param sigma_guess_slope:
    :param sigma_guess_exp_k:
    :param meas_mass_u_cal_min: Minimum of the measured mass uncertainty
        of the calibraiont to use, default is 0.001 m/z, corresponding to
        14.5 ppm at mass 68.994.
        This is only applied under certain conditions, see code for details.
    :param mass_u_ppm_min: Minimum of the mass uncertainty in ppm to use.
        By default no minimum is applied.
    :param fit_chromato_nb_params: Number of mass peaks needed to fit a chromatographic peak.
    :param outfile_path: Path to the output file.
    :param graph_mode_rt_peak_detect: plot the peak detection.
    :param mass_bin_plot_mode: plot the mass bin plot.
    :param plot_baseline_mode: plot the baseline.
    :param save_plots: list of plots to save.
    :param show_plots: list of plots to show.
    :param adduct: adduct to add to CI data, when CI data is specified.
    :param df_peaks: DataFrame containing the peaks.
        Can be given if the data has already been extracted.

    :return:
    """

    logger = logging.getLogger("alpinac.nontarget_peak_extraction")

    df_peaks_given = df_peaks is not None

    kwargs["sigma_guess_slope"] = sigma_guess_slope
    kwargs["sigma_guess_exp_k"] = sigma_guess_exp_k

    if isinstance(path_file, list) or (
        hasattr(path_file, "__iter__") and not isinstance(path_file, str)
    ):
        files = path_file
        extraction_mode = ExctractionMode.MANY_RUNFILES
        # An output file is required
        if outfile_name is None:
            raise ValueError(
                "An output file name is required when extracting many files. "
                "Specify argument `outfile_name`."
            )
        if outfile_path is None:
            raise ValueError(
                "An output path is required when extracting many files. "
                "Specify argument `outfile_path`."
            )
    elif Path(path_file).exists():
        # Single file
        path_file = Path(path_file)
        files = [path_file]
        extraction_mode = ExctractionMode.SINGLE_RUNFILE
    else:
        # Template for many files
        path_file = Path(path_file)
        logger.info(f"File {path_file} does not exist.")
        # Check if it is a template
        files = list(path_file.parent.glob(path_file.name))
        if len(files) == 0:
            raise ValueError(f"File {path_file} does not exist.")
        elif len(files) == 1:
            path_file = files[0]
        else:
            # Set the extraction mode to many files
            extraction_mode = ExctractionMode.MANY_RUNFILES
            logger.info(f"Found {len(files)} files to extract.")

    if df_peaks_given and extraction_mode == ExctractionMode.MANY_RUNFILES:
        raise ValueError(
            "Dataframe containing the peaks cannot be given when extracting many files."
        )

    if outfile_name is None:
        # Generate a default name for the output file
        stem = path_file.stem
        # Remove any * in the name
        stem = stem.replace("*", "")
        if rt_stop_extract is None:
            outfile_name = f"{stem}.frag.all_rts.txt"
        else:
            outfile_name = (
                f"{stem}.frag.{int(rt_start_extract)}s{int(rt_stop_extract)}s.txt"
            )

    # Whether the plot should be done
    do_plots = save_plots + show_plots

    if save_plots and extraction_mode == ExctractionMode.MANY_RUNFILES:
        logger.warning(
            "Currently plots are not properly saved when extracting many files."
        )

    if outfile_path is None:
        # Create a directory with the file name if is does not aleady axist
        if extraction_mode == ExctractionMode.SINGLE_RUNFILE:
            outfile_path = (path_file.parent / path_file.stem).with_suffix(".alpinac")
        else:
            outfile_path = path_file.parent
    else:
        outfile_path = Path(outfile_path)
    outfile_path.mkdir(exist_ok=True)

    out_file = outfile_path / outfile_name

    plots_dir = outfile_path / f"{outfile_name}.plots"
    if save_plots:
        plots_dir.mkdir(exist_ok=True)

    is_first_file = True

    for file_index, path_file in enumerate(files):

        logger.info(f"Extracting peaks from {path_file}")

        cl_run = hdf5Metadata(path_file, mode="r")

        # Set defualt value for the segments
        if ionization_segments is None:
            ionization_segments = {}
            # TODO Should check to read that in the file h5 file if not given

            logger.warn("No ionization segments given.")
            # set default values for the sgments
            n_segments = cl_run.hdf5_dim3
            if n_segments == 1:  # this is EI MS
                ionization_segments[IonizationMethod.EI] = [0, 0]
            else:
                raise ValueError(
                    f"No ionization segments given and {n_segments=} in  file {path_file}."
                )

        logger.info(f"{ionization_segments=}")

        t0 = time.time()

        if mass_list is None:
            if mass_start is None:
                mass_start = cl_run.mass_axis_h5[0]
                logger.debug(
                    "--mass-start not given, using starting in file: " + str(mass_start)
                )
            else:
                logger.debug("--mass start provided: " + str(mass_start))
            mass_start = int(round(mass_start))

            if mass_stop is None:
                mass_stop = cl_run.unit_mass_max
                logger.debug(
                    "--mass-stop not given, using as default: " + str(mass_stop)
                )
            else:
                mass_stop = min(int(round(mass_stop)), cl_run.unit_mass_max)
                logger.debug("--mass-stop provided: " + str(mass_stop))
            this_mass_list = [ii for ii in range(mass_start, mass_stop + 1, 1)]
        else:
            logger.debug("--mass-list was provided:" + str(mass_list))
            this_mass_list = mass_list
        logger.debug(f"{this_mass_list=}")

        if rt_stop_extract is None:
            rt_stop_extract = cl_run.len_time_axis * cl_run.s_per_bin
            logger.info(
                "rt_stop not provided, extract until end of run: "
                + str(int(rt_stop_extract))
            )

        # Add for each mass the rt inteval to extract
        dict_extraction_m_rt = {
            key_mass: [[rt_start_extract, rt_stop_extract, key_mass]]
            for key_mass in this_mass_list
        }

        if not df_peaks_given:

            peak_list = extract_all_peaks(
                cl_run=cl_run,
                dict_extraction_m_rt=dict_extraction_m_rt,
                ionization_segments=ionization_segments,
                save_plots=save_plots,
                show_plots=show_plots,
                plots_dir=plots_dir,
                **kwargs,
            )

            t1 = time.time()
            logger.info(f"Peak Extraction finished: {t1-t0:.2f} s")
            logger.info(f"Number of fragments found above LOD: {len(peak_list)} ")
            if len(peak_list) == 0:
                logger.info(f"No peak detected for file {path_file}. Saving no data.")
                continue

            df_peaks = pd.DataFrame(
                peak_list,
                columns=[
                    "rt",
                    "tof",
                    "tof_u",
                    "area",
                    "area_u",
                    "sigma",
                    "alpha",
                    "m",
                    "m_u",
                    "LOD",
                    "SN",
                    "comp_bin",
                    "ionisation",
                    "m_u_ppm",
                    "m_u_cal_ppm",
                    "width",
                ],
            )
            df_peaks.to_csv(out_file.with_suffix(".peaks_extracted.csv"), index=False)

        logger.info("Preparing data for export")
        # write chromato_list_all into a numpy array:
        logger.debug(f"{df_peaks=}")

        # sort data by retention time:
        df_peaks = df_peaks.sort_values(by=["rt", "tof"], ascending=[True, True])

        # Group peaks by retention time

        count_all_bins = 0

        # here again, we split in windows per time.
        # the spread of peak position is larger for increasing retention time.
        df_peaks["rt_idx"] = (df_peaks["rt"] / cl_run.s_per_bin).round(0).astype(int)
        rt_start, rt_stopp = df_peaks["rt_idx"].min(), df_peaks["rt_idx"].max()
        windows_start_stop = split_into_windows(
            rt_start, rt_stopp, cl_run.s_per_bin, cl_run.len_time_axis
        )

        # Attempt to group the peaks in compounds

        for window_start, window_stopp in windows_start_stop:
            # Window given in rt index
            logger.debug(f"{window_start=}, {window_stopp=}")
            window_middle = (window_start + window_stopp) / 2
            mask_this_window = (df_peaks["rt_idx"] >= window_start) & (
                df_peaks["rt_idx"] <= window_stopp
            )
            df_this_window = df_peaks.loc[mask_this_window]

            # peak position may be spread due to isotopologues,
            # allow for a larger rt_sigma_guess
            rt_sigma_guess = 2.0 * compute_rt_sigma_guess(
                sigma_guess_slope,
                sigma_guess_exp_k,
                window_middle * cl_run.s_per_bin,
            )

            const_pd_PWt_local = max(3, 3.0 * rt_sigma_guess)

            if len(df_this_window) < const_pd.nb_bin_min_RT:
                logger.debug("Not enough peaks in this window")
                continue
            # The 6 was changed to 4 in case of different gc method
            # TODO: see what it is and make a parameter
            delta_bin = cl_run.s_per_bin * rt_sigma_guess / 6.0
            rt_bin_centres = find_bins_np(
                df_this_window["rt"].values,
                delta_bin,
                rt_sigma_guess,
                const_pd.PTh_t,
                cl_run.s_per_bin,
                graph=False,
                mode="histo_rt",
            )

            if len(rt_bin_centres) == 0:
                logger.debug("No rt bin centres found")
                continue

            bin_value_list, no_bins, bin_list = make_bins_np(
                df_this_window["m"].values,
                df_this_window["rt"].values,
                rt_bin_centres,
                cl_run.s_per_bin * const_pd_PWt_local,
                const_pd.nb_bin_min_RT,
                cl_run.s_per_bin,
                graph=Plots.COELUTION_PLOT in show_plots + save_plots,
                mode=MakeBinsModes.NO_SLOPE,
                plot_mode="rt_domain",
                save_graph_path=(
                    plots_dir
                    / f"coelution_rt_{int(window_middle*cl_run.s_per_bin)}.png"
                    if Plots.COELUTION_PLOT in save_plots
                    else None
                ),
            )

            if no_bins == 0:
                logger.warning("Peaks could not be groupped")
                continue

            # update bin value, taking into account previous windows
            df_peaks.loc[mask_this_window, "comp_bin"] = np.where(
                bin_value_list != -1,
                bin_value_list + count_all_bins,
                -1,
            )

            count_all_bins += max(bin_list)

        logger.info(f"Number of compounds found: {df_peaks['comp_bin'].nunique() - 1}")

        # Save the data
        df_peaks["comp_bin"] = np.where(
            df_peaks["comp_bin"].isna().values, -1, df_peaks["comp_bin"]
        )

        if is_first_file:
            # Create the header of the file here
            # we could do it on top, but this lets time for a user who by mistake would override
            is_first_file = False
            with out_file.open("w") as outfile:
                # Write the header
                columns = [
                    "RT",
                    "mass",
                    "mass_u_ppm",
                    "mass_cal_u_ppm",
                    "area",
                    "area_u",
                    "peak_width",
                    "peak_alpha",
                    "compound_bin",
                    "LOD",
                    "SN",
                    "Ionisation",
                    "Adduct",
                    "Spectrum_id",
                ]
                if extraction_mode == ExctractionMode.MANY_RUNFILES:
                    # Extra tab as file names are ususally long
                    columns.insert(0, "File\t")
                outfile.write("\t".join(columns).expandtabs(COLSIZE) + "\n")

        with out_file.open("a") as outfile:
            for _, row in df_peaks.iterrows():

                ionisation_type = IonizationMethod(row["ionisation"])
                # Write the line
                line = [
                    f"{row['rt']:.2f}",
                    f"{row['m']:.7f}",
                    f"{row['m_u_ppm']:.2f}",
                    f"{row['m_u_cal_ppm']:.2f}",
                    f"{row['area']:.2f}",
                    f"{row['area_u']:.2f}",
                    f"{row['width']:.5f}",
                    f"{row['alpha']:.3f}",
                    f"{int(row['comp_bin'])}",
                    f"{row['LOD']:.2f}",
                    f"{row['SN']:.2f}",
                    ionisation_type.name,
                    adduct if ionisation_type == IonizationMethod.CI else "None",
                    # Spectrum id correspond the the ionization type
                    str(ionisation_type.value),
                    "\n",
                ]
                # Add the file at the beggining
                if extraction_mode == ExctractionMode.MANY_RUNFILES:
                    stem = path_file.stem
                    if len(stem) < COLSIZE:
                        # Add an extra tab for shorter file names
                        stem += "\t"
                    line.insert(0, stem)
                outfile.write("\t".join(line).expandtabs(COLSIZE))

    logger.info(f"Output file: {out_file}")

    return out_file
