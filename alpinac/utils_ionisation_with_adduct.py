# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 09:05:18 2021

@author: mygu

The aim of this file is to collect functions inked to handling CI (or other) ionisation type data.
The aim is to handle mass peak that potentially contain adducts.
This file does not do anything about EI type data.

The general idea is:
    - to not change the knapsack algorithm.
    - since the knapsack algorithm returns only suggestions with non-negative DBE values,
        we need to handle the measured masses beforehand, to somehow remove the potential adducts.
    - so we also need to keep track of which adduct was removed. 
"""

