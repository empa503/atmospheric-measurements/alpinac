"""Simple command line to generate knap sack results from a given mass."""

import argparse
import logging
import sys
from alpinac.cli.utils import add_verbose_debug_arguments, add_target_arguments, process_verbose_debug_arguments
from alpinac.compound import Compound
from alpinac.periodic_table import (
    get_string_formula,
)
from alpinac.mode_identification.step1 import step1_knapsack


def main():

    parser = argparse.ArgumentParser(description="Calculate the possible molecules for the mass.")
    parser.add_argument("mass", type=float, help="The mass of the molecule to knapsack.")
    # Add mass uncertanty 
    parser.add_argument(
        "--mass_u",
        help="Mass uncertainty in mass unit",
        action="store",
        default=0.2,
        type=float,
    )
    add_verbose_debug_arguments(parser)
    add_target_arguments(parser)
    args = parser.parse_args()

    process_verbose_debug_arguments(args)

    cmp = Compound(
        [

            #idx_rt = 0
            #idx_m = 1
            #idx_m_u = 2
            #idx_m_cal_u = 3
            #idx_area = 4
            #idx_area_u = 5
            #idx_peak_width = 6
            #idx_peak_alpha = 7
            #idx_comp_bin = 8
            #idx_LOD = 9
            #idx_ionisation = 10
            #idx_adduct = 11
            #idx_run_bin = 12
            #idx_SN = None
           [ # RT
            0.0,
            # mass
            args.mass,
            # mass_u_ppm
            args.mass_u / args.mass * 1e6,
            # mass_cal_u_ppm
            0.0,
            # area
            1.0,
            # area_u
            1.0,
            # peak_width
            1.0,
            # peak_alpha
            0.5,
            # compound_bin
            0,
            # LOD
            0.0,
            # Ionisation
            "EI",
            # Adduct
            None,
            # run_bin
            0,
            ]
            ]
    )


    all_solutions, total_DBE_solutions = step1_knapsack(cmp, target_elements=args.target_elements, target_formula=args.target_formula)
    this_solutions = all_solutions[0] # Only one fragement 

    print(f"Total number of solutions: {len(this_solutions)}")
    for solution in this_solutions:
        print(get_string_formula(solution))

    #print(f"Total number of solutions with DBE: {total_DBE_solutions}")



if __name__ == "__main__":
    main()