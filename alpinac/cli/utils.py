import argparse
import logging

from argparse import ArgumentParser

def add_target_arguments(parser: ArgumentParser):

    parser.add_argument(
        "--target-elements",
        help="chemical elements potentially present, e.g. CHFClBr. The order has no importance.",
        action="store",
    )
    parser.add_argument(
        "--target-formula",
        help=(
            "Chemical formula of the molecular ion.\n"
            "ALPINAC will use only this formula or smaller fragments.\n"
            "For example with CCl4 given,"
            " ALPINAC will use only "
            "C, Cl, CCl, CCl2, CCl3, CCl4 and Cl2 (and all possible isotopologues)."
        ),
        action="store",
    )

def add_verbose_debug_arguments(parser: ArgumentParser):
    parser.add_argument(
        "-d",
        "--debug",
        help="Debugging statements",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        "--info",
        help="Write out the output",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )

def process_verbose_debug_arguments(args: argparse.Namespace):

    logging.basicConfig()
    logging.getLogger("alpinac").setLevel(args.loglevel)

    # Print out the arguments on debug
    logger = logging.getLogger("alpinac.cli.utils")
    logger.debug(args)
