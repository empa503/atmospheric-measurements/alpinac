"""Simple command line to calculate the mass of a molecule given its formula."""

import argparse
import logging

from alpinac.periodic_table import formula_to_mass


def main():

    parser = argparse.ArgumentParser(description="Calculate the mass of a molecule.")
    parser.add_argument("formula", help="The formula of the molecule.")
    parser.add_argument(
        "-d", "--debug", action="store_true", help="Show debug information."
    )
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    mass = formula_to_mass(args.formula)

    print(f"The mass of {args.formula} is {mass}")


if __name__ == "__main__":
    main()
