"""Small script that gives information about isotopes in alpinac"""

import argparse


def main():
    parser = argparse.ArgumentParser(description="Isotopes in alpinac")
    parser.add_argument("isotope", type=str, help="Isotope to get information about")
    args = parser.parse_args()
    print(f"Isotopes of: {args.isotope}")

    from alpinac.periodic_table import periodic_table

    for element_dict in periodic_table:
        if args.isotope not in element_dict['element']:
            continue
        print(element_dict)


if __name__ == "__main__":

    main()
