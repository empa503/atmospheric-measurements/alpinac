# -*- coding: utf-8 -*-
import logging


import networkx as nx

import alpinac
from alpinac.utils_data_extraction import FitModes, FittingMethod, fit_iso_profile
from alpinac.utils_identification import enumerate_all_valid_subfragments
from alpinac.utils_identification import enumerate_all_valid_subfragments_from_lists
import alpinac.periodic_table as chem_data
from alpinac.compound import Compound


# https://pubchem.ncbi.nlm.nih.gov/
from alpinac.isotopologue import Isotopologues

fit_mode_fixed_x_axis = 0
fit_mode_variable_x_axis = 1

class NodeLite():
    """A class for the minimal data to encode a node. """
    total_number_nodes_lite = 0 # a class parameter
    total_number_duplicates = 0 # class parameter
    iso: Isotopologues | None
    def __init__(self, fragment: list, unique_id: int=None, visited: bool=False, idx_measured_mass: int=None) -> None:
        """Constructor of NodeLite, initialize a node with fragment

        INPUT:
        - `fragment`: a list of non-negative integers encoding a chemical formula
        - `unique_id`: the number-id of the node, should be unique for each node
        - `visited`: flag for efficient graph traversal
        - `idx_measured_mass`: the index of the mass for which fragment is a candidate sum-formula
        
        the children will be stored as adjacent nodes of the graph
        """
        self.fragment = fragment
        
        #self.children = [] # a list of other NodeFragment that are children (when it will be safe to do it with adjacent nodes of the graph, remove it: use G.successors(n) or G.neighbors(n))
        self.subgraph_size = 1 # the number of nodes of distinct fragment in the dag (can be a tree) rooted at self (#self=1)
        if unique_id is not None:
            self.unique_id = unique_id
        else:
            self.unique_id = self.get_total_number_nodes()
        self.increment_node_counter() # do it with a class method otherwise it creates an instance field
        self.visited = visited
        self.added_somewhere_as_subfragment = False # needed when building the graph
        
        #if idx_measured_mass is not None:
        if idx_measured_mass is not None:
            #It is none if it is a constructed molecular ion!
            if idx_measured_mass >0: 
                logging.debug("init NodeLite: test NodeLite ok" + str(fragment))
            else:
                logging.debug("init NodeLite: init NodeLite " + str(fragment))

            self.idx_measured_mass = [idx_measured_mass] # index of mass
        else:
            self.idx_measured_mass = None
        # Predicted number of formula solutions using all subsets of abundant fragment formula:
        self.no_all_subfragments = None # will be filled later in compute_all_subfragments_of_nodes
        self.no_all_subfragments_check = None # for test
        self.iso = None # for the class Isotopologue

        #sum signal of this fragment and all its subfragment nodes in the graph (successors)
        # this is the sum of self.iso_list_I_sum and all node_lite.iso.iso_list_I_sum for all node_lite that are subfragments of self
        self.subgraph_sum_I = float(0.0)

        #percentage of signal explained by this fragment and all its subfragment nodes in the graph (successors)
        self.subgraph_percent_I = float(0.0)

        #likelohood value for this fragment (captures data from all the subfragment nodes in the graph)
        self.subgraph_likelihood = float(0.0)
        
        #is the node validated, i.e. has the k factor of the isotopologue profile 
        #at least been once optimised using lmfit?
        self.optimised = False


    def __repr__(self):
        return chem_data.get_string_formula(self.fragment) + " id {}".format(self.unique_id) + " visited: {}".format(self.visited)

    def __str__(self):
        return chem_data.get_string_formula(self.fragment)

    @classmethod
    def increment_node_counter(self):
        self.total_number_nodes_lite += 1
    @classmethod
    def reset_node_counter(self):
        self.total_number_nodes_lite = 0
        self.total_number_duplicates = 0
    @classmethod
    def get_total_number_nodes(self):
        return self.total_number_nodes_lite
    @classmethod
    def increment_number_duplicates(self):
        self.total_number_duplicates += 1
    @classmethod
    def decrement_number_duplicates(self):
        self.total_number_duplicates -= 1
    @classmethod
    def get_total_number_duplicates(self):
        return self.total_number_duplicates

    def is_subfragment(self, other: list) -> bool:
        # coli: check all for each atom, the other has more or equal number of atoms
        i=0
        while i < chem_data.len_frag and self.fragment[i] <= other[i]:
            i += 1
        # All the atom should have a value smaller or equal
        return i >= chem_data.len_frag
    def is_same_fragment(self, other: list) -> bool:
        i=0
        # coli: return np.all(np.array(self.fragment) == np.array(other))
        while i < chem_data.len_frag and self.fragment[i] == other[i]:
            i += 1
        return i >= chem_data.len_frag
    def is_supfragment(self, other: list) -> bool:
        i=0
        while i < chem_data.len_frag and self.fragment[i] >= other[i]:
            i += 1
        return i >= chem_data.len_frag
    def is_strict_supfragment(self, other: list) -> bool:
        # coli: A subfragement that is not having the same n of atoms
        return sum(self.fragment) > sum(other) and self.is_supfragment(other)
    def is_strict_subfragment(self, other: list) -> bool:
        return sum(self.fragment) < sum(other) and self.is_subfragment(other)

    def lost_fragment(self, fragment):
        if self.is_supfragment(fragment):
            lost = [self.fragment[i] - fragment[i] for i in range(chem_data.len_frag)]
            return lost
        if self.is_subfragment(fragment):
            lost = [fragment[i] - self.fragment[i] for i in range(chem_data.len_frag)]
            return lost
        return [0]*self.len_frag

# these are not methods of class NodeLite anymore, but functions
def add_edges_to_node(G: nx.DiGraph, start_node: NodeLite, node: NodeLite) -> tuple[bool, int]:
    """Find all the minimal ancestors of `node` and add edges
    
    search for edges to node, starting search at start_node (search for
    ancestors of node). Do not add edges from node (assume there is no
    smaller element, i.e. no successors). Assume that
    G.add_node(unique_id, node) was done before.

    INPUT:
    - `G`: an initialized graph G, containing start_node and node
    - `start_node`: a node in `G` from where starting to look for
      ancestors of `node`
    - `node`: a new node, in G, without ancestors

    OUTPUT:
    Returns True if the node was added somewhere (or a twin was found),
    and the number of occurences, that is the number of new edges to it.
    If a twin was found (a node with the same fragment but another mass
    index), the new mass index is added to the existing note. It returns
    True, 0.
    
    FUTURE potential development:
    One edge could be added only if the neutral loss 
    (the fragment difference between parent and child) has a positive
    DBE value. This way, e.g. H3 could not be a valid neutral loss.
    But would this work for CI data?
    """
    if start_node.visited:
        return start_node.added_somewhere_as_subfragment, 0 # 0 because if it is already present, do not count it twice
    if start_node.is_supfragment(node.fragment): # yes, it is an ancestor
        if start_node.is_same_fragment(node.fragment): # it should happen rarely...
            # how to distinguish between a same fragment being a possibility for two distinct masses,
            # and in a DAG variant, another occurence of the node? -> with the flag 'visited'
            # if start_node.visited: there was a return statement above, so here this is a twin node, not a duplicate
            start_node.visited = True
            start_node.added_somewhere_as_subfragment = True
            if start_node.idx_measured_mass is not None and node.idx_measured_mass is not None:
                start_node.idx_measured_mass.append(node.idx_measured_mass[0])
                logging.getLogger('alpinac.add_edges_to_node').info(
                    f"Found a duplicate: {chem_data.get_string_formula(start_node.fragment)}" 
                )
            start_node.increment_number_duplicates()
            return True, 0
        # ok, now check each child.
        # if none of the children is visited and none can have it as subfragment, add it as child of start_node
        # it works even if start_node has no successor: it will add node as sucessor
        occ = 0
        added_somewhere = False
        for child_id in G.successors(start_node.unique_id):
            child = G.nodes[child_id]['node_lite']
            added_as_subfragment, occurences = add_edges_to_node(G, child, node) # recursive call
            # if the child is already visited, it returns child.added_somewhere_as_subfragment, 0
            occ += occurences
            added_somewhere = added_somewhere or added_as_subfragment
            if added_as_subfragment:
                start_node.added_somewhere_as_subfragment = True
        if not added_somewhere:
            # add the fragment as child here: add edge to it
            G.add_edge(start_node.unique_id, node.unique_id)
            start_node.subgraph_size += 1 # assume node as no successors, otherwise add node.subgraph_size instead of 1
            occ += 1
        start_node.visited = True
        start_node.added_somewhere_as_subfragment = True
        return True, occ
    start_node.visited = True
    start_node.added_somewhere_as_subfragment = False
    return False, 0

def reset_unvisited(G: nx.DiGraph, start_node: NodeLite=None) -> None:
    """Reset recursively flag NodeLite.visited to False """
    if start_node is None:
        for max_node_id in G.graph['maximal_nodes']:
            max_node = G.nodes[max_node_id]['node_lite']
            reset_unvisited(G, max_node)
    else:
        if start_node.visited:
            # 1. reset sub-graph
            for child_id in G.successors(start_node.unique_id):
                child = G.nodes[child_id]['node_lite']
                reset_unvisited(G, child)
            # 2. reset itself
            start_node.visited = False
        # if start_node is unvisited, do nothing.

def add_maximal_node_and_edges_to_graph_from_max_fragment(
        G: nx.DiGraph, 
        list_max_fragments:list, 
        idx_measured_mass: int=None, 
        molecular_ion: bool=False,
        list_i_spec: list=None,
    ) -> None:
    """Add one maximal node and its edges from it to other former maximal nodes

    Assume that the fragment is a sub-fragment of a node already in the graph, 
    or it is incomparable (it is a singleton).
    It can be used to add a possible molecular ion, 
    in that case `molecular_ion` is set to True

    The new fragment is added to G.graph['maximal_nodes']
    and the former nodes in G.graph['maximal_nodes'] that are
    now its children are removed from G.graph['maximal_nodes'].

    INPUT:
    - `G`: a directed acyclic graph
    - `list_max_fragments`: a list of incomparable fragments that are all incomparable or >= each max fragment of G
    - `idx_measured_mass`: the index of the mass for which the fragments are a candidate sum-formula
    - `molecular_ion`: the fragment is a possible molecular ion, and does not correspond to a measured mass
    OUTPUT: None
    """
    logger = logging.getLogger('alpinac.utils_graph.add_maximal_node_and_edges_to_graph_from_max_fragment')
    logger.info("add_maximal_node_and_edges_to_graph_from_max_fragment(G, list_max_fragments={}, idx_measured_mass={}, molecular_ion={})".format(len(list_max_fragments), idx_measured_mass, molecular_ion))
    logger.debug("max_nodes at init " + str([chem_data.get_string_formula(G.nodes[i_node]['node_lite'].fragment) for i_node in G.graph['maximal_nodes']]))


    if idx_measured_mass is not None:
        if idx_measured_mass in G.graph['candidates_per_measured_mass']:
            # just in case there is already another max element of same mass w.r.t. uncertainty
            G.graph['candidates_per_measured_mass'][idx_measured_mass] += 1
        else: # new fragment with  idx_measured_mass
            G.graph['candidates_per_measured_mass'][idx_measured_mass] = 1
    flag_keep_max_nodes = [True]*len(G.graph['maximal_nodes']) # a flag to say if the node stay maximal after processing all the list of new fragments
    new_max_nodes = []
    
    for i in range(len(list_max_fragments)):
        max_fragment = list_max_fragments[i]
        i_spec = list_i_spec[i] #corresponding spectrum index of fragment to be added
        logger.info("adding #{} {}".format(i, chem_data.get_string_formula(max_fragment)))
    
        if len(G.graph['maximal_nodes']) == 0: # G has no root, it should mean it is empty
            node = NodeLite(fragment = max_fragment, visited = False, idx_measured_mass=idx_measured_mass)
            # set visited to False do avoid the step reset_unvisited
            G.add_node(node.unique_id, node_lite = node)
            new_max_nodes.append(node.unique_id)
            if G.graph['mol_ion_nodes_per_spectrum'][i_spec] is None:
                G.graph['mol_ion_nodes_per_spectrum'][i_spec] = [node.unique_id]
            else:
                G.graph['mol_ion_nodes_per_spectrum'][i_spec].append(node.unique_id)
        else:
            children_of_new_max_node = []
            has_twin = False
            # now check the former maximal nodes if they can be possible children
            for j in range(len(G.graph['maximal_nodes'])):# counter of max node number
                former_max_node_id = G.graph['maximal_nodes'][j]
                former_max_node = G.nodes[former_max_node_id]['node_lite']
                # max_fragment should be larger than former_max_node.fragment or incomparable
                if former_max_node.is_subfragment(max_fragment): # sub or equal
                    if former_max_node.is_same_fragment(max_fragment):
                        # duplicate case
                        # do not add a new node, this is a twin (it should not happen though)
                        # it may happens in case several replicate spectra are treated together
                        if former_max_node.idx_measured_mass is not None and idx_measured_mass is not None:
                            former_max_node.idx_measured_mass.append(idx_measured_mass)
                        former_max_node.increment_number_duplicates()
                        has_twin = True
                    else: # former_max_node.is_strict_subfragment(max_fragment):
                        children_of_new_max_node.append(former_max_node)
                        flag_keep_max_nodes[j] = False
            # now update the list of maximal nodes
            if not has_twin:
                new_max_node = NodeLite(fragment = max_fragment, visited = False, idx_measured_mass=idx_measured_mass)
                G.add_node(new_max_node.unique_id, node_lite = new_max_node)
                G.graph['nodes_validated'].append(new_max_node.unique_id)
                # now add edges from the the node to its children given in children_of_new_max_node
                for child in children_of_new_max_node:
                    G.add_edge(new_max_node.unique_id, child.unique_id)
                    new_max_node.subgraph_size += child.subgraph_size
                new_max_nodes.append(new_max_node.unique_id)
                #G.graph['mol_ion_nodes_per_spectrum'][i_spec] = [new_max_node.unique_id]
                if G.graph['mol_ion_nodes_per_spectrum'][i_spec] is None:
                    G.graph['mol_ion_nodes_per_spectrum'][i_spec] = [new_max_node.unique_id]
                else:
                    G.graph['mol_ion_nodes_per_spectrum'][i_spec].append(new_max_node.unique_id)
                
                
    
    G.graph['maximal_nodes'] = [G.graph['maximal_nodes'][j] for j in range(len(flag_keep_max_nodes)) if flag_keep_max_nodes[j]] + new_max_nodes
    logger.debug("max_nodes at end " + str([chem_data.get_string_formula(G.nodes[i_node]['node_lite'].fragment) for i_node in G.graph['maximal_nodes']]))


def add_nodes_and_edges_to_graph_from_list_fragments(
        G: nx.DiGraph, 
        list_fragments:list,
        idx_measured_mass: int=None, 
    ) -> None:
    """Add nodes for each of the fragments, and the edges to them
    
    Assume that the fragments are incomparable or given in decreasing
    order of mass (very important)
    if there is no edge to a fragment, its unique_id is added to
    G.graph['maximal_nodes'] (it is actually a singleton)
    if the node is a twin (there is already a same fragment encoding for
    a different mass of overlapping uncertainty range), it is not added
    to the Graph, but its mass index is added to the twin node.

    INPUT:
    - `G`: a directed acyclic graph
    - `list_fragments`: a list of fragments incomparable or ordered >=
    - `idx_measured_mass`: the index of the mass for which all the fragments are candidate sum-formulas
    OUTPUT: None
    """
    logger = logging.getLogger("alpinac.add_nodes_and_edges_to_graph_from_list_fragments")
    logger.info(f"{len(list_fragments)=}")

    if idx_measured_mass is not None:
        G.graph['candidates_per_measured_mass'][idx_measured_mass] = len(list_fragments)
        # save the number of candidates per mass for remove_singletons
    for i in range(len(list_fragments)):

        is_first_frag_of_tree = len(G.graph['maximal_nodes']) == 0
        
        fragment = list_fragments[i]
        
        logger.info("adding #{} {}".format(i, chem_data.get_string_formula(fragment)))
        node = NodeLite(
            fragment = fragment,
            # set visited to False do avoid the step reset_unvisited
            visited = not is_first_frag_of_tree,
            idx_measured_mass=idx_measured_mass
        )
        G.add_node(node.unique_id, node_lite = node)
        logger.debug(f'node, {node.unique_id}, {chem_data.get_string_formula(fragment)}, {node.idx_measured_mass}')
        if is_first_frag_of_tree: # G has no root, it should mean it is empty
            G.graph['maximal_nodes'].append(node.unique_id)
        else:
            added_somewhere = False
            total_occ = 0
            
            for max_node_id in G.graph['maximal_nodes']:
                max_node = G.nodes[max_node_id]['node_lite']
                # Check if the node could be a subfragment of the max_node
                added, occ = add_edges_to_node(G, max_node, node)
                added_somewhere = added_somewhere or added
                total_occ += occ
            if added_somewhere and total_occ == 0: # this is a twin, remove the node
                logger.debug("node is a twin, removed: {}".format(node))
                G.remove_node(node.unique_id)
            elif not added_somewhere:
                # add it to the list of max nodes
                G.graph['maximal_nodes'].append(node.unique_id)
                                   
            reset_unvisited(G)

def find_max_nodes_per_spectrum(G: nx.DiGraph) -> None:
    """
    Find the maximal nodes (maximal fragment formulae) for each spectrum, 
    not for the entire graph.
    We start from the maximal nodes of the entire graph 
    which are already stored in G.graph['maximal_nodes'].
    For each of them, we evaluate if they are a max. node of a given spectra. 
    If not, we check their children.
    We do this procedure for each spectrum separately.
    """
    logger = logging.getLogger('alpinac.utils_graph.find_max_nodes_per_spectrum')
    if len(G.graph['maximal_nodes']) == 0:
        raise ValueError("No maximal nodes")
    no_spectra = len(G.nodes[G.graph['maximal_nodes'][0]]['node_lite'].iso.belong_to_spectra)

    for i_spec in range(no_spectra): #len(max_node_iso.belong_to_spectra)):
        #find max nodes for spectrum i_spec
        
        #initialisation: set all nodes to unvisited.
        #reset_unvisited(G) #not sure if this is doing the job for the entire graph
        for node_id in G.nodes:
            G.nodes[node_id]['node_lite'].visited = False
                
        ancestors_list = G.graph['maximal_nodes']
        children_list = []
        
        while len(ancestors_list) > 0:
            #print("find_max_nodes_per_spectrum - loop over ancestors")
            #print("i_spec, ancestors list " + str(i_spec) + " " + str(ancestors_list))
            for node_id in ancestors_list:
                #set node to visited
                if G.nodes[node_id]['node_lite'].visited:
                    continue
                G.nodes[node_id]['node_lite'].visited = True
                
                #old_max_node = G.nodes[old_max_node_id]['node_lite']
                node_iso = G.nodes[node_id]['node_lite'].iso #
                #to which spectra does this node belong? -> iso.belong_to_spectra
                if node_iso.belong_to_spectra[i_spec]:
                    logger.info("i_spec, max node "+ str(i_spec) + " " + str(node_iso.frag_abund_formula))
                    if G.graph['maximal_nodes_per_spectrum'][i_spec] is None:
                        G.graph['maximal_nodes_per_spectrum'][i_spec] = [G.nodes[node_id]['node_lite'].unique_id]
                        #print("G.graph['maximal_nodes_per_spectrum'][i_spec]")
                        #print(G.graph['maximal_nodes_per_spectrum'][i_spec])

                    else:
                        G.graph['maximal_nodes_per_spectrum'][i_spec].append(G.nodes[node_id]['node_lite'].unique_id)
                else:
                    #warning! function successors works like an iterator
                    #https://stackoverflow.com/questions/47161158/networkx-neighbor-set-not-printing
                    for node_child in list(G.successors(node_id)):
                        children_list.append(node_child) #add children 
                #print("i_spec, max node "+ str(i_spec) + " " + str(node_iso.frag_abund_formula))
                #logging.info("nx.descendants "+ str(nx.descendants(G, old_max_node.unique_id)))
                #logging.info("nx.ancestors " + str(nx.ancestors(G, old_max_node.unique_id)))
        
            ancestors_list = children_list.copy()
            children_list = []
    #At this stage, if a spectrum exists but has no solutions at all, its
    #G.graph['maximal_nodes_per_spectrum'][i_spec] wil still be None.
    for i_spec in range(no_spectra):
        if G.graph['maximal_nodes_per_spectrum'][i_spec] is not None:
            for i_node in G.graph['maximal_nodes_per_spectrum'][i_spec]:
                logger.info("i_spec, max node from G: "+ str(i_spec) + " " + G.nodes[i_node]['node_lite'].iso.frag_abund_formula)
        else:
            logger.info("i_spec, max node from G: None.")

def find_node_from_formula(G: nx.DiGraph, node_parent: int, frag: chem_data.FragmentAsAtomCounts, i_spec: int):
    """
    Find, if it exists, the node corresponding to the fragment formula frag.
    The node does not need to be a measured mass of the spectrum i_spec.
    """
    logger = logging.getLogger('alpinac.utils_graph.find_node_from_formula')

    logger.info(f"Searching for node from formula {chem_data.get_string_formula(frag)} starting from parent {G.nodes[node_parent]['node_lite']} ")    
    node_child_found = None
        
    #initialisation: set all nodes to unvisited.
    #reset_unvisited(G) #not sure if this is doing the job for the entire graph
    for node_id in G.nodes:
        G.nodes[node_id]['node_lite'].visited = False
            
       
    #Warning! if the frag contains an added atom, the parent node is a subfragment!
    if G.nodes[node_parent]['node_lite'].is_strict_subfragment(frag):
        ancestors_list = G.graph['maximal_nodes']
        logger.debug(f"Parent not a subfragment, Using maximal_nodes instead: {ancestors_list=}")
    else:
        ancestors_list = [node_parent]
    
    
    
    children_list = []
    
    while len(ancestors_list) > 0:
        for node_id in ancestors_list:
            #set node to visited
            if G.nodes[node_id]['node_lite'].visited:
                continue
            G.nodes[node_id]['node_lite'].visited = True
            
            node_iso = G.nodes[node_id]['node_lite'].iso
            #Warning The fragment formula is a construction so it may not be 
            #associated to a measured mass of the spectra but that's ok.
            if G.nodes[node_id]['node_lite'].is_same_fragment(frag): #node_iso.belong_to_spectra[i_spec] and 
                node_child_found = G.nodes[node_id]['node_lite'].unique_id
                ancestors_list = []
                if G.graph['mol_ion_nodes_per_spectrum'][i_spec] is None:
                    G.graph['mol_ion_nodes_per_spectrum'][i_spec] = [G.nodes[node_id]['node_lite'].unique_id]
                    #print("G.graph['maximal_nodes_per_spectrum'][i_spec]")
                    #print(G.graph['maximal_nodes_per_spectrum'][i_spec])
                else:
                    G.graph['mol_ion_nodes_per_spectrum'][i_spec].append(G.nodes[node_id]['node_lite'].unique_id)
                logger.debug(f"mol ion node found already on graph for: {i_spec=} {node_iso.frag_abund_formula}")
                #continue
            else:
                logger.debug(f"max node not matched : {i_spec=} {node_iso.frag_abund_formula}")

                #warning! function successors works like an iterator
                #https://stackoverflow.com/questions/47161158/networkx-neighbor-set-not-printing
                for node_child in list(G.successors(node_id)):
                    if G.nodes[node_child]['node_lite'].is_supfragment(frag): #G.nodes[node_child]['node_lite'].iso.belong_to_spectra[i_spec] and
                        children_list.append(node_child) #add children 
                        logger.info("New child node: " + G.nodes[node_child]['node_lite'].iso.frag_abund_formula)
    
        ancestors_list = children_list.copy()
        children_list = []
                
    return node_child_found

def add_node_and_edges_to_graph_from_not_max_fragment(
        G: nx.DiGraph,
        node_parent_idx: int,
        frag: chem_data.FragmentAsAtomCounts,
        i_spec: int
    ):
    """
    Add a node generated at step 10
    which is not a maximal fragment
    and which is not on the graph.
    The node does not need to be a measured mass of the spectrum i_spec.
    """
    logger = logging.getLogger('alpinac.utils_graph.add_node_and_edges_to_graph_from_not_max_fragment')

    logger.info("Add node and search for parents and children for " + chem_data.get_string_formula(frag))    

        
    #initialisation: set all nodes to unvisited.
    #reset_unvisited(G) #not sure if this is doing the job for the entire graph
    for node_id in G.nodes:
        G.nodes[node_id]['node_lite'].visited = False
    
    parent_node: NodeLite = G.nodes[node_parent_idx]['node_lite']
    #Warning! if the frag contains an added atom, the parent node is a subfragment!
    if parent_node.is_strict_subfragment(frag):
        ancestors_list = G.graph['maximal_nodes']
    else:
        ancestors_list = [node_parent_idx] #ancestors of nde to add

    #Now add the node
    node = NodeLite(fragment = frag, visited = False, idx_measured_mass=None)

    if not node.is_strict_subfragment(parent_node.fragment):
        # The node is not a full direct descendant of the ancestor node
        # by adding the other maximal nodes we will check for other potential descendants
        ancestors_list.extend(G.graph['maximal_nodes'])

    logger.debug(f"Ancestors list: {ancestors_list} " )
    logger.debug(f"Created node: {node}")
    # set visited to False do avoid the step reset_unvisited
    G.add_node(node.unique_id, node_lite = node)
    if G.graph['mol_ion_nodes_per_spectrum'][i_spec] is None:
        G.graph['mol_ion_nodes_per_spectrum'][i_spec] = [node.unique_id]
    else:
        G.graph['mol_ion_nodes_per_spectrum'][i_spec].append(node.unique_id)


    new_ancestors_list = [] #next list of ancestors to check
    children_ok_list = [] #list of nodes that are children (subfragments) of node to add    
    ancestors_ok_list = []
    
    
    while len(ancestors_list) > 0:
        for ancestor_node_id in ancestors_list:
            ancestor_node: NodeLite = G.nodes[ancestor_node_id]['node_lite']
            #set node to visited
            if ancestor_node.visited:
                continue
            ancestor_node.visited = True
            
            #Warning The fragment formula is a construction so it may not be 
            #associated to a measured mass of the spectra but that's ok.
            node_children_list = list(G.successors(ancestor_node_id)).copy()
            logger.debug(f"{ancestor_node} children list: {node_children_list}")
                
            #if not ancestor_node.is_strict_supfragment(frag): 
            #    # Ancesstor is not a strict supfragment of the node to add
            #    #continue  
            #this is an ancestor, we don't know yet if it is a direct ancestor
            #check its children
            #warning! function successors works like an iterator
            #https://stackoverflow.com/questions/47161158/networkx-neighbor-set-not-printing
            for node_child_id in node_children_list:
                child_node: NodeLite = G.nodes[node_child_id]['node_lite']
                logger.debug(f"Checking to connect child node: {child_node}")
                if child_node.is_strict_supfragment(frag): #G.nodes[node_child]['node_lite'].iso.belong_to_spectra[i_spec] and
                    new_ancestors_list.append(node_child_id) #add children 
                    if child_node.iso:
                        logger.info("New ancestor to check: " + child_node.iso.frag_abund_formula)
                elif child_node.is_strict_subfragment(frag):
                    if node_child_id not in children_ok_list:
                        # Add the child node to the graph
                        children_ok_list.append(node_child_id)
                        child_node.visited = True
                        G.add_edge(node.unique_id, child_node.unique_id)
                        if child_node.iso:
                            logger.info("Direct child node: " + child_node.iso.frag_abund_formula)
                    if ancestor_node.is_strict_supfragment(frag):
                        # If the parent of the child is also one, add the parent.
                        if ancestor_node_id not in ancestors_ok_list:
                            ancestors_ok_list.append(ancestor_node_id)
                            G.add_edge(ancestor_node.unique_id, node.unique_id)
                            if ancestor_node.iso:
                                logger.info("Direct parent node: " + ancestor_node.iso.frag_abund_formula)                       
                        # remove the orignal bound, as this frag is now between parent and child
                        G.remove_edge(ancestor_node.unique_id, child_node.unique_id)
        ancestors_list = new_ancestors_list.copy()
        new_ancestors_list = []
       
    return None   
 
       
def update_graph_data_after_removing_nodes(G: nx.DiGraph, removed_nodes: list) -> None:
    """Remove the node numbers from nodes_validated and added_nodes """
    logger = logging.getLogger('alpinac.utils_graph.update_graph_data_after_removing_nodes')
    if 'nodes_validated' in G.graph:
        for s in removed_nodes:
            if s in G.graph['nodes_validated']:
                G.graph['nodes_validated'].remove(s)
    if 'added_nodes' in G.graph:
        for s in removed_nodes:
            if s in G.graph['added_nodes']:
                G.graph['added_nodes'].remove(s)

def remove_all_singletons(G: nx.DiGraph):
    """remove all maximal nodes that have no child
    """
    max_nodes = [G.nodes[n]['node_lite'] for n in G.graph['maximal_nodes']]
    not_singletons = [node for node in max_nodes if len(G.succ[node.unique_id]) > 0]
    singletons = [node for node in max_nodes if len(G.succ[node.unique_id]) == 0]
    G.graph['maximal_nodes'] = [node_i.unique_id for node_i in not_singletons]
    if len(G.graph['candidates_per_measured_mass']) > 0:
        for s in singletons:
            if s.idx_measured_mass is not None:
                for i_m in s.idx_measured_mass:
                    G.graph['candidates_per_measured_mass'][i_m] -= 1
    singleton_numbers = [s.unique_id for s in singletons]
    G.remove_nodes_from(singleton_numbers)
    if 'no_nodes_singletons' in G.graph:
        G.graph['no_nodes_singletons'] += len(singleton_numbers)
    update_graph_data_after_removing_nodes(G, singleton_numbers)
    return singletons

def remove_singletons(G: nx.DiGraph):
    """remove maximal nodes that have no child

    Morever, check that all the maximal nodes of one mass index are not
    all the nodes of that mass index. If a mass index has only singleton
    nodes, do not remove them (otherwise there is no candidate anymore
    for that mass index).

    """
    logger = logging.getLogger('alpinac.utils_graph.remove_singletons')
    logger.debug(f"{list(G.nodes.keys())=}")
    logger.debug(f"{G.graph['maximal_nodes']=}")
    max_nodes = [G.nodes[n]['node_lite'] for n in G.graph['maximal_nodes']]
    not_singletons = [node for node in max_nodes if len(G.succ[node.unique_id]) > 0]
    singletons = [node for node in max_nodes if len(G.succ[node.unique_id]) == 0]
    singleton_numbers = [s.unique_id for s in singletons]
    # if idx_measured_mass is not provided, remove all singletons
    if len(G.graph['candidates_per_measured_mass']) == 0:
        G.remove_nodes_from(singleton_numbers)
        G.graph['maximal_nodes'] = [node_i.unique_id for node_i in not_singletons]
        update_graph_data_after_removing_nodes(G, singleton_numbers)
        return singletons
    # now check that a set of singletons is not all for a mass
    # that is, do not remove singletons of a mass which has only singletons
    # make batches of singletons according to their idx_measured_mass
    singletons_dict: dict[float, list[NodeLite]] = {}
    removed_singletons = []
    G.graph['maximal_nodes'] = [node_i.unique_id for node_i in not_singletons] # and add the singletons that are not removed
    for s in singletons:
        if s.idx_measured_mass is None:
            G.remove_node(s.unique_id)
            removed_singletons.append(s)
        else:
            for i_m in s.idx_measured_mass:
                if i_m in singletons_dict:
                    singletons_dict[i_m].append(s)
                else:
                    singletons_dict[i_m] = [s]
    # check that removing the singletons will not remove all the candidates for a mass
    logger.debug(f"{singletons_dict=}")
    for i_m in singletons_dict:
        other_frag_available_this_mass  = len(singletons_dict[i_m]) < G.graph['candidates_per_measured_mass'][i_m]
        if not other_frag_available_this_mass:
            # re-add the nodes of singletons_dict[i_m] to the list of maximal elements
            for s in singletons_dict[i_m]:
                logger.debug(f"re-add {s.unique_id} to maximal nodes")
                if s.unique_id not in G.graph['maximal_nodes']:
                    G.graph['maximal_nodes'].append(s.unique_id)
            continue # To the next mass 

        # otherwise remove the singletons
        for s in singletons_dict[i_m]:
            if len(s.idx_measured_mass) == 1: # no twin node
                G.remove_node(s.unique_id)
                removed_singletons.append(s)
            else:
                # in case there are 'twin' nodes: another mass could be sharing that singleton
                # Remove only this mass
                # In case it should be removed, it will be removed in the next iteration
                s.idx_measured_mass.remove(i_m)
                s.decrement_number_duplicates()
            G.graph['candidates_per_measured_mass'][i_m] -= 1
    singleton_numbers = [s.unique_id for s in removed_singletons]
    if 'no_nodes_singletons' in G.graph:
        G.graph['no_nodes_singletons'] += len(singleton_numbers)
    update_graph_data_after_removing_nodes(G, singleton_numbers)
    return removed_singletons

def update_parents_after_removed_node(G: nx.DiGraph, parent_numbers: list, removed_iso_list_sum_signal: float, cl_comp: Compound) -> None:
    """Ipdate data of parents recursively
    """
    for parent_number in parent_numbers:
        parent = G.nodes[parent_number]['node_lite']
        if parent.visited:
            continue
        parent.visited = True
        # update size of subgraph
        parent.subgraph_size -= 1
        parent.subgraph_sum_I -= removed_iso_list_sum_signal
        parent.subgraph_percent_I = float(100.0)*parent.subgraph_sum_I/cl_comp.sum_I
        parent.subgraph_likelihood = (float(100.0) - abs(float(100.0)-parent.subgraph_percent_I))* parent.subgraph_size / parent.no_all_subfragments
        parents = G.predecessors(parent_number)
        update_parents_after_removed_node(G, parents, removed_iso_list_sum_signal, cl_comp)

def reset_parents_unvisited(G: nx.DiGraph, parent_numbers: list) -> None:
    """set NodeLite.visited to False recursively upwards"""
    for parent_number in parent_numbers:
        parent = G.nodes[parent_number]['node_lite']
        if parent.visited:
            parent.visited = False
            parents = G.predecessors(parent_number)
            reset_parents_unvisited(G, parents)

def remove_node_update_edges(G: nx.DiGraph, node: NodeLite, cl_comp: Compound, update_data: bool=False) -> None:
    """Remove one node and update graph data

    INPUT:
    - `G`: directed acyclic graph
    - `node`: the node to be removed
    - `cl_comp`: Compound data (to update the likelihood of the parent nodes)

    OUTPUT: None (the graph itself is updated in place)

    This function updates the edges before removing a node in the graph.
    A node encodes a fragment, it has parent nodes encoding sup-fragments, and child nodes encoding sub-fragments.
    When removing a node, its childen (the sub-fragments) should be reconnected to the parents only in certain cases.

    1. remove all edges (p, n) and (n, c) where p are parents and c are children
    2. for each child, its parents cannot be grand-parents of n (the removed node) by construction of G
       set edges between parents of n and children of n in appropriate cases
    """
    # 1. lists the parents of node
    parents = list(G.predecessors(node.unique_id))
    if len(parents) == 0:
        G.graph['maximal_nodes'].remove(node.unique_id)
    children = list(G.neighbors(node.unique_id))
    for parent_number in parents:
        G.remove_edge(parent_number, node.unique_id)
    if update_data:
        update_parents_after_removed_node(G, parents, node.iso.iso_list_I_sum, cl_comp)
        reset_parents_unvisited(G, parents)
    #else:
    #    for parent_number in parents:
    #        G.nodes[parent_number]['node_lite'].subgraph_size -= 1

    for child_number in children:
        G.remove_edge(node.unique_id, child_number)
        # compare the child's parents to the node's parents (some of the child's grand-parents)
        # if one of the **other** parents of the child is a sub-fragment of one of the node's parents,
        # nothing to so.
        # otherwise, add the child as a child of the considered node's parent
        childs_parents_number = list(G.predecessors(child_number))
        # compare childs_parent to all nodes'parents in parents
        for parent_number in parents:
            is_subfragment = False
            for childs_parent_number in childs_parents_number:
                is_subfragment = is_subfragment or G.nodes[childs_parent_number]['node_lite'].is_subfragment(G.nodes[parent_number]['node_lite'].fragment)
            if not is_subfragment:
                G.add_edge(parent_number, child_number)

        if len(list(G.predecessors(child_number))) == 0:
            G.graph['maximal_nodes'].append(child_number)

    G.remove_node(node.unique_id)

def print_maximal_nodes(G: nx.DiGraph) -> None:
    logging.info("maximal fragments are {}".format(" ".join([G.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in G.graph['maximal_nodes']])))

def check_maximal_nodes(G: nx.DiGraph) -> bool:
    """Check that G.graph['maximal_nodes'] is made of exactly all the maximal nodes"""
    effective_maximal_nodes = [node_number for node_number in G.nodes if len(list(G.predecessors(node_number))) == 0]
    missing_max_node = [node_i for node_i in effective_maximal_nodes if node_i not in G.graph['maximal_nodes']]
    false_max_node = [node_i for node_i in G.graph['maximal_nodes'] if node_i not in effective_maximal_nodes]
    if len(false_max_node) > 0:
        logging.info("Error maximal nodes: {} ({}) in G.graph['maximal_nodes'] are not in effective_maximal_nodes".format(false_max_node, " ".join([G.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in false_max_node])))
    if len(missing_max_node) > 0:
        logging.info("Error maximal nodes: {} ({}) in effective_maximal_nodes are not in G.graph['maximal_nodes']".format(missing_max_node, " ".join([G.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in missing_max_node])))
    #return len(missing_max_node) == 0 and len(false_max_node) == 0
    if not (len(missing_max_node) == 0 and len(false_max_node) == 0):
        raise ValueError("Error maximal nodes: missing_max_node = {} ({}) false_max_node = {} ({})".format(missing_max_node, " ".join([G.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in missing_max_node]), false_max_node, " ".join([G.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in false_max_node])))

def compute_all_subfragments_of_nodes(
    G: nx.DiGraph,
    min_measured_mass:float,
    start_node: NodeLite=None,
    check: bool=False
) -> None:
    """Computes the number of theoretical sub-fragments of each node
    iteratively with the knapsack algorithm

    This is the same as compute_all_subfragments_of_nodes_with_lists 
    but less efficient 

    INPUT:
    - `G` a directed acyclic graph whose maximal nodes (numbers) are
    given in G.graph['maximal_nodes']
    - `min_measured_mass`: the minimal mass measured by the machine
    (such as 23.0 m/z)
    - `start_node`: start at this node and recursively inspect sub-nodes
    - `check`: if True, set field mynodelite.no_all_subfragments_check,
    otherwise, set mynodelite.no_all_subfragments (should be the default)

    OUTPUT: None, the information is stored in
    G.nodes[node_number]['node_lite'].no_all_subfragments<_check>
    """
    if start_node is None:
        for max_node_id in G.graph['maximal_nodes']:
            max_node = G.nodes[max_node_id]['node_lite']
            compute_all_subfragments_of_nodes(G, min_measured_mass, max_node, check=check)
        reset_unvisited(G)
        return
    if start_node.visited:
        return
    no_all_subfragments, list_subfragments = enumerate_all_valid_subfragments(start_node.fragment, min_mass=min_measured_mass)
    #logging.info(str([chem_data.get_string_formula(list_subfragments[i]) for i in range(len(list_subfragments))]))
    if check:
        start_node.no_all_subfragments_check = no_all_subfragments
    else:
        start_node.no_all_subfragments = no_all_subfragments
    # recursive call
    start_node.visited = True
    for child_id in G.successors(start_node.unique_id):
        child = G.nodes[child_id]['node_lite']
        compute_all_subfragments_of_nodes(G, min_measured_mass, child, check=check)

def compute_all_subfragments_of_nodes_with_lists(
    G: nx.DiGraph,
    min_measured_mass:float,
    start_node: NodeLite=None,
    parent_node: NodeLite=None,
    multi_valent: list=None,
    mono_valent: dict=None,
) -> None:
    """Computes the number of theoretical sub-fragments of each node
    recursively with the knapsack algorithm

    INPUT:
    - `G` a directed acyclic graph whose maximal nodes (numbers) are
    given in G.graph['maximal_nodes']
    - `min_measured_mass`: the minimal mass measured by the machine
    (such as 23.0 m/z)
    - `start_node`: start at this node and recursively inspect sub-nodes
    - `parent_node`: the parent of `start_node`
    - `multi_valent`: list of partial fragments of the parent node made
    of multi-valent atoms only, None if start_node is None or maximal
    - `mono_valent`: list of partial fragments of the parent node made
    of mono-valent atoms only, None if start_node is None or maximal

    OUTPUT: None, the information is stored in
    G.nodes[node_number]['node_lite'].no_all_subfragments

    Recursive call, with a Depth-first-search algorithm.
    If start_node is None, start with all the maximal elements.
    Set to visited the nodes that have been processed.
    To improve efficiency, try to share some parts of knapsack
    enumeration. With inputs max_mass = fragment.mass and mass_min = 0.0
    the knapsack outputs all the subfragments. It computed internally a
    list of subfragments made of multi-valent atoms, and a list of sets
    of at most v mono-valent atoms, where v is the highest valence of
    the fragments in the multi-valent list.
    Sort by increasing order of mass both lists, and cut each list at
    the mass of the fragment made of all the multi-valent atoms of
    start_node, then refine the list by discarding all fragments that
    are not subfagments of multi-valent(start_node.fragment).
    Do the same for the list of mono-valents.
    In this way, the re-run of the knapsack is replaced by list
    filtering. It could be faster. Need to be checked.
    """
    logger = logging.getLogger("alpinac.utils_graph.compute_all_subfragments_of_nodes_with_lists")
    if start_node is None:
        for max_node_id in G.graph['maximal_nodes']:
            max_node = G.nodes[max_node_id]['node_lite']
            logger.debug(f"processing maximal node {max_node}")
            compute_all_subfragments_of_nodes_with_lists(G, min_measured_mass, max_node)
        reset_unvisited(G)
        return
    if start_node.visited:
        return
    logger.debug(f"processing node {start_node}")
    if multi_valent is None or mono_valent is None:
        # we are at a maximal element. Call knapsack.
        no_all_subfragments, list_subfragments, multi_valent, mono_valent = enumerate_all_valid_subfragments(start_node.fragment, min_mass=min_measured_mass, return_partial_lists=True)
    else: # filter the partial lists and compute the number of subfragments of start_node
        no_all_subfragments, list_subfragments, multi_valent, mono_valent = enumerate_all_valid_subfragments_from_lists(start_node.fragment, multi_valent, mono_valent, parent_node.fragment, min_mass=min_measured_mass)
    start_node.no_all_subfragments = no_all_subfragments
    logger.debug(f"processing node {start_node}, {no_all_subfragments} subfragments")
    # recursive call
    start_node.visited = True
    for child_id in G.successors(start_node.unique_id):
        child = G.nodes[child_id]['node_lite']
        if not child.visited:
            compute_all_subfragments_of_nodes_with_lists(G, min_measured_mass, child, start_node, multi_valent, mono_valent)

def node_edge_labels_colors_for_pydot(G: nx.DiGraph):
    node_labels = {}
    node_colors = [0]*G.number_of_nodes()
    idx= 0
    for key_node in G.nodes():
        node_labels[key_node] = chem_data.get_string_formula(G.nodes[key_node]['node_lite'].fragment)
        node_colors[idx] = chem_data.get_mass_single_ionised(G.nodes[key_node]['node_lite'].fragment)
        idx+=1
    edge_labels = {}
    for key_edge in G.edges():
        (u,v) = key_edge
        edge_labels[key_edge] = chem_data.get_string_formula(G.nodes[u]['node_lite'].lost_fragment(G.nodes[v]['node_lite'].fragment))
    return node_labels, node_colors, edge_labels

def set_str_formula_and_attribute_for_pydot(G: nx.DiGraph, start_node: NodeLite=None) -> None:
    if start_node is None:
        for max_node_id in G.graph['maximal_nodes']:
            set_str_formula_and_attribute_for_pydot(G, G.nodes[max_node_id]['node_lite'])
        reset_unvisited(G)
    else:
        if not start_node.visited:
            G.nodes[start_node.unique_id]['str_formula'] = chem_data.get_string_formula(start_node.fragment)
            G.nodes[start_node.unique_id]['attribute'] = chem_data.get_mass_single_ionised(start_node.fragment)
            start_node.visited = True
            for child_id in G.successors(start_node.unique_id):
                child = G.nodes[child_id]['node_lite']
                G.edges[(start_node.unique_id, child.unique_id)]['label'] = chem_data.get_string_formula(start_node.lost_fragment(child.fragment))
                set_str_formula_and_attribute_for_pydot(G, child)

def initialise_isotopologue_set(G: nx.DiGraph, cl_comp, add_mol_ion: bool=False, mol_ion_nodes: list = None) -> None:
    """Initialise the Isotopologue class of each node.

    Do it with a depth-first-search graph traversal so that it will be
    possible in an improved version to avoid non-promising nodes.
    TODO, Work in progress
    """
    
    list_nodes_to_initialise = []
    if add_mol_ion == False:
        list_nodes_to_initialise = G.nodes()
    else:
        list_nodes_to_initialise = mol_ion_nodes

    for node_id in list_nodes_to_initialise:
        node = G.nodes[node_id]['node_lite']
        logging.debug("initialise_isotopologue_set: in graph, node idx meas mass " + str(node.idx_measured_mass))
        node.iso = Isotopologues(node.fragment, node.idx_measured_mass, cl_comp)
        node.iso.create_isotopologue_suite()





def graph_optimise_isotopologues(
    G: nx.DiGraph, 
    cl_comp: Compound, 
    ppm_mass_res: float, 
    plot_mode,
    fitting_method: FittingMethod,
):
    """
    Optimise the contribution of each candiate node to the measured signal.
    First make list of non-overlapping mass sub-windows 
    in the domain of candidate nodes.
    Then for each list, make measured itensity profile
    (generate intensity values at a chosen mass resolution)
    
    MYGU 2022.02.28: this function will be affected
    by fitting several measured mas profiles 
    while keeping a unique graph of candidate nodes.
    
    Need new attribute to each node: index refering to which measured profile?
    Each node need list of index of meas profile for each node
    
    Then loop over index of meas mass
    for each index, select all nodes which corespond to
    and then make list of non-overlapping masses

    Parameters
    ----------
    G : nx.DiGraph
        DESCRIPTION.
    cl_comp : Compound
        DESCRIPTION.
    ppm_mass_res : float
        DESCRIPTION.
    plot_mode : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    
    #mygu 20220503 
    #replaced k_guesses by just k_guesses at it is not used anywhere else
    logger = logging.getLogger("alpinac.graph_optimise_isotopologues")
    logger.info("Entering graph_optimise_isotopologues")

    list_removed_nodes = []


    
    #CONSIDER LIST OF CANDIDATE SOLUTIONS FIRST
    #Depending on candidate isotopologues, split mass window into smaller,
    #no-overlapping mass sub-windows
    #if there is only one node, make only one mass window (no sub-windows)
    #These lists of mass windows are done considering the list of candiate solutions,
    #not considering the list of (various spectra of) measured masses.
    #So there are valid for all considered measured spectra.

    #mygu 20220503 17:05 maybe it would make sense to branch here for i_spec?
    #take only candidates that were generated for the selected spectra?
    #otherwise we also get non-relevant ones
    #TODO

    groups_of_nodes = []  #list of node_key 
    groups_of_nodes_i_spec = [] #for each item in groups_of_nodes, index of spectrum to which all nodes relate
    
    
    for i_spec in range(cl_comp.no_spectra): 

        
        #list_nodes_masses = [(G.nodes[i_node]['node_lite'].iso.frag_abund_mass, i_node) for i_node in G.graph['nodes_validated']]

        #Find all candidate nodes having at least one match with a
        #measured mass of the spectrum i_spec
        list_nodes_masses = []
        for i_node in G.graph['nodes_validated']:            
            if G.nodes[i_node]['node_lite'].idx_measured_mass is not None:
                found = False
                i_m = 0
                while i_m < len(G.nodes[i_node]['node_lite'].idx_measured_mass) and not found:
                    if cl_comp.meas_mass_spec_id[G.nodes[i_node]['node_lite'].idx_measured_mass[i_m]] == i_spec:
                        found = True
                        list_nodes_masses.append((G.nodes[i_node]['node_lite'].iso.frag_abund_mass, i_node))
                    i_m += 1

        #Order list of nodes by increasing mass
        list_nodes_masses.sort()
        list_nodes = [node_number for (mass, node_number) in list_nodes_masses]

        #+++++++++++++++++++++++++++++++++++++++++++++++
        #CREATE GROUPS OF NODES FOR WHICH THE CORRESPONDING MASS WINDOW
        #DO NOT OVERLAP WITH THE MASS WINDOW
        #FROM ANOTHER GROUP OF NODES
        #+++++++++++++++++++++++++++++++++++++++++++++++
        i = 0
        while i < len(list_nodes):
            iso = G.nodes[list_nodes[i]]['node_lite'].iso
            w_start = round(iso.iso_list_m_min) - cl_comp.mass_axis_offset
            w_stop  = round(iso.iso_list_m_max) + cl_comp.mass_axis_offset
            #print('w_start:' + '{:.4f}'.format(w_start), 'i_start: ' + str(i))
            #print('w_stop:' + '{:.4f}'.format(w_stop), 'i_stop: ' + str(i))
    
            # find overlapping nodes whose exact minimum mass is lower than w_stop: masses overlap
            j = i
            while (j+1) < len(list_nodes) \
                and G.nodes[list_nodes[j+1]]['node_lite'].iso.iso_list_m_min < w_stop:
                j += 1
                w_stop = max(w_stop, G.nodes[list_nodes[j]]['node_lite'].iso.iso_list_m_max)
                #print('w_stop:' + '{:.4f}'.format(w_stop), 'i_stop: ' + str(j))
            # now there is a range of masses for the nodes i to j included (j+1 excluded)
            groups_of_nodes.append(list_nodes[i:j+1])
            groups_of_nodes_i_spec.append(i_spec)
            i = j+1

    groups_of_nodes_is_already_fitted = [False]*len(groups_of_nodes) #boolean

    for i_w_m in range(len(groups_of_nodes)):

        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # IS THE LIST OF NODES ALREADY FITTED? IF YES, DO NOT FIT AGAIN
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        #list of list of nodes fitted together : groups_of_nodes[i_w_m]
        #Here check if the list of nodes to be fitted together
        #has been fitted already.
        #so we use list as attribute to the graph,
        #containing the list of list of nodes fitted together,
        #this is groups_of_nodes

        i_list = 0
        while i_list <len(G.graph['list_of_fitted_nodes']) and not groups_of_nodes_is_already_fitted[i_w_m]:
            if groups_of_nodes[i_w_m] == G.graph['list_of_fitted_nodes'][i_list]:
                groups_of_nodes_is_already_fitted[i_w_m] = True
                #print("lmfit job already done with this group of nodes, continue")
            i_list += 1
        
        
        if not groups_of_nodes_is_already_fitted[i_w_m]: #len(groups_of_nodes[i_w_m]) > 0 and

            i_spec = groups_of_nodes_i_spec[i_w_m]            
            #One window is made of nodes all from the same spectra.
            
            #there is one k_opt for each node; one node is a list of isotopolgues
            k_opt_list = [float(0.0)] * len(groups_of_nodes[i_w_m])      
            delta_mass_opt_list = float(0.0)
            

            #re-initialise things here?
            cl_comp.initialise_mass_profile()
            #reconstruct measured mass profile:

            w_start = round(min([G.nodes[groups_of_nodes[i_w_m][i_m]]['node_lite'].iso.iso_list_m_min for i_m in range(len(groups_of_nodes[i_w_m]))]))
            w_stop =  round(max([G.nodes[groups_of_nodes[i_w_m][i_m]]['node_lite'].iso.iso_list_m_max for i_m in range(len(groups_of_nodes[i_w_m]))]))

            #print('Mass window: ' + str([w_start, w_stop]))
            
            #For each window, optimise quantity of each isotopologue
            #treat this as optimisation routine, using lmfit
            #if all iso masses have apex below LOD, remove isotopologue from list of candidate
            #(or set factor to zero and fixed)
            
            #create x axis of the mass profile
            cl_comp.do_meas_mass_profile_x(w_start, w_stop, ppm_mass_res)
            delta_mass = 0.0
            
            #from chosen spectrum, find all matching measured masses 
            #within this mass domain and
            #calculate measured mass profile at each x value on the x axis (mass axis)
            cl_comp.do_meas_mass_profile(delta_mass, i_spec)
            
            #print('Indexes of measured masses: ' + str(cl_comp.meas_mass_profile_idx))
        
            if len(cl_comp.meas_mass_profile_idx) > 0 :
                #optimise only if at least one matching measured mass found.
                
                k_guesses = [G.nodes[i_node]['node_lite'].iso.k_guess[i_spec] for i_node in groups_of_nodes[i_w_m]]
                
                #Handle potential cases where there are more k_guess values to optimise
                #than the number of measured masses
                #(this would be an undetermined problem)
                indexes = [i for i in range(len(groups_of_nodes[i_w_m]))]
                no_candidates = len(indexes)
                min_k = min(k_guesses)
                while no_candidates > 0 and len(cl_comp.meas_mass_profile_x) < no_candidates:
                    #too many possibilities.
                    #Eliminate less likely possibilities.
                    
                    indexes_valid = [i for i in range(len(k_guesses)) if k_guesses[i] > min_k]
                    no_candidates = len(indexes_valid)
                    if no_candidates > 0:
                        min_k = min([k_guesses[i] for i in indexes_valid])
                
                if no_candidates < len(groups_of_nodes[i_w_m]):
                    #elements have been removed
                    if groups_of_nodes_is_already_fitted[i_w_m]:
                        #it's almost impossible since there were too many possibilities
                        groups_of_nodes_is_already_fitted[i_w_m] = False
                        G.graph['list_of_fitted_nodes'].remove(groups_of_nodes[i_w_m])
                    groups_of_nodes[i_w_m] = [groups_of_nodes[i_w_m][i] for i in indexes_valid]
                    k_guesses = [k_guesses[i] for i in indexes_valid]
                    

                #Generate mass profile for each candidate set of isotopologues.
                #iso_list_idx: list of indices of chemical formuale assigned to the node, isotopologues_list: list of isotopologues of corresponding formula
                prepare_fitting(
                    cl_comp,
                    [i for i in range(len(groups_of_nodes[i_w_m]))],
                    [G.nodes[i_node]['node_lite'].iso for i_node in groups_of_nodes[i_w_m]],
                    fitting_method = fitting_method
                )
                
                #check that number of data is higher than number of candidate isotopologue series
                if len(cl_comp.meas_mass_profile_idx) > 0 and len(cl_comp.meas_mass_profile_x) >= len(groups_of_nodes[i_w_m]):                    
                  #k_guesses_max = max([cl_comp.meas_I[i_m] for i_m in cl_comp.meas_mass_profile_idx])
                                        
                    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    #PLUG IN HERE ALTERNATIVE METHOD TO DETERMINE K_OPT_LIST
                    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    #MYGU 20220520 but I don't think it is that simple
                    
                    #output needed: 
                        #k_opt_list, list containing optimised value of k 
                        #for each candidate isotopologue set in the group
                        
                    #inputs for optimisation:
                        #k_guesses: guess values for all k values
                        #cl_comp.iso_profiles: np array containing each candidate isotopologue profile
                    
                        #the input measured mass profile will be updated just before the fit using a method of the compound class
                        #it is right now in cl_comp.meas_mass_profile (np array)

                    # TODO testing
                    if fitting_method == FittingMethod.continuous:
                        # TODO put everything in a function!! repeated a second time below
                        if cl_comp.unit_mass_resolution:
                            iso_fit_mode = FitModes.fixed_x_axis
                        else:
                            iso_fit_mode = FitModes.variable_x_axis
                        #addendum after NF3 test
                        #if only one measured mass but several candidates, fix x_axis otherwise will move without proper constrain
                        if len(cl_comp.meas_mass_profile_idx) == 1 and len(groups_of_nodes[i_w_m]) > 1:
                            iso_fit_mode = FitModes.fixed_x_axis
                        
                        k_guesses_min = min(min([cl_comp.meas_I[i_m] for i_m in cl_comp.meas_mass_profile_idx]), min([cl_comp.meas_LOD[i_m] for i_m in cl_comp.meas_mass_profile_idx]), cl_comp.LOD)/1000.0
  
                        k_opt_list, delta_mass_opt_list = fit_iso_profile(
                            cl_comp, 
                            delta_mass, 
                            k_guesses,
                            k_guesses_min,
                            i_spec,
                            cl_comp.iso_profiles, 
                            iso_fit_mode, 
                            graph = False)
                    elif fitting_method == FittingMethod.discrete:
                        alpinac.requires_sup(f"Only 'fitting_method' {FittingMethod.continuous} is available without the sups.") 
                        multiples_of_uncertainties_for_tolerance_mass_calib = 1
                        multiples_of_uncertainties_for_tolerance_fit = 2.55
                        from alpinac_sups import discrete_fitting_optimization
                        # add isotopic profile to cl comp
                        k_opt_list, delta_mass_opt_list = discrete_fitting_optimization.fit_iso_stick_spectra(
                            cl_comp,
                            iso_idx_list_k_guesses = k_guesses,
                            i_spec = i_spec,  
                            graph = False,
                            multiples_of_uncertainties_for_tolerance_mass_calib = multiples_of_uncertainties_for_tolerance_mass_calib,
                            multiples_of_uncertainties_for_tolerance_fit = multiples_of_uncertainties_for_tolerance_fit,
                        )
                    else:
                        raise ValueError(f" Fitting method {fitting_method} is not a valid method. Please select valid method in input.")
   
                else:
                    logger.warn('Not enough measured points to constrain fit')
                  

            for i_node in range(len(groups_of_nodes[i_w_m])):
                key_node = groups_of_nodes[i_w_m][i_node]
                iso = G.nodes[key_node]['node_lite'].iso
                iso.k_opt[i_spec] = k_opt_list[i_node]
                iso.k_guess[i_spec] = k_opt_list[i_node]
                iso.iso_list_I_opt[i_spec] = [min(iso.iso_list_I_rel[i_iso] * iso.k_opt[i_spec], iso.iso_list_I_meas_max[i_spec][i_iso]) for i_iso in range(iso.iso_list_len)]


    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #after optimisation over all measured spectra:
    #update graph, remove node if signal < threshold

    for key_node in G.graph['nodes_validated']:
        iso = G.nodes[key_node]['node_lite'].iso
        iso.iso_list_I_sum = sum(sum(val) for val in iso.iso_list_I_opt)
        
        if iso.iso_list_I_sum * 3.0 < cl_comp.LOD:
            logger.info("** node below LOD removed: {}".format(iso.frag_abund_formula))
            remove_node_update_edges(G, G.nodes[key_node]['node_lite'], cl_comp)
            G.graph['nodes_validated'].remove(key_node)
            list_removed_nodes.append(key_node)
            G.graph['no_nodes_below_LOD'] += 1


    list_removed_nodes_singletons = [sgl.unique_id for sgl in remove_singletons(G)]
    #list_removed_nodes_singletons = [sgl.unique_id for sgl in remove_all_singletons(G)]
    list_removed_nodes += list_removed_nodes_singletons


    #Add all groups of nodes fitted without removed nodes
    #to list of grouped fitted nodes
    for i_w_m in range(len(groups_of_nodes)):
        
        contain_removed_nodes = False
        i_node = 0
        while i_node < len(groups_of_nodes[i_w_m]) and not contain_removed_nodes:
            if groups_of_nodes[i_w_m][i_node] in list_removed_nodes:
                contain_removed_nodes = True
            i_node += 1
                
        if contain_removed_nodes and groups_of_nodes_is_already_fitted[i_w_m]:
            G.graph['list_of_fitted_nodes'].remove(groups_of_nodes[i_w_m])
            
        elif not contain_removed_nodes and not groups_of_nodes_is_already_fitted[i_w_m]:
            G.graph['list_of_fitted_nodes'].append(groups_of_nodes[i_w_m])
        #does the group contain a removed node?
        #is the group saved as already fitted?
        
        #if removed node + was already fitted: remove from list of fitted groups
        
        #if removed node + was not already fitted: do nothing
        
        #if no removed node + was already fitted: do nothing
        
        #if no removed node + was not already fitted: add to list of fitted groups
        


def graph_order_by_att_likelihood(G: nx.DiGraph, cl_comp):
    """
    Computes the intensity of signal

    Parameters
    ----------
    G : networkx DiGraph. Each node is a fragment potential solution, is an instance of class isotopologue.
        DESCRIPTION.
    cl_comp : instance of class compound. Contains measured informations.
        DESCRIPTION.

    Returns
    -------
    None

    
    In particular:
        G.nodes_ordered_by_likelihood is a list of key of nodes, ordered by decreasing likelihood.

    """
    logger = logging.getLogger('alpinac.utils_graph.graph_order_by_att_likelihood')


    for key_node in G.nodes:
        node = G.nodes[key_node]['node_lite']
        isotopologue = G.nodes[key_node]['node_lite'].iso
        logger.debug(f"node {key_node} {node=} {isotopologue=}")

        set_subfragments = nx.descendants(G, key_node)

        #sum of optimised signal that can be assigned to this node
        #and all its subfragments
        node.subgraph_sum_I = isotopologue.iso_list_I_sum + sum([G.nodes[key_node_anc]['node_lite'].iso.iso_list_I_sum for key_node_anc in set_subfragments])

        #logging.info(str(G.nodes[key_node]['node_lite'].iso.frag_abund_formula) + "  " + str(node.subgraph_sum_I))
        node.subgraph_percent_I = float(100.0)*node.subgraph_sum_I/cl_comp.sum_I

        #Is this node created by the knapsack (in this case it has a found measured mass)
        #or is it a constructed molecular ion (in this case it has no measured mass)?
        
        if isotopologue.frag_abund_idx is None:
            node_found_in_meas_data = int(0)
        else:
            node_found_in_meas_data = int(1)

        #20200805 modification of metrics
        #number of explained nodes is (1+ len(set_subfragments))
        #we want to give more likelihood to the maximal node being closest to 100% signal, without overshoot
        logger.debug(f"{node.subgraph_percent_I =}")
        logger.debug(f"{node_found_in_meas_data =}")
        logger.debug(f"{len(set_subfragments)=} ")
        logger.debug(f"{node.no_all_subfragments=}")

        no_all_subfragments = node.no_all_subfragments
        if no_all_subfragments is None:
            logger.warning(f"'no_all_subfragments' is None but should not be for {node}. Alpinac will not be able to score it for the molecular ion correctly.")
            # Should propagate the min mass at least for that
            no_all_subfragments, _ =  enumerate_all_valid_subfragments(node.fragment)
            logger.debug(f"recalculated: {no_all_subfragments=} ")
        node.subgraph_likelihood = (
            (100.0 - abs(100.0-node.subgraph_percent_I))
            * (node_found_in_meas_data + len(set_subfragments))
            / no_all_subfragments
        )
        # TODO replace 1+len(set_subfragments) by node.subgraph_size

    list_likelihood = [(G.nodes[key_node]['node_lite'].subgraph_likelihood, key_node) for key_node in G.nodes]
    list_likelihood.sort(reverse = True)
    G.nodes_ordered_by_likelihood = [node_index for (value, node_index) in list_likelihood]

    logger.info('Fragment' + '\t' + '% Opt. signal' + '\t' + 'Likelihood')
    if len(G.graph['nodes_validated']) == 0:
        for idx_node in range(min(10, G.number_of_nodes())):
            node = G.nodes[G.nodes_ordered_by_likelihood[idx_node]]['node_lite']
            logger.info("{}\t{:.1f}\t{:.1f}".format(node.iso.frag_abund_formula, node.subgraph_percent_I, node.subgraph_likelihood))
            #print("{}\t{:.1f}\t{:.1f}\t{:.1f}\t{:.1f}\t{:.1f}".format(node.iso.frag_abund_formula, node.iso.k_guess_init[0], node.iso.k_opt[0], node.iso.iso_list_I_sum/cl_comp.sum_I*100.0, node.subgraph_percent_I, node.subgraph_likelihood))
    else:
        for idx_node in range(min(10, len(G.graph['nodes_validated']))):
            if G.nodes_ordered_by_likelihood[idx_node] in G.graph['nodes_validated']:
                node = G.nodes[G.nodes_ordered_by_likelihood[idx_node]]['node_lite']    
                logger.info("{}\t{:.1f}\t{:.1f}".format(node.iso.frag_abund_formula, node.subgraph_percent_I, node.subgraph_likelihood))
        

def graph_percent_sum_signal(G: nx.DiGraph, sum_I: float):
    """This function computes two values used to determine
    if the optimsation loop should be continued further or stopped.
    
    (1) Computes the proportion of signal explained by the validated nodes 
    in G.graph['nodes_validated']

    First sum the signal intensities for each node in G.graph['nodes_validated']
    Secondly, add signal intensities of all sub-fragments on the graph.
    
    (2) Compute the number of measured masses explained by at least one validated node,
    and the number of measured masses that have a candidate fragments to explain them,
    but still not "validated".
    
    
    """
    sum_signal = sum([G.nodes[key_node]['node_lite'].iso.iso_list_I_sum for key_node in G.graph['nodes_validated']])
    percent_sum_signal = sum_signal/sum_I*float(100.0)

    
    list_masses_to_optimise = []
    no_masses_to_optimise = 0

    no_optimised_masses = 0
    list_masses_optimised = []

    # here this code lists all the indices of masses that can be explained 
    # by a fragment in the graph, considering the isotopologues.
    # a variant would be to list all the masses, then sort the list, 
    # then copy the list without the duplicates, to avoid many tests.
    # moreover, this list is constant as long as the number of (validated?) nodes in the graph does not change.
    for key_node in G.nodes:

        iso = G.nodes[key_node]['node_lite'].iso
        for mi in iso.meas_mass_idx:
            for mii in mi:
                if mii != None and mii not in list_masses_to_optimise:
                    list_masses_to_optimise.append(mii)


    no_masses_to_optimise = len(list_masses_to_optimise)
    #logging.info("List masses to optimise")
    #logging.info(list_masses_to_optimise)


    for key_node in G.graph['nodes_validated']:

        iso = G.nodes[key_node]['node_lite'].iso
        for mi in iso.meas_mass_idx:
            for mii in mi:
                if mii != None and mii not in list_masses_optimised:
                    #if mi != None and mi not in list_masses_optimised:
                        list_masses_optimised.append(mii)

    
    no_optimised_masses = len(list_masses_optimised)
    #logging.info("list_masses_optimised  " + str(list_masses_optimised))


    
    return sum_signal, percent_sum_signal, no_optimised_masses, no_masses_to_optimise




def get_list_identified_unidentified_mass(G:nx.DiGraph, cl_comp):
    """
    Parameters
    ----------
    G : TYPE: networkx graph. Each node contains an instance of Isotopologues.
    cl_comp: instance of Compound

    Returns
    -------
    List of indexes of identified masses.
    List of indexes of non-identified masses.
    Computes total assigned signal for each measured mass. Total may be > measured.
    """
    #used 20200730
    identified_mass_idx_list = []
    unidentified_mass_idx_list = []
    #assigned_I = [float(0.0)]*cl_comp.meas_len
    for i_node in G.nodes:
        iso = G.nodes[i_node]['node_lite'].iso
        if iso.meas_mass_idx is not None:
            for idx_m in range(len(iso.meas_mass_idx)):
                if iso.meas_mass_idx[idx_m] is not None:
                    for i_m in range(len(iso.meas_mass_idx[idx_m])):
                        if iso.meas_mass_idx[idx_m][i_m] is not None:
                            #assigned_I[idx_m] += min(iso.iso_list_I_rel[idx_m] * iso.k_guess, cl_comp.meas_I[idx_m])
                            if iso.meas_mass_idx[idx_m][i_m] not in identified_mass_idx_list:
                                identified_mass_idx_list.append(iso.meas_mass_idx[idx_m][i_m])


    for idx_m in range(cl_comp.meas_len):
        if idx_m not in identified_mass_idx_list:
            unidentified_mass_idx_list.append(idx_m)

    return identified_mass_idx_list, unidentified_mass_idx_list


def prepare_fitting(
    cl_comp: Compound,
    iso_list_idx: list[int],
    isotopologues_list: list,
    fitting_method: FittingMethod = FittingMethod.continuous,
):
    if fitting_method == FittingMethod.continuous:
        cl_comp.do_iso_profiles(
            iso_list_idx, isotopologues_list
        )
    elif fitting_method == FittingMethod.discrete:
        alpinac.requires_sup(f"Only 'fitting_method' {FittingMethod.continuous} is available without the sups.") 
        from alpinac_sups import discrete_fitting_optimization 

        # add isotopic profile to cl comp
        discrete_fitting_optimization.do_iso_discrete(cl_comp, iso_list_idx, isotopologues_list)
    else:
        raise ValueError(f"{fitting_method=} is not a valid method. Please select valid {FittingMethod} in input.")