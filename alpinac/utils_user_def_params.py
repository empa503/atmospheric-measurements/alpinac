def make_colors():
    # ******************
    # define colors for plots, colors will be used in plots in the defined order
    # https://xkcd.com/color/rgb/
    color1_names = [
        "xkcd:blue",
        "xkcd:green",
        "xkcd:orange",
        "xkcd:pink",
        "xkcd:rose",
        "xkcd:grey blue",
        "xkcd:light orange",
        "xkcd:goldenrod",
        "xkcd:forest green",
        "xkcd:burnt orange",
        "xkcd:apple green",
        "xkcd:reddish purple",
        "xkcd:dull blue",
        "xkcd:hot pink",
        "xkcd:peach",
        "xkcd:mauve",
        "xkcd:dark red",
        "xkcd:taupe",
        "xkcd:cerulean",
        "xkcd:eggplant",
        "xkcd:dark rose",
        "xkcd:dark salmon",
        "xkcd:slate",
        "xkcd:black",
    ]
    color2_names = [
        "xkcd:sky blue",
        "xkcd:teal",
        "xkcd:peach",
        "xkcd:mauve",
        "xkcd:dark red",
        "xkcd:taupe",
        "xkcd:cerulean",
        "xkcd:eggplant",
    ]

    # grey scale
    # color3_names = ['xkcd:grey', 'xkcd:slate', 'xkcd:black', 'xkcd:grey', 'xkcd:slate', 'xkcd:black', 'xkcd:grey', 'xkcd:slate', 'xkcd:black']

    color3_names = [
        "xkcd:dusty pink",
        "xkcd:grey",
        "xkcd:wine red",
        "xkcd:grey purple",
        "xkcd:dark rose",
        "xkcd:dark salmon",
        "xkcd:slate",
        "xkcd:black",
        "xkcd:eggplant",
        "xkcd:taupe",
        "xkcd:mauve",
    ]

    return color1_names, color2_names, color3_names
