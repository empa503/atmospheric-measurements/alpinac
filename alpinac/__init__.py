"""Alpinac package.
"""

SUPS_AVAILABLE: bool
try:
    import alpinac_sups

    SUPS_AVAILABLE = True
except ImportError:
    SUPS_AVAILABLE = False


def requires_sup(fallback_instructions: str):
    """Decorator to check if alpinac_sups is available.

    If not, it raises an ImportError.

    :param fallback_instructions: instructions to give to the user
        if alpinac_sups is not available

    :return: the decorator
    """

    if not SUPS_AVAILABLE:
        raise ImportError(
            "alpinac_sups is not available. "
            f"Please {fallback_instructions}."
        )
