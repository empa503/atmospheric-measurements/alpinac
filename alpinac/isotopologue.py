# -*- coding: utf-8 -*-

import logging
from alpinac.compound import Compound
import alpinac.periodic_table as chem_data
from alpinac.utils_identification import generate_all_isotopolog_fragments_with_relative_proba
from alpinac.utils_identification import number_rare_isotopes, nb_abundant_isotopes


logger = logging.getLogger(__name__)

class Isotopologues():
    """
    This is a class to gather, for each generated abundant fragment,
    all the info needed to then associate each family of fragment
    with the best estimated intensity.
    The aim is to eliminate all solutions that are likely not present,
    because no isotopes have been detected.
    In this step, uncertainties will be associated to each intensity value.
    """
    cl_comp = None
    

    def __init__(self, frag_abund: list, meas_mass_idx: list, cl_comp: Compound):

        """Initialise instance with minimal data

        INPUT:
        - `frag_abund`: encodes a fragment (as a list of integers), contains abundant atoms only.
        - `cl_comp`: instance of class Compound (already initialised)
        - `meas_mass_idx`: list of indexes of corresponding measured mass


        iso_rare: list of fragments, all fragments containing rare isotopes,
        being isotopologues of frag_abundant.
        
        I: intensity of frag_abundant.
        At start: I is the measured intensity for the measured mass
        to which frag_abundant is associated.
        u_I: uncertainty of I, 
        at start it is I_u (the standard deviation of data compared to fit).
        """



        #information about the abundant fragment, here meaning the fragment made of abundant atoms only
        #note: this 'abundant fragment' may not be the one with maximum intensity,
        #depending on the isotopic profile.
        self.frag_abund = frag_abund
        self.frag_abund_mass = chem_data.get_mass_single_ionised(self.frag_abund)
        self.frag_abund_formula = chem_data.get_string_formula(self.frag_abund)
        self.DBE = chem_data.get_DBE_value(self.frag_abund)

        #************************************************************
        #info to link iso to measured data

        #copy the address of the parameters:
        #(so that we dont need to pass all these parameters again later in the methods)
        #this is only an address! if somewhere else the variables are updated that's fine.
        self.set_cl_comp(cl_comp)        
        self.no_spectra = cl_comp.no_spectra#len(self.cl_comp.max_adduct_frag_list)
        self.spectra_idx_list = cl_comp.spectra_idx_list
        self.frag_abund_idx = meas_mass_idx # list of index of first reference fragment made of only abundant atoms
        
        #To which spectra belong the generated formula?
        #self.belong_to_spec = [False]*self.no_spectra
        


    @classmethod
    def set_cl_comp(self, cl_comp: Compound):
        self.cl_comp = cl_comp

    def create_isotopologue_suite(self, test_new_variant:bool = True):
        """Generate the isotopologues.
        
        20200416 new: not part of init anymore.
        This part is expensive and done only if the node 
        is not a singleton after initialisation of the graph.
        
        INPUT: None
        OUTPUT: None
        """

        logger.info("generating isotopocules for {}".format(chem_data.get_string_formula(self.frag_abund)))
        #list of isotopocules
        #>>>only one list for all spectra
        self.iso_list = []
        #list of exact, theoretical intensities of each isotopocule 
        #relative to abundant fragment
        #>>>only one list for all spectra, because this is the theoretical value of relative intensity
        self.iso_list_I_rel = []
        
        #generate all rare isotopologues in case they exist
        frag_iso_list = []
        if number_rare_isotopes(self.frag_abund) == 0 \
        and nb_abundant_isotopes(self.frag_abund) > 0:
            #generate formula of isotopologues
            # improvement: generate isotopic patterns for each abundant atom,
            # compute respective probability, then mix all: combine the patterns,
            # multiply the probabilities
            if self.frag_abund_idx != None:
                LOD_threshold_ratio = float(self.cl_comp.LOD/sum([self.cl_comp.meas_I[self.frag_abund_idx[i]] for i in range(len(self.frag_abund_idx))]))
            else:
                #no idx of mass: this is a candidate molecular ion
                LOD_threshold_ratio = float(1)
            
            #logging.info("LOD_threshold_ratio: " + str(LOD_threshold_ratio))
            arbitrary_choice_of_linear_factor = float(1.1)
            if test_new_variant:
                frag_iso_list, list_I_rel = generate_all_isotopolog_fragments_with_relative_proba(self.frag_abund, min_relative_proba = float(LOD_threshold_ratio/arbitrary_choice_of_linear_factor))
                self.iso_list = frag_iso_list
                self.iso_list_I_rel = list_I_rel
                max_I_rel = max(self.iso_list_I_rel)# should be at index 0
            else:
                frag_iso_list, list_I_rel = generate_all_isotopolog_fragments_with_relative_proba(self.frag_abund)
                #Eliminate fragments expected to be below LOD
                #the best would be to not generate them at all (pruning Loos et al. ?)
                for i_iso in range(len(frag_iso_list)):
                    #logging.info('isp p test: ' + '{:.5f}'.format(self.iso_list_I_rel) + '\t' + '{:.5f}'.format(cl_comp.meas_I[self.frag_abund_idx]))
                    if list_I_rel[i_iso]*arbitrary_choice_of_linear_factor >= LOD_threshold_ratio:
                        self.iso_list.append(frag_iso_list[i_iso])
                        self.iso_list_I_rel.append(list_I_rel[i_iso])
                max_I_rel = max(self.iso_list_I_rel)
            # remove too low isotopologues (so start to compare at the end)
            idx = len(self.iso_list_I_rel)
            while idx > 0 and self.iso_list_I_rel[idx-1] < LOD_threshold_ratio:
                idx -= 1
            if idx < len(self.iso_list_I_rel): # delete isotopologues of relative intensity below LOD_threshold_ratio
                self.iso_list_I_rel = self.iso_list_I_rel[:idx]
                self.iso_list = self.iso_list[:idx]
            # normalise so that max_I_rel == 1
            if max_I_rel > 1.0:
                self.iso_list_I_rel = [val/max_I_rel for val in self.iso_list_I_rel]
        logger.info("result {} {}".format(", ".join([chem_data.get_string_formula(si) for si in self.iso_list]), ", ".join(["{:.4f}".format(fi) for fi in self.iso_list_I_rel]) ))
        #logging.info('**********')
        #logging.info(self.frag_abund_formula)
        #logging.info('iso_list_I_rel: ' + str(self.iso_list_I_rel))
            
        #length of list of isotopologue presumably above LOD
        self.iso_list_len = len(self.iso_list)


        #**************************************************************
        #Now we need to link the word of theoretical solutions 
        #(generated by the knapsack, and isotopolgues of it)
        #to the word of measured data (masses, intensities).
        
        #for each mass of isotopologue, intensity of matching closest measured mass
        #this is the upper bound for assigned intensity
        #MYGU 20220319 modif: list of list, one list for each measured profile
        #self.iso_list_I_meas_max = [[float(0.0)] * self.iso_list_len]*self.no_spectra
        self.iso_list_I_meas_max = [[float(0.0) for i in range(self.iso_list_len)] for j in range(self.no_spectra)]
        #access float value using: self.iso_list_I_meas_max[i_spec][i_iso]


        #optimised intensity: min between measured matching mass and optimised signal based on isotopologue profile optimisation
        #MYGU 20220319 modif: list of list, one list for each measured profile
        #self.iso_list_I_opt = [0.0] * self.iso_list_len # PB computed in utils_graph not in this class
        self.iso_list_I_opt = [[float(0.0) for i in range(self.iso_list_len)] for j in range(self.no_spectra)]
        
        #sum of maximum optimised signal for this fragment and all its isotopocules
        #>>>only one float value for all measured spectra
        self.iso_list_I_sum = float(0.0) # PB computed in utils_graph not in this class
        
        #calculate the boundaries of the mass domain covered by the isotopologue set:
        #this is used later to define sub-domain of the entire mass domain
        #to run the fit to the measured data, along a sub-domain of the mass axis.
        #>>>mass_iso_list is the same for all measured spectra
        self.iso_list_mass = [chem_data.get_mass_single_ionised(vec) for vec in self.iso_list]
        self.iso_list_m_min = min(self.iso_list_mass) #>>>only one float value for all measured spectra
        self.iso_list_m_max = max(self.iso_list_mass) #>>>only one float value for all measured spectra

        #now we look for the index of measured masses where the theoretical mass of each isotopologue
        #can be found, and we associate each theoretical (or "candidate") isotopologue with this index
        #we do not use LOD so far
        #note 20200416: weakness: from one spectra, allow only one measured mass to be associated with one isotopologue.
        #if mass uncertainties are large, there could be several.
        #20200723: this is not relevant as we are interested in the closest mass peak.
        # it will contain a mass index idx so that the mass cl_comp.meas_mass[idx] matches the isotopologue mass
        #>>>which shape here?
        #self.meas_mass_idx = [None]*self.iso_list_len
        self.meas_mass_idx = [[None for i in range(self.iso_list_len)] for i_spec in range(self.no_spectra)]
        self.belong_to_spectra = [False for i_spec in range(self.no_spectra)]
        #logging.info('ini max signal frag abund: ' + self.frag_abund_formula + '  ' + str(self.cl_comp.meas_I[self.frag_abund_idx]) +';  ' + str(self.frag_abund_idx))


        if self.frag_abund_idx != None:
            #this is not an added candidate molecular ion
            self.find_idx_meas_mass_from_exact_mass()
        
        #print("create_isotopologue_suite " + self.frag_abund_formula + " " + str(self.meas_mass_idx))    
            

        #factor to multiply to theoretical isotopocule profile 
        #to fit measured profile
        #MYGU 20220319
        #modif to account for multiple measured profiles
        #k_guess_init, k_guess, k_guesses_min, k_guesses_max
        #are not float anymore but list of len of cl_comp.max_adduct_frag_list        
        self.k_guess_init =  [float(0.0)]*self.no_spectra #float(0.0)
        self.k_guess =       [float(0.0)]*self.no_spectra #float(0.0)
        self.k_guesses_min = [float(0.0)]*self.no_spectra #float(0.0)
        self.k_guesses_max = [float(0.0)]*self.no_spectra #float(0.0)
        #k_guess but after optimisation
        self.k_opt = [float(0.0)]*self.no_spectra #float(0.0)
        
        #compute k_guess, factor to multiply to relative iso profile to match measured profile
        #if this solution is taken individually
        
        self.init_k_guess()
        
        #print("***iso init  " + self.frag_abund_formula + " " + str(self.iso_list_I_sum))
        
            

    def find_idx_meas_mass_from_exact_mass(self):
        """
        For each isotopologue and each spectrum, 
        find the (unique) closest measured mass,
        within the specified uncertainty criteria.
        Candidate formula made of major isotopes only are already matched.
        Here we only need to match the candiate isotopologues 
        containing minor isotopes.
        For such non-major isotopologues, 
        the mass uncertainty criteria should be loose (broader),
        because in practice they are most of the time "on the side"
        of the peak of a major isotopologue.
        
        New information saved:
        - self.meas_mass_idx: the index of the closest measured mass
            (one index for each isotopologue and for each spectrum)
        - self.iso_list_I_meas_max: maximum intensity value 
            for each candidate isotopologue and each spectrum,
            this is the measured intensity of the matching measured mass.

        Issue with handling CI and EI data together:
            a minor isotopologue should be assigned to a mass of a spectra
            only if its main formula (without minor isotope) is already assigned to this spectra.
            otherwise, e.g. H4CCl (valid for CI) may be assigned to the mass of H[13C]Cl of EI...
            
            Another approach may be to use a check on the DBE such as
            
        """
        for i_m in range(len(self.frag_abund_idx)):
            if self.frag_abund_idx[i_m] is not None:
                i_spec = self.cl_comp.meas_mass_spec_id[self.frag_abund_idx[i_m]]
                self.belong_to_spectra[i_spec] = True
            
        for i_iso in range(self.iso_list_len):
            
            #exact mass of isotopologue:
            iso_mass = self.iso_list_mass[i_iso]
            #self.frag_abund_mass is used as starting point because we know already its index in the list of measured masses.
            #print("iso_mass " + str(self.iso_list_mass[i_iso]) )
            #exact mass difference between abundant and rare isotopologues:
            iso_mass_diff = self.frag_abund_mass - iso_mass

            #For the candidate fragment made of major isotopes only
            #(generated by the knapsack)
            #the index of the measured mass is already in self.frag_abund_idx
            if iso_mass_diff == 0.0:
                for i_m in range(len(self.frag_abund_idx)):
                    i_spec = self.cl_comp.meas_mass_spec_id[self.frag_abund_idx[i_m]]
                    self.meas_mass_idx[i_spec][i_iso] = self.frag_abund_idx[i_m]
                    self.iso_list_I_meas_max[i_spec][i_iso] = self.cl_comp.meas_I[self.meas_mass_idx[i_spec][i_iso]]
                    #self.belong_to_spectra[i_spec] = True
                    #print("iso_mass_diff == 0.0, i_spec, self.meas_mass_idx "  + " " + str(i_spec) + " " + str(self.meas_mass_idx))
                
            #For all other candidate fragments containing minor isotopes                     
            else:
                for i_spec in range(self.no_spectra): 
                    #A candidate iso formula is allowed to search
                    #for measured mass matches in a spectrum
                    #if the main fragment already belong to this spectrum
                    
                    if self.belong_to_spectra[i_spec]:
                        #now loop over all measured masses of one spectrum 
                        #to find the (unique) closest measured mass
                        #here use binary search?
                        m_i_spec = [
                            i_m for i_m in range(self.cl_comp.meas_len)
                            if self.cl_comp.meas_mass_spec_id[i_m] == i_spec]
                        
                        #print("find match mass for iso len(m_i_spec)" + str(len(m_i_spec)))
                        min_mass_diff = min([abs(self.cl_comp.meas_mass[i_m] - iso_mass) for i_m in m_i_spec])
    
                        """
                        m_i_list_t = [
                            i_m for i_m in m_i_spec
                            if abs(self.cl_comp.meas_mass[i_m] - iso_mass) == min_mass_diff]
                        print("find match mass for iso len(m_i_list_t)" + str(len(m_i_list_t)))
                        """
                        m_i_list = [
                            i_m for i_m in m_i_spec
                            if abs(self.cl_comp.meas_mass[i_m] - iso_mass) == min_mass_diff
                            and abs(self.cl_comp.meas_mass[i_m] - iso_mass) <= self.cl_comp.meas_mass_u_peak_width[i_m]]
                        
                        
                        #print("find match mass for iso len(m_i_list)" + str(len(m_i_list)))
                        
                        if len(m_i_list) >1:
                            raise ValueError("List of matching masses from one spectra and for one isotopologue has length more than one.")                   
                        elif len(m_i_list) == 1:                       
                            self.meas_mass_idx[i_spec][i_iso] = m_i_list[0]
                            self.belong_to_spectra[i_spec] = True
                        else:
                            self.meas_mass_idx[i_spec][i_iso] = None
                        #print("m_i_list, i_spec, self.meas_mass_idx "  + str(m_i_list) + " " + str(i_spec) + " " + str(self.meas_mass_idx))
                            
                        if self.meas_mass_idx[i_spec][i_iso] != None:
                            self.iso_list_I_meas_max[i_spec][i_iso] = self.cl_comp.meas_I[self.meas_mass_idx[i_spec][i_iso]]
                            
        #print(self.frag_abund_formula + " " + str(self.meas_mass_idx))

    
    
    def init_k_guess(self):
        """
        This is ALPINAC STEP 4
        
        This function is run once at the initialisation of each instance of isotopologue.
        Compute k_guess (float), the factor such that:
            self.iso_list_I_rel * k_guess = self.iso_list_I_opt
        As a first approximation for the initialisation we assume:
            self.iso_list_I_rel * k_guess = self.iso_list_I_meas_max
        We get:
            k_guess = self.iso_list_I_meas_max / self.iso_list_I_rel
        In practice it gives better results like this:
            k_guess = sum(self.iso_list_I_meas_max) / sum(self.iso_list_I_rel)
        Not like this (equation 4 from paper Guillevic et al.):
            k_guess = mean([self.iso_list_I_meas_max[i_iso] / self.iso_list_I_rel[i_iso] for i_iso in self.iso_list_len])
            
        So we do not use equation 4 from paper Guillevic et al. anymore.
                    
        #If no measured mass is assigned, self.iso_list_I_meas_max = zero
        
        For one set of isotopologues, there is one k_guess value for each spectrum.
        Implicitely we assume that theoretical relative intensities 
        of one set of isotopologues are fixed
        (otherwise it would imply that isotopic fractionation is allowed).

        """
        
        for i_spec in range(self.no_spectra):
            
            """
            #first compute k_guess for masses with a match only
            i_iso_list = [i_iso for i_iso in range(self.iso_list_len) if self.meas_mass_idx[i_spec][i_iso] is not None]
            if len(i_iso_list)>0:
                self.k_guess_init[i_spec] =\
                    sum([self.iso_list_I_meas_max[i_spec][i_iso] for i_iso in i_iso_list])\
                    /sum([self.iso_list_I_rel[i_iso] for i_iso in i_iso_list])#/len(i_iso_list)
            
            #Now estimate which not found masses are likely below LOD
            iso_list_LOD_test = [i_iso for i_iso in range(self.iso_list_len) 
                                if (self.iso_list_I_rel[i_iso] * self.k_guess_init[i_spec] > self.cl_comp.LOD 
                                or self.k_guess_init[i_spec]*self.iso_list_I_rel[i_iso] < self.iso_list_I_meas_max[i_spec][i_iso])] #or self.meas_mass_idx[i_spec][i_iso] is not None

            #If an isotopologue has no matching measured mass because
            #it is probably below LOD, do not consider.
            #Otherwise, take into account (this will decrease value of k_guess).
            if len(iso_list_LOD_test)>0:
                self.k_guess_init[i_spec] =\
                    sum([self.iso_list_I_meas_max[i_spec][i_iso] for i_iso in iso_list_LOD_test])\
                    /sum([self.iso_list_I_rel[i_iso] for i_iso in iso_list_LOD_test]) #/len(iso_list_LOD_test)
            """
            
            #super-simplified version would be:
            #self.k_guess[i_spec] = sum([self.iso_list_I_meas_max[i_spec][i_iso] for i_iso in range(self.iso_list_len)]) / sum([self.iso_list_I_rel[i_iso] for i_iso in range(self.iso_list_len)])
            self.k_guess_init[i_spec] = sum(self.iso_list_I_meas_max[i_spec]) / sum(self.iso_list_I_rel)
            
            self.iso_list_I_opt[i_spec] = [min(self.iso_list_I_rel[i_iso] * self.k_guess_init[i_spec], self.iso_list_I_meas_max[i_spec][i_iso]) for i_iso in range(self.iso_list_len)]


        self.k_guess = self.k_guess_init.copy()
        self.k_opt = self.k_guess_init.copy()
        self.iso_list_I_sum = sum(sum(val) for val in self.iso_list_I_opt) #iso_list_I_opt has 2 dimensions
        


    def __repr__(self):
        s = "["
        for frag in self.iso_list:
            s += chem_data.get_string_formula(frag) + ", "
        s += "]"
        return s
