"""Command line usage for alpinac."""

import logging
import os
from pathlib import Path
import sys


import argparse

from alpinac.cli.utils import (
    add_target_arguments,
    add_verbose_debug_arguments,
    process_verbose_debug_arguments,
)
from alpinac.utils_data_extraction import FittingMethod

parser = argparse.ArgumentParser(description="Identify a sustance using alpinac.")

parser.add_argument(
    "filepath",
    help="Path of the file(s) to read. Can use wildcard* to specify a file pattern.",
)

add_target_arguments(parser)
parser.add_argument(
    "--show-figs",
    help="Whether to show the figures.",
    action="store_true",
)

add_verbose_debug_arguments(parser)
# option to raise the errors
parser.add_argument(
    "-r",
    help="If True, raise an error if the identification of a compound fails, otherwise will proceed with the next compound.",
    action="store_true",
    dest="raise_error",
)
parser.add_argument(
    "--output_dir",
    help=(
        "The directory where the output should be stored. \n"
        "By default it would be stored in a directory with the name of "
        "the input file without the suffix, at the same path as the input file."
    ),
    action="store",
)
parser.add_argument(
    "--fitting-method",
    type=FittingMethod,
    help=(
        "Defines if continuous or discrete fitting of isotopologue mass spectra to the measured mass spectra"
        "should be applied. Discrete fitting is expected to be faster and more reliable if many isotopic"
        "profiles potentially match."
    ),
    default="continuous",
    action="store",
)
parser.add_argument(
    "-c",
    "--compound",
    help="Compund to analyze (instead of analysing the whole file)",
    action="store",
    dest="compound",
    default=None,
    type=int,
)


def main():

    args = parser.parse_args()

    # log_fmt = (
    #     '"%(pathname)s", line %(lineno)d, \n'
    #     '%(message)s'
    # )
    # Set the logging value
    process_verbose_debug_arguments(args)

    logger = logging.getLogger("alpinac.mode_identification")
    arg_path = Path(args.filepath)
    if not arg_path.exists():
        logger.error(f"File path '{arg_path}' does not exist")
        sys.exit(1)

    # Loop around all the files with the same pattern
    for path_file in arg_path.parent.rglob(arg_path.name):
        logger.info(f"Reading '{path_file}'")
        # Import only there, to speed up the command line
        from alpinac.mode_identification.main_from_file import (
            make_identification_from_file,
        )

        method_performance_results = make_identification_from_file(
            path_file,
            target_elements=args.target_elements,
            target_formula=args.target_formula,
            show_plt_figures=args.show_figs,
            output_dir=args.output_dir,
            fitting_method=args.fitting_method,
            raise_error=args.raise_error,
            compounds=[] if args.compound is None else [args.compound],
        )
        logger.debug(f"Reading '{path_file}'")


if __name__ == "__main__":
    main()
