import logging
from pathlib import Path
from typing import Any
from alpinac.abtract_outputs import OutputObject
from alpinac.compound import Compound
import networkx as nx
from alpinac.mode_identification.utils import MesureMassesTypes
import alpinac.periodic_table as chem_data
from alpinac.plot_figures import plot_graph_of_frag
from alpinac.utils_graph import (
    add_maximal_node_and_edges_to_graph_from_max_fragment,
    compute_all_subfragments_of_nodes_with_lists,
    graph_order_by_att_likelihood,
    initialise_isotopologue_set,
    find_max_nodes_per_spectrum,
    find_node_from_formula,
    add_node_and_edges_to_graph_from_not_max_fragment,
)
from alpinac.utils_identification import enumerate_all_valid_subfragments

# TODO: check if there is an actual class / type
NodeLite = Any


class CandidateResults(OutputObject):
    """Results for the step 10.

    All candidate molecular ion(s) is (are) saved in this.
    Warning! There may be several candidate molecular ions.

    TODO 20221009 Lionel maybe? (written by MYGU)
    It would be good to save
    the reconstructed molecular ions for each spectrum,
    not as a whole list for all spectra.
    Mabe this class could be split between all fragments and molecular ions,
    and the class for the molecular ions could be called for each spectrum?

    :attr most_likely_fragments: The most likely fragments.
    :attr most_likely_mol_ions: The most likely molecular ions.
    :attr most_likely_mol_ions_i_spec: index of spectrum corresponding to each most_likely_mol_ions.
    :attr most_likely_mol_ions_spec_adduct: adducts of the ions.
    :attr most_likely_mol_ions_ranking: ranking of the mol ions.

    :attr max_frag_ranks: The ranking for the likelihood of the corresponding
        node in :py:attr:`most_likely_fragments`.
    :attr mol_ion_ranks: The ranking for the likelihood of the corresponding
        node in :py:attr:`most_likely_mol_ions`.

    :attr mol_ion_formulae: chemical formula of the molecular ion
    :attr mol_ion_formulae_str: chemical formula of the molecular ion as a str


    """

    all_fragments: list[NodeLite]
    all_fragments_ranking: list[int]

    max_fragments: list[NodeLite]
    max_fragments_ranking: list[int]

    most_likely_mol_ions: list[NodeLite]
    most_likely_mol_ions_i_spec: list[int]
    most_likely_mol_ions_spec_adduct: list[str]
    most_likely_mol_ions_ranking: list[int]

    mol_ion_formulae: list[Any]
    mol_ion_formulae_str: str

    def __init__(self) -> None:
        super().__init__()
        self.all_fragments = []
        self.all_fragments_ranking = []

        self.max_fragments = []
        self.max_fragments_ranking = []

        self.most_likely_mol_ions = []
        self.most_likely_mol_ions_i_spec = []
        self.most_likely_mol_ions_spec_adduct = []
        self.most_likely_mol_ions_ranking = []

    def save(self, dir: Path):
        """Save the str in the files."""
        with open(dir / "all_fragments.txt", "w") as f:
            f.write(nodes_to_str(self.all_fragments, self.all_fragments_ranking))

        with open(dir / "max_fragments.txt", "w") as f:
            f.write(nodes_to_str(self.max_fragments, self.max_fragments_ranking))

        with open(dir / "most_likely_mol_ions.txt", "w") as f:
            f.write(
                nodes_to_str(
                    self.most_likely_mol_ions,
                    self.most_likely_mol_ions_ranking,
                    self.most_likely_mol_ions_i_spec,
                    self.most_likely_mol_ions_spec_adduct,
                )
            )  # self.most_likely_mol_ions_i_spec,


def nodes_to_str(
    nodes_list: list[NodeLite],
    node_idxs: list[int] = None,
    node_spec_idxs: list[int] = None,
    node_spec_adduct_formula: list[str] = None,
) -> str:
    """Transform the list of node to a printable text.

    :arg nodes_list: the list of nodes to be written
    :arg node_idxs: an optional index to write. (Can be used to write a rank.)
    :arg node_idxs: another optional index to write. (Can be used to write the index of the spectrum.)

    :return text: A str containing the nodes in a table format.

    """
    if node_idxs is None:
        node_idxs = range(1, len(nodes_list) + 1)
    if node_spec_idxs is None:
        node_spec_idxs = [0] * len(nodes_list)
    if node_spec_adduct_formula is None:
        node_spec_adduct_formula = ["unknown"] * len(nodes_list)
    SPACE_IN_TABS = 14
    header = "\t".join(
        [
            "spectrum id.",
            "max. adduct",
            "ranking",
            "formula",
            "DBE",
            "likelihood",
        ]
    ).expandtabs(SPACE_IN_TABS)
    content = "\n".join(
        [
            "{}\t{}\t{}\t{}\t{}\t{:.2f}".format(
                idx_spec,
                adduct,
                idx,
                node.iso.frag_abund_formula,
                node.iso.DBE,
                node.subgraph_likelihood,
            ).expandtabs(SPACE_IN_TABS)
            for node, idx, idx_spec, adduct in zip(
                nodes_list, node_idxs, node_spec_idxs, node_spec_adduct_formula
            )
        ]
    )

    return header + "\n" + content


def step10_create_candidate_molecular_ion(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    show_plt_figures: bool,
    nodes_maximal_fragments: list,
    min_measured_mass: float,
    percent_sum_signal: float,
    measured_masses_type: int,
    no_iteration: int,
) -> CandidateResults:
    """Suggest most likely molecular ion(s)

    At the end of Step 9, the majority of measured masses
    have been assigned a chemical formula. Using this information,
    we develop a simple algorithm to identify or
    reconstruct likely molecular ions.
    The molecular ion may or may not be present in the list of measured masses!
    The following algorithm implicitly makes the assumption that all
    multivalent atoms present in the true molecular ion have been
    detected in at least one fragment and correctly identified.
    It also implicitely makes the assumption that in any maximal fragment,
    a maximum of one monovalent atom was lost.

    The three conditions
    of the SENIOR theorem have to be fulfilled, as listed by
    Kind and Fiehn 2007
    Kind T, Fiehn O (2007) Seven golden rules for heuristic filtering of
    molecular formulas obtained by accurate mass spectrometry. BMC
    Bioinform 8(105):1-20. https://doi.org/10.1186/1471-2105-8-105

    1 The sum of valences or the total number of atoms
    having odd valences is even.

    2 The sum of valences is greater than or equal to twice
    the maximum valence. This rule prevents fragments
    such as CFCl to be considered as valid molecular ion.

    3 The sum of valences is greater than or equal to twice
    the number of atoms minus 1. For EI data, by
    construction all fragment formulae have a non-negative
    DBE value, therefore this rule is fulfilled.
    However this is not the case for CI data with potential adducts.

    We first identify the maximal fragments for each spectrum separately.

    For each spectrum, we start from the list of maximal fragments still part of
    the graph at the end of Step 9, and separate them into
    two groups, with odd or even sum valence.
    All maximal fragments with even valence fulfil the first
    condition of the SENIOR theorem. We then test for the
    second condition. All maximal fragments fulfilling these
    two criteria are added to the list of potential molecular ions.

    Using all the maximal fragments with odd valence,
    we enumerate all possibilities of adding one monovalent
    atom to each of them, using all monovalent atoms
    present in all fragments on the graph (assigned to the spectrum).
    Each newly constructed
    maximal fragment, if fulfilling the second
    SENIOR condition, is considered a potential molecular
    ion. It is added to the graph with all other fragments
    (if not already present, for ex. assigned to another spectrum),
    and its likelihood value is computed.


    :arg G_of_frag: graph containing all validated nodes
    :arg cl_comp: instance of class Compound, contain the measured data
    :arg nodes_maximal_fragments: list of integer,
        each integer is the index of a node on the graph,
        each node listed here is a maximal fragment
        (after removal of all non-validated nodes).
        Warning! Maximal nodes may or may not contain adduct!
    :arg min_measured_mass: The smallest possible measured mass.
        This is minimum mass that can be detected given
        the TOF experimental settings (high-pass filter settings).
        Only chemical formulae with a mass larger than this will be given by the algorithm.
    :arg percent_sum_signal: set by user, minimum percentage of the measured signal
        that should be matched by all solutions.
    :arg measured_masses_type: either few masses or enough masses.
        Plots and info logging are done differently for the two cases.
    :arg no_iteration: number of iteration for the optimisation routine,
        needed for the info logger only (move somewhere else?)

    :return: The result for the candidate molecular ion.

    """
    logger = logging.getLogger(
        "alpinac.mode_identfication.step10_create_candidate_molecular_ion"
    )

    results = CandidateResults()

    if  G_of_frag.number_of_nodes() == 0:
        logger.error("No valid nodes were found for fragment")
        return results
    
    # Assign all validated maximal nodes to each spectrum.
    # One maximal node can be assigned to several spectrum.
    find_max_nodes_per_spectrum(G_of_frag)
    # Now all maximal nodes for ech spectrum are saved in:
    # G.graph['maximal_nodes_per_spectrum'][i_spec]

    # Loop over each spectrum
    for i_spec in range(cl_comp.no_spectra):

        if G_of_frag.graph["maximal_nodes_per_spectrum"][i_spec] is None:
            logger.info(
                f"Spectrum {i_spec}"
                " has no solution. No reconstruction of molecular ion is possible."
            )
            continue

        logger.info(f">>>>>>>Reconstructing molecular ion(s) for spectrum {i_spec}")

        old_nodes_per_spectrum = list(G_of_frag.nodes)

        # Candidate fragment formulae with odd DBE
        candidate_oddDBE_node_parent = []  # node key of the parent
        candidate_oddDBE_frag = []
        candidate_oddDBE_frag_adduct_removed = []

        # Candidate fragment formulae with even DBE
        # with or without removed adducts
        # Corresponding node of parent (frag before removing adduct)
        candidate_mol_ion_node_parent = []
        # node of effective frag formula (after removing adduct)
        candidate_mol_ion_node = []
        # list of candidate fragment formula for the molecular ion (without adducts)
        candidate_mol_ion_frags = []
        # fragment formula of removed adduct
        candidate_mol_ion_frag_adduct_removed = []

        # fragment (vector) which are max fragments and will be added to graph
        candidate_mol_ion_frag_add_to_graph = []
        # corresponding i_spec
        candidate_mol_ion_frag_add_to_graph_i_spec = []
        # fragment which are not max fragments and should be added to graph using another method
        candidate_mol_ion_not_max_frag_add_to_graph = []
        # corresponding i_spec
        candidate_mol_ion_not_max_frag_add_to_graph_i_spec = []
        # parent node of the candidate (or supnode before removing adduct)
        candidate_mol_ion_not_max_frag_add_to_graph_parent = []

        # get formula of max_adduct for this spectrum
        # already contain special fake atoms with fake DBE!
        adduct = cl_comp.spectra_adduct_frag[i_spec]
        # enumerate all possible fragments from max adduct
        (
            no_all_subfragments,
            adduct_list_subfragments,
        ) = enumerate_all_valid_subfragments(
            adduct, min_mass=0, return_partial_lists=False
        )
        if sum(adduct) > 0:
            logger.info(
                "Adduct formula for this spectrum is: "
                + str(chem_data.get_string_formula(adduct))
                + "."
            )
            logger.debug(f"Number of subfragments for adduct: {no_all_subfragments}")
        else:
            logger.info("Adduct formula for this spectrum is None.")

        # For each spectrum,
        # for each maximal fragment formula of a given spectrum -> G.graph['maximal_nodes_per_spectrum'][i_spec]
        # for each subfragment of the adduct of a given spectrum -> adduct_list_subfragments
        # remove adduct from maximal fragment
        # check if DBE >= 0
        # if True -> treat as max frag from EI
        # if False -> drop candidate

        # At this stage, if a spectrum exists but has no solutions at all, its
        # G.graph['maximal_nodes_per_spectrum'][i_spec] wil still be None.

        for max_node in G_of_frag.graph["maximal_nodes_per_spectrum"][i_spec]:
            for i_adduct in range(no_all_subfragments + 1):
                # add 1 to loop length so that this will loop as well for EI where no_all_subfragments = 0
                candidate_frag = G_of_frag.nodes[max_node][
                    "node_lite"
                ].iso.frag_abund.copy()
                if i_adduct == 0:
                    # This is the case where we assume, for any ionisation type,
                    # that the fragment formula does not contain an adduct.
                    # Note that fragments from CI do not necessarily
                    # contain an adduct.
                    adduct_removed = False
                    adduct_removed_frag = [0] * chem_data.len_frag
                    candidate_DBE = G_of_frag.nodes[max_node]["node_lite"].iso.DBE
                    candidate_chem_formula = G_of_frag.nodes[max_node][
                        "node_lite"
                    ].iso.frag_abund_formula

                else:
                    # This is the case where a potential adduct is removed.
                    adduct_removed = True
                    adduct_removed_frag = adduct_list_subfragments[i_adduct - 1]
                    logger.debug(
                        "Adduct to remove "
                        + str(chem_data.get_string_formula(adduct_removed_frag))
                    )
                    for i_a in range(
                        len(G_of_frag.nodes[max_node]["node_lite"].iso.frag_abund)
                    ):
                        if adduct_removed_frag[i_a] > 0:
                            candidate_frag[
                                chem_data.dict_adduct_to_element[i_a]
                            ] -= min(
                                candidate_frag[chem_data.dict_adduct_to_element[i_a]],
                                adduct_removed_frag[i_a],
                            )

                    candidate_DBE = chem_data.get_DBE_value(candidate_frag)
                    candidate_chem_formula = chem_data.get_string_formula(
                        candidate_frag
                    )
                    logger.info(
                        f"candidate mol. ion after removing adduct: {candidate_chem_formula}"
                    )

                if candidate_DBE >= 0:
                    # this maximal fragment is a solution for:
                    # a spectrum without adducts
                    # or it is a solution for a spectrum with adducts
                    # but it does not contain an adduct (DBE >=0)
                    # or it is a wrong solution containing both adduct
                    # and double bounds or rings but the fact
                    # that it is a wrong solution cannot be detected here

                    if (candidate_DBE) - int(candidate_DBE) == 0:
                        # even DBE number, this may be a molecular ion
                        # Check: The sum of valences is greater than or equal
                        # to twice the maximum valence
                        # cf. Kind and Fiend 2007, p. 8, 1st column, rule (ii).
                        # With this test, e.g. CFCl cannot be a molecular ion.
                        if chem_data.test_SENIOR_rule_ii(candidate_frag):
                            if candidate_frag not in candidate_mol_ion_frags:
                                candidate_mol_ion_frags.append(candidate_frag)
                                candidate_mol_ion_frag_adduct_removed.append(
                                    adduct_removed_frag
                                )
                                candidate_mol_ion_node_parent.append(max_node)

                                if not adduct_removed:
                                    # No adduct removed, child and parent node are the same
                                    # node is already on the graph, does not need to be added to the graph
                                    candidate_mol_ion_node.append(max_node)
                                    if (
                                        G_of_frag.graph["mol_ion_nodes_per_spectrum"][
                                            i_spec
                                        ]
                                        is None
                                    ):
                                        G_of_frag.graph["mol_ion_nodes_per_spectrum"][
                                            i_spec
                                        ] = []
                                    G_of_frag.graph["mol_ion_nodes_per_spectrum"][
                                        i_spec
                                    ].append(max_node)
                                else:
                                    # Will be done at next step: Search if node without adduct is a child node
                                    candidate_mol_ion_node.append(None)

                                logger.info(
                                    f"Candidate formula mol. ion even DBE: {candidate_chem_formula}"
                                )

                    else:
                        # Odd DBE number: at least one atom is missing.
                        # Later on we assume that exactly one atom is missing.
                        if candidate_frag not in candidate_oddDBE_frag:
                            candidate_oddDBE_node_parent.append(max_node)
                            candidate_oddDBE_frag.append(candidate_frag)
                            candidate_oddDBE_frag_adduct_removed.append(
                                adduct_removed_frag
                            )
                            logger.info(
                                f"Candidate formula odd DBE:  {candidate_chem_formula}"
                            )
                            # logger.info("Added candidate mol. ion odd DBE:  " + str(chem_data.get_string_formula(G_of_frag.nodes[max_node]['node_lite'].iso.frag_abund)))
                            # logger.info(G_of_frag.nodes[max_node]['node_lite'].iso.frag_abund)

                # Implicitely, if DBE<0, this is not a valid molecular ion,
                # do nothing.

        # For each spectrum, compute maximal fragment.
        # Do not do it for all spectra together otherwise
        # adduct will be added to EI spectrum and that would be wrong
        candidate_frag_odd_and_even = candidate_oddDBE_frag + candidate_mol_ion_frags
        if len(candidate_oddDBE_node_parent) > 0:
            # list max number for each present atom
            # within the entire set of validated fragment formulae
            # Compute max fragment for each spectrum
            # frag_max = [max([G_of_frag.nodes[max_node]['node_lite'].iso.frag_abund[i_a] for max_node in max_nodes_odd_and_even]) for i_a in range(chem_data.len_frag)]
            frag_max = [
                max([_frag[i_a] for _frag in candidate_frag_odd_and_even])
                for i_a in range(chem_data.len_frag)
            ]
            for i_a in range(chem_data.len_frag):
                if frag_max[i_a] > 0 and i_a in chem_data.idx_mono_valent:
                    # necessary to add +1 for e.g. CCl4 where max detected frag is CCl3
                    frag_max[i_a] += 1
            logger.info(
                f"Spectrum, constructed max frag: {i_spec} {chem_data.get_string_formula(frag_max)}"
            )

            # now for each candidate fragment with odd DBE, add one more monovalent atom at a time
            for _i_frag in range(len(candidate_oddDBE_frag)):
                for i_a in chem_data.idx_mono_valent:
                    candidate_max_frag = candidate_oddDBE_frag[_i_frag].copy()
                    if candidate_max_frag[i_a] < frag_max[i_a]:
                        candidate_max_frag[i_a] += 1
                        if (
                            candidate_max_frag not in candidate_mol_ion_frags
                            and chem_data.test_SENIOR_rule_ii(candidate_max_frag)
                        ):
                            candidate_mol_ion_frags.append(candidate_max_frag)
                            candidate_mol_ion_frag_adduct_removed.append(
                                candidate_oddDBE_frag_adduct_removed[_i_frag]
                            )
                            candidate_mol_ion_node_parent.append(max_node)
                            candidate_mol_ion_node.append(None)
                            logger.info(
                                "Candidate mol. ion after adding atom: "
                                + str(chem_data.get_string_formula(candidate_max_frag))
                            )

        # All fragment formulae for which a modification was done
        # (an adduct was removed and/or an atom added)
        # are potentially already on the graph and should not be added to the graph.
        logger.info(f"Total candidate mol. ion(s) {len(candidate_mol_ion_frags)}")
        for _i in range(len(candidate_mol_ion_frags)):
            # check if it is a new candidate
            this_candidate_frag = candidate_mol_ion_frags[_i]
            is_extra_candidate = candidate_mol_ion_node[_i] is None
            logger.info(
                f"Candidate mol. ion: {is_extra_candidate=} {chem_data.get_string_formula(this_candidate_frag)}"
            )
            if not is_extra_candidate:
                # This candidate was already on the graph.
                continue
            # Fragment formula was modified: An adduct was removed and/or an atom was added.
            # Check if resulting formula
            # already belong to an existing node.
            # Check if "new" frag after removing adduct is a child of the parent
            # node_child_found = 0
            logger.info("Search if extra candidate is already on graph")
            # TODO: coli 2023-06-22: check if this is really what we need, and necessary ?
            node_child_found = find_node_from_formula(
                G_of_frag,
                candidate_mol_ion_node_parent[_i],
                this_candidate_frag,
                i_spec,
            )
            # G.graph['mol_ion_nodes_per_spectrum'][i_spec] was updated if necessary.
            logger.info(f"Node child found: {node_child_found}")

            if node_child_found is not None:
                # coli: 2023-06-22, This candidate parent was  on the graph again ???
                continue
            # check as well if the node_child is a maximal fragment
            # otherwise it cannot be added using the same method
            # because edges are only created from parent to child
            is_max_frag = True
            i_max_frag = 0
            while is_max_frag and i_max_frag < len(
                G_of_frag.graph["maximal_nodes"]
            ):
                test_node = G_of_frag.graph["maximal_nodes"][i_max_frag]
                test_node_lite = G_of_frag.nodes[test_node]["node_lite"]
                if not test_node_lite.is_strict_subfragment(this_candidate_frag):
                    is_max_frag = False
                    logger.debug(f"Found a supfragment node {test_node_lite}")
                i_max_frag += 1
            if is_max_frag:
                # Will be added later to the graph
                candidate_mol_ion_frag_add_to_graph.append(
                    this_candidate_frag
                )
                candidate_mol_ion_frag_add_to_graph_i_spec.append(i_spec)
            else:
                # Will be added later with a special method to connect with parents
                candidate_mol_ion_not_max_frag_add_to_graph.append(
                    this_candidate_frag
                )
                candidate_mol_ion_not_max_frag_add_to_graph_i_spec.append(
                    i_spec
                )
                candidate_mol_ion_not_max_frag_add_to_graph_parent.append(
                    candidate_mol_ion_node_parent[_i]
                )


        # For each spectrum, add new frag to new nodes on the graph.
        # Warning: do not add new frag globally for all spectra
        # otherwise there is no way to assign new nodes to a specific spectrum.
        if len(candidate_mol_ion_frag_add_to_graph) > 0:
            logger.info("===Add molecular ion(s) to graph which are max frag.===")
            add_maximal_node_and_edges_to_graph_from_max_fragment(
                G_of_frag,
                list_max_fragments=candidate_mol_ion_frag_add_to_graph,  # [candidate_mol_ion_frags[i] for i in range(len(candidate_mol_ion_frags)) if candidate_mol_ion_frag_add_to_graph[i]],
                molecular_ion=True,
                list_i_spec=candidate_mol_ion_frag_add_to_graph_i_spec,
            )
            idx_new_nodes_per_spectrum = [
                i_node
                for i_node in G_of_frag.nodes
                if i_node not in old_nodes_per_spectrum
            ]
            logger.info(
                f"New nodes (max frag) added to graph: {idx_new_nodes_per_spectrum}"
            )
            initialise_isotopologue_set(
                G_of_frag,
                cl_comp,
                add_mol_ion=True,
                mol_ion_nodes=idx_new_nodes_per_spectrum,
            )
            old_nodes_per_spectrum = list(G_of_frag.nodes)  # re-initialise

            for i_node in idx_new_nodes_per_spectrum:
                compute_all_subfragments_of_nodes_with_lists(
                    G_of_frag,
                    min_measured_mass=min_measured_mass,
                    start_node=G_of_frag.nodes[i_node]["node_lite"],
                )  # force to start computation with new constructed frag!

        if len(candidate_mol_ion_not_max_frag_add_to_graph) > 0:
            logger.info("===Add molecular ion(s) to graph which are not max frag.===")
            # G: nx.DiGraph, node_parent: int, frag: list, i_spec: int
            for _i in range(len(candidate_mol_ion_not_max_frag_add_to_graph)):
                add_node_and_edges_to_graph_from_not_max_fragment(
                    G_of_frag,
                    candidate_mol_ion_not_max_frag_add_to_graph_parent[_i],
                    candidate_mol_ion_not_max_frag_add_to_graph[_i],
                    candidate_mol_ion_not_max_frag_add_to_graph_i_spec[_i],
                )

            idx_new_nodes_per_spectrum = [
                i_node
                for i_node in G_of_frag.nodes
                if i_node not in old_nodes_per_spectrum
            ]
            logger.info(
                f"New nodes (not max frag) added to graph: {idx_new_nodes_per_spectrum} "
            )
            initialise_isotopologue_set(
                G_of_frag,
                cl_comp,
                add_mol_ion=True,
                mol_ion_nodes=idx_new_nodes_per_spectrum,
            )

            for i_node in idx_new_nodes_per_spectrum:
                compute_all_subfragments_of_nodes_with_lists(
                    G_of_frag,
                    min_measured_mass=min_measured_mass,
                    start_node=G_of_frag.nodes[i_node]["node_lite"],
                )  # force to start computation with new constructed frag!

        if (
            len(candidate_mol_ion_frag_add_to_graph)
            + len(candidate_mol_ion_not_max_frag_add_to_graph)
        ) == 0:
            logger.info(
                "===All candidate molecular ions are already on the graph, add nothing.==="
            )
            idx_new_nodes_per_spectrum = []

        if G_of_frag.graph["mol_ion_nodes_per_spectrum"][i_spec] is not None:
            logger.info(
                f"===Molecular ion(s) for spectrum {i_spec} :"
                f" {G_of_frag.graph['mol_ion_nodes_per_spectrum'][i_spec]}.==="
            )
        else:
            logger.info(f"===No molecular ion for spectrum {i_spec} .===")

    G_of_frag.graph["no_nodes_validated"] = G_of_frag.number_of_nodes()
    logger.info("*********************************")
    if measured_masses_type == MesureMassesTypes.few:
        logger.info(
            "Identification done separately for each maximal fragment, total % "
            "{:.2f}".format(percent_sum_signal)
        )
    else:
        logger.info(
            f"***%Sum signal after {no_iteration} iterations: {percent_sum_signal:.2f}"
        )
    logger.info(
        "knapsack solutions:        "
        + str(G_of_frag.graph["no_nodes_knapsack"])
        + "\t"
        + str(100)
        + " %"
    )
    logger.info(
        "nodes removed, < LOD:      "
        + str(G_of_frag.graph["no_nodes_below_LOD"])
        + "\t"
        + str(
            G_of_frag.graph["no_nodes_below_LOD"]
            / G_of_frag.graph["no_nodes_knapsack"]
            * 100
        )
        + " %"
    )
    logger.info(
        "nodes removed, singletons: "
        + str(G_of_frag.graph["no_nodes_singletons"])
        + "\t"
        + str(
            G_of_frag.graph["no_nodes_singletons"]
            / G_of_frag.graph["no_nodes_knapsack"]
            * 100
        )
        + " %"
    )
    logger.info(
        "nodes not optimised:       "
        + str(G_of_frag.graph["no_nodes_not_optimised"])
        + "\t"
        + str(
            G_of_frag.graph["no_nodes_not_optimised"]
            / G_of_frag.graph["no_nodes_knapsack"]
            * 100
        )
        + " %"
    )
    logger.info(
        "nodes validated:           "
        + str(G_of_frag.graph["no_nodes_validated"])
        + "\t"
        + str(
            G_of_frag.graph["no_nodes_validated"]
            / G_of_frag.graph["no_nodes_knapsack"]
            * 100
        )
        + " %"
    )
    logger.info("*********************************")

    logger.info("***Most likely fragments***")

    graph_order_by_att_likelihood(G_of_frag, cl_comp)
    # runtime_total = time.time() - t0_total

    mol_ion_nodes = []
    # idx_new_max_nodes = []
    # this variable was used before modification to treat several spectra together.
    # It is not the best way of storing info as several infos are lost:
    # for which spectrum is it a molecular ion?
    # which adduct was removed?
    # which atom was added?
    for i_spec in range(cl_comp.no_spectra):
        if G_of_frag.graph["mol_ion_nodes_per_spectrum"][i_spec] is not None:
            for node in G_of_frag.graph["mol_ion_nodes_per_spectrum"][i_spec]:
                if node not in mol_ion_nodes:
                    mol_ion_nodes.append(node)

    # G_of_frag.graph['maximal_nodes']
    candidate_mol_ion_formulae = [
        G_of_frag.nodes[key_node]["node_lite"].iso.frag_abund_formula
        for key_node in mol_ion_nodes
    ]
    candidate_mol_ion_formulae_str = ""

    if G_of_frag.number_of_nodes() == 0:
        logger.error(
            "There are no solutions. Consider adding other elements or increasing the mass uncertainty."
        )

    else:

        # MYGO DOTO WIP 2022.10.16
        # clean-up result class!
        # save molecular ions reconstructed for each spectrum separately.

        # extract the remaining maximal nodes
        nodes_maximal_fragments = G_of_frag.graph["maximal_nodes"]
        # max_frag_ranks = []
        # mol_ion_ranks = []

        for idx_node in range(G_of_frag.number_of_nodes()):
            node = G_of_frag.nodes_ordered_by_likelihood[idx_node]
            node_lite = G_of_frag.nodes[node]["node_lite"]
            results.all_fragments.append(node_lite)
            results.all_fragments_ranking.append(idx_node + 1)

            if node in nodes_maximal_fragments:
                # add it to the output to whow the maximal fragments
                results.max_fragments.append(node_lite)
                results.max_fragments_ranking.append(idx_node + 1)

        logger.info("****************************************")
        logger.info(f"{len(results.max_fragments)} most likely maximal fragments(s)")
        logger.info(
            "\n" + nodes_to_str(results.max_fragments, results.max_fragments_ranking)
        )

        # Loop over each spectrum
        for i_spec in range(cl_comp.no_spectra):
            mol_ion_rank = 0
            logger.info("****************************************")
            logger.info(
                "\t".join(["i_spec", "ranking", "formula", "DBE", "likelihood"])
            )
            if G_of_frag.graph["mol_ion_nodes_per_spectrum"][i_spec] is not None:

                for idx_node in range(G_of_frag.number_of_nodes()):
                    node = G_of_frag.nodes_ordered_by_likelihood[idx_node]
                    node_lite = G_of_frag.nodes[node]["node_lite"]
                    if node in G_of_frag.graph["mol_ion_nodes_per_spectrum"][i_spec]:
                        mol_ion_rank += 1
                        results.most_likely_mol_ions.append(node_lite)
                        results.most_likely_mol_ions_i_spec.append(i_spec)
                        if sum(cl_comp.spectra_adduct_frag[i_spec]) == 0:
                            adduct_formula = "None"
                        else:
                            # use "normal" atom names instead of special atom names
                            adduct_frag = [0] * chem_data.len_frag
                            for i_a in range(len(cl_comp.spectra_adduct_frag[i_spec])):
                                if cl_comp.spectra_adduct_frag[i_spec][i_a] > 0:
                                    adduct_frag[
                                        chem_data.dict_adduct_to_element[i_a]
                                    ] = cl_comp.spectra_adduct_frag[i_spec][i_a]
                                    adduct_frag[i_a] = 0
                            adduct_formula = chem_data.get_string_formula(adduct_frag)
                        results.most_likely_mol_ions_spec_adduct.append(adduct_formula)
                        results.most_likely_mol_ions_ranking.append(mol_ion_rank)

                        logger.info(
                            "\t".join(
                                [
                                    str(i_spec),
                                    str(mol_ion_rank),
                                    node_lite.iso.frag_abund_formula,
                                    str(node_lite.iso.DBE),
                                    "{:.2f}".format(node_lite.subgraph_likelihood),
                                ]
                            )
                        )

                        # Add the string formula to the candidates
                        node_formula = chem_data.get_string_formula_sub(
                            node_lite.iso.frag_abund
                        )
                        candidate_mol_ion_formulae_str += node_formula + "; "

            logger.info(
                f"{mol_ion_rank} Most likely molecular ion(s) for spectrum of index {i_spec}"
            )
            logger.info(
                "\n"
                + nodes_to_str(
                    results.most_likely_mol_ions,
                    results.most_likely_mol_ions_ranking,
                    results.most_likely_mol_ions_i_spec,
                    results.most_likely_mol_ions_spec_adduct,
                )
            )

        if measured_masses_type != MesureMassesTypes.few and show_plt_figures:
            plot_graph_of_frag(G_of_frag)

    # Store in the results
    results.mol_ion_formulae = candidate_mol_ion_formulae
    results.mol_ion_formulae_str = candidate_mol_ion_formulae_str

    return results
