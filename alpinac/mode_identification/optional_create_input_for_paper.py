import logging
import alpinac.periodic_table as chem_data
from alpinac.compound import Compound
import networkx as nx


def optional_create_output_for_paper(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    nodes_maximal_fragments: list,
    formulas_path: str,
    path_file:str,
):
    logger = logging.getLogger('alpinac.mode_identfication.optional_create_output_for_paper')
    """Optional. Create an output file with the formula of the fragment, its mass, signal, likelihood, and ranking.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """ 
    fragment_results_step5 = []

    #for article: create list with
    #frag formula, Exact mass, Assigned signal, Likelihood, ranking & Max. element
    rank = 0
    for key_node in G_of_frag.nodes_ordered_by_likelihood:
    
        rank += 1
        #if key_node in nodes_maximal_fragments
        #is_maximal_frag = 'False'
        if key_node in nodes_maximal_fragments:
            is_maximal_frag = 'True'
        else:
            is_maximal_frag = 'False'

        node = G_of_frag.nodes[key_node]['node_lite']
        iso = node.iso
        fragment_results_step5.append([chem_data.get_string_formula_sub(iso.frag_abund),                               
                                    iso.frag_abund_mass,
                                    iso.iso_list_I_sum/cl_comp.sum_I*100.0,
                                    node.subgraph_sum_I/cl_comp.sum_I*100.0,
                                    node.subgraph_likelihood,
                                    rank,
                                    is_maximal_frag])

    frag_result_file_name = 'frag_res.' + path_file.stem + '.txt'
    frag_result_file = formulas_path / frag_result_file_name
    with frag_result_file.open('w') as file:
        file.write('formula'+ '\t' +
                    'correct'+ '\t' +
                    'mass'+ '\t' +
                    'iso contrib. to meas. signal'+ '\t' +
                    'att contrib. to meas. signal'+ '\t' +
                    'likelihood'+ '\t' +
                    'rank'+ '\t' +
                    'max. el.'+ '\n')
        for i in range(len(fragment_results_step5)):
            file.write(str(fragment_results_step5[i][0] + "\Sup{+}" ) + '\t' + '&' + '\t' +
                        #str(fragment_results_step5[i][1]) + '\t' + '&' + '\t' +
                        '{:.5f}'.format(fragment_results_step5[i][1]) + '\t' + '&' + '\t' +
                        '{:.1f}'.format(fragment_results_step5[i][2]) + '\t' + '&' + '\t' +
                        '{:.1f}'.format(fragment_results_step5[i][3]) + '\t' + '&' + '\t' +
                        '{:.1f}'.format(fragment_results_step5[i][4]) + '\t' + '&' + '\t' +
                        str(fragment_results_step5[i][5]) + '\t' + '&' + '\t' +
                        str(fragment_results_step5[i][6]) + '\t' + '\\' +'\n')

    logger.info("Outputfile including fragment information written to "+formulas_path)