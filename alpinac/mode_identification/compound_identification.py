import cProfile
import logging
import func_timeout
import matplotlib.pyplot as plt
import matplotlib
from alpinac import const_identification as const_id
from alpinac.abtract_outputs import PlotOutput
from alpinac.compound import Compound, plot_compund_peaks
from alpinac.mode_identification.output_saver import IdentificationOutput, IdentificationTookTooLongOutput
from alpinac.mode_identification.step1 import step1_knapsack
from alpinac.mode_identification.step2 import step2_directedgraph
from alpinac.mode_identification.step3 import step3_initialize_isotopologues
from alpinac.mode_identification.step4 import (
    step4_getting_maximal_fragment_contribution,
)
from alpinac.mode_identification.step5to8 import (
    step5to8_optimization,
    step5to8_optimization_underconstrained,
)
from alpinac.mode_identification.step9 import step9_eliminate_non_optimized_sets
from alpinac.mode_identification.step10 import step10_create_candidate_molecular_ion

from alpinac.mode_identification.run_times import RuntimesOutput
from alpinac.mode_identification.step_validation import step_validation
from alpinac.mode_identification.utils import MesureMassesTypes

from alpinac.utils_data_extraction import FitModes, FittingMethod




def _compound_identification(
    cl_comp: Compound,
    target_elements: str,
    target_formula: str,
    min_measured_mass: float,
    show_plt_figures: bool = False,
    do_validation: bool = False,
    expected_compound_name: str = None,
    max_opt_signal: float = const_id.max_opt_signal,
    fitting_method: FittingMethod = FittingMethod.continuous,
) -> IdentificationOutput:
    """Run the alpinac algorithm on one compound."""

    logger = logging.getLogger("alpinac._compound_identification")
    identification_output = IdentificationOutput()
    identification_output.cl_comp = cl_comp

    if not show_plt_figures:
        # since we don't plot the figures in an interactive way, we
        # can switch to a non-interactive backend (matplotlib) that
        # can only write to files. More info: https://matplotlib.org/stable/users/explain/backends.html
        matplotlib.use("agg")
    # plot original compound
    fig, ax = plt.subplots(figsize=(10, 5))  # Larger
    plot_compund_peaks(cl_comp, ax=ax)
    if show_plt_figures:
        plt.show()

    identification_output.plots.append(PlotOutput(fig, "original_coumpound"))


    # 20200131: here, before the knpasack,
    # it would be good to index as 'not used'
    # masses that are impossible
    # and result from ringing of the machine.
    # this could be done at least for relatively small fragments,
    # where we know a mass domain where nothing is possible.

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 1: KNAPSACK (cf. Fig. 1 in Guillevic et al. 2021)              +
    # Generate all fragment formulae matching the measured masses.        +
    # Use most abundant isotopes only.                                    +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    logger.info("STEP 1: KNAPSACK")
    logger.info(f"*** Fragment index: {cl_comp.fragment_index} ***")
    (
        all_solutions,
        total_DBE_solutions,
    ) = step1_knapsack(
        cl_comp, target_elements=target_elements, target_formula=target_formula
    )

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 2: Initialise directed graph                                   +
    # Connect a fragment to closest, larger fragments.                    +
    # Eliminate singletons.                                               +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    """---------------------------------------------------------------------
    Now we have all abundant-isotope formulae matching the mass criteria
    and the double-bound-equivalent criteria.

    We want to eliminate all wrong formulae.
    at step2, we build the graph with all nodes.
    On the directed graph with all formulas, the formula should not be a singleton
    otherwise it is eliminated.

    Now we prepare the directed graph. To minimise the comparisons when building the graph,
    the fragments are expected to be given by decreasing mass. It is very important,
    otherwise the algorithm of graph initialisation is wrong:
    add_nodes_and_edges_to_graph_from_list_fragments
    ---------------------------------------------------------------------"""

    logger.info("STEP 2: Initialise directed graph")
    G_reversed_edges, plots_output = step2_directedgraph(
        total_DBE_solutions=total_DBE_solutions,
        all_solutions=all_solutions,
        cl_comp=cl_comp,
        show_plt_figures=show_plt_figures,
    )
    identification_output.plots.extend(plots_output)

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 3: Initialise isotopocule sets                       +
    # Generate rare-isotope formulae.                                    +
    # compute isotopocule profiles.                                      +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    # TODO properties of G_reversed_edges will be changed within
    logger.info("STEP 3: Initialise isotopologue sets")
    G_reversed_edges = step3_initialize_isotopologues(G_reversed_edges, cl_comp)
    identification_output.G_reversed_edges = G_reversed_edges

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 4: Compute max. contribution of each set             +
    # for each isotopocule set, individually.                            +
    # Prepare for computation of likelihood estimator.                   +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    logger.info(
        "STEP 4: Compute max. contribution of each set of isotopologues to exp. signal"
    )
    G_of_frag = step4_getting_maximal_fragment_contribution(
        G_reversed_edges=G_reversed_edges,
        const_id_min_detect_mass=min_measured_mass,
    )

    identification_output.G_of_frag = G_of_frag

    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # ALPINAC: START OF ITERATIVE LOOP                                  +
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    if cl_comp.meas_len < const_id.min_no_masses:
        # TODO BUG no_iteration is not generated for this condition!!
        # So if this part of loop is exc. there will be an error below. Ask Lionel what to do.
        # less than 6 measured masses. This is an under-constrained system.
        measured_masses_type = MesureMassesTypes.few
        (
            G_of_frag,
            nodes_maximal_fragments,
            percent_sum_signal,
            sum_signal,
            no_iteration,
        ) = step5to8_optimization_underconstrained(
            G_of_frag=G_of_frag,
            cl_comp=cl_comp,
            const_id_max_opt_signal=max_opt_signal,
            fitting_method=fitting_method,
        )

    else:
        # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        # for cases with at least 6 measured masses
        # do not work with maximal fragments, start with most likely fragments.
        measured_masses_type = MesureMassesTypes.enough
        (
            G_of_frag,
            nodes_maximal_fragments,
            percent_sum_signal,
            sum_signal,
            no_iteration,
        ) = step5to8_optimization(
            G_of_frag=G_of_frag,
            cl_comp=cl_comp,
            const_id_max_opt_signal=max_opt_signal,
            all_solutions=all_solutions,
            fitting_method=fitting_method,
        )

    identification_output.nodes_maximal_fragments = nodes_maximal_fragments
    identification_output.percent_sum_signal = percent_sum_signal

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 9: Eliminate not optimised sets                                +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logger.info("STEP 9: Eliminate not optimised sets.")
    # remove all nodes that have not been optimised
    # This is done for all cases, with more or less than 6 measured masses.
    G_of_frag = step9_eliminate_non_optimized_sets(
        G_of_frag=G_of_frag,
        cl_comp=cl_comp,
        const_id_max_opt_signal=max_opt_signal,
        percent_sum_signal=percent_sum_signal,
        fitting_method=fitting_method,
    )

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # STEP 10: create candidate molecular ion(s)                          +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logger.info("STEP 10: create candidate molecular ion(s)  ")
    # Guessing the molecular ion is not a piece of cake.
    # We do that only now, using validated maximal elements
    # We create extra molecular formula and add to the graph,
    # without searching to match measured masses.

    # A node may be a max element in the set of validated but not in the total set.
    # So we need to remove all non-validated nodes first,
    # and update the list of maximal nodes.

    # take one max element with max likelihood.
    # If DBE is an even number,
    # this may be a molecular ion, do not add more atoms.
    # If DBE is an odd number,
    # it needs at least one more atom to be a molecular ion.

    reconstruction_results = step10_create_candidate_molecular_ion(
        G_of_frag=G_of_frag,
        cl_comp=cl_comp,
        show_plt_figures=show_plt_figures,
        nodes_maximal_fragments=nodes_maximal_fragments,
        min_measured_mass=min_measured_mass,
        percent_sum_signal=percent_sum_signal,
        measured_masses_type=measured_masses_type,
        no_iteration=no_iteration,
    )
    identification_output.reconstruction_results = reconstruction_results

    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # EXTRA STEP: VALIDATION (if filename part of validation set)    +
    # The reconstructed fragments are compared                       +
    # to theoretically possible fragments, with re-arrangements.     +
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    # TODO optional, should be removed in a final version
    # In case a solution is found:
    if do_validation and G_of_frag.number_of_nodes() != 0 and False:
        validation_dict_results = step_validation(
            G_of_frag=G_of_frag,
            cl_comp=cl_comp,
            show_plt_figures=show_plt_figures,
            compound_name=expected_compound_name,
            idx_new_max_nodes=reconstruction_results.new_max_nodes,
            old_max_nodes=reconstruction_results.old_max_nodes,
            sum_signal=sum_signal,
        )
        identification_output.validation_dict_results = validation_dict_results


    return identification_output





def compound_identification(
    cl_comp: Compound,
    target_elements: str,
    target_formula: str,
    min_measured_mass: float,
    show_plt_figures: bool = False,
    do_validation: bool = False,
    expected_compound_name: str = None,
    max_opt_signal: float = const_id.max_opt_signal,
    fitting_method: FittingMethod = FittingMethod.continuous,
    timeout_time: float | None = None,
    compound_identification_function: callable = _compound_identification,
) -> IdentificationOutput:
    """
    Run the alpinac algorithm on one compound with time constrain, if possible.

    If the show_plt_figures, the time limit will be ignored and the compound will take
    as much time as it has to. Otherwise, the identification will take at most
    `timeout_time` seconds and the plot will only be written to files using
    Matplotlib renderer `agg` (more info: https://matplotlib.org/stable/users/explain/backends.html).
    This is done because the packages `matplotlib.pyplot` and `func_timeout` have
    some problems with each other and the main thread ends up outside the main loop.
    """
    logger = logging.getLogger("alpinac.compound_identification")
    
    args = [
        cl_comp,
        target_elements,
        target_formula,
        min_measured_mass,
        show_plt_figures,
        do_validation,
        expected_compound_name,
        max_opt_signal,
        fitting_method,
    ]
    
    # start profiler for compound
    pr = cProfile.Profile()
    pr.enable()
    try:
        if show_plt_figures or timeout_time is None:
            if timeout_time is not None:
                logging.warning(
                    "You can not show plots and have a timeout for each compound: there will be no Timeout."
                )


            identification_output = compound_identification_function(*args)
        else:
            identification_output = func_timeout.func_timeout(
                timeout_time,
                compound_identification_function,
                args=args,
            )
    except func_timeout.FunctionTimedOut:
        logger.warning(
            f"Compound number {cl_comp.fragment_index} took more than {timeout_time/60} minutes to run."
        )
        identification_output= IdentificationTookTooLongOutput(timeout_time=timeout_time)
    
    except Exception as e:
        logger.error(f"Error in compound number {cl_comp.fragment_index}.")
        logger.exception(e)
        identification_output = IdentificationOutput()

    # stopping profiling and print out results for steps
    pr.disable()
    identification_output.runtimes = RuntimesOutput(pr)
    
    return identification_output