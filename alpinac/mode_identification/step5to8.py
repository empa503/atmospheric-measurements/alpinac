import logging
from alpinac.compound import Compound
import networkx as nx
from alpinac.isotopologue import Isotopologues
from alpinac.utils_data_extraction import FitModes, FittingMethod
from alpinac.utils_graph import graph_optimise_isotopologues, graph_order_by_att_likelihood, graph_percent_sum_signal
import alpinac.const_identification as const_id


def step5to8_optimization_underconstrained(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    const_id_max_opt_signal: float,
    fitting_method: FittingMethod
) -> tuple[nx.DiGraph, list, float, float, int]:
    logger = logging.getLogger('alpinac.mode_identfication.step5to8_optimization_underconstrained')
    """#TODO:here should be a 1 line sentence to briefly explain the function.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """ 


    #less than 6 measured masses. This is an under-constrained system.
    logger.info("***There are less than 6 measured masses. Treating each maximal fragment separately.***")
    #do not fit all solutions together, the risk that a bad one excludes the correct one is too high.
    #optimise each maximal fragment separately.
    #TODO: kaho: needed to add it, because expected as input in later stages of the program
    no_iteration = 0

    nodes_maximal_fragments = G_of_frag.graph['maximal_nodes'] # this gives a list of node numbers, for maximal nodes
    logger.info("maximal nodes are: {}".format(" ".join([G_of_frag.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in nodes_maximal_fragments])))

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # ALPINAC: MODE FEW MASSES, STEP 5 TO 8                          +
    # OPTIMISE EACH MAXIMAL FRAGMENT WITH ALL ITS CHILDREN           +
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logger.info('ALPINAC: MODE FEW MASSES, STEP 5 TO 8')
    #runtime_step7_opt_loop = 0.0
    #TODO: ask myriam if this is really adding the children
    #       We thought they are not validated in the few mass mode
    for node_max in nodes_maximal_fragments:
        G_of_frag.graph['nodes_validated'] = []
        if node_max in G_of_frag and node_max is not None:
            logger.info('Maximal fragment: ' + G_of_frag.nodes[node_max]['node_lite'].iso.frag_abund_formula)
            G_of_frag.graph['nodes_validated'].append(node_max)
            set_subfragments = nx.descendants(G_of_frag, node_max)
            for val in set_subfragments:
                if val not in G_of_frag.graph['nodes_validated'] and val is not None:
                    G_of_frag.graph['nodes_validated'].append(val)
            logger.info('all nodes of maximal fragment, incl. max.: ' + str(len(G_of_frag.graph['nodes_validated'])))

            #t0_step7_opt_loop = time.time()
            graph_optimise_isotopologues(
                G_of_frag,
                cl_comp,
                const_id.ppm_mass_res,
                plot_mode = False,
                fitting_method=fitting_method
            )
            #runtime_step7_opt_loop += time.time() - t0_step7_opt_loop

    nodes_maximal_fragments = G_of_frag.graph['maximal_nodes'] # this gives a list of node numbers, for maximal nodes
    logger.info("+++maximal fragments are: {}".format(" ".join([G_of_frag.nodes[node_i]['node_lite'].iso.frag_abund_formula for node_i in nodes_maximal_fragments])))

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # ALPINAC: MODE FEW MASSES, STEP 6: Select most likely max fragment  +
    # and all its children, mark all others as "not validated"           +
    # Add max frag and children until signal reaches thereshold.         +
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logger.info('ALPINAC: MODE FEW MASSES, STEP 6: Select most likely max fragment')
    graph_order_by_att_likelihood(G_of_frag, cl_comp)

    G_of_frag.graph['nodes_validated'] = []
    sum_signal = float(0.0)
    percent_sum_signal = float(0.0)
    idx_node = 0

    while idx_node < len(G_of_frag.nodes_ordered_by_likelihood) and percent_sum_signal <= const_id_max_opt_signal:
        #take the most likely maximal node
        node_max = G_of_frag.nodes_ordered_by_likelihood[idx_node]
        logger.info("node_max  " + str(node_max))
        if node_max in nodes_maximal_fragments and node_max not in G_of_frag.graph['nodes_validated']:
            G_of_frag.graph['nodes_validated'].append(node_max)
            #logger.info("nodes_validated   " + str(G_of_frag.graph['nodes_validated']))

            set_subfragments = nx.descendants(G_of_frag, node_max)
            for val in set_subfragments:
                if val not in G_of_frag.graph['nodes_validated'] and val is not None:
                    G_of_frag.graph['nodes_validated'].append(val)
            

            sum_signal, percent_sum_signal, no_optimised_masses, no_masses_to_optimise = graph_percent_sum_signal(G_of_frag, cl_comp.sum_I)
            logger.info("Mode few masses, percent_sum_signal  " + str(percent_sum_signal))
        idx_node += 1
    logger.info("+++Mode few masses, nodes_validated:   " + str(G_of_frag.graph['nodes_validated']))

    return G_of_frag, nodes_maximal_fragments, percent_sum_signal, sum_signal, no_iteration


def step5to8_optimization(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    const_id_max_opt_signal: float,
    all_solutions: list[list[str]],
    fitting_method: FittingMethod,
) -> tuple[nx.DiGraph, list, float, float, int]:
    logger = logging.getLogger('alpinac.mode_identfication.step5to8_optimization')
    """#TODO:here should be a 1 line sentence to briefly explain the function.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """ 

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # ALPINAC: STEP 5: Rank fragments                                    +
    # Per fragment including all its children:                           +
    # rank according to likelihood estimator                             +
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    logger.info('ALPINAC: STEP 5: Rank fragments')
    graph_order_by_att_likelihood(G = G_of_frag, cl_comp = cl_comp) # TODO Aurore rename as graph_order_nodes_by_likelihood

    #all existing nodes are now ordered by decreasing likelihood in G.nodes_ordered_by_likelihood
    
    #for article: write output in file
    nodes_maximal_fragments = G_of_frag.graph['maximal_nodes'] # this gives a list of node numbers, for maximal nodes

    """
    optional_create_output_for_paper(
        G_of_frag=G_of_frag,
        cl_comp=cl_comp,
        nodes_maximal_fragments=nodes_maximal_fragments,
        formulas_path=formulas_path,
        path_file=path_file
    )
    """
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # ALPINAC: NOW ENTERING LOOP,                                    +
    # STEPS 5 TO 8                                                   +
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    runtime_step7_opt_loop = 0

    #now fit several fragment formulas together,
    #each fragment added to the list of node one after the other,
    #taking all existing children per fragment

    #INITIALISATION OF LOOP
    list_optimised_masses = []
    no_iteration = 0
    no_added_nodes = 0
    sum_signal = 0.0
    percent_sum_signal = 0.0
    ratio_not_opt_masses = sum([1 for v in all_solutions if len(v)>0])
    not_optimised_masses = True


    #the loop goes on as long as:
        #the number of "validated" nodes is less than the number of nodes in G
        #as safety: the number of added nodes is less than a fixed value (to improve: threshold set as function of no. meas masses?)
        #the minimum fraction of fitted signal is not reached
        #the minimum fraction of not optimised measured masses

    while (
        len(G_of_frag.graph['nodes_validated']) < G_of_frag.number_of_nodes() 
        and not_optimised_masses 
        and no_added_nodes < const_id.max_added_nodes
        and (
            percent_sum_signal <= const_id_max_opt_signal
            or ratio_not_opt_masses > const_id.max_ratio_not_opt_masses
        )
    ):

        nx.draw(G_of_frag)

        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        #mygu 20201005
        #if the percentage of explained signal has reached the pre-defined set threshold (e.g., 95 percent),
        #but the fraction of measured masses having a validated node assigned to them has not reached its set threshold (e.g., 0.9),
        #the likelihood list is modified to make sure that at the upcoming step 6, 
        #a new candidate node assigned to a still-not-explained mass will be selected to enter the optimisation loop.
        #This may not be an optimal solution and may be changed or deleted later.
        #Re-order the likelihood list, by selecting first the nodes corresponding to a non-explained measured mass,
        #(note that all these nodes are still ordered by decreasing likelihood),
        #and put afterwards all the other nodes (still in their own decreasing likelihood order).
        if (
            percent_sum_signal > const_id_max_opt_signal 
            and not_optimised_masses
        ):
            logger.info("===Sum signal > " + str(const_id_max_opt_signal) + " , now adding masses of non-fitted measured masses")
            nodes_of_not_opt_masses = []

            for key_node in G_of_frag.nodes_ordered_by_likelihood:
                # Test if we add nodes corresponding to a non-explained mass
                test_add = True
                

                iso = G_of_frag.nodes[key_node]['node_lite'].iso
                i_spec = 0
                while (i_spec < cl_comp.no_spectra and test_add):
                    # Iterate over the masses
                    # Stop iterating when a mass is met in one spectra
                    # TODO: ask if this is the desired behaviour
                    
                    #idx_m_to_add < len(iso.meas_mass_idx[i_spec])#[i_spec]
                    # Check all the measured mass indexes if one of them has a non measured mass
                    idx_m_to_add = 0
                    while idx_m_to_add < len(iso.meas_mass_idx[i_spec]) and test_add:
                    #for i_spec in range(cl_comp.no_spectra):
                    #Old: for i_m in range(len(iso.meas_mass_idx[idx_m_to_add])):
                        if (
                            #TODO: check with https://gitlab.empa.ch/abt503/climate-gases/halosearch/-/blob/0ba04e42851d69298e8241204920507aff819491/mode_identification.py
                            iso.meas_mass_idx[i_spec][idx_m_to_add] is not None
                            and iso.meas_mass_idx[i_spec][idx_m_to_add] in list_optimised_masses
                        ):
                            test_add = False
                        idx_m_to_add += 1
                    i_spec += 1
                if test_add:
                    nodes_of_not_opt_masses.append(key_node)


            #logger.info("Added nodes of non-opt masses: " + str(len(nodes_ordered_by_likelihood_new)))
            #now add next all remaining nodes:
            if len(nodes_of_not_opt_masses) > 0:
                logger.info('{:.2f}'.format(ratio_not_opt_masses)  + " frac. not opt. masses; " + str(len(nodes_of_not_opt_masses)) + " node(s) of non-optimised masses to be added.")
                for i_add_node in range(G_of_frag.number_of_nodes()):
                    if G_of_frag.nodes_ordered_by_likelihood[i_add_node] not in nodes_of_not_opt_masses:
                        # Keep the old likelyhood order 
                        nodes_of_not_opt_masses.append(G_of_frag.nodes_ordered_by_likelihood[i_add_node])
            else:
                logger.info("WARNING! There are no nodes of non-optimise masses to add.")

                # Reorder the nodes likelyhood
                G_of_frag.nodes_ordered_by_likelihood = nodes_of_not_opt_masses.copy()
        #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        # ALPINAC STEP 6: Select most likely fragments                       +
        # and all their children                                             +
        #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        logger.info('ALPINAC STEP 6: Select most likely fragments')
        #the list of nodes ordered by likelihood potentially changes at each iteration
        #we want to add the most likely node not yet added
        #we need to check the entire list of added nodes
        i_add = 0
        node_added = None
        added_node = 0
        while (
            i_add < G_of_frag.number_of_nodes() 
            # As long as we don't select a node
            and added_node == 0
        ):
            # Try to select the next more likely node
            candidate_node = G_of_frag.nodes_ordered_by_likelihood[i_add]

            # Condition to know if we can take this node
            if (
                candidate_node in G_of_frag
                and candidate_node not in G_of_frag.graph['nodes_validated'] 
                and candidate_node is not None
            ):
                # Take the node
                added_node += 1
                no_iteration += 1
                node_added = candidate_node
            else:
                # Will check for the next node
                i_add +=1

        if node_added is None:
            logger.info('ALPINAC STEP 6: No addequate candidate found as most likely fragments')
        else:

            G_of_frag.graph['nodes_validated'].append(node_added)
            #logger.info('Nodes valid. at l. 478: ' + str(G_of_frag.graph['nodes_validated']))
            #logger.info('list_optimised_masses: ' + str(list_optimised_masses))

            #update list of validated masses
            #not sure this is the right place, what if the node is deleted after?
            for key_node in G_of_frag.graph['nodes_validated']:
                iso: Isotopologues = G_of_frag.nodes[key_node]['node_lite'].iso
                for i_iso in range(iso.iso_list_len):
                    # Looks at all the isotopologues of known substances
                    for i_spec in range(cl_comp.no_spectra):
                        # Check each spectra that has this isotopologue
                        iso_value_in_spec = iso.meas_mass_idx[i_spec][i_iso]
                        if (
                            # Isotopologue for this must exist
                            iso_value_in_spec is not None 
                            # The isotopologue has not been already added to the list
                            and iso_value_in_spec not in list_optimised_masses
                        ):
                            list_optimised_masses.append(iso_value_in_spec)

            # Get the child fraaments
            set_subfragments = nx.descendants(G_of_frag, node_added)
            for val in set_subfragments:
                # Adds node that were not already validated
                if val not in G_of_frag.graph['nodes_validated'] and val is not None:
                    G_of_frag.graph['nodes_validated'].append(val)
                    added_node += 1

            #logger.info(str([G_of_frag.nodes[val]['node_lite'].iso.frag_abund_formula for val in set_subfragments]))
            no_added_nodes += added_node
            logger.info('****************************************')
            logger.info('Added node: ' + str(G_of_frag.nodes[node_added]['node_lite'].iso.frag_abund_formula) + " and " + str(added_node-1) + " childrens. Total: " + str(no_added_nodes))


            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # ALPINAC STEP 7: Optimise multiple isotopocule sets                 +
            # Optimise contribution of isotopocule sets to fit measured profile  +
            # Use Python package lmfit                                           +
            # Eliminate sets < LOD.                                              +
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            logger.info('ALPINAC STEP 7: Optimise multiple isotopocule sets ')




            #t0_opt_loop = time.time()
            graph_optimise_isotopologues(
                G_of_frag,  
                cl_comp, 
                const_id.ppm_mass_res, 
                plot_mode=False,
                fitting_method=fitting_method
            )
            #runtime_step7_opt_loop += (time.time() - t0_opt_loop)

            #update ranking of nodes
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            # ALPINAC STEP 8: Update directed graph                              +
            # Eliminate (singletones, or not connected) and reconnect nodes      +
            # which are not eliminated                                           +
            #    TODO: ask Myriam, if interpr. correctly                         +
            #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            logger.info('ALPINAC STEP 8: Updating directed graph ')
            graph_order_by_att_likelihood(G_of_frag, cl_comp)


            #***************************************
            #Compute explained sum signal
            sum_signal, percent_sum_signal, no_optimised_masses, no_masses_to_optimize = graph_percent_sum_signal(G_of_frag, cl_comp.sum_I)
            ratio_not_opt_masses = float(no_masses_to_optimize-no_optimised_masses)/float(no_masses_to_optimize)
            if no_optimised_masses >= no_masses_to_optimize:
                #if no_masses_to_optimize == 0:
                not_optimised_masses = False

            #**************************************
            #compute likelihood of being present for each chemical element
            #chem_elem_likelihood= [float(0)]*len(G_of_frag.nodes[max_node]['node_lite'].iso.frag_abund
            

            logger.info('***Fraction of not optimised masses: ' + '{:.4f}'.format(ratio_not_opt_masses))
            logger.info('***% Sum signal after ' + str(no_iteration) + ' iterations: ' + '{:.2f}'.format(percent_sum_signal))
            logger.info('masses: [optimised; solution(s) exist; measured] ' + str([no_optimised_masses, no_masses_to_optimize, cl_comp.meas_len]))

    return G_of_frag, nodes_maximal_fragments, percent_sum_signal, sum_signal, no_iteration
