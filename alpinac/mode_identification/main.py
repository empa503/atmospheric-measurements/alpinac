"""The make identification here makes the identification directly from data specified throug python."""

from pathlib import Path
import time
import logging
from typing import Any

import func_timeout
from alpinac.compound import Compound

import alpinac.const_identification as const_id
from alpinac.mode_identification.compound_identification import (
    compound_identification,
)
from alpinac.mode_identification.output_saver import (
    IdentificationOutput,
    IdentificationTookTooLongOutput,
)

import numpy as np

from alpinac.utils_data_extraction import FitModes, FittingMethod


def make_identification(
    mass: list[float],
    mass_uncertainty: list[float] | float = 1.0,
    mass_cal_uncertainty: list[float] | float = 0.0,
    area: list[float] | float = 0.0,
    area_uncertainty: list[float] | float = 0.0,
    peak_width: list[float] | float = 10e-6,
    peak_alpha: list[float] | float = 0.0,
    retention_time: list[float] | float = 0.0,
    ionisation_type: list[str] | str = "EI",
    limit_of_detection: list[float] | float = 0.0,
    signal_to_noise_ratio: list[float] | float = 0.0,
    compound_index: list[int] = [],
    spectrum_id: list[int] = None,
    adduct: list[str | list[str]] | str = "",
    target_elements: str | None = None,
    target_formula: str = None,
    show_plt_figures: bool = False,
    run_id=None,
    min_measured_mass: float = 23.0,
    max_opt_signal: float = const_id.max_opt_signal,
    do_validation: bool = False,
    expected_compound_name: str = None,
    fitting_method: FittingMethod = FittingMethod.continuous,
    timeout_time=600,
) -> dict[int, IdentificationOutput]:
    """Make identification for data array.

    :arg mass: measured masses (ionised, not corrected for electron loss). Unit [m/z].
    :arg mass_uncertainty: The measurement uncertainty (standard deviation) of the mass. Unit [ppm].
    :arg mass_cal_uncertainty: The uncertainty on the mass calibration.
        This value should be obtained frm the mass calibration routine. Unit [ppm].
    :arg area: signal intensity, value already integrated
        in the mass domain and in the retention time domain.
    :arg area_uncertainty: experimental standard deviation, usually better than 1%. Not used so far.
    :arg peak_width: width of the peak in the mass domain, can be computed beforehand based on TOF data.
        Ths value is used as tolerance to estimate
        if a candidate mass can be matched to a measured mass
        (see class isotopologue, function find_idx_meas_mass_from_exact_mass).
    :arg peak_alpha: contribution of lorentian signal to peak shape in the mass domain.
    :arg retention_time: A list of the retention time for each peak.
        This is optional at the moment and not used by alpinac.
    :arg ionisation_type: "EI" for electron ionisation and "CI" for chemical ionisation.
    :arg limit_of_detection: this value should be estimated by the user, based on experimental data.
        This LOD value will be automatically set as not more than the smallest area in the dataset,
        but the LOD given here can also be much smaller than the smallest area in the dataset.
    :arg signal_to_noise_ratio: not used.
    :arg compound_index: A list of integers containing for each peak an integer id
        that specifies to which spectrum the peak is included.
        Negative values are ignored.
    :arg spectrum_id: ID of the spectrum (or spectra). Needed to identify which mass belong to which spectra.
        Note that several replicate measurements with the exact same ionisation settings can be treated together,
        or data assumed to belong to the same substance but measured with different ionisation settings.
    :arg adduct: for each mass, list of formula of the adduct(s).
        If a peak contained no adduct, it must be specified by the empty string "".
        If this is one formula, this is the "maximal" formula (e.g., H3) and alpinac will use H3, H2 or H.
    :arg target_elements: chemical element to use for the knapsack.
        If None, the default list is used.
    :arg target_formula: target chemical formula. This is used as the maximal possible formula.
    :arg min_measured_mass: The smallest possible measured mass.
        This is minimum mass that can be detected given
        the TOF experimental settings (high-pass filter settings).
        Only chemical formulae with a mass larger than this will be given by the algorithm.
    :arg do_validation: Whether to do the validation step.
    :arg expected_compound_name: The name of the compound expected for the
        validation.
    :arg timeout_time: Maximal amount of seconds that take the identification of a compound can take.

    :return identification_output: A dictionary mapping each compound_index
        given in the arguments to a :py:class:`IdentificationOutput`.
    """
    logger = logging.getLogger("alpinac.mode_identification")
    t_start = time.process_time()
    # Get the number of peacks given
    mass = np.array(mass)
    n_peaks = len(mass)
    logger.info(f"Received {n_peaks=}.")
    # A template error message
    wrong_len_message = "Wrong size for arg '{}'. Got {} expected " f"{n_peaks}."

    # Check the different arguments

    if isinstance(retention_time, float):
        retention_time = np.full(n_peaks, retention_time)

    if isinstance(mass_uncertainty, float):
        mass_uncertainty = np.full(n_peaks, mass_uncertainty)
    elif isinstance(mass_uncertainty, list):
        mass_uncertainty = np.array(mass_uncertainty)
    assert len(mass_uncertainty) == n_peaks, wrong_len_message.format(
        "mass_uncertainty", len(mass_uncertainty)
    )
    # convert mass from ppm to m/z
    mass_uncertainty = mass_uncertainty * 1e-6 * mass

    if isinstance(mass_cal_uncertainty, float):
        mass_cal_uncertainty = np.full(n_peaks, mass_cal_uncertainty)
    elif isinstance(mass_cal_uncertainty, list):
        mass_cal_uncertainty = np.array(mass_cal_uncertainty)
    assert len(mass_cal_uncertainty) == n_peaks, wrong_len_message.format(
        "mass_cal_uncertainty", len(mass_cal_uncertainty)
    )
    # convert mass from ppm to m/z
    mass_cal_uncertainty = mass_cal_uncertainty * 1e-6 * mass

    if isinstance(area, float):
        area = np.full(n_peaks, area)

    if isinstance(area_uncertainty, float):
        area_uncertainty = np.full(n_peaks, area_uncertainty)

    if isinstance(peak_width, float):
        peak_width = np.full(n_peaks, peak_width)
    if np.any(peak_width <= 0.0):
        raise ValueError(f"'peak_width' must be greater than 0.0 . Not {peak_width}")

    if isinstance(peak_alpha, float):
        peak_alpha = np.full(n_peaks, peak_alpha)

    if isinstance(ionisation_type, str):
        ionisation_type = np.full(n_peaks, ionisation_type)
    assert len(ionisation_type) == n_peaks, wrong_len_message.format(
        "ionisation_type", len(ionisation_type)
    )
    # Check that the value given are not false
    mask_valid_ionisations = (ionisation_type == "EI") | (ionisation_type == "CI")
    if not np.all(mask_valid_ionisations):
        raise ValueError(
            "Unkonwn value(s) for 'ionisation_type': "
            f"{ionisation_type[mask_valid_ionisations]}. "
            "Ionisation type must be either 'EI' or 'CI'."
        )

    if isinstance(limit_of_detection, float):
        limit_of_detection = np.full(n_peaks, limit_of_detection)

    if isinstance(signal_to_noise_ratio, float):
        signal_to_noise_ratio = np.full(n_peaks, signal_to_noise_ratio)

    # Assume it is only one compound
    if not compound_index:
        compound_index = [0] * n_peaks
    else:
        if len(compound_index) != n_peaks:
            raise ValueError(
                wrong_len_message.format("compound_index", len(compound_index), n_peaks)
            )

    # Extract the ids
    if spectrum_id is None:
        spectrum_id = np.zeros(n_peaks, dtype=int)
    list_frag_idx = [i for i in np.unique(spectrum_id) if i >= 0]
    logger.info(f"Found coumponds with ids: {list_frag_idx}")

    if run_id is None:
        run_id = np.zeros(n_peaks)
    if isinstance(adduct, str):
        adduct = [adduct] * n_peaks

    # Reconstruct the 'beautiful' batch_fragment dictionary
    # Maps the compound id (from list_frag_idx) to a list
    # of peaks data they contain (as a tuple where each position is a fragment)
    # The position can be found in alpinac.io_tools
    batch_fragments: dict[int, list[tuple[Any]]] = {}
    for index in range(n_peaks):
        # Add each peak to the batch fragment with info in correct order (idx)
        peak_data = [
            retention_time[index],
            mass[index],
            mass_uncertainty[index],
            mass_cal_uncertainty[index],
            area[index],
            area_uncertainty[index],
            peak_width[index],
            peak_alpha[index],
            spectrum_id[index],
            limit_of_detection[index],
            ionisation_type[index],
            adduct[index],
            run_id[index],
            signal_to_noise_ratio[index],
        ]
        spec_id = spectrum_id[index]
        if spec_id not in batch_fragments.keys():
            # Create an empty list if this index is not already there
            batch_fragments[spec_id] = []
        batch_fragments[spec_id].append(tuple(peak_data))

    logger.debug(f"{batch_fragments=}")
    identification_outputs: dict[int, IdentificationOutput] = {}

    for compound_number in list_frag_idx:
        # Create the compound object
        cl_comp = Compound(
            one_batch_fragment=batch_fragments[compound_number],
            compound_number=compound_number,
        )

        logger.debug(f"Compound created : {cl_comp}")
        logger.debug(f"{cl_comp.meas_mass=}")
        logger.debug(f"{cl_comp.meas_mass_min=}")
        logger.debug(f"{cl_comp.meas_mass_max=}")

        try:
            identification_outputs[
                compound_number
            ] = compound_identification(
                cl_comp,
                target_elements,
                target_formula,
                min_measured_mass,
                show_plt_figures,
                do_validation,
                expected_compound_name,
                max_opt_signal,
                fitting_method,
                timeout_time,
            )
        except func_timeout.FunctionTimedOut:
            logger.info(
                f"Compound {compound_number} took more than {timeout_time} seconds to be identified."
            )
            identification_outputs[compound_number] = IdentificationTookTooLongOutput(
                timeout_time
            )

    t_stop = time.process_time()
    logger.info("Duration: " + "{:.2f}".format(t_stop - t_start))

    return identification_outputs


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, handlers=[logging.StreamHandler()])
    logging.getLogger("matplotlib").setLevel(logging.WARNING)

    mz = [
        28.0303309,
        29.0378048,
        30.0458923,
        27.0223337,
        31.0494122,
        26.014517,
    ]
    m_u = 10 * np.array(
        [
            6.06,
            2.77,
            1.88,
            1.7,
            10.58,
            2.59,
        ]
    )
    m_cal_u = [
        2.52,
        3,
        3.45,
        2.6,
        3.87,
        2.7,
    ]
    area = [
        799697.83,
        209165.62,
        266008.67,
        213036.96,
        7055.63,
        92527.35,
    ]

    output = make_identification(
        mz,
        mass_uncertainty=m_u,
        mass_cal_uncertainty=m_cal_u,
        area=area,
        # target_elements="CHCl",
        # show_plt_figures=True,
        fitting_method=FittingMethod.discrete,
    )
    for key, out in output.items():
        print(out)
        out.save("data/test_outputs/test_main")
