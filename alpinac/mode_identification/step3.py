import logging
from alpinac.compound import Compound
from alpinac.utils_graph import initialise_isotopologue_set
import networkx as nx


def step3_initialize_isotopologues(
    G_reversed_edges: nx.DiGraph,
    cl_comp: Compound,
) -> nx.DiGraph:

    """#TODO:here should be a 1 line sentence to briefly explain the function.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """   
        
    # Set the logger for this function
    logger = logging.getLogger('alpinac.mode_identfication.step3_initialize_isotopologue_set')   
        
    #t0_step3_enum_iso = time.time()
    initialise_isotopologue_set(G_reversed_edges, cl_comp)
    logger.info("initialise_isotopologue_set done.")
    #runtime_step3_enum_iso = time.time() - t0_step3_enum_iso
    return G_reversed_edges