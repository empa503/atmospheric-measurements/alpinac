import logging
from alpinac.compound import Compound
import networkx as nx
import alpinac.periodic_table as chem_data
from alpinac.validation.validation_utils import compute_validation_results, find_validation_idx_from_database, validate_frag_list
from alpinac.validation.mol_validation import mol_validation


def step_validation(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    show_plt_figures: bool,
    compound_name: str,
    idx_new_max_nodes: list,
    old_max_nodes:list,
    sum_signal:float,
) -> nx.DiGraph:
    """#TODO:here should be a 1 line sentence to briefly explain the function.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """ 
    logger = logging.getLogger('alpinac.mode_identfication.step_validation')
    
    validation_dict_results = {}
    validation_dict_results["frac_correct_frag"]  = 0
    validation_dict_results["frac_true_signal_to_total_signal"] = 0
    validation_dict_results["frac_true_frag_top10"] = 0
    validation_dict_results["frac_true_signal_top10"] = 0
        
    logger.info('ALPINAC EXTRA STEP: VALIDATION (if filename part of validation set)  ')
    list_identified_fragments = [G_of_frag.nodes[key_node]['node_lite'].iso.frag_abund  for key_node in G_of_frag.nodes_ordered_by_likelihood if key_node not in idx_new_max_nodes]
    list_identified_nodes = [key_node for key_node in G_of_frag.nodes_ordered_by_likelihood if key_node not in idx_new_max_nodes]
    #logger.info(list_identified_fragments)

    list_checked = [None]*len(list_identified_fragments)

    val_name = "---"

    
    #find correct index in mol_validation, index 6 should be same as text string after val_ in filename
    idx_val = find_validation_idx_from_database(compound_name, mol_validation)
    if idx_val is not None:

        #for mol_tuple in mol_validation[idx_val]:
        #for mol_tuple in mol_validation:
        #load SMILES code of corresponding true molecule
        smiles = mol_validation[idx_val]["SMILES code"]
        mol_name = mol_validation[idx_val]["Sum formula"]

        str_sum_formula = chem_data.get_string_formula(chem_data.get_fragment_from_string_formula(mol_name))

        list_checked = validate_frag_list(smiles, mol_name, list_identified_fragments, draw_graph= show_plt_figures)
        #for i in range(len(list_identified_fragments)):
        #    logger.info(str(chem_data.get_string_formula(list_identified_fragments[i])) + "\t" + str(list_checked[i]))

        validation_dict_results, fragment_results = compute_validation_results(list_identified_nodes, list_checked, old_max_nodes, G_of_frag, sum_signal, cl_comp.sum_I)
        validation_dict_results["val_name"] = val_name
        #export_name = mol_validation[idx_val]["Compound"]

    return validation_dict_results