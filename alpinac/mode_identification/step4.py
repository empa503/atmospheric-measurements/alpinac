import logging
import networkx as nx

from alpinac.utils_graph import compute_all_subfragments_of_nodes_with_lists


def step4_getting_maximal_fragment_contribution(
    G_reversed_edges: nx.DiGraph,
    const_id_min_detect_mass: float,
) -> nx.DiGraph:
    """Compute the optimum signal contribution from each set of isotopologues.

    Potentially, any candidate chemical formula may contribute
    to the measured intensity profile along the mass
    axis. This step optimises the optimum
    contribution from each node taken individually
    (including all minor isotopologues), to match
    the measured mass profile.

    :arg G_reversed_edges: graph of nodes.
    :arg const_id_min_detect_mass: The smallest possible measured mass.
        This is minimum mass that can be detected given
        the experimental settings of the detector (high-pass filter settings).
        Only chemical formulae with a mass larger than this will be produced by the algorithm.

    :return: G_of_frag.

    """

    logger = logging.getLogger("alpinac.mode_identfication.step4_max_contrib_of_frag")
    logger.debug("min. detected mass = {}".format(const_id_min_detect_mass))

    # generate the list of all potential subfragments
    compute_all_subfragments_of_nodes_with_lists(
        G_reversed_edges, min_measured_mass=const_id_min_detect_mass
    )

    # now each node has a field G_reversed_edges.nodes[n]['node_lite'].subgraph_size and
    # G_reversed_edges.nodes[n]['node_lite'].no_all_subfragments

    ########################################################################
    # Now, change G_reversed_edges to G_of_frag so that the edges are now pointing to the larger fragments

    G_of_frag = G_reversed_edges
    G_of_frag.graph["added_nodes"] = list(G_reversed_edges.nodes)
    G_of_frag.graph["list_of_fitted_nodes"] = []

    return G_of_frag
