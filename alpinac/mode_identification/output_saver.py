import json
import operator
from enum import IntEnum
from pathlib import Path
from alpinac.abtract_outputs import OutputSaver, PlotOutput
from alpinac.compound import Compound
from alpinac.mode_identification.step10 import CandidateResults
from alpinac.mode_identification.run_times import RuntimesOutput
import alpinac.periodic_table as chem_data
import pandas as pd
import networkx as nx

from alpinac.utils_graph import get_list_identified_unidentified_mass

EXPAND_TABS = 25
RESULT_NOT_FOUND = -1


class Ires(IntEnum):
    PERC_NORM_I = 0
    IDX_M = 0
    RT = 1
    I = 2
    I_NORM = 3
    M_MEAS = 4
    M_EXACT = 5
    M_MEAS_U = 6
    M_DIFF = 7
    FORMULA = 8
    DBE = 9
    I_FRAG = 10
    I_FRAG_NORM = 11
    IONIZATION = 12
    ADDUCT = 13
    SPEC = 14


class IdentificationOutput(OutputSaver):
    """An output saver for alpinac."""

    runtimes: RuntimesOutput

    runfile: Path | None = None

    G_reversed_edges: nx.DiGraph
    G_of_frag: nx.DiGraph

    nodes_maximal_fragments: int = None
    percent_sum_signal: int = None

    cl_comp: Compound

    reconstruction_results: CandidateResults

    plots: list[PlotOutput]

    validation_dict_results: dict  # TODO define waht is exactly in there

    results_mygu_df: pd.DataFrame

    def __init__(self) -> None:
        self.results_mygu_df = pd.DataFrame(
            columns=[
                "perc_signal",
                "rt",
                "tot_intensity",
                "perc_tot_intensity",
                "meas_mass",
                "exact_mass",
                "exact_mass_u",
                "mass_diff",
                "formula",
                "DBE",
                "intensity",
                "intensity_fraction",
                "ionization_mode",
                "max_allowed_adduct",
                "spectra_name",
            ]
        )
        super().__init__()
        self.plots = []

    def save(self, dir: Path | str):
        dir = Path(dir)

        self._add_attribute_to_objects("runtimes")
        self._add_attribute_to_objects("reconstruction_results")

        super().save(dir)

        self._save_results_mygu_format(dir)
        self._save_performance(dir)
        self._save_txt_file_mz_I(dir)
        self._save_peaks(dir)

        for plot in self.plots:
            plot.save(dir)

    def _save_txt_file_mz_I(self, dir: Path):
        """Save the mz and I of the spectra in a txt file."""
        file = dir / "mz_I.txt"
        with open(file, "w") as f:
            for m, I in zip(self.cl_comp.meas_mass, self.cl_comp.meas_I):
                f.write(f"{m}\t{I}\n".expandtabs(20))

    def _save_peaks(self, dir: Path):
        """Save the data on all the peaks."""
        data = {
            "mz": self.cl_comp.meas_mass,
            "I": self.cl_comp.meas_I,
            "mass_u_ppm": self.cl_comp.meas_mass_U_combined_ppm,
            "ionization": self.cl_comp.ionisation,
            "formulae": [
                "/".join(group[1]["formula"].to_list())
                for group in self.results_mygu_df.groupby(
                    ["meas_mass", "tot_intensity"]
                )
            ],
        }
        lines = "\n".join(
            [
                "\t".join([str(data[key][i]) for key in data.keys()])
                for i in range(len(self.cl_comp.meas_mass))
            ]
        )
        file = dir / "peaks.txt"
        with open(file, "w") as f:
            f.write("\t".join(data.keys()).expandtabs(EXPAND_TABS) + "\n")
            f.write(lines.expandtabs(EXPAND_TABS))

    def _save_results_mygu_format(self, dir: Path):
        # logger.info('max mass index ' + str(cl_comp.meas_len))
        cl_comp = self.cl_comp
        G_of_frag = self.G_of_frag

        # First loop to extract which masses have been identified
        # Will store the result data in the lists
        results_found_list = []  # *len(meas_mass)
        results_not_found_list = []

        for i_node in self.G_of_frag.nodes():
            iso = self.G_of_frag.nodes[i_node]["node_lite"].iso

            for i_iso in range(len(iso.iso_list)):
                # found_meas_match = False
                candidate_mass = iso.iso_list_mass[i_iso]

                for i_spec in range(self.cl_comp.no_spectra):
                    # compute sum of given isotopocule in case it matches several measured masses
                    # sum_meas_I = sum([cl_comp.meas_I[idx_m] for idx_m in iso.meas_mass_idx[i_iso] if idx_m is not None])
                    # iso.iso_list_I_meas_max
                    adduct_name = "None"
                    spectra_name = self.cl_comp.spectra_name_list[i_spec]
                    if self.cl_comp.adduct_maxfrag_list[i_spec] is not None:
                        adduct_name = chem_data.get_string_formula(
                            self.cl_comp.adduct_maxfrag_list[i_spec]
                        )

                    I_meas_max = iso.iso_list_I_meas_max[i_spec][i_iso]
                    if I_meas_max == 0:
                        I_meas_max = float(1.0)
                    # for i_m in range(len(iso.meas_mass_idx[i_iso])):
                    #    idx_m = iso.meas_mass_idx[i_iso][i_m]

                    if iso.meas_mass_idx[i_spec][i_iso] is not None:
                        #    sum_meas_I = sum([cl_comp.meas_I[idx_m] for idx_m in iso.meas_mass_idx[i_iso]])
                        # for i_m in range(len(iso.meas_mass_idx[i_iso])):
                        idx_m = iso.meas_mass_idx[i_spec][i_iso]  # [i_m]
                        # found_meas_match = True
                        results_found_list.append(
                            [
                                idx_m,
                                cl_comp.meas_rt[idx_m],
                                cl_comp.meas_I[idx_m],
                                cl_comp.meas_I[idx_m] * 100.00 / cl_comp.meas_I_max,
                                cl_comp.meas_mass[idx_m],
                                candidate_mass,
                                cl_comp.meas_mass_U_combined[idx_m]
                                / cl_comp.meas_mass[idx_m]
                                * 10**6,
                                (candidate_mass - cl_comp.meas_mass[idx_m])
                                / cl_comp.meas_mass[idx_m]
                                * 10**6,
                                chem_data.get_string_formula(iso.iso_list[i_iso]),
                                iso.DBE,
                                iso.iso_list_I_opt[i_spec][i_iso]
                                * cl_comp.meas_I[idx_m]
                                / I_meas_max,
                                iso.iso_list_I_opt[i_spec][i_iso] / I_meas_max,
                                cl_comp.ionisation[idx_m],
                                adduct_name,
                                cl_comp.meas_mass_spectra_name[idx_m],  # spectra_name
                            ]
                        )
                        # how to still keep results were no solution was found?
                    # if not found_meas_match:
                    else:
                        results_not_found_list.append(
                            [
                                RESULT_NOT_FOUND,
                                0.0,
                                iso.iso_list_I_opt[i_spec][i_iso],
                                iso.iso_list_I_opt[i_spec][i_iso]
                                * 100.00
                                / cl_comp.meas_I_max,
                                0.0,
                                candidate_mass,
                                0.0,
                                0.0,
                                chem_data.get_string_formula(iso.iso_list[i_iso])
                                + ", not found",
                                iso.DBE,
                                iso.iso_list_I_opt[i_spec][i_iso],
                                0.0,
                                "--",
                                adduct_name,
                                spectra_name,
                            ]
                        )

        # weighted average retention time with the weights being the Intesities
        intensity_sum = sum(
            [
                results_found_list[i_res][Ires.I]
                for i_res in range(len(results_found_list))
            ]
        )
        if intensity_sum == 0:
            # Avoid division by 0
            intensity_sum = 1

        rt_average = (
            sum(
                [
                    results_found_list[i_res][Ires.RT]
                    * results_found_list[i_res][Ires.I]
                    for i_res in range(len(results_found_list))
                ]
            )
            / intensity_sum
        )

        # add unidientified masses to the list of fragments:
        (
            identified_mass_idx_list,
            unidentified_mass_idx_list,
        ) = get_list_identified_unidentified_mass(self.G_of_frag, cl_comp)

        for idx_m in unidentified_mass_idx_list:
            adduct_name = "None"
            adduct_idx = cl_comp.meas_mass_adduct_id[idx_m]
            if cl_comp.adduct_maxfrag_list[adduct_idx] is not None:
                adduct_name = chem_data.get_string_formula(
                    cl_comp.adduct_maxfrag_list[adduct_idx]
                )
            if hasattr(cl_comp, "meas_mass_u"):
                rel_u = cl_comp.meas_mass_u[idx_m] / cl_comp.meas_mass[idx_m] * 10**6
            else:
                # uncertainty defined as 2sigma (both sides)
                rel_u = 1 / cl_comp.meas_mass[idx_m] * 10**6
            results_not_found_list.append(
                [
                    idx_m,
                    cl_comp.meas_rt[idx_m],
                    cl_comp.meas_I[idx_m],
                    cl_comp.meas_I[idx_m] * 100.00 / cl_comp.meas_I_max,
                    cl_comp.meas_mass[idx_m],
                    0.0,
                    rel_u,
                    0.0,
                    "unidentified",
                    0.0,
                    cl_comp.meas_I[idx_m],
                    1.0,
                    cl_comp.ionisation[idx_m],
                    adduct_name,
                    cl_comp.meas_mass_spectra_name[idx_m],
                ]
            )

        # Merge the two lists
        results_list = results_found_list + results_not_found_list

        # Sort according to these indexes
        results_list.sort(
            key=operator.itemgetter(Ires.I_FRAG, Ires.I_FRAG_NORM),
            reverse=True,
        )

        out_file = dir / f"results_file_mygu.txt"

        with out_file.open("w") as out_formula_file:
            out_formula_file.write(f"Run file: {self.runfile} \n")

        with out_file.open("a") as out_formula_file:
            out_formula_file.write(
                "##########################################################################################\n"
            )
            out_formula_file.write("Largest detected fragment series:" + "\n")
            out_formula_file.write(
                (
                    "Formula\t" "Likelihood indicator (%)\t" "DBE\t" "Molecular ion?\n"
                ).expandtabs(EXPAND_TABS)
            )

            if G_of_frag.number_of_nodes() > 0:
                # write suggested molecular ions
                for idx_node in range(G_of_frag.number_of_nodes()):
                    if (
                        G_of_frag.nodes_ordered_by_likelihood[idx_node]
                        in self.nodes_maximal_fragments
                    ):
                        node = G_of_frag.nodes[
                            G_of_frag.nodes_ordered_by_likelihood[idx_node]
                        ]["node_lite"]
                        is_mol_ion = (
                            node.iso.frag_abund_formula
                            in self.reconstruction_results.mol_ion_formulae
                        )
                        # logger.info("{}\t{}\t{:.1f}\t{}".format(node.iso.frag_abund_formula, node.iso.DBE, node.subgraph_likelihood, idx_node+1))
                        out_formula_file.write(
                            f"{node.iso.frag_abund_formula}+:\t"
                            f"{node.subgraph_likelihood:.2f}\t"
                            f"{node.iso.DBE}\t"
                            f"{is_mol_ion}\n".expandtabs(EXPAND_TABS)
                        )

            out_formula_file.write(
                "##########################################################################################\n"
            )
            out_formula_file.write(
                (
                    "% norm I, exact;\t"
                    "RT [s];\t"
                    "Total Intensity;\t"
                    "% of Total Intensity;\t"
                    "Meas. mass [m/z];\t"
                    "Exact mass [m/z];\t"
                    "Exact mass, u [ppm];\t"
                    "Mass diff [ppm];\t"
                    "Formula;\t"
                    "DBE;\t"
                    "Intensity;\t"
                    "Intensity, fraction;\t"
                    "Ionization mode;\t"
                    "Max. allowed adduct;\t"
                    "Spectra name\n"
                ).expandtabs(EXPAND_TABS)
            )
            table_str = self.__convert_results_list_to_str_and_df(
                cl_comp, rt_average, unidentified_mass_idx_list, results_list
            )
            out_formula_file.write(table_str)

    def __convert_results_list_to_str_and_df(
        self, cl_comp, rt_average, unidentified_mass_idx_list, results_list
    ):
        """
        Format the results_list into a string.

        First, all results from results_list whose intensity is lower than LOD
        are removed.
        If the mass of result was not found, its RT is set equal to the total
        average of RT (rt_average). Otherwise, if the result's mass was not
        unidentified, the symbol "+" is added to the formula of the result
        (ion).
        The ionization number of each result is converted into the ionization
        string (0==EI, 1==CI).
        The first column of the formatted table is set to be equal to the
        percentage of signal for the chemical formula, compared to the sum
        of measured signal.
        Each result of the results_list is added in a dataframe and replaced
        in the results_lists by the formatted string of that result.
        Finally, the list of string is joined, separated by a new tab.

        Parameters
        ----------
        cl_comp : Compound
            What ALPINAC is trying to identify.
        rt_average : int
            The averaged Retention time (by intensity) in seconds.
        unidentified_mass_idx_list: list[int]
            List of indexes of non-identified masses, for more details see
            :ref:`alpinac.utils_graph.get_list_identified_unidentified_mass`.
        results_list : list[list]
            The list of the results of ALPINAC.

        Returns
        -------
        str
            Formatted table of the results_list, as above described.
        DataFrame
            Processed results_list, as above described.

        See Also
        --------
        :ref:`__convert_line_to_str`
        """
        results = [line for line in results_list if line[Ires.I] >= cl_comp.LOD]
        for id_result, result in enumerate(results):
            if result[Ires.IDX_M] == RESULT_NOT_FOUND:
                result[Ires.RT] = rt_average
            elif result[Ires.IDX_M] not in unidentified_mass_idx_list:
                result[Ires.FORMULA] += "+"
            result[Ires.IONIZATION] = (
                chem_data.ionisation_CI_string
                if result[Ires.IONIZATION] == chem_data.ionisation_CI
                else chem_data.ionisation_EI_string
            )
            result[0] = result[Ires.I] / cl_comp.sum_I * result[Ires.I_FRAG_NORM] * 100
            self.results_mygu_df.loc[id_result] = result
            self.__convert_result_to_str(results, id_result, result)
        return "\n".join(results)

    def __convert_result_to_str(self, results_lst, id_result, result):
        """Format the given result of the results_lst list into a string.

        The result should store the data in the same order as the enum
        :ref:`Ires`.

        Measurements in mz are rounded to the 6th digit, while all other
        numbers are rounded to the 2nd digit.

        The final string consists of the elements from result, separated by
        tabs (equal to :ref:`EXPAND_TABS` spaces). The string overwrites
        the result in the results_lst list.

        Parameters
        ----------
        results_lst : list[list]
            The list containing the results of ALPINAC.
        id_result : int
            The index of the result from the results_lst list that we want to
            format as a string
        result : list
            The result from the list results_lst that we want to format. It
            must contain (in the same order as written): % norm I (exact),
            RT [s], Total Intensity, % of Total Intensity, Meas. mass [m/z],
            Exact mass [m/z], Exact mass u [ppm], Mass diff [ppm], Formula,
            DBE, Intensity, Intensity (fraction), Ionization mode, Max.
            allowed adduct, Spectra name
        """
        results_lst[id_result] = (
            f"{result[Ires.PERC_NORM_I]:.2f}\t"
            f"{result[Ires.RT]:.2f}\t"
            f"{result[Ires.I]:.2f}\t"
            f"{result[Ires.I_NORM]:.2f}\t"
            f"{result[Ires.M_MEAS]:.6f}\t"
            f"{result[Ires.M_EXACT]:.6f}\t"
            f"{result[Ires.M_MEAS_U]:.2f}\t"
            f"{result[Ires.M_DIFF]:.2f}\t"
            f"{result[Ires.FORMULA]}\t"
            f"{result[Ires.DBE]:.2f}\t"
            f"{result[Ires.I_FRAG]:.2f}\t"
            f"{result[Ires.I_FRAG_NORM]:.2f}\t"
            f"{result[Ires.IONIZATION]}\t"
            f"{result[Ires.ADDUCT]}\t"
            f"{result[Ires.SPEC]}"
        ).expandtabs(EXPAND_TABS)

    def _save_performance(self, dir: Path):
        """Save the performance of the algorithm for systematic testing purposes, as those in described in https://doi.org/10.1186/s13321-021-00544-w

        cl_comp.meas_len = 6 # number of peaks in the measured spectrum
        cl_comp.sum_I = 1587492.0599999996 # total intensity of all mass peaks
        self.percent_sum_signal = 86.47202291100456 #percentage of the measured spectrum which is explained by the estimated molecular ion and its subfragments (validated nodes)
        G_of_frag.graph['no_nodes_knapsack'] = 4 # number of total solutions (formulas) generated by the knapsack with a double-bound equivalent > 0 (can exist)
        self.reconstruction_results.mol_ion_formulae_str = 'C\\Sub{2}H\\Sub{6}; ' # string(s) of the reconstructed chemical formula(e)

        TODO mygu, kaho(see added text above after 'algorithm', ok?): What are exaclty these performance metrics ?
        What do we need to have here?

        """
        cl_comp = self.cl_comp
        G_of_frag = self.G_of_frag
        perf_file = dir / "performance.txt"

        if hasattr(self.reconstruction_results, "mol_ion_formulae_str"):
            mol_ion_formulae_str = self.reconstruction_results.mol_ion_formulae_str
        else:
            mol_ion_formulae_str = "None"
        with perf_file.open("w") as f:
            f.write(
                "\n".join(
                    [
                        f"{cl_comp.meas_len = }",
                        f"{cl_comp.sum_I = }",
                        f"{self.percent_sum_signal = }",
                        f"{G_of_frag.graph['no_nodes_knapsack'] = }",
                        f"{mol_ion_formulae_str = }",
                        # Following were commented but could be set back
                        # cl_comp.meas_mass_u_median,
                        # cl_comp.meas_mass_u_median_ppm,
                        # self.percent_sum_signal,
                        # G_of_frag.graph["no_nodes_knapsack"]
                        # G_of_frag.graph["no_nodes_singletons"]/ G_of_frag.graph["no_nodes_knapsack"]* 100
                        # G_of_frag.graph["no_nodes_not_optimised"]/ G_of_frag.graph["no_nodes_knapsack"]* 100
                        # G_of_frag.graph["no_nodes_validated"]/ G_of_frag.graph["no_nodes_knapsack"]* 100
                    ]
                )
            )

class IdentificationTookTooLongOutput(IdentificationOutput):
    def __init__(self,timeout_time:int = 600) -> None:
        super().__init__()
        self.timeout_time=timeout_time
    def save(self, dir: Path | str):
        dir = Path(dir)
        dir.joinpath("took_too_much.txt").write_text(
                f"It took too much time to this compound. timeout_time in seconds: {self.timeout_time}"
            )
