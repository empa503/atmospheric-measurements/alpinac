import cProfile
import logging
from pathlib import Path
from pstats import SortKey, Stats
import sys
from alpinac.abtract_outputs import OutputObject
from alpinac.compound import Compound
import networkx as nx
import code


class RuntimesOutput(OutputObject):
    """Saves the run time of the algorithm."""

    run_times_dic: dict
    profiler: cProfile.Profile

    def __init__(self, profiler: cProfile.Profile) -> None:
        super().__init__()
        self.profiler = profiler
        self.run_times_dic = {}

    def add_time(self, name: str, time: float):
        if name in self.run_times_dic:
            raise ValueError(f"{name} was already registered")
        self.run_times_dic[name] = time

    def save(self, dir: Path):

        # Extract some infor from the frofiler
        sortby = SortKey.CUMULATIVE
        ps = Stats(self.profiler).strip_dirs().sort_stats(sortby)
        runtime_stats = ps.get_stats_profile()
        runtime_total = ps.total_tt
        self.logger.info("Total time: " + str(runtime_total))
        fctlist = [
            "step1_knapsack",
            "step2_directedgraph",
            "step3_initialize_isotopologues",
            "step5to8_optimization",
            "step5to8_optimization_underconstrained",
        ]
        for f in fctlist:
            if f in runtime_stats.func_profiles:
                self.add_time(f, runtime_stats.func_profiles[f].cumtime)
        self.add_time("total", runtime_total)

        # Save all the function runtimes
        original_stdout = ps.stream
        with open(dir / "runtimes.stats", "w") as f:
            ps.stream = f
            ps.print_stats()
        # Save the runtimes only for the steps
        with open(dir / "runtimes_steps.stats", "w") as f:
            ps.stream = f
            ps.print_stats("\(step")
        ps.stream = original_stdout

        with open(dir / "runtimes.log", "w") as text_file:
            text_file.write(
                "\n".join(
                    [
                        "Runtimes [ms]:",
                        *[
                            f"{key}: \t {time}"
                            for key, time in self.run_times_dic.items()
                        ],
                    ]
                )
            )
