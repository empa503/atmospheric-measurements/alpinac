"""Contain the 1rs step of the mode identification."""



import logging
import time
from types import NoneType
import copy

from alpinac.compound import Compound
import alpinac.periodic_table as chem_data
from alpinac.utils_identification import binary_search_nearest_low_sorted_list_of_tuples, define_knapsack_target, knapsack_double, knapsack_double_with_lists

# To specifier any logger
# logger = logging.getLogger("nameofthelogger")
# logger.handlers.append(logging.StreamHandler()) # Adds a stream handlers that writes in terminal or cell output
# logger.setLevel(logging.DEBUG)

def step1_knapsack(
    cl_comp: Compound,
    target_elements: str| None,
    target_formula: str| None,
) -> tuple[list[list[str]], list[list[str]], int]:
    """Generate all fragment formulae matching the measured masses. Use most abundant isotopes only.                  
    
    Possible atom assemblages matching the
    measured masses, within uncertainty, are exhaustively
    generated. This step usually produces a large
    number of possible chemical formulae 
    (many of them are wrong and aliminated at later steps).
    Only formulae with zero or positive double-bound equivalent are generated
    (CI is handled in a specific way to allow negative DBE formulae).
    To speed up the enumeration, only major isotope are used, where applicable.

    :arg cl_comp: instance of the class Compound. This contains the list of measured masses
        for which chemical formulae will be generated.
        For each measured mass, all formulae within the 95% confidence mass uncertainty 
        provided in cl_comp are generated.
    :arg target_elements: string containing all chemical elements (e.g., C for carbon)
        used for the enumeration of chemical formulae. If None, a default string is used.
        Step 1 runs faster in case only present chemical elements are used.
        If a really present chemical element is not given here as input,
        the identification will be wrong.
    :arg target_formula: a chemical formula that is used as maximal formula for the enumeration.
        No chemical formula containing more elements than this will be generated.
        The finally obtained solutions may be sub-formulae of this target_formula.

    :return: for each measured mass in cl_comp, return the list of generated chemical formulae.
        Each chemical formula is itself encoded as a list, 
        with pre-defined position in the list for each chemical element,
        and at each position the number of corresponding chemical element.

    """
    # Set the logger for this function
    logger = logging.getLogger('alpinac.mode_identfication.step1_knapsack')


    logger.debug("LOD = {}".format(cl_comp.LOD))
    logger.debug("masses at input of knapsack with uncertainty range and intensity:")
    logger.debug(" mass           massmin        massmax        Intensity")
    for id_m in range(cl_comp.meas_len):
        logger.debug("{:12.8f}   {:12.8f}   {:12.8f}   {:8.2f}".format(cl_comp.meas_mass[id_m], cl_comp.meas_mass_min[id_m], cl_comp.meas_mass_max[id_m], cl_comp.meas_I[id_m]))

    
    logger.info('***Knapsack: Generating fragments***')

    t0_knapsack = time.time()
    #find formula for abundant isotopologues for the measured masses:
    #this is the knapsack part
    all_solutions = [None] * cl_comp.meas_len
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # DEFINE SPECIAL FAKE ATOMS FOR ADDUCT FORMATION
    # WITH VALENCE = 2
    # SET ALL OTHER ATOMS TO MAX BASED ON MASS
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
    #idea Aurore: add adduct in here, define special fake atoms with "neutral" DBE that means valence = 2
    #and define maximum value of each
    
    #define list(s) of measured masses for which the same maximal adduct apply.
    #Run the knappsack for each of these list(s).
    #For example all masses measured with EI is one list, with no adduct.
    #Each CI measurement with a specific ionisation gas is a list, with a specific maximal adduct.
    
    #We do not want to allow adduct formation for EI data!
    
    total_solutions = 0
    total_DBE_solutions = 0
    #***mygu 20220218 extra loop to handle adducts***        
    for i_ad in range(len(cl_comp.adduct_maxfrag_list)):
        #find indexes of masses belonging to this group having the same adduct
        i_ad_list = [i_m for i_m in range(cl_comp.meas_len) if cl_comp.meas_mass_adduct_id[i_m] == i_ad]            
        i_ad_list.reverse() #sort with heaviest mass first

        if cl_comp.adduct_maxfrag_list[i_ad] is not None:
            logger.info("Knappsack for {} measured masses with adduct formation: {}".format(len(i_ad_list), chem_data.get_string_formula(cl_comp.adduct_maxfrag_list[i_ad])))
        else:
            logger.info("Knappsack for {} measured masses without adduct formation.".format(len(i_ad_list)))
        
        idx_max_mass = i_ad_list[0]
        #run definition of max fragment and knpasack for each ionisation type separately
        #otherwise we cannot apply different adduct (or no adduct) to different ionisation types
        idx_list_kn, max_no_each_element = define_knapsack_target(
            cl_comp.meas_mass_max[idx_max_mass], #this is the maximum mass, taking into account uncertainty
            target_elements, 
            target_formula,
            cl_comp.adduct_maxfrag_list[i_ad] #this is a formula containing special atoms
            )
        logger.debug(f"{idx_list_kn = }")
        logger.debug(f"{max_no_each_element = }")
    
        #mygu 20220218 this is the loop change to call knapsack for each adduct type
        #idx_m = cl_comp.meas_len-1
        #idx_m = cl_comp.meas_len-1 #modify here!!!
        number_solutions, valid_DBE_solutions, all_solutions[idx_max_mass], list_multi, list_mono = knapsack_double(
            cl_comp.meas_mass[idx_max_mass], 
            cl_comp.meas_mass_max[idx_max_mass], 
            cl_comp.meas_mass_min[idx_max_mass], 
            idx_list_kn, 
            max_no_each_element, 
            return_lists=True, 
            #verbose=True
            )
        logger.debug(f"List element of all_solutions is {all_solutions[idx_max_mass]}, {type(all_solutions[idx_max_mass])}")
        total_solutions += number_solutions
        total_DBE_solutions += valid_DBE_solutions

        #for idx_m in range(cl_comp.meas_len-2, -1, -1): #modify here!!!
        for idx_m in i_ad_list:
            # filter the two lists
            if len(list_multi) > 0:
                idx_multi = binary_search_nearest_low_sorted_list_of_tuples(
                    list_multi, 
                    cl_comp.meas_mass_max[idx_m], 
                    0
                    )
                list_multi = list_multi[:idx_multi+1]
            if len(list_mono) > 0:
                idx_mono = binary_search_nearest_low_sorted_list_of_tuples(
                    list_mono, 
                    cl_comp.meas_mass_max[idx_m], 
                    0
                    )
                list_mono = list_mono[:idx_mono+1]

            number_solutions, valid_DBE_solutions, all_solutions[idx_m] = knapsack_double_with_lists(
                cl_comp.meas_mass_max[idx_m], 
                cl_comp.meas_mass_min[idx_m], 
                idx_list_kn, 
                list_multi, 
                list_mono, 
                double_check_DBE=False
                )
            total_solutions += number_solutions
            total_DBE_solutions += valid_DBE_solutions


    #All measured masses with high resolution have been previously corrected 
    #assuming one electron was lost.
    #Remove here again the mass of one electron, to get back to "clean" measured masses.
    #This is to prepare to handle double-ionised masses.
    
    if not cl_comp.unit_mass_resolution:
        cl_comp.meas_mass = [cl_comp.meas_mass[idx_m] - chem_data.electron_mass for idx_m in range(cl_comp.meas_len)]
    
    

    t1_knapsack = time.time()
    runtime_step1_knapsack = t1_knapsack - t0_knapsack
    logger.info(str(total_DBE_solutions) + ' solutions for ' + str(cl_comp.meas_len) +' measured masses in ' + '{:.2f}'.format(runtime_step1_knapsack) + 's: ')
    #+ str([len(v) for v in all_solutions]))
    # logger.info mass and number of solutions when it is not zero
    #logger.info("\n".join(["{}: {} {}".format(cl_comp.meas_mass[i], len(all_solutions[i]), [chem_data.get_string_formula(frag) for frag in all_solutions[i]]) for i in range(len(all_solutions)) if len(all_solutions[i]) != 0]))
    #raise ValueError("test")

    if total_DBE_solutions == 0:
        #logger.info("There were no knapsack solutions. Consider adding other elements.")
        raise ValueError(
            "There were no knapsack solutions. "
            "Consider adding other elements or increasing your mass uncertainty."
        )
    

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Here must plug in handling of adduct formation.
    # remove special atoms with fake valence
    # transfer number of fake atoms to the real ones
    # mygu 20220218
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # TODO kaho: This is the block where you can add the information on the adduct
    
    #MYGU TODO: come back to code before addition of "all_solutions_adducts" because not needed
    
    #make a copy of all_solutions filled with zero
    all_solutions_adducts = copy.deepcopy(all_solutions) #make an independent copy
    #MYGU 20220824
    #for some unknown reason the following does not work, it only makes a reference, not a real independent copy
    #all_solutions_adducts = all_solutions.copy()
    #all_solutions_adducts = list(all_solutions)
    
    removed_duplicates_adduct = 0

    for i_m in range(len(all_solutions)):
        i= 0
        while i < len(all_solutions[i_m]):
            
            vec = copy.deepcopy(all_solutions[i_m][i]) #.copy()
            all_solutions_adducts[i_m][i] = [0 for i_a in range(len(vec))]
            vec_adduct = [0 for i_a in range(len(vec))]
            #logger.info("test vec is adduct: {}".format(chem_data.get_string_formula(all_solutions[i_m][i])))
            is_adduct = False
            for i_ad in chem_data.dict_adduct_to_element:
                if vec[i_ad] >0:
                    if not is_adduct: is_adduct = True
                    vec[chem_data.dict_adduct_to_element[i_ad]] += vec[i_ad]
                    vec_adduct[chem_data.dict_adduct_to_element[i_ad]] += vec[i_ad]
                    vec[i_ad] = 0
                    logger.debug("vec adduct, created: " + chem_data.get_string_formula(vec_adduct))
            #now check if this solution already exists
            if is_adduct:
                
                if vec in all_solutions[i_m]: #the solution is already is the list of all other solutions
                    logger.debug("adduct solution removed: {}".format(chem_data.get_string_formula(all_solutions[i_m][i])))
                    all_solutions[i_m].remove(all_solutions[i_m][i])
                    all_solutions_adducts[i_m].remove(all_solutions_adducts[i_m][i])
                    removed_duplicates_adduct += 1
                    
                    
                    idx_vec_duplicate = [i_sol for i_sol in range(len(all_solutions[i_m])) if vec == all_solutions[i_m][i_sol]]
                    #if idx_vec_duplicate == []:
                    #    raise ValueError("bug")
                    
                    #check if new vec_adduct is a supfragment of previous vec_duplicate
                    i_el=0
                    while i_el < chem_data.len_frag and all_solutions_adducts[i_m][idx_vec_duplicate[0]][i_el] <= vec_adduct[i_el]:
                        i_el += 1
                    # All the atom should have a value smaller or equal
                    if i_el >= chem_data.len_frag:                                    
                        all_solutions_adducts[i_m][idx_vec_duplicate[0]] = copy.deepcopy(vec_adduct)
                        logger.debug("vec adduct, is adduct: " + chem_data.get_string_formula(all_solutions_adducts[i_m][idx_vec_duplicate[0]]))
                        logger.debug("list_frag_adduct_str = {}".format([chem_data.get_string_formula(fi) for ri in all_solutions_adducts for fi in ri]))

                    #still need to add info that this vec may contain an adduct
                    #need to find it
                    
                else:
                    logger.info(">>> adduct solution is unique: {}".format(chem_data.get_string_formula(vec)))
                    all_solutions[i_m][i] = copy.deepcopy(vec) #.copy()
                    all_solutions_adducts[i_m][i] = copy.deepcopy(vec_adduct) #.copy()
                    logger.debug("vec adduct, is unique: " + chem_data.get_string_formula(all_solutions_adducts[i_m][i]))
                    logger.debug("list_frag_adduct_str = {}".format([chem_data.get_string_formula(fi) for ri in all_solutions_adducts for fi in ri]))

                    i+=1
            else:
                i+=1
    logger.info("Total removed duplicates-adducts: " + str(removed_duplicates_adduct))
    #now all fake atoms to account for adduct formation have been removed.
    #Also the information which atom may be due to adduct formation is now lost.
    #The only meaning ful info is: a fragment with DBE<0 contains an adduct.

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Here could plug in handling of double ionised masses.
    # mygu 20210813
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    #for each generated knapsack formula:
        #compute double-ionised mass: m(++) = (mass(formula)- 2 * mass_electron)/2
        #compare with measured mass
        #how to handle electron loss? 2 electron were lost
        #if mass within tolerance: add as solution with ionisation info
        
    
    #each candidate with double ionisation should be stored 
    #at its own node, connected as a child to the node having same formula
    #but single ionisation.


    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    non_identified_mass = [cl_comp.meas_mass[i] for i in range(len(all_solutions)) if len(all_solutions[i]) == 0]
    logger.info("non-identified masses, abundant-isotope formulae only: {}".format(len(non_identified_mass)))
    #Note: a mass not identified at end of STEP 1 may be cause by a rare-isotope fragment, will be generated at STEP 3.


    #logger.info("non-identified masses: {} {}".format(len(non_identified_mass), non_identified_mass))
    #logger.info('*** Knapsack, candidate elements: ***')
    #logger.info(str([chem_data.element[i] for i in idx_list_kn]))

    #logger.info('*** Knapsack, used elements: ***')
    #cl_comp.update_comp_variable(all_solutions)
    #logger.info(str([chem_data.element[i_element] for i_element in cl_comp.knapsack_list_elements_idx]))

    if total_DBE_solutions < 15:
        logger.info("all_solutions = {}".format([[chem_data.get_string_formula(fi) for fi in ri] for ri in all_solutions]))
    logger.info("list_frag_str = {}".format([chem_data.get_string_formula(fi) for ri in all_solutions for fi in ri]))
    logger.info("list_frag_adduct_str = {}".format([chem_data.get_string_formula(fi) for ri in all_solutions_adducts for fi in ri]))

    # Commented the following block during the Refactoring modeidentification 
    #if run_type.is_NIST():
    #    #we know the target molecule
    #    #so we force the code to fit the maximum of data signal and measured peaks.
    #    const_id.max_added_nodes = total_DBE_solutions + 1
    #    const_id_max_opt_signal = 99.990
    #    const_id_max_ratio_not_opt_masses = 0.001


    return all_solutions, total_DBE_solutions #all_solutions_adducts, 