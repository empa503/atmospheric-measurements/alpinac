from enum import Enum


class MesureMassesTypes(Enum):
    """Enumerate the different possibilities for the number of masses measured."""
    enough = 0
    few = 1
    many = 2