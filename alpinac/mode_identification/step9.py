import logging
import time
from alpinac.compound import Compound
import networkx as nx
import alpinac.const_identification as const_id
from alpinac.utils_data_extraction import FittingMethod
from alpinac.utils_graph import graph_optimise_isotopologues, graph_order_by_att_likelihood, graph_percent_sum_signal, remove_node_update_edges, remove_singletons


def step9_eliminate_non_optimized_sets(
    G_of_frag: nx.DiGraph,
    cl_comp: Compound,
    const_id_max_opt_signal: float,
    percent_sum_signal: float,
    fitting_method: FittingMethod,
) -> nx.DiGraph:
    logger = logging.getLogger('alpinac.mode_identfication.step9_eliminate_non_optimized_sets')
    logger.debug("min. detected mass = {}".format(const_id_max_opt_signal))    
    """#TODO:here should be a 1 line sentence to briefly explain the function.

    TODO: Here yo can explaine the whole function in more than one line. (expected behaviour)

    # Here specify the arguments in rst format
    :arg name_of_the_arg: # TODO a descritpion of what the arg is doing.
        and you can continue on the next line if too long but
        start the next lines with indents.
    :arg nameofarg2: ...
    :arg cl_comp: TODO add doc
    # You dont need to add type because it will be in the func signature

    :return: The thing that is returned.

    """   
    nodes_to_be_removed = [i_remain_node for i_remain_node in G_of_frag.nodes if i_remain_node not in G_of_frag.graph['nodes_validated']]
    for i_remain_node in nodes_to_be_removed:
        remove_node_update_edges(G_of_frag, G_of_frag.nodes[i_remain_node]['node_lite'], cl_comp)
    G_of_frag.graph['no_nodes_not_optimised'] += len(nodes_to_be_removed)
    logger.info("graph max nodes " + str(G_of_frag.graph['maximal_nodes']))

    #check again for singletons!
    old_percent_sum_signal = percent_sum_signal
    #logger.info("old_percent_sum_signal " + str(old_percent_sum_signal))
    list_nodes_singletons = [node.unique_id for node in remove_singletons(G_of_frag)]

    sum_signal, percent_sum_signal, no_optimised_masses, no_masses_to_optimize = graph_percent_sum_signal(G_of_frag, cl_comp.sum_I)
    #logger.info("percent_sum_signal " + str(percent_sum_signal))
    if ((percent_sum_signal-old_percent_sum_signal) < float(-5.0)) or percent_sum_signal < const_id_max_opt_signal:
        logger.info("***More singleton removed, optimise signal one more time***")
        #at least 5% of signal now lost because validated nodes have now been removed as singleton
        #because all remaining nodes have been removed.
        #update signal one last time

        graph_optimise_isotopologues(
            G_of_frag,
            cl_comp,
            const_id.ppm_mass_res,
            plot_mode=False, 
            fitting_method = fitting_method
        )


        graph_order_by_att_likelihood(G_of_frag, cl_comp)

        sum_signal, percent_sum_signal, no_optimised_masses, no_masses_to_optimize = graph_percent_sum_signal(G_of_frag, cl_comp.sum_I)



    #**********************************************
    #optimisation of signal for all selected nodes is done.
    
    #Print optimised k_guess
    
    logger.info("***Optimised k_guess***")
    for key_node in G_of_frag.nodes:
        iso = G_of_frag.nodes[key_node]['node_lite'].iso
        logger.info(iso.frag_abund_formula + " " + 
                str(iso.k_guess_init) + " " + 
                str(iso.k_guess) + " " + 
                str(iso.k_opt)+ " " +
                str(iso.iso_list_len))
    
    return G_of_frag