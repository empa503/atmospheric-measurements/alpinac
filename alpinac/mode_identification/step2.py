import logging
import networkx as nx

from alpinac.compound import Compound
from alpinac.abtract_outputs import PlotOutput
from alpinac.plot_figures import plot_forest_of_frag_with_pydot
from alpinac.utils_graph import (
    add_nodes_and_edges_to_graph_from_list_fragments,
    remove_singletons,
    set_str_formula_and_attribute_for_pydot,
)


def step2_directedgraph(
    total_DBE_solutions: int,
    all_solutions: list[list[str]],
    cl_comp: Compound,
    show_plt_figures: bool,
) -> tuple[nx.DiGraph, list[PlotOutput]]:

    """Organise all generated fragment formulae in a directed graph. Connect a fragment to closest, larger fragments.

    The aim of Step 2 is to organise all chemical formulae
    generated in Step 1 according to a specific order, to help
    identify and delete unlikely chemical formulae.
    Each generated fragment formula becomes a node on a graph.
    The graph is a directed graph and created using the networkx package.
    An edge is set from a node to another if the chemical
    formula of the first fragment admits the chemical
    formula of the second one as subformula (e.g. CCl3 has subfragment CCl).
    A node is connected to children (sub-formulae)
    and parents or ancestors (larger formulae containing the formula of the node).
    A node without parents is a maximal node.
    A node withour child is a leaf.
    A node not connected to any other node is a singleton.
    Because we assume that the detected fragment formulae all originate
    from the fragmentation of the same molecule, all correct formulae
    should all be connected together.
    It is extremely unlikely that a singleton would be correct,
    except in the case of a noble gas.
    Singletons are eliminated except if they are the only solution to a measured mass.
    At step 3, each node will be given an instance of the class isotopologue.
    Several maximal fragments are allowed.
    To account for potential co-elution of different molecules, several connected
    components are allowed.
    The obtained graph can be seen as a pseudo-fragmentation graph.
    Tt is likely that some edges are actually not structurally possible,
    but this is not relevant at this stage.

    The idea is that the graph organize the solutions,
    while the class Compound contains the measured data.

    To minimise the comparisons when building the graph,
    the fragments are expected to be given by decreasing mass. It is very important,
    otherwise the algorithm of graph initialisation is wrong:
    add_nodes_and_edges_to_graph_from_list_fragments

    :arg total_DBE_solutions: number of chemical formulae generated at step 1.
    :arg cl_comp: instance of the class Compound.
    :arg show_plt_figures: parameter indicating to plot figures or not
        (set to False to save time).

    :return: a directed graph, an instance of DiGraph from package networkx.

    """

    # Set the logger for this function
    logger = logging.getLogger("alpinac.mode_identfication.step2_directed_graph")
    plots = []

    
    logger.debug(f"LOD = {cl_comp.LOD}")
    logger.debug(f"{cl_comp.meas_len=}")
    logger.debug(f"{len(all_solutions)=}")
    logger.debug(f"{total_DBE_solutions=}")

    logger.info("***Generating directed graph***")

    # create empty directed graph.
    # maximal_nodes is a list of maximal elements, "roots" in the graph.
    G_reversed_edges = nx.DiGraph(
        maximal_nodes=[],  # maximal nodes of the entire graph
        maximal_nodes_per_spectrum=[None]
        * cl_comp.no_spectra,  # maximal nodes for each spectrum, one list for each spectrum
        # MYGU 20220918 one could have also done one list of max nodes per adduct...
        mol_ion_nodes_per_spectrum=[None] * cl_comp.no_spectra,
        # mapping mass_idx -> list of nodes dict[int, list[frags]]
        candidates_per_measured_mass={},
        no_nodes_knapsack=total_DBE_solutions,
        no_nodes_singletons=0,
        no_nodes_below_LOD=0,
        no_nodes_validated=0,
        no_nodes_not_optimised=0,
        added_nodes=[],
        nodes_ordered_by_likelihood=[],
        nodes_validated=[],
        nodes_molecular_ions=[],
    )
    # Add all elements in decreasing order of mass,
    # so that the subfragments of any fragment are added after it.
    # first, add the heaviest nodes, there are all maximal elements.
    # (there can be other maximal elements, of smaller mass).
    for i in range(len(all_solutions) - 1, -1, -1):
        list_frag = all_solutions[i]
        if len(list_frag) == 0:
            # Do not add masses with no nodes
            continue
        add_nodes_and_edges_to_graph_from_list_fragments(
            G_reversed_edges, list_frag, idx_measured_mass=i
        )
    # candidates_per_measured_mass: save the number of candidates per mass for remove_singletons

    logger.info(f"build graph and edges of all {total_DBE_solutions} fragments")
    max_nodes = [
        G_reversed_edges.nodes[node_idx]["node_lite"]
        for node_idx in G_reversed_edges.graph["maximal_nodes"]
    ]
    singletons = [n for n in max_nodes if len(G_reversed_edges.succ[n.unique_id]) == 0]
    if len(max_nodes) <= 30:
        logger.info(
            "{} maximal element(s) {}".format(
                len(max_nodes), " ".join([str(ni) for ni in max_nodes])
            )
        )
    else:
        logger.info("{} maximal elements".format(len(max_nodes)))
    logger.info(
        f"{len(singletons)} singleton(s) {[str(ni) for ni in singletons]}"
    )

    if total_DBE_solutions <= 200:
        set_str_formula_and_attribute_for_pydot(G_reversed_edges)
        fig = plot_forest_of_frag_with_pydot(
            G_reversed_edges,
            show_plt_figures=show_plt_figures,
        )
        plots.append(PlotOutput(fig, "raw_directed_graph"))
    else:
        logger.warning("not ploting directed graph, too many nodes")

    removed_singletons = remove_singletons(G_reversed_edges)
    singletons_str = " ".join(
        [str(ni) + f" ({ni.idx_measured_mass[0]})" for ni in removed_singletons]
    )
    logger.info(f"{len(removed_singletons)} removed singleton(s): {singletons_str}")

    logger.info("recompute maximal elements")
    max_nodes = [
        G_reversed_edges.nodes[n]["node_lite"]
        for n in G_reversed_edges.graph["maximal_nodes"]
    ]
    if len(max_nodes) <= 30:
        logger.info(f"{len(max_nodes)} maximal element(s) {max_nodes}")
    else:
        logger.info(f"{len(max_nodes)} maximal elements")
    # now compute likelihood: k_guess, number of successors in the graph, number of subfragments given by knapsack
    # the singletons where already removed
    if len(max_nodes) <= 50:
        set_str_formula_and_attribute_for_pydot(G_reversed_edges)
        fig = plot_forest_of_frag_with_pydot(
            G_reversed_edges,
            show_plt_figures=False,
        )
        plots.append(PlotOutput(fig, "directed_graph_after_step_2"))

    return G_reversed_edges, plots
