"""
Non-target screening
Use knapsack algorithm
Based on list of exact masses as measured by our TOF,
to produce a list of potential fragment formula,
within mass uncertainty.

Strategy for uncertainty: unit is same as the quantity it is related to (no percent, no ppm).
The uncertainty is also always 1 sigma (or k=1), because to sum up, values with k=1 should be used.
"""
import logging
from os import PathLike
from pathlib import Path

from alpinac import const_identification as const_id
from alpinac.mode_identification.output_saver import IdentificationOutput
from alpinac.mode_identification.compound_identification import (
    compound_identification,
)
from alpinac.mode_identification.step_initialization import (
    initialization,
    step_initialize_compound,
)
from alpinac.utils_data_extraction import FittingMethod


def make_identification_from_file(
    path_file: PathLike,
    target_elements: str | None = None,
    target_formula: str = None,
    show_plt_figures: bool = False,
    output_dir: Path | None = None,
    fitting_method: FittingMethod = FittingMethod.continuous,
    raise_error: bool = True,
    compounds: list[int] = [],
    timeout_time: float | None = None,
) -> dict[int, IdentificationOutput]:
    """The main function of non-target identification of fragments from a file.

    The file is obtained using alpinac.mode_extraction.

    :param path_file: the path to the file to process
    :param target_elements: a string of target elements, possibly present,
        e.g. "CHOCl"
    :param target_formula:  the string of the target formula, e.g. "C6Br6"
        for NIST data
    :param show_plt_figures: to show the figures. If False, the figures
        will be saved in the output dir
    :param output_dir: a Path filename to save all the data from alpinac
        if this is not given, it will create a directory with the path
        and name as the file (wihtout the suffix) and save the output there.
    :param fitting_method: the method to use for fitting the data
    :param raise_error: if True, raise an error if the identification of
        a compound fails, otherwise will proceed with the next compound.
    :param compounds: If given, the list of compound to do.
    :param timeout_time: Maximal amount of seconds that take the identification of a compound can take.
    :return: a dictionary of IdentificationOutput, one for each compound

    """
    logger = logging.getLogger("alpinac.mode_identification")

    path_file = Path(path_file)

    logger.info("processing file {}".format(str(path_file)))

    # Define the output directory
    if output_dir is None:
        # Make a dir with the same name as the file
        output_dir = Path(path_file).with_suffix("")
    output_dir = Path(output_dir)
    if output_dir.is_dir():
        logger.warning(
            f"Output directory {output_dir} already exists."
            "Contents might be replaced."
        )
    output_dir.mkdir(exist_ok=True)

    file_handler = logging.FileHandler(str(output_dir / "alpinac.log"))
    format = logging.Formatter(fmt='%(process)d %(name)s %(asctime)s %(levelname)-8s %(message)s',datefmt='%Y-%m-%d %H:%M:%S')
    file_handler.setFormatter(format)
    logging.getLogger("alpinac").addHandler(
        file_handler
    )

    """
    log_file = output_dir / "alpinac.log"
    if log_file.exists():
        logger.warn("Erasing the log file from previous run.")
        log_file.unlink()
    #logging.basicConfig(
    #    filename=str(log_file),
    #    format=
    #)
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s \n%(message)s'
    ))
    logging.getLogger("alpinac").addHandler(
        file_handler
    )
    """

    (
        target_formula,
        list_frag_idx,
        batch_fragments,
        const_id_max_opt_signal,
        do_val,
    ) = initialization(path_file=path_file)

    logger.debug("target_elements " + str(target_elements))
    logger.debug("target_formula " + str(target_formula))
    if target_elements is None and target_formula is None:
        target_elements = const_id.default_chem_elements
        logger.info("target_elements " + str(target_elements))

    logger.debug(f"{batch_fragments=}")
    identification_outputs: dict[int, IdentificationOutput] = {}

    for compound_number in list_frag_idx:
        if compounds and compound_number not in compounds:
            logger.info(
                f"Passing {compound_number=} because not requested for {compounds=}"
            )
            continue

        cl_comp, const_id_min_detect_mass = step_initialize_compound(
            batch_fragments=batch_fragments,
            compound_number=compound_number,
        )

        compound_outdir = Path(output_dir, f"Compound_{compound_number}")
        compound_outdir.mkdir(exist_ok=True)

        try:
            identification_outputs[
                compound_number
            ] = compound_identification(
                cl_comp,
                target_elements,
                target_formula,
                const_id_min_detect_mass,
                show_plt_figures,
                do_val,
                None,
                const_id_max_opt_signal,
                fitting_method,
                timeout_time,
            )
            identification_outputs[compound_number].save(compound_outdir)
        except Exception as exp:
            if raise_error:
                raise RuntimeError(
                    f"Failed identification of compound {compound_number}."
                ) from exp
            else:
                import traceback

                logger.error(
                    f"Failed identification of compound: {compound_number=}, {exp}"
                )
                with open(compound_outdir / "error.log", "w") as f:
                    f.write(traceback.format_exc())

    logging.getLogger("alpinac").removeHandler(file_handler)
    return identification_outputs
