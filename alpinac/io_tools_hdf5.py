"""Io tool modules of h5 files.

H5 files are containing the chromato-spectrometry data.
"""

from __future__ import annotations

import logging
import math
import os
import re
import threading
from datetime import datetime
from enum import Enum, StrEnum, auto
from functools import cached_property
from pathlib import Path, PurePath
from typing import Generator

import h5py
import numpy as np
import pandas as pd
import xarray as xr

from alpinac.io_tools import (
    MassCalibrationData,
    MassCalibrationDataGCMS,
    MassCalibrationH5,
)
from alpinac.mass_calibration.utils import number_of_params
from alpinac.utils import IonizationMethod
from alpinac.utils_data_extraction import (
    f_mass_to_tofidx,
    f_tofidx_to_mass,
    interpol_mass_cal,
)

# hdf5 opening mode
hdf5_frag_extraction = 0
hdf5_mass_cal = 1


def h5_files_from_to(
    directory: os.PathLike,
    from_dt: datetime = datetime.min,
    to_dt: datetime = datetime.max,
) -> Generator[Path, None, None]:
    """Return all the files from the given directory in the time period requested."""

    if from_dt >= to_dt:
        raise ValueError(
            "Time period requested is invalid, from must be smaller than to"
        )

    directory = Path(directory)

    for f in directory.glob("*.*.*.*.h5"):
        dt = datetime.strptime(f.stem[:11], "%y%m%d.%H%M")
        if from_dt < dt < to_dt:
            yield f


class Paths(Enum):
    GCWERKS_CHROMATOGRAM = auto()
    UNIT_MASS_CHROMATOS = auto()
    TOF_PIPELINE_SAVE = auto()
    TOF_PIPELINE_LOAD = auto()
    MASS_CALIB_FILE = auto()


class hdf5Metadata:
    """Class to handle the metadata of the h5 files.

    Some getters and setters are used to access and modify the data in the h5 file.

    * mass_calibration
    * extraction_pipeline

    """

    extraction_pipeline: xr.Dataset | None

    _mass_calibration_data: dict[str, MassCalibrationData] | None = None
    _ionization_segments: dict[IonizationMethod, tuple[int, int]] | None = None

    _pipeline_lock = threading.Lock()

    def __init__(
        self,
        file_path: Path,
        mode: str = "r",
        ionization_segments: dict[IonizationMethod, tuple[int, int]] | None = None,
    ) -> None:
        """
        Import data from a file of format 'hdf5' containing metadata and array

        Type of data in a hdf5 file:

        -group: somewhat works like dictionnaries.

        -attribute:
        Attributes are a critical part of what makes HDF5 a “self-describing” format.
        They are small named pieces of data attached directly to Group and Dataset objects.
        This is the official way to store metadata in HDF5.
        Each Group or Dataset has a small proxy object attached to it, at <obj>.attrs.
        To retrieve the contents of a scalar dataset, you can use the same syntax as in NumPy:
        result = dset[()].
        In other words, index into the dataset using an empty tuple.

        """
        self.logger = logging.getLogger("alpinac.hdf5Metadata")
        self.file_path = Path(file_path)
        self.toffile = PurePath(file_path).name
        self.out_dir = self.file_path.with_name(
            self.file_path.name[:-3] + ".alpinac"
        )  # Remove the h5 suffix and add alpinac
        self.out_dir.mkdir(exist_ok=True)

        if ionization_segments is not None:
            self._ionization_segments = ionization_segments
        else:
            self._ionization_segments = {
                IonizationMethod.EI: (0, 1),
            }

        pattern_tof = r"(\d{6})\.(\d{4})\.([a-zA-Z0-9_-]+)\.(\d{1,2})"
        tof_file_name_suffixes = re.findall(pattern_tof, self.toffile)
        self.logger.info(tof_file_name_suffixes)
        if len(tof_file_name_suffixes) == 4:
            # gcwerk_filename
            tof_file_name_suffixes = tof_file_name_suffixes[0]
            # logging.info(tof_frag_file_name_suffixes)
            # logging.info(tof_frag_file_name_suffixes[3])
            # logging.info(tof_frag_file_name_suffixes[0][0])

            self.toffile_name = self.file_path.stem
            file_aammdd = tof_file_name_suffixes[0]
            file_hhmm = tof_file_name_suffixes[1]

            self.file_datetime = datetime(
                year=int("20" + file_aammdd[0:2]),
                month=int(file_aammdd[2:4]),
                day=int(file_aammdd[4:6]),
                hour=int(file_hhmm[0:2]),
                minute=int(file_hhmm[2:4]),
            )
            self.sample_type = tof_file_name_suffixes[2]
            self.sample_port = tof_file_name_suffixes[3]

        if self.file_path.suffix == ".h5":
            # input name is hdf5 name
            self.toffile_name_h5 = self.toffile
        else:
            raise ValueError(f'{self.file_path} is not a file with ".h5" extension.')

        # self.hdf5_path = self.dict_path['path_hdf5'] / str(self.file_datetime.year)

        # size of the file:
        # self.hdf5_size = self.toffile_object.stat().st_size

        # Creates pointers to the right place in the hdf5 file
        # creating these pointer is really fast
        # what takes time is to load the data in a variable so we want to avoid that

        with h5py.File(file_path, mode) as tof_file:

            # group_peakdata = tof_file["PeakData"]
            # pt_peakdata = group_peakdata["PeakData"]
            # pt_peaktable = group_peakdata["PeakTable"]
            hdf5_group_fullspectra = tof_file["FullSpectra"]
            self.pt_tofdata = hdf5_group_fullspectra["TofData"]

            # find number of time bins:
            group_timingdata = tof_file["TimingData"]
            pt_buftimes = group_timingdata["BufTimes"]  # .value
            self.hdf5_dim1, self.hdf5_dim2 = (
                pt_buftimes.shape
            )  # nb of writes, nb of bufs

            self.logger.info(f"dim1 (time)   : {self.hdf5_dim1}")
            self.logger.info(f"dim2 (timebuf): {self.hdf5_dim2}")

            self.hdf5_dim3 = len(self.pt_tofdata[0, 0])
            self.n_segments = self.hdf5_dim3

            # dim4_mass=len(mass_axis)
            # time in between each recorded intensity value:
            self.s_per_bin = float(group_timingdata.attrs.get("BlockPeriod")) * 1e-9

            self.len_time_axis = self.hdf5_dim1 * self.hdf5_dim2
            self.len_mass_axis = hdf5_group_fullspectra["MassAxis"].shape[0]

            self.logger.info(f"dim3 (segment): {self.hdf5_dim3}")
            self.logger.info(f"dim4 (tof/mz) : {self.len_mass_axis}")

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # LOOKING FOR MASS CALIBRATION INFORMATION:
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # saved in FullSpectra:

        # FullSpectra has attributes (infos that are not arrays, i.e. metadata)
        # http://docs.h5py.org/en/stable/high/attr.html
        # the mass calib is saved in these attributes:

        # 20190724 Have the modes changed over time?

        self.mc_h5 = MassCalibrationH5(self.file_path)

        len_mass_axis_np = np.zeros((1, 1))
        len_mass_axis_np[0, 0] = self.len_mass_axis

        # indexes used to fit peaks in TOF domain and save results:
        # if hdf5 data are loaded, it is obviously to fit a peak in tof domain.
        self.res_list_no_col = 9
        self.i_rt = 0  # retention time (seconds)
        self.i_tof = 1  # time of flight index value
        self.i_A = 2  # area value of peak in mass domain
        self.i_sigma = 3  # sigma of peak in mass domain
        self.i_alpha = 4  # alpha of peak in mass domain
        self.i_baseline = 5  # baseline value over the mass axis
        self.i_mass_int = 6  # integer mass value
        self.i_mass_exact = 7  # exact mass value (high resolution)
        self.i_ppm = 8  # ppm of mass average

    def get_path(
        self,
        path: Paths,
        ionization_method: IonizationMethod | None = None,
        gcwerks_dir: os.PathLike | None = None,
    ) -> Path:
        match path:
            case Paths.GCWERKS_CHROMATOGRAM:
                if gcwerks_dir is None:
                    gcwerks_dir = self.out_dir
                else:
                    gcwerks_dir = Path(gcwerks_dir)
                return gcwerks_dir / f"{self.file_path.stem}.chromatogram.gcwerks"
            case Paths.UNIT_MASS_CHROMATOS:
                # Parquet file with the dataframe
                return (
                    self.out_dir / f"{self.file_path.stem}.unit_mass_chromatos.parquet"
                )
            case Paths.TOF_PIPELINE_SAVE:
                return (
                    self.out_dir
                    / f"pipeline_{datetime.now().strftime('%Y%m%d_%H%M')}.nc"
                )
            case Paths.TOF_PIPELINE_LOAD:
                # Return the last pipeline saved
                pipelines = list(self.out_dir.glob("pipeline_*.nc"))
                if not pipelines:
                    raise FileNotFoundError("No pipeline saved")
                return sorted(pipelines)[-1]
            case Paths.MASS_CALIB_FILE:
                if ionization_method is None:
                    ionization_method = IonizationMethod.EI
                # Deprecated way of saving the mass calibration in the same directory
                old_file = self.file_path.with_suffix(
                    f".{ionization_method.name}_mc.txt"
                )
                if old_file.is_file():
                    return old_file

                return self.out_dir / old_file.name
            case _:
                raise NotImplementedError(f"Path {path} not implemented")

        raise ValueError(f"Path {path} not found in {Paths.__members__=}")

    @cached_property
    def unit_mass_max(self) -> float:
        return math.floor(
            f_tofidx_to_mass(np.array(self.len_mass_axis), self.mc_h5.params)
        )

    @cached_property
    def mass_axis_h5(self) -> np.ndarray:
        """Default mass axis of the h5 file."""
        with h5py.File(self.file_path, "r") as tof_file:
            return tof_file["FullSpectra/MassAxis"][()]

    def load_mass_calibration(self) -> dict[IonizationMethod, MassCalibrationData]:
        """Load the mass calibration data from the h5 file."""

        # General mass calibration
        path_file_mass_cal_all_segments = self.file_path.with_suffix(".mc.txt")

        calib_data_for_ionization: dict[IonizationMethod, MassCalibrationData] = {}

        for ionization_mode in self._ionization_segments.keys():
            mass_cal_file = self.get_path(Paths.MASS_CALIB_FILE, ionization_mode)
            # Get the mass calibration data for the ionization method and seperately for EI and CI
            if mass_cal_file.is_file():
                # Found a specific mass calibration file for this spectra mode
                calib = MassCalibrationDataGCMS(mass_cal_file)
            elif path_file_mass_cal_all_segments.is_file():
                # Get the default mass calibration file for all ionizations
                if len(self._ionization_segments) > 1:
                    self.logger.warning(
                        f"Missing specific mass calibration file for {ionization_mode=} and file {self.file_path=}\n"
                        f"General calibration .mc.txt will be used for {ionization_mode=}. Not recommended!"
                    )
                else:
                    self.logger.info(
                        f"general mass calibration file is used (EI assumed): {path_file_mass_cal_all_segments}"
                    )
                calib = MassCalibrationDataGCMS(path_file_mass_cal_all_segments)
            else:
                self.logger.warning(
                    f"h5 file {self.file_path} missing mass calibration file {path_file_mass_cal_all_segments} \n"
                    "Using default mass calibration from the h5 file."
                )
                calib = self.mc_h5

            calib_data_for_ionization[ionization_mode] = calib

        return calib_data_for_ionization

    @property  # for EI and CI
    def mass_calibration_data(self) -> dict[IonizationMethod, MassCalibrationData]:

        if (
            not hasattr(self, "_mass_calibration_data")
            or self._mass_calibration_data is None
        ):
            self._mass_calibration_data = self.load_mass_calibration()

        return self._mass_calibration_data

    @mass_calibration_data.setter  # for EI and CI
    def mass_calibration_data(self, value: dict[str, MassCalibrationData]):
        if not isinstance(value, dict[str, MassCalibrationData]):
            raise TypeError(
                f"Expected Dicionary of MassCalibrationData, got {type(value)}"
            )
        self._mass_calibration_data = value

    @cached_property
    def calib_substances(self) -> list[str]:
        """Calibration substances present in the h5 file."""

        with h5py.File(self.file_path, "r") as tof_file:
            n_masses = tof_file["FullSpectra"].attrs["MassCalibration nbrPoints"][0]
            return [
                tof_file["FullSpectra"].attrs[f"MassCalibration l{idx}"].decode()
                for idx in range(1, n_masses + 1)
            ]

    @cached_property
    def time_axis(self) -> np.ndarray:
        """Default time axis of the h5 file."""

        with h5py.File(self.file_path, "r") as tof_file:
            ta = tof_file["TimingData/BufTimes"][()].flatten()
        # Last 0 values must be changed
        index = -1
        while ta[index] == 0:
            index -= 1
        # Assume it continues linearly
        ta[index + 1 :] = ta[index] + self.s_per_bin * np.arange(1, -index)

        return ta

    @cached_property
    def total_ion_current(self) -> np.ndarray:
        """Calculate the total ion current for each time point."""
        with h5py.File(self.file_path, "r") as tof_file:
            tof_data = tof_file["FullSpectra/TofData"]
            return tof_data[()].sum(axis=(2, 3)).flatten()

    def import_hdf5_data(
        self,
        time_start: float | int = 0.0,
        time_stop: float | int | None = None,
        idx_dim3_start: int = 0,
        idx_dim3_stop: int = 0,
        idx_tof_start: int = 0,
        idx_tof_stopp: int | None = None,
        return_timeaxis: bool = False,
    ) -> np.ndarray | tuple[np.ndarray, np.ndarray]:
        """
        Import a selected domain of hdf5 tof data
        Data are originally saved in 4 dimensions
        Dimension 3 is the segments dimensions.
        Segments contain different data based on the ionization type.
        It is only 1 if the instrument is EI.
        For the Ci-Ei instrument, dimension 3 can be 0 to 3 or more.
        Indexes are similar to numpy:  [start, end[

        Dimensions 1 and 2: time
        dimension 4: mass

        The output is a numpy array resised to 2 dimensions:
            1 dimension for the time axis and one dimension for the mass axis.

        Input:
            time_start: retention time, in second, when to start extracting data
            time_stop:  retention time, in second, when to stop extracting data

            idx_dim3_start: index of segements to start (inlusive)
            idx_dim3_stop: index of segements to stop (exlusive)
        """

        # Convert time in second to time idx in index number
        # (as if there were only one time dimension,
        # the conversion to a 2-D time dimension is done afterwards)

        # logging.info(str([idx_time_stop- idx_time_start, idx_tof_stop- idx_tof_start]))

        # re-organise data on one time dimension instead of two:
        # self.time_axis = [pt_buftimes[idx_dim1,idx_dim2] for idx_dim1 in range(dim1_buftime)  for idx_dim2 in range(dim2_buftime)]
        # self.tof_data = np.array([pt_tofdata[idx_dim1,idx_dim2,0,:] for idx_dim1 in range(dim1_buftime)  for idx_dim2 in range(dim2_buftime)])

        # Titian Steiger speed improvement 20200214
        # time_axis not used anymore, we use index * s_per_bin instead.
        # self.time_axis = pt_buftimes[()].flatten()

        # ================================================================
        # to load the entire dataset (cunsumes memory!)
        # self.tof_data = pt_tofdata[()].reshape(self.hdf5_dim1*self.hdf5_dim2, pt_tofdata.shape[-1])

        # ================================================================
        # to load only the needed bit of data
        # based on input tart and stop time in seconds (e.g., GC run seconds),
        # find corresponding index on time axis in hdf5 data.
        # Note: in the hf5 datafile the time axis is in two dimensions
        # to optimise data storage.

        assert isinstance(time_start, (int, float))
        idx_time_start = np.searchsorted(self.time_axis, time_start, side="left")
        if time_stop is not None:
            assert isinstance(time_stop, (int, float))
            idx_time_stopp = np.searchsorted(self.time_axis, time_stop, side="right")
        else:
            idx_time_stopp = self.len_time_axis

        self.logger.debug(
            f"len of time axis for extraction: {idx_time_stopp - idx_time_start}"
        )

        # Buf index
        idx_dim2_start = idx_time_start % self.hdf5_dim2
        idx_dim2_stopp = idx_time_stopp % self.hdf5_dim2

        # Time index
        idx_dim1_start = idx_time_start // self.hdf5_dim2
        idx_dim1_stopp = idx_time_stopp // self.hdf5_dim2 + 1
        # The last buf will be included in the next one

        # Ensure boundaries are respected
        idx_dim1_start = min(max(0, idx_dim1_start), self.hdf5_dim1)
        idx_dim1_stopp = min(max(0, idx_dim1_stopp), self.hdf5_dim1)

        if idx_tof_stopp is None:
            self.logger.debug("Using last tof idx")
            idx_tof_stopp = self.len_mass_axis

        self.logger.debug(
            "\n".join(
                [
                    "dimensions for hdf5 extraction:",
                    f"dim1: {idx_dim1_start=}, {idx_dim1_stopp=}",
                    f"dim2: {idx_dim2_start=}, {idx_dim2_stopp=}",
                    f"tof : {idx_tof_start=}, {idx_tof_stopp=}",
                ]
            )
        )
        # We first have to extract on the dim 1, all the tof data
        tof_data_extracted_shape = (
            (idx_dim1_stopp - idx_dim1_start) * self.hdf5_dim2,
            idx_tof_stopp - idx_tof_start,
        )
        self.logger.debug(f"{tof_data_extracted_shape=}")

        with h5py.File(self.file_path, "r") as tof_file:

            pt_tofdata = tof_file["FullSpectra/TofData"]
            if self.hdf5_dim3 == 1:
                # EI only instrument
                tof_data = pt_tofdata[
                    idx_dim1_start:idx_dim1_stopp,
                    :,
                    :,
                    idx_tof_start:idx_tof_stopp,
                ].reshape(tof_data_extracted_shape)
            else:
                if idx_dim3_stop == 0 or idx_dim3_stop > self.hdf5_dim3:
                    raise ValueError(
                        f"{idx_dim3_stop=} is not valid. "
                        f"Data has {self.hdf5_dim3} segments. idx_dim3_stop must be between 0 and {self.hdf5_dim3}"
                    )
                # EI-CI instrument
                pt_tofdata_interm = pt_tofdata[
                    idx_dim1_start:idx_dim1_stopp,
                    :,
                    idx_dim3_start:idx_dim3_stop,
                    idx_tof_start:idx_tof_stopp,
                ]
                # Sum over the segments
                tof_data = np.sum(pt_tofdata_interm, axis=2).reshape(
                    tof_data_extracted_shape
                )

            # Now we access the buffer time axis
            self.tof_data = tof_data[
                idx_dim2_start : (tof_data_extracted_shape[0] - self.hdf5_dim2)
                + idx_dim2_stopp,
                :,
            ]

        if return_timeaxis:
            # Also find the coresponding time values
            time_idx_start = idx_dim1_start * self.hdf5_dim2 + idx_dim2_start
            time_idx_stopp = (idx_dim1_stopp - 1) * self.hdf5_dim2 + idx_dim2_stopp
            return self.tof_data, self.time_axis[time_idx_start:time_idx_stopp]

        return self.tof_data

    def load_data(
        self,
        time_interval: tuple[float, float | None] = (0, None),
        mass_interval: tuple[float, float | None] | float = (0, None),
        full_drift_correction: bool = False,
        spectra_mode: str = "EI",
    ) -> pd.DataFrame:
        """Load all the intensities comprised in the selected intervals."""

        # indexes of time
        start_time = time_interval[0]
        if time_interval[1] is None:
            end_time = self.len_time_axis * self.s_per_bin
        else:
            end_time = time_interval[1]

        idx_time_start = int(start_time / self.s_per_bin)
        idx_time_stop = math.ceil(end_time / self.s_per_bin)

        if idx_time_start >= self.len_time_axis or idx_time_stop >= self.len_time_axis:
            raise ValueError(
                f"Time interval out of range {self.len_time_axis * self.s_per_bin} s"
            )
        # Indexes of masses
        if isinstance(mass_interval, (float, int)):
            mass_interval = (mass_interval - 0.3, mass_interval + 0.3)
        mass_start = mass_interval[0]
        mass_stop = mass_interval[1]

        start_rt_params = interpol_mass_cal(
            start_time,
            self.mass_calibration_data[spectra_mode].mass_cal_parameters,
            self.mass_calibration_data[spectra_mode].mass_cal_mode,
        )
        end_rt_params = interpol_mass_cal(
            end_time,
            self.mass_calibration_data[spectra_mode].mass_cal_parameters,
            self.mass_calibration_data[spectra_mode].mass_cal_mode,
        )
        smallest_tofid = int(
            min(
                f_mass_to_tofidx(mass_start, start_rt_params),
                f_mass_to_tofidx(mass_start, end_rt_params),
            )
        )
        smallest_tofid = max(0, smallest_tofid)
        if mass_stop is None:
            largest_tofid = self.len_mass_axis
        else:
            largest_tofid = (
                math.ceil(
                    max(
                        f_mass_to_tofidx(mass_stop, start_rt_params),
                        f_mass_to_tofidx(mass_stop, end_rt_params),
                    )
                )
                + 1
            )

        with h5py.File(self.file_path, "r") as tof_file:
            tof_data = tof_file["FullSpectra/TofData"]

            self.logger.debug(
                f"{smallest_tofid=} {largest_tofid=} {idx_time_start=} {idx_time_stop=}"
            )
            self.logger.debug(f"{tof_data=}")

            # The time is always on the first two dims
            len_dim0 = tof_data.shape[0]
            len_dim1 = tof_data.shape[1]
            start_idx = idx_time_start // len_dim1
            stop_idx = (idx_time_stop // len_dim1) + 1
            self.logger.debug(f"{start_idx=} {stop_idx=}")
            data = (
                tof_data[start_idx:stop_idx, :, :, smallest_tofid:largest_tofid]
                .sum(axis=2)
                .reshape(-1, largest_tofid - smallest_tofid)
            )
        # logging.info('Data loaded')

        if not full_drift_correction:
            mass_cal_params = interpol_mass_cal(
                (start_time + end_time) / 2,
                self.mass_calibration_data[spectra_mode].mass_cal_parameters,
                self.mass_calibration_data[spectra_mode].mass_cal_mode,
            )
            mz_values = f_tofidx_to_mass(
                np.linspace(
                    smallest_tofid, largest_tofid, largest_tofid - smallest_tofid
                ),
                mass_cal_params,
            )

            import xarray as xr

            return xr.DataArray(
                data,
                coords={
                    "RT": np.linspace(
                        self.s_per_bin * len_dim1 * start_idx,
                        self.s_per_bin * len_dim1 * stop_idx,
                        (stop_idx - start_idx) * len_dim1,
                    ),
                    "m/z": mz_values,
                },
                dims=["RT", "m/z"],
            )

        # Create retention times on the bins
        retention_times = np.repeat(
            np.linspace(
                self.s_per_bin * len_dim1 * start_idx,
                self.s_per_bin * len_dim1 * stop_idx,
                (stop_idx - start_idx) * len_dim1,
            ),
            largest_tofid - smallest_tofid,
        )
        tof_ids = np.tile(
            np.linspace(smallest_tofid, largest_tofid, largest_tofid - smallest_tofid),
            (stop_idx - start_idx) * len_dim1,
        )
        mass_cal_params = interpol_mass_cal(
            retention_times,
            self.mass_calibration_data[spectra_mode].mass_cal_parameters,
            self.mass_calibration_data[spectra_mode].mass_cal_mode,
        )
        mz_values = f_tofidx_to_mass(tof_ids, mass_cal_params)

        self.logger.debug(
            f"{retention_times.shape=} {mz_values.shape=} {data.shape=}, {data.reshape(-1).shape=}"
        )

        return pd.DataFrame(
            {"rt": retention_times, "mz": mz_values, "intensity": data.reshape(-1)}
        )

    def export_mc_hdf5_tw(
        self,
        params_optimised,
        mass_cal_mode: int,
    ):
        """
        Update the mass calibration parameters in the h5 file.
        Documentation about TofDaq
        /FullSpectra/MassCalibration

        Dataset type 	2D array of 64-bit float
        # of elements 	NbrWrites * MassCalibration nbrParameters

        This dataset exists only if Recalibration > 1
        (Automatic recalibration set to “after every write”
        or “after every n-th write”).
        For every write the mass calibration parameters are stored
        (n times the same set for “after every n-th write”).
        The first entry corresponds to the calibration parameters
        stored as an attribute of the /FullSpectra group.
        Recalibrating a file which contains this dataset
        will only adjust a and b stored as attributes and the values
        in this dataset are left untouched.
        Rename or delete the dataset to some other name
        to use the mass calibration stored as attributes.

        Additional info:
        For compatibility with TofDaq and Tofware,
        the mass cal should be written in the hdf5 file,
        not in a separate file.
        """

        self.logger.warning(
            f"`export_mc_hdf5_tw` is currently not compatible with the h5 format."
        )

        valid_params = [
            params for params in params_optimised if params["mode"] == mass_cal_mode
        ]

        nb_params = number_of_params(mass_cal_mode)

        df_params = pd.DataFrame.from_records(valid_params)

        da = xr.DataArray(
            np.array([df_params[f"p{i}"].values for i in range(1, nb_params + 1)]).T,
            dims=["time", "params"],
            coords={
                "time": df_params["rt"].values,
                "params": np.arange(1, nb_params + 1),
            },
            attrs={
                "mass_cal_mode": mass_cal_mode,
                "created": datetime.now().isoformat(),
                "author": "alpinac",
            },
        )
        # required: compatibility with attributes.
        # Mass cal mode should be same, number of parameters should be compatible.
        with h5py.File(self.file_path, "r+") as tof_file:
            hdf5_group_fullspectra = tof_file["FullSpectra"]
            hdf5_group_fullspectra.attrs["MassCalibMode"] = mass_cal_mode

            for param in range(1, nb_params + 1):
                if f"p{param}" not in df_params.columns:
                    continue
                hdf5_group_fullspectra.attrs[f"MassCalibration p{param}"] = df_params[
                    f"p{param}"
                ].mean()

            # Delete the additional parameters
            for param in range(nb_params + 1, 6):
                if f"p{param}" in hdf5_group_fullspectra.attrs:
                    del hdf5_group_fullspectra.attrs[f"MassCalibration p{param}"]

            # Number of parameters
            hdf5_group_fullspectra.attrs["MassCalibration nbrParameters"] = nb_params

        # Save the mass calibration in the h5 file
        da.to_netcdf(
            self.file_path,
            group="FullSpectra/MassCalibration",
            mode="a",
            engine="h5netcdf",
        )

    def get_unit_mass_chromatos(self, with_tic: bool = False) -> pd.DataFrame | None:
        """Get the unit mass chromatograms from the h5 file."""
        file_path = self.get_path(Paths.UNIT_MASS_CHROMATOS)
        if not file_path.exists():
            return None

        try:
            df = pd.read_parquet(file_path)
            if "tic" in df.columns:
                # remove the total ion current
                tic_col = df.pop("tic")
            # Transform all columns names from string to float
            df.columns = df.columns.astype(float)
            # Avoid defragmentation warning
            df = df.copy()
            if with_tic:
                df["tic"] = tic_col
        except Exception as e:
            self.logger.error(
                f"Error loading unit mass chromatograms from {file_path}: {e}"
            )
            return None
        return df

    def save_unit_mass_chromatos(self, df: pd.DataFrame):
        """Save the unit mass chromatograms in the h5 file."""
        file_path = self.get_path(Paths.UNIT_MASS_CHROMATOS)
        if file_path.exists():
            self.logger.warning(
                f"Overwriting existing unit mass chromatograms in {file_path}"
            )
            file_path.unlink()

        df.to_parquet(file_path)

    @property
    def extraction_pipeline(self) -> xr.Dataset | None:
        """Get the extraction pipeline from the h5 file."""
        with self._pipeline_lock:

            try:
                file_path = self.get_path(Paths.TOF_PIPELINE_LOAD)
            except FileNotFoundError:
                return None
            try:
                ds = xr.load_dataset(file_path)
            except Exception as e:
                self.logger.error(f"Error loading pipeline from {file_path}: {e}")
                return None
            return ds

    @extraction_pipeline.setter
    def extraction_pipeline(self, value: xr.Dataset):

        self.logger.info(f"Setting extraction pipeline to {self.file_path}")

        if "description" not in value.attrs:
            value.attrs["description"] = (
                f"Extraction pipeline from {datetime.now().strftime('%Y-%m-%d %H:%M')}"
            )

        save_path = self.get_path(Paths.TOF_PIPELINE_SAVE)
        if save_path.exists():
            self.logger.warning(f"Overwriting existing pipeline in {save_path}")
            save_path.unlink()

        value.to_netcdf(save_path)

        self._extraction_pipeline = value

        self.logger.info(f"Extraction pipeline saved in {self.file_path}")
