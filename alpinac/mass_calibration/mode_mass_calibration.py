"""Mass calibration for combined chromatography and mass spectrometry."""

import itertools
import logging
import math
import time
from enum import Enum, auto
from pathlib import Path
from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import alpinac.const_peak_detection as const_pd
from alpinac.io_tools import export_mc
from alpinac.io_tools_hdf5 import Paths, hdf5Metadata
from alpinac.mass_calibration.utils import (
    MassCalibOutput,
    MassCalParams,
    number_of_params,
    f_mass_to_tofidx,
    f_tofidx_to_mass,
)
from alpinac.periodic_table import formula_to_mass
from alpinac.utils import IonizationMethod, normalize
from alpinac.utils_data_extraction import (
    MakeBinsModes,
    TrackingMassesMode,
    find_bins_np,
    fit_linear_np,
    fit_pseudoVoigt,
    fit_tofidx_to_mass,
    make_bins_np,
    make_mass_cal_dict_of_frag,
    peak_detect,
)
from alpinac.utils_user_def_params import make_colors

# colors for graph:
color1_names, color2_names, color3_names = make_colors()


class Plots(Enum):
    """Avaliable plots."""

    PEAK_DETECTION = auto()
    PSEUDOVOIGT_FITTING = auto()
    HISTOGRAM_CENTERS = auto()
    PARAMS_FIT = auto()
    MASS_BIN = auto()
    # Final plots (only for the results)
    MASS_DRIFT = auto()
    PARAMETERS = auto()
    RESIDUALS_VS_MASS = auto()
    SIGMA_VS_MASS = auto()


def make_mass_calibration(
    path_file: Path,
    mass_cal_write: MassCalibOutput = MassCalibOutput.TXT,
    dict_spikes: dict[str, list[str]] = None,
    spike_duration: float = 5.0,  # seconds, RT window duration from beginning to end of spike.
    mass_cal_mode_index: int = 2,
    mass_diff_ppm_threshold: float = 20.0,  # max 20 ppm residual allowed for mass cal
    averaging_timspan: float = 360.0,  # Seconds
    spacing_timespan: float = 120.0,  # Seconds
    continuous_acceptable_ratio: float = 0.7,  # ratio of continuous peaks to spikes
    ionization_method: IonizationMethod = IonizationMethod.EI,
    plots: list[Plots] = [],
    show_plots: bool = True,
    mass_drift_in_mass_unit: bool = True,
    df_peaks: (
        pd.DataFrame | None
    ) = None,  # specify the peaks directly there instead of extracting them
    **extraction_kwargs,
):
    """Make a mass calibration.

    The mass calibration uses peaks found in background.
    These are fragements which leak during the whole run and are very useful for the
    calibration of the mass axis.

    It will poceed the following way

    * check which masses and which retention times should be used as calibrant
    * extract peaks corresponding to the target masses and times
        * Sum over the rt window
        * Fit properly mass peaks using pesudovoigt
        * If more than one peak is found, select the closest one to the expected mass
    * fit calibration parameters to the masses present
    * calculate the uncertainty of the mass calibration
    * export the calibraion parameters

    :arg dict_spikes: to allow for either tracking masses over time or using spikes,
        create dictionnary of time and masses.
        {
            "12.86": ["CF3", "CF4", ...],
            "1200.86": [ "CNF", ...],
        }
        If an entry has key "0.0" it will be treatead as continuous and not spiked.
        The script will then try to find that mass all along the time.
        If this is not specified. Default substances will be used.
    #each exact mass is a dict key. For each key there is the list of rt_idx_list
    :arg mass_cal_mode_index: The index of the mass calibration method to use.
    :arg mass_diff_ppm_threshold: The maximum mass difference allowed for mass calibration.
        Will not use masses for mass calibration that have larger residuals.
    :arg ionization_method: The mode of the spectra. Either EI or CI. (Different calibrations)


    :arg plots: A list of plots to show.
    :arg show_plots: If True, show the plots. Otherwise, do not show them,
        only saves them.

    """

    logger = logging.getLogger("alpinac.make_mass_calibration")

    path_file = Path(path_file)

    cl_run = hdf5Metadata(path_file)

    if dict_spikes is None:
        # Find the substances used as calibrant
        dict_spikes = {"0.0": cl_run.calib_substances}
        logger.info(f"Read continuous spikes from h5 files: {dict_spikes}")

    t0 = time.time()

    # Dict_frag contaings for each fragement a list of rt intevals to extradct
    # dict_frag_modes, for each fragmeetn wheter continuous or spike
    # masscal_list_exact, the exact masses matching the fragments
    # rt_idx_centre_list, the rt index of the centre of the rt interval

    dict_frag, dict_frag_mode, masscal_list_exact, rt_idx_centre_list = (
        make_mass_cal_dict_of_frag(
            dict_spikes,
            spike_duration,
            cl_run.len_time_axis,
            cl_run,
            averaging_timspan,
            spacing_timespan,
        )
    )
    logger.info(f"{dict_frag=}")
    logger.info(f"{dict_frag_mode=}")
    logger.info(f"{masscal_list_exact=}")
    logger.info(f"{rt_idx_centre_list=}")

    logger.info("Starting mass calibration for " + str(cl_run.toffile_name_h5))

    if df_peaks is None:
        df_peaks = extract_data_for_mass_cal(
            dict_frag,
            dict_frag_mode,
            cl_run,
            plots=plots,
            show_plots=show_plots,
            **extraction_kwargs,
        )
        df_peaks.to_csv(
            cl_run.out_dir / path_file.with_suffix(".peaks_mass_cal.csv").name,
            index=False,
        )

    logger.info(f"Finished extracting peaks")

    df_continuous_peaks = df_peaks.loc[
        df_peaks["frag_mode"] == TrackingMassesMode.CONTINUOUS.value
    ]

    if len(df_continuous_peaks):
        # Filter the peaks and group them to which mass they follow
        nb_bin_min_tofidx_mass_cal = const_pd.nb_bin_min_tofidx
        tofidx_bin_centres = find_bins_np(
            df_continuous_peaks["tof"].values,
            const_pd.delta_tofidx_discrete_mass_cal,
            const_pd.PWh_tofidx,
            const_pd.PTh_tofidx,
            const_pd.h_nb_per_bin,
            graph=Plots.HISTOGRAM_CENTERS in plots and show_plots,
            mode="histo_tofidx",
            remove_long_0_areas=True,
        )
        logger.info(f"For continuous peaks, found {len(tofidx_bin_centres)} centers")
        logger.debug(f"{tofidx_bin_centres=}")
        # tofidx_bin_centres is now a list of best guess centre (on x axis, that is tof-index) of identified peaks
        # now we assign each peak result to the closest centre
        # and we clean up the buckets

        bin_values, no_bins, _ = make_bins_np(
            df_continuous_peaks["rt"].values,
            df_continuous_peaks["tof"].values,
            tofidx_bin_centres,
            const_pd.std_dev_p_tofidx_max_mass_cal,
            nb_bin_min_tofidx_mass_cal,
            cl_run.s_per_bin,
            graph=Plots.MASS_BIN in plots and show_plots,
            mode=MakeBinsModes.ROLLING,
            plot_mode="mass_domain",
        )

        logger.debug(f"{no_bins=}")
        logger.debug(f"{bin_values=}")
        df_continuous_peaks["bin_center"] = bin_values
        # Remove non assigned peaks
        df_continuous_peaks = df_continuous_peaks.loc[bin_values != -1]

        # Take only the centers that have enought peaks
        counts = df_continuous_peaks.groupby("bin_center").count()["tof"]
        logger.debug(f"Bin centers: {counts=}")
        ratios = counts / len(rt_idx_centre_list)
        # Keep only the max ratios
        centers_to_keep = (ratios > continuous_acceptable_ratio).index
        df_continuous_peaks = df_continuous_peaks.loc[
            df_continuous_peaks["bin_center"].isin(centers_to_keep)
        ]
        df_peaks = df_continuous_peaks

    # ************************
    # MASS CALIBRATION
    # ************************
    # Choose the mass calibration mode
    nb_arg_fit_tofidx_to_mass = number_of_params(mass_cal_mode_index)

    # For each rt_index, find corresponding masses on the calibration list,
    # according to the found tof indexes.
    # the mass calibration fit is done separalety for each rt-index, using the specific list
    # made for each rt index.
    # This means that the number of points used for each rt index may not be the same.
    params_optimised = []

    h5_params = cl_run.mc_h5.params

    for idx_rt in range(len(rt_idx_centre_list)):
        logger.info(f"Fitting the parameters at retention time idx {idx_rt}")
        # indexes of masses matching this rt index
        mask = df_peaks["rt"] == rt_idx_centre_list[idx_rt]
        df = df_peaks.loc[mask]

        rt_s = rt_idx_centre_list[idx_rt] * cl_run.s_per_bin

        logger.debug(
            f"{mass_cal_mode_index=} " f"{mass_diff_ppm_threshold=} " f"{df=} "
        )

        unique_masses = df["mass_exact"].unique()
        if not len(unique_masses) > nb_arg_fit_tofidx_to_mass + 1:
            logger.warning(
                f"Not enough data points for mass cal fit at rt = {rt_s:.2f} [s] "
                f" got {len(unique_masses)} needs more than {nb_arg_fit_tofidx_to_mass+1}."
                " Add more peaks for calibration."
            )
            continue

        this_params_optimised, mass_diff_ppm_list = fit_tofidx_to_mass(
            df["tof"].values,
            df["mass_exact"].values,
            mass_cal_mode=mass_cal_mode_index,
            mass_diff_ppm_threshold=mass_diff_ppm_threshold,
            graph=Plots.PARAMS_FIT in plots and show_plots,
            rt=rt_idx_centre_list[idx_rt] * cl_run.s_per_bin,
            mass_list=df["fragment"].values,
            mass_cal_default=h5_params,
        )
        logger.debug(f"{mass_diff_ppm_list=}")

        df_peaks.loc[mask, "ppm"] = mass_diff_ppm_list
        this_params_optimised["rt"] = rt_s
        params_optimised.append(this_params_optimised)

    # Remove outliers (fitted masses that are porabably not the expected fragment)
    mask_correct_mass = df_peaks["ppm"].abs() < mass_diff_ppm_threshold
    df_peaks = df_peaks.loc[mask_correct_mass].copy()
    logger.info(
        f"Removed {sum(~mask_correct_mass)} masses as fitted masses were "
        "too far away from the orginal calib."
    )
    logger.info(f"Peaks extracted from mass calibration \n {df_peaks=}")

    if len(df_peaks) == 0:
        raise RuntimeError(
            f"No points for mass calibration. "
            "Check which peaks where fitted to understand why peaks were removed. "
            "Alternativley you can try to add more substances to the mass calibration "
            "or lower the intensity threshold."
        )

    # ***UNCERTAINTY OF MASS CALIBRATION***
    # WE TAKE THE AVERAGE BIAS AND ADD 2 SIGMA OF THE BIAS = 2*u
    # the uncertainty varies with the mass.
    # calculate, per mass, average bias and std dev of average bias

    ppms_grouped = df_peaks.groupby("mass_exact")["ppm"]

    mass_cal_u: pd.Series = np.maximum(
        ppms_grouped.mean() / 2 + ppms_grouped.std(),
        ppms_grouped.max() * 1.1 / 2.0,
    )
    mass_cal_u = mass_cal_u.dropna()

    # ************************
    # Perform a linear fitting from tof to sigma
    # express sigma as a function of TOF index
    sigma_tofidx_slope, sigma_tofidx_b, _, _ = fit_linear_np(
        df_peaks["tof"].values, df_peaks["sigma"].values
    )

    # by approximation, we use the general, unique set of mass cal parameters:
    # if h5_params['mode'] == 0 or h5_params['mode'] == 1 or h5_params['mode'] == 2:
    df_peaks["mz_h5_cal"] = f_tofidx_to_mass(df_peaks["tof"].values, h5_params)
    df_peaks["sigma_mz"] = (
        f_tofidx_to_mass(df_peaks["tof"].values + df_peaks["sigma"].values, h5_params)
        - df_peaks["mz_h5_cal"].values
    )

    sigma_m_slope, sigma_m_b, _, _ = fit_linear_np(
        df_peaks["mz_h5_cal"].values, df_peaks["sigma_mz"].values
    )
    df_peaks["rt_s"] = df_peaks["rt"] * cl_run.s_per_bin

    alpha_tofidx_average = np.mean(df_peaks["alpha"])
    logger.debug(f"alpha_tofidx_average: {alpha_tofidx_average:.3f}")

    mass_cal_statistics = {
        "sigma_tofidx_slope": sigma_tofidx_slope,
        "sigma_tofidx_b": sigma_tofidx_b,
        "sigma_m_slope": sigma_m_slope,
        "sigma_m_b": sigma_m_b,
        "alpha_tofidx_average": alpha_tofidx_average,
    }

    if mass_cal_write == MassCalibOutput.TXT:
        export_mc(
            cl_run.get_path(Paths.MASS_CALIB_FILE, ionization_method=ionization_method),
            rt_idx_centre_list,
            params_optimised,
            sigma_tofidx_slope,
            sigma_tofidx_b,
            sigma_m_slope,
            sigma_m_b,
            alpha_tofidx_average,
            mass_cal_u.index.values,
            mass_cal_u.values,
            masscal_list_exact,
            cl_run.s_per_bin,
            mass_cal_mode_index,
        )
        df_peaks.to_csv(
            cl_run.out_dir / path_file.with_suffix(".calibration_peaks.csv").name,
            index=False,
        )

    elif mass_cal_write == MassCalibOutput.WRITE_HDF5:
        # export mass calibration into hdf5 file,
        # write it in TofDaq-compatible format
        cl_run.export_mc_hdf5_tw(
            params_optimised,
            mass_cal_mode_index,
        )
        logger.info("hdf5 file updated with new mass cal.")

    else:
        raise ValueError(
            f"Invalid value for {mass_cal_write=}, expected "
            "'MassCalibOutput.TXT','MassCalibOutput.WRITE_HDF5'"
        )

    t1 = time.time()
    logger.info(f"Mass calibration done in: {t1 - t0:.1f} seconds")

    if plots:
        make_final_plots(
            df_peaks,
            cl_run,
            plots,
            params_optimised,
            mass_cal_statistics,
            show_plots=show_plots,
            mass_drift_in_mass_unit=mass_drift_in_mass_unit,
        )

    return None


def extract_data_for_mass_cal(
    dict_frag: dict[str, list[tuple[int, int, int]]],
    dict_frag_mode: dict[str, TrackingMassesMode],
    cl_run: hdf5Metadata,
    segments: tuple[int, int] | None = None,
    mass_domain_range: float = 0.2,
    mass_domain_range_ppm: float | None = None,
    mass_peak_noise_threshold: float = 1.3,
    tofid_base_correction: Callable[[float], float] | None = None,
    sigma_tofidx_b_default: float = 0.36456746757561603,
    sigma_tofidx_slope_default: float = 4.8076576034728846e-5,
    plots: list[Plots] = [],
    show_plots: bool = True,
) -> pd.DataFrame:
    """Extract the data for mass calibration.

    :arg dict_frag: A dictionary of fragments to track over time.
        The format is:
        {
            "fragment": [(rt_start, rt_end, rt_center), ...],
            ...
        }
    :arg dict_frag_mode: A dictionary of the mode of the fragments.
        The format is:
        {
            "fragment": TrackingMassesMode,
            ...
        }
    :arg cl_run: The hdf5Metadata object of the run.

    ## Extraction parameters

    :arg segments: A tuple specifying which segments to use.
        Will use all segments between [start, end[ .
    :arg mass_domain_range: A mass value (atomic mass unit) range to which
        to look at. The range to look for the mass is : mass +/- mass_domain_range.
    :arg mass_domain_range_ppm: A value in ppm of the mass domain range.
        The extraction will crop both, in ppm and in atomic mass unit.
        Similar to `mass_domain_range` but in ppm.
        This is useful mostly for smaller masses which can quickly span
        a large range in ppm.
        If given, the extraction will crop to the smallest of the two.
    :arg mass_peak_noise_threshold: Threshold use to determine if a peak is a
        noise or a peak. It is a ration between the target peak intensity and
        the median of intensity value in the domain.
    :arg sigma_tofidx_b_default: Default parameter for how we fit the
        pseudo voigt to mass profiles
    :arg sigma_tofidx_slope_default: Default parameter for how we fit the
        pseudo voigt to mass profiles

    """

    logger = logging.getLogger("alpinac.mass_calibration.extract_data_for_mass_cal")
    all_dfs = []

    default_mass_cal = cl_run.mc_h5.params

    # Guess the ionization method
    # TODO: remove that once properly implemented
    if cl_run.hdf5_dim3 == 1:  # this is EI MS
        ionisation_method = IonizationMethod.EI
    else:
        ionisation_method = IonizationMethod.EI_CI

    # Set defualt value for the segments
    if segments is None:
        # Should check to read that in the file if not given
        logger.warning("No ionization segemnts given")
        if ionisation_method == IonizationMethod.EI:
            dim3_start = 0
            dim3_stop = 9
        elif ionisation_method == IonizationMethod.EI_CI:
            dim3_start = 0
            dim3_stop = 1
        else:
            raise ValueError(f"No default value for {ionisation_method=}")
    else:
        # User given segments
        dim3_start, dim3_stop = segments

    for fragment, retention_tof_list in dict_frag.items():
        frag_mode = dict_frag_mode[fragment]

        mass_exact = formula_to_mass(fragment)
        mass_int = int(round(mass_exact))

        this_mass_domain = mass_domain_range

        if mass_domain_range_ppm is not None:
            this_mass_domain = min(
                this_mass_domain, mass_domain_range_ppm * mass_exact * 1e-6
            )
        logger.debug(
            f"Extracting peaks for {fragment} ({mass_exact:4f} mz) in mass domain {this_mass_domain:4f}"
        )

        tofid_range = f_mass_to_tofidx(
            np.array(
                [
                    mass_exact - this_mass_domain,
                    mass_exact,
                    mass_exact + this_mass_domain,
                ]
            ),
            default_mass_cal,
        )
        if tofid_base_correction is not None:
            tofid_range = tofid_base_correction(tofid_range)

        # Create the tofid axis
        tofid_int_left, tofid_int, tofid_int_right = tofid_range.round(0).astype(int)
        tofidx_series = np.arange(tofid_int_left, tofid_int_right + 1, dtype=float)

        logger.info(
            f"Candidate: {fragment}; mass_target: {mass_exact:.2f}; ToF: {tofid_int}"
        )
        logger.debug(f"{tofidx_series=}")

        # Check the tofid is in range
        if tofid_int_right > cl_run.len_mass_axis:
            raise ValueError(
                f"Mass of {fragment} = {mass_exact:.2f} is too high for the mass in the file. "
                "Please remove this fragement and any other with a higher mass."
            )

        if frag_mode == TrackingMassesMode.CONTINUOUS:
            # here load selected hdf5 data
            # To save time, load all at once if continuous mode.
            logger.debug(f"Loading data for {fragment} in continuous mode")
            logger.debug(f"{tofid_int_left=}, {tofid_int_right=}")
            tof_data_full_rt_range = cl_run.import_hdf5_data(
                int(0),
                cl_run.len_time_axis * cl_run.s_per_bin,
                dim3_start,
                dim3_stop,
                tofid_int_left,
                tofid_int_right + 1,
            )

        results = {
            col: []
            for col in [
                "rt",  # retention time (seconds)
                "tof",  # time of flight index value
                "area",  # area value of peak in mass domain
                "sigma",  # sigma of peak in mass domain
                "alpha",  # alpha of peak in mass domain
                "baseline",  # baseline value over the mass axis
            ]
        }

        # ===================================================================
        # LOOP OVER TIME INTERVALS FOR A GIVEN EXACT MASS
        # ===================================================================

        for rt_range in retention_tof_list:  # take one time slice
            rt_start, rt_end, rt_center = rt_range

            if frag_mode == TrackingMassesMode.CONTINUOUS:
                # load selected tof data for the full RT domain,
                # sum over retention time
                tof_data = tof_data_full_rt_range[rt_start:rt_end, :]

            elif frag_mode == TrackingMassesMode.SPIKE:
                # Import only the spike data
                tof_data = cl_run.import_hdf5_data(
                    rt_start * cl_run.s_per_bin,
                    rt_end * cl_run.s_per_bin,
                    dim3_start,
                    dim3_stop,
                    tofid_int_left,
                    tofid_int_right + 1,
                )
            else:
                raise NotImplementedError(
                    f"Unimplemented TrackingMassesMode {frag_mode}"
                )
            # Sum over the RT
            i_series = np.sum(tof_data, axis=0)

            peak_detected = peak_detect(
                tofidx_series,
                i_series,
                const_pd.WTtofidx,
                const_pd.PTtof_mass_cal,
                const_pd.tof_per_bin,
                graph=Plots.PEAK_DETECTION in plots and show_plots,
                mode="tofidx_peak_detect",
            )
            logger.debug(f"{rt_center=}, {peak_detected=}")
            # speed up process: eliminate candidate peak
            # that are below const_pd.mass_peak_noise_threshold* median.

            if len(peak_detected) > 1:
                # Filter out the peaks that have a too small intentsity
                logger.debug(f"peaks before filter:{peak_detected}")
                # Calculate median using first third of data, to be closer to baseline
                i_series_increasing_indexes = i_series.argsort()
                # Get median of the less (one third) intensitive peaks
                i_series_median = np.median(
                    i_series[i_series_increasing_indexes[0 : int(len(i_series) / 3)]]
                )
                threshold_intensity = mass_peak_noise_threshold * i_series_median
                indexes = np.flatnonzero(peak_detected[:, 1] >= threshold_intensity)
                peak_detected = peak_detected[indexes, :]
                logger.debug(
                    f"peaks after filter:{peak_detected}, {threshold_intensity=}"
                )

            if len(peak_detected) == 0:
                continue

            # Fit a profile to the peak
            mu_opt, A_opt, sigma_opt, alpha_opt, baseline_opt_pv = fit_pseudoVoigt(
                x=tofidx_series,
                y=i_series,
                mu_guess=peak_detected[:, 0],
                apex_guess=peak_detected[:, 1],
                alpha_guess=const_pd.mc_alpha_guess,
                n_per_bin=const_pd.tof_per_bin,
                sigma_tofidx_slope=sigma_tofidx_slope_default,
                sigma_tofidx_b=sigma_tofidx_b_default,
                graph=Plots.PSEUDOVOIGT_FITTING in plots and show_plots,
                mode="MassCal",
            )

            n_peaks_fitted = len(mu_opt)
            if n_peaks_fitted == 0:
                continue

            # we got the peaks in TOF domain for one unit mass
            results["rt"].extend([rt_center] * n_peaks_fitted)
            results["tof"].extend(mu_opt)
            results["area"].extend(A_opt)
            results["sigma"].extend(sigma_opt)
            results["alpha"].extend(alpha_opt)
            results["baseline"].extend([baseline_opt_pv] * n_peaks_fitted)

        df = pd.DataFrame(results)

        if len(df) == 0:
            logger.warning(f"No peaks found for {fragment}")
            continue

        logger.info(f"Found {len(df)} peaks in the time domain for {fragment}")
        logger.debug(f"{df=}")

        df["mass_int"] = mass_int
        df["ppm"] = 0.0
        df["fragment"] = fragment

        # sort by increasing time of flight index:
        df = df.sort_values(by=["tof"])

        df["mass_exact"] = mass_exact
        df["frag_mode"] = frag_mode.value

        logger.info(f"{fragment} {mass_exact:.2f}: {len(df)} solutions in time")
        all_dfs.append(df)

    # Combine all the dataframes
    df_peaks = pd.concat(all_dfs, ignore_index=True)

    return df_peaks


def make_final_plots(
    df_peaks: pd.DataFrame,
    cl_run: hdf5Metadata,
    plots: list[Plots],
    params: list[MassCalParams],
    mass_cal_statistics: dict[str, float],
    show_plots: bool = False,
    mass_drift_in_mass_unit: bool = True,
):

    logger = logging.getLogger("alpinac.mass_calibration.make_final_plots")
    logger.info("Making final plots")
    logger.debug(f"{mass_cal_statistics=}")
    logger.debug(f"{params=}")
    logger.debug(f"\n{df_peaks=}")

    # cmap = plt.get_cmap("nipy_spectral")

    frags_mass = pd.Series(
        df_peaks["fragment"].values, index=df_peaks["mass_exact"].values
    )
    frags_mass = frags_mass[~frags_mass.index.duplicated(keep="first")]
    frags_mass = frags_mass.sort_index()
    # normalized = pd.Series(normalize(frags_mass.index), index=frags_mass.values)
    # color_of_frag = {frag: cmap(norm_mass) for frag, norm_mass in normalized.items()}

    color_iter = itertools.cycle(color1_names)
    color_of_frag = {frag: next(color_iter) for frag in frags_mass.values}
    logger.debug(f"{color_of_frag=}")

    df_peaks["color"] = df_peaks["fragment"].map(color_of_frag)

    if Plots.PARAMETERS in plots:

        # make graph with p1, p2, p3 optimised values as function of rt:
        fig, axes = plt.subplots(5, 1, sharex=True, figsize=(8, 10))
        (ax1, ax2, ax3, ax4, ax_drift) = axes
        axes[-1].set_xlabel("Retention time [s]")
        # fig, ax1 = plt.subplots(figsize=(8,4))
        ax1.set_title("Optimised parameters")

        ax4.set_ylabel("Residual [ppm]")
        ax_drift.set_ylabel("Mass drift [tofid]")

        tof_ids = {}
        test_masses = np.array([20.0, 50.0, 100.0, 200.0])

        for this_rt_params in params:
            if this_rt_params is None:
                # Mass calib did not work
                continue
            rt = this_rt_params["rt"]

            for p, color, ax in zip(
                ["p1", "p2", "p3"],
                ["xkcd:red", "xkcd:black", "xkcd:blue"],
                [ax3, ax2, ax1],
            ):
                if p in this_rt_params and this_rt_params[p]:
                    ax.plot(rt, this_rt_params[p], "*", color=color)
                ax.set_ylabel(p)

            # Calculate the mass drift at this rt index
            test_masses_tof = f_mass_to_tofidx(test_masses, this_rt_params)
            tof_ids[rt] = test_masses_tof

        # Calculate the mass drift
        df_to_plot = pd.DataFrame(tof_ids, index=test_masses).T.sort_index()
        df_drift = df_to_plot - df_to_plot.iloc[0]
        for mass in test_masses:
            ax_drift.plot(df_drift.index, df_drift[mass], label=f"{mass:.1f}")
        ax_drift.legend(title="Mass")

        ax4.scatter(
            df_peaks["rt_s"],
            df_peaks["ppm"],
            c=df_peaks["color"],
        )

        fig.tight_layout()

        filename = (
            f"{cl_run.file_path.stem}_mc_{int(const_pd.mc_avrg_min)}min_params.png"
        )

        plt.savefig(
            cl_run.out_dir / filename,
            bbox_inches="tight",
            transparent=True,
            dpi=150,
        )
        if show_plots:
            plt.show()

    # ****************************************************
    if Plots.RESIDUALS_VS_MASS in plots:
        fig, ax1 = plt.subplots(figsize=(8, 4))
        ax1.ticklabel_format(axis="both", style="plain", useMathText=False)
        ax1.grid(True, color="xkcd:grey", linestyle="--", linewidth=0.5)
        mass_range = np.arange(20, 300, 10)
        ax1.plot(
            mass_range,
            mass_cal_statistics["sigma_m_b"]
            + mass_range * mass_cal_statistics["sigma_m_slope"],
            linestyle="--",
            color="xkcd:royal blue",
        )

        ax1.scatter(
            df_peaks["mz_h5_cal"],
            df_peaks["ppm"],
            c=df_peaks["color"],
        )

        ax1.set_xlabel("Centre of mass [m/z]")
        ax1.set_ylabel("Residual [ppm]")

        fig.tight_layout()
        filename = f"{cl_run.file_path.stem}_{int(const_pd.mc_avrg_min)}min_residuals_vs_mass.png"

        plt.savefig(
            cl_run.out_dir / filename,
            bbox_inches="tight",
            transparent=True,
            dpi=150,
        )
        if show_plots:
            plt.show()

    # ****************************************************
    if Plots.SIGMA_VS_MASS in plots:

        fig, ax1 = plt.subplots(figsize=(8, 4))
        ax1.ticklabel_format(axis="both", style="plain", useMathText=False)
        ax1.grid(True, color="xkcd:grey", linestyle="--", linewidth=0.5)
        mass_range = np.arange(20, 300, 10)
        ax1.plot(
            mass_range,
            mass_cal_statistics["sigma_m_b"]
            + mass_range * mass_cal_statistics["sigma_m_slope"],
            linestyle="--",
            color="xkcd:royal blue",
        )

        ax1.scatter(
            df_peaks["mz_h5_cal"],
            df_peaks["sigma_mz"],
            c=df_peaks["color"],
        )

        ax1.set_xlabel("Centre of mass [m/z]")
        ax1.set_ylabel("Sigma [m/z]")

        fig.tight_layout()
        filename = (
            f"{cl_run.file_path.stem}_{int(const_pd.mc_avrg_min)}min_sigma_vs_mass.png"
        )
        plt.savefig(
            cl_run.out_dir / filename,
            bbox_inches="tight",
            transparent=True,
            dpi=150,
        )
        if show_plots:
            plt.show()

    # ****************************************************
    # Figure: FWHM vs mass
    # FWHM = 2 * sigma_m * sqrt(2 ln(2))
    """
    fig, ax1 = plt.subplots(figsize=(8,4))
    ax1.ticklabel_format(axis = 'both', style = 'plain', useMathText=False)
    ax1.grid(True, color='xkcd:grey', linestyle='--', linewidth=0.5)
    #m_plt = [m for m in range(20, 300, 10)]
    #ax1.plot(m_plt, [sigma_m_b + m * sigma_m_slope for m in m_plt],  linestyle='--', color = 'xkcd:royal blue')
    
    for i in range(len(mass_cal_list_np_final)):
        plt_mass = f_tofidx_to_mass(mass_cal_list_np_final[i,cl_run.i_tof], h5_params)
        plt_sigma_m = f_tofidx_to_mass(mass_cal_list_np_final[i, cl_run.i_tof] + mass_cal_list_np_final[i,cl_run.i_sigma], h5_params) - plt_mass
        plt_mass_res =  (2 * plt_sigma_m * math.sqrt(2 * math.log(2))) /plt_mass *1000000
        ax1.plot(plt_mass, plt_mass_res,'.', color = str(color1_names[int(mass_cal_list_np_final[i,cl_run.i_mass_exact])]))
        
    text_leg = [None] * (len(masscal_formula_list)+1) #masscal_formula_list.copy
    text_leg[0] = 'fit'
    text_leg[1:len(text_leg)] = masscal_formula_list
    #ax1.legend(text_leg)
    plt.xlabel('Centre of mass [m/z]') 
    ax1.set_ylabel('FWHM [ppm]')           
    
    fig.tight_layout()
    filename = str(cl_run.toffile_name) + '_' + str(int(const_pd.mc_avrg_min)) + 'min' + '_mass_res_ppm'  + '.png'
    filename_path = const_path.masscal_output / filename
    plt.savefig(filename_path, bbox_inches='tight', transparent = True, dpi = 150)
    plt.show()
    """

    # ****************************************************
    # Figure: mass resolution = mass / FWHM
    # FWHM = 2 * sigma_m * sqrt(2 ln(2))
    """
    fig, ax1 = plt.subplots(figsize=(8,4))
    ax1.ticklabel_format(axis = 'both', style = 'plain', useMathText=False)
    ax1.grid(True, color='xkcd:grey', linestyle='--', linewidth=0.5)
    #m_plt = [m for m in range(20, 300, 10)]
    #ax1.plot(m_plt, [sigma_m_b + m * sigma_m_slope for m in m_plt],  linestyle='--', color = 'xkcd:royal blue')
    
    for i in range(len(mass_cal_list_np_final)):
        plt_mass = f_tofidx_to_mass(mass_cal_list_np_final[i,cl_run.i_tof], h5_params)
        plt_sigma_m = f_tofidx_to_mass(mass_cal_list_np_final[i, cl_run.i_tof] + mass_cal_list_np_final[i,cl_run.i_sigma], h5_params) - plt_mass
        plt_mass_res = plt_mass / (2 * plt_sigma_m * math.sqrt(2 * math.log(2)))
        ax1.plot(plt_mass, plt_mass_res,'.', color = str(color1_names[int(mass_cal_list_np_final[i,cl_run.i_mass_exact])]))
        
    text_leg = [None] * (len(masscal_formula_list)+1) #masscal_formula_list.copy
    text_leg[0] = 'fit'
    text_leg[1:len(text_leg)] = masscal_formula_list
    #ax1.legend(text_leg)
    plt.xlabel('Centre of mass [m/z]') 
    ax1.set_ylabel('Mass resolution [mass / FWHM]')           
    
    fig.tight_layout()
    filename = str(cl_run.toffile_name) + '_' + str(int(const_pd.mc_avrg_min)) + 'min' + '_mass_res'  + '.png'
    filename_path = const_path.masscal_output / filename
    plt.savefig(filename_path, bbox_inches='tight', transparent = True, dpi = 150)
    plt.show()
    """

    # ***************************************
    # Figure: mass drift vs retention time
    if Plots.MASS_DRIFT in plots:

        fig, ax1 = plt.subplots(figsize=(8, 6))
        ax1.ticklabel_format(axis="both", style="plain", useMathText=False)
        ax1.grid(True, color="xkcd:grey", linestyle="--", linewidth=0.5)

        for fragment_formula_str in df_peaks["fragment"].unique():
            # Get indexes of this mass
            df_this = df_peaks.loc[
                df_peaks["fragment"] == fragment_formula_str
            ].sort_values(by=["rt"])
            if len(df_this) == 0:
                continue

            if mass_drift_in_mass_unit:
                # Get the drift in ppm
                first_value = df_this["mz_h5_cal"].iloc[0]
                drift = (df_this["mz_h5_cal"] - first_value) / first_value * 1e6

            else:
                drift = df_this["tof"] - df_this["tof"].iloc[0]

            color = color_of_frag[fragment_formula_str]

            ax1.plot(
                df_this["rt_s"],
                drift,
                "--.",
                color=color,
                label=fragment_formula_str,
            )

        ax1.legend(
            title="Fragments",
            ncol=3,
        )
        ax1.set_xlabel("Retention time [s]")
        ax1.set_ylabel(
            f"Mass diff from first mass [{'ppm' if mass_drift_in_mass_unit else 'tofid'}]"
        )

        fig.tight_layout()
        filename = f"{cl_run.file_path.stem}_mass_drift.png"
        fig.savefig(
            cl_run.out_dir / filename,
            bbox_inches="tight",
            transparent=True,
            dpi=150,
        )
        if show_plots:
            plt.show()

    # ****************************************************
    # Figure: alpha vs TOF-index
    """
    fig, ax1 = plt.subplots(figsize=(8,8))
    ax1.ticklabel_format(axis = 'both', style = 'plain', useMathText=False)
    ax1.grid(True, color='xkcd:grey', linestyle='--', linewidth=0.5)
    
    for idx_mass_exact in range(len(masscal_list_exact)):
        #plt_tofidx_list = [line[1] for line in mass_cal_list[idx_mass_exact]]
        #plt_mass_list = [f_tofidx_to_mass(line[1], h5_params) for line in mass_cal_list[idx_mass_exact]]
        #plt_alpha_list =  [line[2] for line in mass_cal_list[idx_mass_exact]]
        ax1.plot(plt_tofidx_list, plt_alpha_list, '.', color = str(color1_names[idx_mass_exact]))
    
    ax1.legend(masscal_formula_list)
    plt.xlabel('Centre of TOF index [TOF-index]') 
    ax1.set_ylabel('alpha [no unit]')           
    
    fig.tight_layout()
    #filename = 'alpha_vs_tofidx_' +str(const_pd.mc_avrg_min) + 'min_' + str(toffile_name) + '.png'
    #filename_path = const_path.masscal_output / filename
    #plt.savefig(filename_path, bbox_inches='tight', transparent = True, dpi = 150)
    plt.show()
    """

    return None
