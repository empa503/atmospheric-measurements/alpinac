"""Utilties for mass calibration."""

import logging
from enum import Enum

import numpy as np

MassCalParams = dict[str, int | float]


class MassCalibOutput(Enum):
    TXT = 0
    WRITE_HDF5 = 1


def raise_mass_cal_mode_unsupported(mass_cal_mode: int) -> None:
    """Raise an error if the mass calibration mode is not supported"""

    raise ValueError(
        f"Mass calibration mode {mass_cal_mode} is not supported. \n"
        "Only modes 0, 1, 2, 3, 4, 5 are supported. "
    )


def number_of_params(mass_cal_mode: int) -> int:
    """Return the number of parameters for the mass calibration"""
    match mass_cal_mode:
        case 0 | 1:
            n = 2
        case 2 | 5:
            n = 3
        case 3:
            n = 4
        case 4:
            n = 5
        case _:
            raise_mass_cal_mode_unsupported(mass_cal_mode)

    return n


def mass_cal_string(mass_cal_params: MassCalParams) -> str:
    """Return a string representation of the mass calibration parameters"""

    mass_cal_mode = mass_cal_params["mode"]

    p1 = mass_cal_params.get("p1", None)
    p2 = mass_cal_params.get("p2", None)
    p3 = mass_cal_params.get("p3", None)
    p4 = mass_cal_params.get("p4", None)
    p5 = mass_cal_params.get("p5", None)

    match mass_cal_mode:
        case 0:
            return f"i(m) = {p1:.2f} * sqrt(m) + {p2:.2f}"
        case 1:
            return f"i(m) = {p1:.2f} / sqrt(m) + {p2:.2f}"
        case 2:
            return f"i(m) = {p1:.2f} * m ** {p3:.5f} + {p2:.2f}"
        case 3:
            return f"i(m) = {p1:.2f} * sqrt(m) + {p2:.2f}  + {p3:.5f} * (m - {p4:.2f}) ** 2"
        case 4:
            return f"i(m) = {p1:.2f} * sqrt(m) + {p2:.2f}  + {p3:.5f} * m**2 + {p4:.2f} * m + {p5:.2f}"
        case 5:
            return f"m(i) = {p1:.2f} * i**2 + {p2:.2f} * i + {p3:.5f}"

        case _:
            raise_mass_cal_mode_unsupported(mass_cal_mode)


def _f_tofix_to_mass_mode_3_and_4(
    tofidx: np.ndarray, mass_cal_params: MassCalParams
) -> np.ndarray:

    logger = logging.getLogger(__name__)
    p1, p2, p3, p4, p5 = [mass_cal_params.get(f"p{i}", None) for i in range(1, 6)]
    assert p4 is not None, "p4 should be given for mode 3 and 4"

    match mass_cal_params["mode"]:
        case 3:
            assert p5 is None, "p5 should be None for mode 3"
            # This is the result of solving the polynomial equation
            # It is quite tedious to do by hand, but it is possible
            # A mathematician would call it trivial
            coeffs = [
                p3**2,  # m4
                -2 * p3**2 * p4,  # m3
                2 * p3 * (p3 * p4**2 - tofidx + p2 - p3 * p4),  # m2
                (4 * p3**2 * p4**2) - (2 * p3**2 * p4**3) - p1**2,  # m1
                # m0
                tofidx**2
                + tofidx * (-2 * p2 + 4 * p3 * p4 - 2 * p3 * p4**2)
                + (
                    p2**2
                    - 2 * p3**2 * p4**3
                    - 4 * p2 * p3 * p4
                    + p3**2 * p4**2
                    + 2 * p2 * p3 * p4**2
                ),
            ]

        case 4:
            assert p5 is not None, "p5 should be given for mode 4"
            # Merge the two constant parameters
            pc = p2 + p5

            # Solving the polynomial equation
            coeffs = [
                p3**2,  # m4
                2 * p3 * p4,  # m3
                p4**2 + 2 * p3 * pc - 2 * p3 * tofidx,  # m2
                2 * p4 * pc - p1**2 - 2 * p4 * tofidx,  # m1
                tofidx**2 - 2 * tofidx * pc + pc**2,  # m0
            ]

        case _:
            raise ValueError(f"Invalid mode {mass_cal_params['mode']}")

    # Now some coeffs are constants and some are arrays, wo wee need to solve
    # the polynomials for each tofix

    coeffs_arrays = [np.full_like(tofidx, c) if np.isscalar(c) else c for c in coeffs]
    # Reverse the coefficients in order to use np.roots
    coeffs_arrays = np.array(list(reversed(coeffs_arrays)))
    logger.debug(f"coeffs: {coeffs_arrays}")

    fit_roots = lambda x: np.polynomial.Polynomial(x).roots()

    roots = [fit_roots(c) for c in coeffs_arrays.T]
    logger.debug(f"roots: {roots}")

    # We need to find the real roots
    def select_real_roots(roots):
        for mass_root in roots:
            logger.debug(f"{mass_root=}")
            if mass_root.real > 0 and mass_root.imag == 0:
                return mass_root.real

        return 0.0

    # Select the real roots
    masses = np.array([select_real_roots(r) for r in roots])

    return masses


def f_tofidx_to_mass(tofidx: np.ndarray, mass_cal_params: MassCalParams) -> np.ndarray:
    """Calculate mass [m/z] based on time of flight index


    :arg tofidx: Time of flight index (as float)
        Centre of tof peak, result of tof peak integration
    :arg mass_cal_params: Mass calibration parameters.
    :return masses: Masses [m/z] corresponding to the tofidx
    """
    match mass_cal_params["mode"]:
        case 0:
            mass = ((tofidx - mass_cal_params["p2"]) / mass_cal_params["p1"]) ** 2

        case 1:
            mass = (mass_cal_params["p1"] / (tofidx - mass_cal_params["p2"])) ** 2

        case 2:
            mass = ((tofidx - mass_cal_params["p2"]) / mass_cal_params["p1"]) ** (
                1.0 / mass_cal_params["p3"]
            )

        case 3 | 4:
            # Requires solving a polynomial equation
            mass = _f_tofix_to_mass_mode_3_and_4(tofidx, mass_cal_params)

        case 5:
            mass = (
                mass_cal_params["p1"] * tofidx**2
                + mass_cal_params["p2"] * tofidx
                + mass_cal_params["p3"]
            )
        case _:
            raise_mass_cal_mode_unsupported(mass_cal_params["mode"])

    return mass


def f_mass_to_tofidx(mass: np.ndarray, mass_cal_params: MassCalParams) -> np.ndarray:
    """Calculate the tof index based on the mass.

    :arg mass: Masses [m/z] of the peaks.
    :arg mass_cal_params: Mass calibration parameters.
    :return tofidx: Time of flight indexes
    """
    match mass_cal_params["mode"]:
        case 0:
            tofidx = mass_cal_params["p1"] * np.sqrt(mass) + mass_cal_params["p2"]

        case 1:
            tofidx = mass_cal_params("p1") / np.sqrt(mass) + mass_cal_params["p2"]

        case 2:
            tofidx = (
                mass_cal_params["p1"] * mass ** mass_cal_params["p3"]
                + mass_cal_params["p2"]
            )

        case 3:
            tofidx = (
                mass_cal_params["p1"] * np.sqrt(mass)
                + mass_cal_params["p2"]
                + mass_cal_params["p3"] * (mass - mass_cal_params["p4"]) ** 2
            )

        case 4:
            tofidx = (
                mass_cal_params["p1"] * np.sqrt(mass)
                + mass_cal_params["p2"]
                + mass_cal_params["p3"] * mass**2
                + mass_cal_params["p4"] * mass
                + mass_cal_params["p5"]
            )

        case 5:
            a, b, c = (
                mass_cal_params["p1"],
                mass_cal_params["p2"],
                mass_cal_params["p3"] - mass,
            )
            # This is like solving a 2 order equation
            delta = b**2 - 4 * a * c
            if np.any(delta) < 0:
                raise ValueError("0 values found")
            # only keep the positive solution ?
            tofidx = (-b + np.sqrt(delta)) / (2 * a)

        case _:
            raise_mass_cal_mode_unsupported(mass_cal_params["mode"])

    return tofidx
