"""Utilities for extracting the data.

Implementation of algorithm from http://stackoverflow.com/a/22640362/6029703
"""

from __future__ import annotations

import logging
import math
import operator
from enum import Enum
from pathlib import Path
from typing import TYPE_CHECKING, Any, Iterable

import matplotlib.pyplot as plt
import numpy as np
from lmfit import Minimizer, Model
from lmfit.parameter import Parameters
from scipy.special import gamma  # for Voigt function # wofz, factorial
from scipy.stats import t as Student_t

import alpinac.const_peak_detection as const_pd
from alpinac.mass_calibration.utils import (
    f_tofidx_to_mass,
    f_mass_to_tofidx,
    mass_cal_string,
    number_of_params,
)
from alpinac.periodic_table import formula_to_mass
from alpinac.utils_user_def_params import make_colors

if TYPE_CHECKING:
    from alpinac.compound import Compound
    from alpinac.io_tools_hdf5 import hdf5Metadata

# define colors
color1_names, color2_names, color3_names = make_colors()

logger = logging.getLogger("alpinac.utils_data_extraction")


class TrackingMassesMode(Enum):
    """Mode of tracking a mass.

    CONTINUOUS: The mass is tracked continuously over the whole chromatogram.
    SPIKE: The mass is tracked only during a spike, at a specific time range.
    """

    CONTINUOUS = 0
    SPIKE = 1


# fmt: off

def find_time(rt_suspect, rt_suspect_U, s_per_bin, time_axis):
    rt_time_idx_start=int(round((rt_suspect-rt_suspect_U)/s_per_bin))
    rt_time_idx_stop =int(round((rt_suspect+rt_suspect_U)/s_per_bin))
    rt_time_idx =[idx for idx in range(rt_time_idx_start, rt_time_idx_stop)]
    time_timeseries = [time_axis[idx] for idx in range(rt_time_idx_start,rt_time_idx_stop)]
    return rt_time_idx, time_timeseries

    
def find_closest_centre(x_exact, array_x):
    """
    Array_x has len>1
    Each array_x[i] is a list of x_values belonging to the same centre of x (mass of TOF-index)
    We want to find which array_x[i] has the centre closest to x_exact
    and return the index of this list only
    """
    idx_best =0
    min_diff = abs(np.median([array_x[0][i][1] for i in range(len(array_x[0]))]) - x_exact)
    diff = abs(np.median([array_x[1][i][1] for i in range(len(array_x[1]))]) - x_exact)
    #logging.info('Mass diff 0: ' + str(min_diff) + '; mass diff 1: ' + str(diff))
    
    
    #as buckets are ordered by increasing masses, 
    #median of masses of bucket should also be ordered by increasing masses
    idx_array_x = 1
    #do len test first!
    while idx_array_x < len(array_x) and diff < min_diff:
        idx_best = idx_array_x #+ 1
        min_diff = diff
        idx_array_x += 1
        #logging.info(idx_array_m)
        if idx_array_x < len(array_x): # need to add this otherwise 'list index out of range' for last line
            diff = abs(np.median([array_x[idx_array_x][i][1] for i in range(len(array_x[idx_array_x]))]) - x_exact)
            #logging.info('Mass diff ' +str(idx_array_x) +  ': ' + str(diff))
 
    return idx_best




def peak_detect(
    x, y,
    PW, PT, n_per_bin,
    graph: bool = False, 
    mode: str = 'tofidx_peak_detect',
    additional_plot_info: str = ""
):
    """
    This function detects zero to multiple peaks in x, y values. x can be time or mass.
    A peak is detected based on slope calculation and detection parameters:
    minimum slope value, slope going from positive to negative value.
    The function returns:
    peak_position_final: list of rough estimate(s) of peak centre position(s) 
    peak_value_final: list of rough estimate(s) of intensity at peak centre position(s)

    Arguments:
    x: np.array of floats (not integer! even if they are actually indices values)
    y: np.array of floats
    PW: expected peak width, unit same as x
    PT: peak threshold, minimum abs(slope) to consider a peak is present
    n_per_bin: number of point per unit of x. n_per_bin * PW gives number of points making a peak.
    For the RT domain, n_per_bin is s_per_bin (usually 0.166, 6 pts per seconds)
    For the time of flight domain, n_per_bin is one.
    additional_plot_info: an additional string to attach to the title

    
    return:
    peak_position_final, np.array of floats
    peak_value_final, np.array of floats
    """

    logger = logging.getLogger('alpinac.utils_data_extraction.peak_detect')
    
    #logging.info('Detection thereshold: ' + str(PT))
    #n has to be an odd number (1,3,5...)
    n = int(PW/n_per_bin/2)
    if (n % 2) == 0: #n is an even number
        n+=1
    
    if n < 5: n = 5
    #if n > 17: n = 17
    
    if len(y)-n < 5: #20191002
        logger.debug(f"No peak detected. Unsufficient data for peak detection filter.")
        return []

    slope_list = np.zeros((len(y)-n,4))
    
    #20190905 the x are always evenly spaced so
    #den can be calculated just once
    den = np.sum(x[0:n]**2) - (np.sum(x[0:n]))**2/n
    
    for i in range(len(y)-n): #window for slope calculation, i is starting point
        x_sum = np.sum(x[i:i+n]) #last stated index is included
        #logging.info(x[i:i+n])
        #logging.info(x_sum)
        #logging.info(den)
        if den != 0:
            y_sum = np.sum(y[i:i+n])
            num = np.sum(x[i:i+n] * y[i:i+n]) - x_sum*y_sum/n            
            slope = num/den
        else : slope = 0
        slope_list[i:] = [x[i+int(n/2)], y[i+int(n/2)], slope, 0.0]
        #20190906 mygu the above looks correct but I still don't understand why
        #slope_list[i:] = [x[i+int(n/2)+1], y[i+int(n/2)+1], slope, 0.0]
    #logging.info(slope_list)
    
    for idx in range(len(slope_list)-1):      
        #Detection of peaks:
        if slope_list[idx,2] > 0 and slope_list[idx+1,2] <= 0: #and slope_value3 < 0
            #this is a peak
            slope_list[idx, 3] = 1.0
        #detection of valleys:
        elif slope_list[idx,2] < 0 and slope_list[idx+1,2] >= 0: # and slope_value3 > 0
            #this is a valley
            slope_list[idx, 3] = -1.0
    #logging.info(slope_list)
    #logging.info(len(slope_list))
    #201909096 mygu I think it does not work the following way:
    #slope_list[:, 3] = np.where(slope_list[:,2] > 0 and slope_list[idx+1,2] <= 0, 1.0, 0.0)
    #slope_list[:, 3] = np.where(slope_list[idx,2] < 0 and slope_list[idx+1,2] >= 0, -1.0, 0.0)
    
    #find indexes of slope list where a peak has been detected:
    peak_idx = np.argwhere(slope_list[:,3]>0)
    #same for the valleys:
    valley_idx = np.argwhere(slope_list[:,3]<0)
    #logging.info(peak_idx)
    #logging.info(valley_idx)
    
    idx_p = 0
    while len(peak_idx)>0 and idx_p < len(peak_idx):
        idx_peak = int(peak_idx[idx_p])

        if idx_p ==0:
            idx_start = 0
            if len(peak_idx)==1:
                idx_stop  = len(slope_list)-1
            elif len(peak_idx)>1:
                idx_stop  = int(peak_idx[idx_p+1]+1)
        elif idx_p > 0: #and idx_p == len(peak_idx)-1:
            idx_start = min(int(peak_idx[idx_p-1]+1), len(slope_list)-2)
            if idx_p == len(peak_idx)-1:
                idx_stop  = len(slope_list)-1
            else:
                #idx_start = int(peak_idx[idx_p-1]+1)
                idx_stop  = int(peak_idx[idx_p+1]+1) #the last index is out of the loop (?)
        
        #logging.info(idx_start)
        #logging.info(idx_stop)
        
        slope_max = max(slope_list[idx_start:idx_peak+1, 2])
        slope_min = min(slope_list[idx_peak:idx_stop, 2])
        slope_extrem = max(slope_max, abs(slope_min))
        
        if slope_extrem >= PT:
            #calculate more precise x position
            #assign y max value
            if idx_peak+1 < len(slope_list):
                x_value_zero_slope = slope_list[idx_peak,0] - slope_list[idx_peak,2] \
                * (slope_list[idx_peak+1,0] - slope_list[idx_peak,0]) \
                / (slope_list[idx_peak+1,2] - slope_list[idx_peak,2])
                slope_list[idx_peak,0] = x_value_zero_slope
                #look for max value of y around peak position:
            slope_list[idx_peak,1] = max(slope_list[max(0, idx_peak-1):min(idx_peak+1, len(slope_list)-1),1])
            #if the peak is on the last index of the list, do nothing    
            idx_p += 1  
            
        else:
            slope_list[peak_idx[idx_p],3] = 0
            peak_idx = np.argwhere(slope_list[:,3]>0) #le should be -1
            #logging.info(peak_idx)

    #check if data starts by tail of a peak:
    if len(valley_idx) > 0 and len(peak_idx) > 0 and valley_idx[0] < peak_idx [0]:
        #data start by tail of a peak peaking before the considered window
        #we arbitrarily assign a peak to the first point in the window
        slope_min = min(slope_list[0:int(valley_idx[0])+1, 2])
        if abs(slope_min) > PT:
            slope_list[0,3] = 1
            
    #take all valid peaks:
    #peak_position_final = slope_list[slope_list[:,3]>0, 0]
    #peak_value_final = slope_list[slope_list[:,3]>0, 1]
    
    peak_detected = slope_list[slope_list[:,3]>0, 0:2] #last index not included it seems
    
    if graph == True:
        
        #PLOT DATA AND SLOPE
        fig, ax1 = plt.subplots(figsize=(8,4))
        fig.suptitle(f'Peak detection {additional_plot_info}', fontsize=12)
        ax1.plot(x, y, '--bo') #linestyle='--', marker='o', color='b', , markersize = 10
        ax1.tick_params('y', colors='b')
        #ax1.set_xlim(1300, 1800)
        #ax1.set_ylim(0, 3000)

        #plot slope on second y-axis: 
        ax2 = ax1.twinx()
        ax2.plot(slope_list[:,0], slope_list[:,2], 'g-')
        ax2.plot(slope_list[:,0], np.zeros(len(slope_list)), 'k--')
        ax2.set_ylabel('slope', color='g')
        #ax2.set_ylim(-1000, 1000)
        #ax2.set_ylim(-5, 5)
        ax2.tick_params('y', colors='g')
        
        #legend of axis:
        if mode == 'tofidx_peak_detect':
            ax1.set_xlabel('Time of flight index [no unit]')
            ax1.set_ylabel('Intensity [V]', color='b')
        elif mode == 'rt_peak_detect':
            ax1.set_xlabel('Chromatographic retention time [s]')
            ax1.set_ylabel('Intensity [V * ToF index]', color='b')
        elif mode == 'histo_tofidx': 
            ax1.set_xlabel('Time of flight index [no unit]')
            ax1.set_ylabel('Number of peak centres', color='b')
        elif mode == 'histo_rt': 
            ax1.set_xlabel('Chromatographic retention time [s]')
            ax1.set_ylabel('Number of peak centres', color='b')
        else:
            ax1.set_xlabel('x axis')
            ax1.set_ylabel('y axis', color='b')
            
            
        
    
        if len(peak_detected) > 0:  
            ax1.plot(peak_detected[:,0], peak_detected[:,1], 'o', color = 'xkcd:peach', markersize = 12)
        
        #need to add same condition as when creating the figure
        #plt.savefig('peak_detect_'+ str(int(round(peak_position_final[0])))+'.png', bbox_inches='tight', transparent = True, dpi = 300)
        
        #plt.savefig('peak_detect_test293.png', bbox_inches='tight', transparent = True, dpi = 300)
        fig.tight_layout()
        plt.show()

    
    return peak_detected

def make_mass_cal_dict_of_frag(
    dict_spikes,
    spike_duration,
    max_filament_value_pos,
    cl_run: hdf5Metadata,
    averaging_timspan: float = 360., # Seconds 
    spacing_timespan: float = 120., # Seconds
) -> tuple[
    dict[str, list[list[int, int, int]]],
    dict[str, TrackingMassesMode],
    list[float],
    list[int],
]:
    
    """Read the dictionary of where the fragement spikes.

    TODO: one could add the when the filament is on to know when we should look for stuff
    #cl_run.read_filament_value()
    

    Parameters
    ----------
    dict_spikes : TYPE dictionnary
        DESCRIPTION. Each key is the centre rt of a spike.
        Each key contains a list of chemial formula to use for that specific spike.
    ...
    Returns
    -------
    dict_frag: dictionnary
        Each key is a fragment formula. Each key contain a list 
        with the rt_index spans and rt_index center where to search for this mass.
        ex. [0, 1800, 900] -> seach between 0 and 1800 using center 900
    dict_frag_mode: dictonary with the mode of tracking for each fragment
    masscal_list_exact: The exact mass of each fragment
    rt_idx_centre_list: List of center of rt spans (already present as 3 arg in the dict frag.)
    """

    mode_continuous_status = False
    dict_frag: dict[str, list[list[int, int, int]]] = {}
    dict_frag_mode: dict[str, TrackingMassesMode]  = {}
    
    #[frag] -> rt_idx_start, rt_idx_stop, rt_idx_centre
    masscal_list_exact = []
    rt_idx_centre_list = []
        
    for key_spike in dict_spikes:
        
        if key_spike == '0.0':
            #these are the fragments to be treated as continuous extraction.
            if mode_continuous_status == False:
                mode_continuous_status = True
            
            #
            nb_pt_per_time_avrg = int(round( averaging_timspan /cl_run.s_per_bin))
            nb_pt_per_time_spacing = int(round(spacing_timespan /cl_run.s_per_bin))
            
            rt_start = 0.0 #seconds, start of mass calibration
            rt_idx_start = int(rt_start/cl_run.s_per_bin)
            
            #position of last index with minimum xx min average
            #and filament on
            idx_last = min(max_filament_value_pos*cl_run.hdf5_dim2, cl_run.len_time_axis - np.remainder(cl_run.len_time_axis, nb_pt_per_time_avrg) - nb_pt_per_time_avrg)
            rt_idx_list = np.linspace(rt_idx_start, idx_last, int(round((idx_last-rt_idx_start)/nb_pt_per_time_spacing)) +1, dtype = int)

            if len(rt_idx_centre_list) == 0:
                #save centre of each time slice:
                rt_idx_centre_list = rt_idx_list + int(round(nb_pt_per_time_avrg/2))
            

            for candidate_frag in dict_spikes[key_spike]:
                if candidate_frag not in dict_frag:
                    dict_frag[candidate_frag] = []
                    dict_frag_mode[candidate_frag] = TrackingMassesMode.CONTINUOUS
                for idx_rt in range(len(rt_idx_list)):
                    if idx_rt == len(rt_idx_list)-1:
                        rt_idx_stop = cl_run.len_time_axis
                    else:
                        rt_idx_stop = rt_idx_list[idx_rt] + nb_pt_per_time_avrg
                    dict_frag[candidate_frag].append([rt_idx_list[idx_rt], 
                                                      rt_idx_stop, 
                                                      rt_idx_list[idx_rt] + int(round(nb_pt_per_time_avrg/2))])                 
                    


        else:

            spike_start = float(key_spike) - spike_duration/2
            spike_stop = float(key_spike) + spike_duration/2
            spike_mid = float(key_spike)

            max_time = cl_run.len_time_axis * cl_run.s_per_bin

            if spike_start < 0:
                logger.warning(f"Spike start of spike {key_spike} s, set to 0 s")
                spike_start = 0
                spike_stop = spike_duration
                spike_mid = spike_duration/2
            
            if spike_stop > max_time:
                logger.warning(f"Spike stop of spike {key_spike} s, set to {max_time} s")
                spike_stop = max_time
                spike_start = max_time - spike_duration
                spike_mid = max_time - spike_duration/2

            for candidate_frag in dict_spikes[key_spike]:
                if candidate_frag not in dict_frag:
                    dict_frag[candidate_frag] = []
                    dict_frag_mode[candidate_frag] = TrackingMassesMode.SPIKE
                
                
                dict_frag[candidate_frag].append(
                    [
                        int(round(spike_start / cl_run.s_per_bin)), 
                        int(round(spike_stop / cl_run.s_per_bin)), 
                        int(round(spike_mid / cl_run.s_per_bin))
                    ]
                )
                
    masscal_list_exact = [formula_to_mass(key, ionized=True) for key in dict_frag]


    if mode_continuous_status == True:
        #there is at least one continuous mass.
        #Assign a rt centre from it to the spikes
        #Do not do this if there are only spikes
        for key in dict_frag:
            #logging.info(key)
            if dict_frag_mode[key] == TrackingMassesMode.SPIKE:
                
                for i_slice in range(len(dict_frag[key])):
                    rt_centre = dict_frag[key][i_slice][2]
                    if rt_centre not in rt_idx_centre_list:
                        min_interval = min([abs(rt - rt_centre) for rt in rt_idx_centre_list])
                        idx_min = [i for i in range(len(rt_idx_centre_list)) if abs(rt_idx_centre_list[i] - rt_centre) == min_interval]
                        dict_frag[key][i_slice][2] = rt_idx_centre_list[idx_min][0]
    else:
       
        rt_idx_centre_list = [int(round((float(key_spike)/cl_run.s_per_bin))) for key_spike in dict_spikes]
        
    
 
    return dict_frag, dict_frag_mode, masscal_list_exact, rt_idx_centre_list   

def calibrate_mass(rt_tofidx_peak, tofidx_peak, tofidx_peak_u, mass_cal_parameters):
    mass_calib_params = {}
    mass_calib_params['mode'] = 2
    
    #find preceeding and following rt from mass_cal_parameters:
    #do linear interpolation respective to rt of mass_cal_parameters
    if rt_tofidx_peak <= min([line[0] for line in mass_cal_parameters]):
        idx_mass_cal = 0
        mass_calib_params['p1'] = mass_cal_parameters[idx_mass_cal][1]
        mass_calib_params['p2'] = mass_cal_parameters[idx_mass_cal][2]
        mass_calib_params['p3'] = mass_cal_parameters[idx_mass_cal][3]
    elif rt_tofidx_peak >= max([line[0] for line in mass_cal_parameters]):
        idx_mass_cal = len(mass_cal_parameters)-1
        mass_calib_params['p1'] = mass_cal_parameters[idx_mass_cal][1]
        mass_calib_params['p2'] = mass_cal_parameters[idx_mass_cal][2]
        mass_calib_params['p3'] = mass_cal_parameters[idx_mass_cal][3]
    else: 
        i = 0
        #line = tofidx_spectrum_list[i]
        while i < len(mass_cal_parameters) and  mass_cal_parameters[i][0] < rt_tofidx_peak:
            idx_mass_cal = i #take the index of the cal time right before the measured rt position
            i+=1
            
        linear_interpol_weight = (rt_tofidx_peak - mass_cal_parameters[idx_mass_cal][0])  / (mass_cal_parameters[idx_mass_cal+1][0] - mass_cal_parameters[idx_mass_cal][0])
        mass_calib_params['p1'] = mass_cal_parameters[idx_mass_cal][1] * (1-linear_interpol_weight) + mass_cal_parameters[idx_mass_cal+1][1] * linear_interpol_weight
        mass_calib_params['p2'] = mass_cal_parameters[idx_mass_cal][2] * (1-linear_interpol_weight) + mass_cal_parameters[idx_mass_cal+1][2] * linear_interpol_weight
        mass_calib_params['p3'] = mass_cal_parameters[idx_mass_cal][3] * (1-linear_interpol_weight) + mass_cal_parameters[idx_mass_cal+1][3] * linear_interpol_weight   
    mass_centre   = f_tofidx_to_mass(tofidx_peak, mass_calib_params)
    mass_centre_u = f_tofidx_to_mass(tofidx_peak + tofidx_peak_u, mass_calib_params) - mass_centre
        
    return mass_centre, mass_centre_u


def interpol_mass_cal(rt_meas, mass_cal_parameters, mass_cal_mode):
    """
    Do interpolation of mass calibration parameters:
    for each time, calculate corresponding mass calibration parameters at this time.
    
    rt_meas: float (not list of float)
    """

    mass_cal_param_interp = {}
    mass_cal_param_interp['mode'] = mass_cal_mode
    
    if mass_cal_mode == 2:
    
        rt_cal_list = [line[0] for line in mass_cal_parameters]
        p1_cal_list = [line[1] for line in mass_cal_parameters]
        p2_cal_list = [line[2] for line in mass_cal_parameters]
        p3_cal_list = [line[3] for line in mass_cal_parameters]
        
        #calculate values of parameters for each time in rt_list:
        mass_cal_param_interp['p1'] = np.interp(rt_meas, rt_cal_list,  p1_cal_list)
        mass_cal_param_interp['p2'] = np.interp(rt_meas, rt_cal_list,  p2_cal_list)
        mass_cal_param_interp['p3'] = np.interp(rt_meas, rt_cal_list,  p3_cal_list)
    
    else:
        raise ValueError(
            "Error in reading mass calibration. "
            f"Received {mass_cal_mode} ."
            "Only modes 2 is supported. "
        )       
    return mass_cal_param_interp

# fmt: on


def fit_tofidx_to_mass(
    tofidx_meas: np.ndarray,
    mass_exact: np.ndarray,
    mass_cal_mode: int,
    mass_diff_ppm_threshold: float,
    graph: bool = False,
    rt: float | None = None,
    mass_list: np.ndarray | None = None,
    mass_cal_default: dict[str, float | int] | None = None,
):
    """Make a fit for the mass calibration parameters.

    Find the parameters matching the best the different mass calibration functions.

    :arg tofidx_meas: The measured tof index of the peaks.
    :arg mass_exact: The exact mass of the peaks.
    :arg mass_cal_mode: The mode of the mass calibration
    :arg mass_diff_ppm_threshold: The threshold for the acceptable difference

    :arg mass_diff_ppm_threshold: The threshold for the acceptable difference
        between the measured and the exact mass.
    :arg mass_cal_default: Dictionarry containing the value to use by default
        for the mass calibration.

    :arg rt: optional for the plots.
    :arg mass_list: optional for the plots.

    """
    logger = logging.getLogger("alpinac.fit_tofidx_to_mass")

    # 1. Guess the parameters fitting a linear function on the square root of the masses
    square_root_masses = np.sqrt(mass_exact)
    if mass_cal_mode == 1:
        square_root_masses = 1 / square_root_masses

    p1_guess, p2_guess, _, _ = fit_linear_np(square_root_masses, tofidx_meas)

    tofidx_meas_archive = tofidx_meas.copy()
    mass_exact_archive = mass_exact.copy()

    no_params = number_of_params(mass_cal_mode)

    mass_cal_params = {}
    mass_cal_params["mode"] = mass_cal_mode

    guess_params = Parameters()

    # Make the optimisation for modes that need it
    guess_params.add("mode", value=mass_cal_mode, vary=False)
    # Avoid it to be negative or zero
    guess_params.add("p1", value=p1_guess, min=1e-6)
    guess_params.add("p2", value=p2_guess)

    match mass_cal_mode:
        case 0:
            guess_params.add("p3", value=0.5, vary=False)
        case 1:
            guess_params.add("p3", value=-0.5, vary=False)
        case 2:
            guess_params.add("p3", value=0.5, min=0.498, max=0.502)
        case 3:
            guess_params.add("p3", value=0.0)
            guess_params.add("p4", value=0.0)
        case 4:
            guess_params.add("p3", value=0.0)
            guess_params.add("p4", value=0.0)
            guess_params.add("p5", value=0, vary=False)
        case _:
            raise NotImplementedError(
                f"Unkown default calib function for {mass_cal_mode =}"
            )

    logger.debug(f"Starting an optimization for x:{tofidx_meas=}  y:{mass_exact=}")
    logger.debug(f"{guess_params=}")

    def cost_f_tof_to_mass(mass_cal_params, tofidx, mass):
        """Loss function for the mass calibration."""
        model = f_tofidx_to_mass(tofidx, mass_cal_params)
        # Add a weight for tofids which are close to each others

        return (model - mass) / mass

    # Loop until all points are within the threshold
    # Each iteration makes a fit and remove the points that are too far away from the fit
    while True:

        model = f_tofidx_to_mass(tofidx_meas, guess_params)

        # Look at the model error to decide which points to remove
        mass_diff_ppm = (model - mass_exact) / mass_exact * 1e6

        # Remove only one by one, get the worst point
        to_remove = np.argmax(np.abs(mass_diff_ppm))
        if abs(mass_diff_ppm[to_remove]) < mass_diff_ppm_threshold:
            logger.debug(f"All points in range")
            break

        logger.debug(f"{mass_diff_ppm = }, {mass_diff_ppm_threshold=}, {to_remove=}")

        logger.debug(
            f"Removing {tofidx_meas[to_remove]} with mass {mass_exact[to_remove]}"
        )
        to_keep = np.ones(len(tofidx_meas), dtype=bool)
        to_keep[to_remove] = False

        if np.sum(to_keep) <= no_params:
            logger.debug(f"Not enough points left")
            break

        if np.all(to_keep):
            logger.debug(f"All points are in group")
            break

        tofidx_meas = tofidx_meas[to_keep]
        mass_exact = mass_exact[to_keep]

        # Optimize the parameters
        tof_to_mass_optimised = Minimizer(
            cost_f_tof_to_mass, guess_params, fcn_args=(tofidx_meas, mass_exact)
        ).minimize(method="leastsq")

        for param_index in range(1, no_params + 1):
            param_value = tof_to_mass_optimised.params[f"p{param_index}"].value
            mass_cal_params[f"p{param_index}"] = param_value
            # Update the guess parameters
            guess_params[f"p{param_index}"].value = param_value

    # Check if there are enough parameters for fitting
    if len(tofidx_meas) > no_params:
        mass_cal_params["mode"] = guess_params["mode"].value
        for i in range(1, no_params + 1):
            if f"p{i}" not in guess_params:
                continue
            mass_cal_params[f"p{i}"] = guess_params[f"p{i}"].value

    else:
        logger.error(
            f"Fitting failed, using default mass cal default {mass_cal_default}"
        )
        mass_cal_params = mass_cal_default

    model_archive = f_tofidx_to_mass(tofidx_meas_archive, mass_cal_params)
    model = f_tofidx_to_mass(tofidx_meas, mass_cal_params)

    mass_diff_ppm_archive = (
        (model_archive - mass_exact_archive) / mass_exact_archive * 1e6
    )
    mass_diff_ppm = (model - mass_exact) / mass_exact * 1e6

    if graph:
        fig, ax1 = plt.subplots(figsize=(8, 4))
        title = "mass vs tof index fit"
        if rt is not None:
            title += f" at RT={rt:.2f} s"
        fig.suptitle(title, fontsize=12)
        ax1.set_xlabel("ToF index")
        ax1.set_ylabel("mass [m/z]")
        ax1.tick_params("y", colors="b")
        min_tofidx = min(tofidx_meas_archive)
        max_tofidx = max(tofidx_meas_archive)
        x = np.linspace(min_tofidx, max_tofidx, 1000)
        if mass_cal_default:
            ax1.plot(
                x,
                f_tofidx_to_mass(x, mass_cal_default),
                label=f"Default mass cal \n {mass_cal_string(mass_cal_default)}",
                color="grey",
            )
        ax1.plot(
            x,
            f_tofidx_to_mass(x, mass_cal_params),
            label=f"Mass cal fit \n {mass_cal_string(mass_cal_params)}",
        )
        ax1.scatter(
            tofidx_meas_archive,
            mass_exact_archive,
            label="Extracted masses",
            color="grey",
        )
        ax1.scatter(
            tofidx_meas, mass_exact, label="Masses used for fit", color="orange"
        )
        if mass_list is not None:
            shown = []
            for mass_name, tofid, mass in zip(
                mass_list, tofidx_meas_archive, mass_exact_archive
            ):
                if mass_name in shown:
                    continue
                shown.append(mass_name)
                ax1.text(
                    tofid,
                    mass,
                    mass_name,
                    fontsize=12,
                    color="orange",
                    ha="right",
                )
        ax1.legend(loc="upper left")
        ax2 = ax1.twinx()
        ax2.set_ylabel("mass diff [ppm]", color="r")
        ax2.tick_params("y", colors="r")
        ax2.scatter(
            tofidx_meas_archive,
            mass_diff_ppm_archive,
            color="red",
            label="Difference with fit [ppm]",
        )
        ax2.scatter(
            tofidx_meas,
            mass_diff_ppm,
            color="green",
            label="Difference with fit for accepted masses [ppm]",
        )
        ax2.hlines(y=[0], xmin=min_tofidx, xmax=max_tofidx, color="r", linestyle="-")
        ax2.hlines(
            y=[-mass_diff_ppm_threshold, mass_diff_ppm_threshold],
            xmin=min_tofidx,
            xmax=max_tofidx,
            color="r",
            linestyle="--",
        )
        ax2.legend(loc="center right")
        fig.tight_layout()
        plt.show()

    return mass_cal_params, mass_diff_ppm_archive


# fmt: off


#**************************************************************************
def fit_linear(x, y):
    n= len(y)
    x_sum = sum(x)
    y_sum = sum(y)
    x_square_sum = sum([x[i]**2 for i in range(n)])
    x_y_sum = sum([x[i] * y[i] for i in range(n)])
    num_linear_fit_slope = n * x_y_sum - x_sum*y_sum
    num_linear_fit_b = x_square_sum * y_sum - x_sum * x_y_sum
    den_linear_fit = n * x_square_sum - x_sum**2

    if den_linear_fit != 0:
        linear_fit_slope = num_linear_fit_slope/den_linear_fit
        linear_fit_b = num_linear_fit_b/den_linear_fit
    else: 
        linear_fit_slope = 0
        linear_fit_b = sum(y)/n #default: straight line
        
    linear_fit = [x[i] * linear_fit_slope + linear_fit_b for i in range(n)]
    std_dev_linear_fit = np.sqrt(sum([(linear_fit[i]- y[i])**2 for i in range(n)]) / float(n-1))
    return linear_fit_slope, linear_fit_b, linear_fit, std_dev_linear_fit


#**************************************************************************
def fit_linear_np(x: np.ndarray, y: np.ndarray) -> tuple[float, float, np.ndarray, np.ndarray]:
    """Linear fit with np.array as inputs.

    :math:`y = a * x + b`
    
    :param x: x values
    :param y: y values

    :return:
        a: The slope of the linear fit
        b: The intercept of the linear fit
        linear_fit: The y values of the linear fit
        std_dev_linear_fit: The standard deviation of the linear fit

    """
    n= len(y)
    x_sum = np.sum(x)
    den = np.sum(x**2) - x_sum**2/n
    if den != 0:
        y_sum = np.sum(y)
        x_y_sum = np.sum(x * y)
        num = x_y_sum - x_sum*y_sum/n      
        a = num/den
        num_b = (np.sum(x**2)  * y_sum - x_sum * x_y_sum) / n
        b = num_b/den
    else : 
        a = 0
        b = np.sum(y)/n
     
    linear_fit = x * a + b 
    std_dev_linear_fit = np.sqrt(np.sum((linear_fit- y)**2) / float(n-1))
    return a, b, linear_fit, std_dev_linear_fit

#*************************************************************************
    
def cost_f_iso_profile(params, iso_profiles, i_spec, cl_comp):
    #y is the target, measured mass profile.
    
    #update mass-profile based on mass-offset:
    cl_comp.do_meas_mass_profile(params['mass_offset'], i_spec)
    
    model = np.zeros(len(cl_comp.meas_mass_profile_x))
    for idx_peak in range(params['nb_peaks'].value):
        model += params['k'+str(idx_peak)] * iso_profiles[idx_peak]

    cost = (model - cl_comp.meas_mass_profile)  #* weight_list
    return cost

class FitModes(Enum):
    fixed_x_axis = 0
    variable_x_axis = 1

class FittingMethod(Enum):
    discrete = "discrete"
    continuous = "continuous"
    
def fit_iso_profile(
    cl_comp: Compound,
    delta_mass: float, 
    iso_idx_list_k_guesses: list[float],
    k_guesses_min: float, 
    i_spec, 
    iso_profiles, 
    iso_fit_mode: FitModes, 
    graph: bool,
):
    """Fitting of the measured mass profile to the sum of the candidate isotopologue profiles."""

    #output needed: 
        #k_opt_list, list containing optimised value of k 
        #for each candidate isotopologue set in the group
        
    #inputs for optimisation:
        #k_guesses: guess values for all k values
        #cl_comp.iso_profiles: np array containing each candidate isotopologue profile
    
        #the input measured mass profile will be updated just before the fit using a method of the compound class
        #it is right now in cl_comp.meas_mass_profile (np array)

    no_node = len(iso_idx_list_k_guesses)
    #count_delete_sum = 1
    
    #graph = True
    params = Parameters()
    params.add('nb_peaks', value = no_node,  vary = False)
    
    
    if iso_fit_mode == FitModes.variable_x_axis:
        params.add('mass_offset', value = delta_mass , min = delta_mass - 2* cl_comp.delta_mass_u, max = delta_mass + 2* cl_comp.delta_mass_u)
    elif iso_fit_mode == FitModes.fixed_x_axis:
        params.add('mass_offset', value = delta_mass, vary=False)
    else:
        raise ValueError(f"Unkonwn fit Mode {iso_fit_mode} should be in {FitModes}")
        
        
    for idx_peak in range(no_node):
        #parameters per peak:
        params.add('k'+str(idx_peak), value = iso_idx_list_k_guesses[idx_peak], min = k_guesses_min) #, max = k_guesses_max, vary = iso_idx_list_k_guesses[idx_peak] > 0
        
    #***end of make parameters***
    
    minner = Minimizer(cost_f_iso_profile, params, fcn_args=(iso_profiles, i_spec, cl_comp)) #, method='leastsq'
    peak_optimised = minner.minimize(method = 'leastsq')
    
    delta_mass_opt = peak_optimised.params['mass_offset'].value
    
    k_opt = [None] *no_node
    profile_opt = np.zeros(len(cl_comp.meas_mass_profile_x))
    for idx_peak in range(no_node):
        k_opt[idx_peak] = peak_optimised.params['k'+str(idx_peak)].value
        profile_opt+= k_opt[idx_peak] * iso_profiles[idx_peak]
        
    #test: per iso profile, are all peaks below the LOD? if yes, remove index from list. or not?
    #alternative: fix k to zero and vary to false
    
    if graph == True:
        text_size = 12
        fig, ax1 = plt.subplots(figsize=(5,4))
        #fig.suptitle('Meas mass profile vs isotope profiles', fontsize=12)
        ax1.set_xlabel('Mass [m/z]', fontsize=text_size)
        ax1.set_ylabel('Intensity [area]', fontsize=text_size) #, color='b'
        #ax1.tick_params('y', colors='b')
        # Kaho, TODO uncomment, if more interested in low intensities
        #ax1.set_yscale('log')
        #ax2 = ax1.twinx()
        #ax2.plot(cl_comp.meas_mass_profile_x, (peak_optimised.residual*(-1)), 'c.')
        #ax2.set_ylabel('residuals', color='c')
        #ax2.set_ylim(-max(cl_comp.meas_mass_profile), max(cl_comp.meas_mass_profile))
        #ax2.tick_params('y', colors='c')    


        for i_iso in range(no_node):
            ax1.plot(cl_comp.meas_mass_profile_x, iso_profiles[i_iso] * k_opt[i_iso], '-', color = str(color1_names[i_iso]))
        #ax1.plot(mu_guess, apex_guess, 'o', color='xkcd:peach', markersize = 12)
        ax1.plot(cl_comp.meas_mass_profile_x, cl_comp.meas_mass_profile, 'k--')
        
            

        #fig.tight_layout()
        plt.show()
        """
        for i_iso in range(no_node):
            text_size = 12
            fig, ax1 = plt.subplots(figsize=(5,2))
            #fig.suptitle('Meas mass profile vs isotope profiles', fontsize=12)
            ax1.set_xlabel('Mass [m/z]', fontsize=text_size)
            ax1.set_ylabel('Intensity [area]', fontsize=text_size) #, color='b'
            #ax1.tick_params('y', colors='b')
            ax1.set_yscale('log')
            ax1.plot(cl_comp.meas_mass_profile_x, iso_profiles[i_iso] * k_opt[i_iso], '-', color = str(color1_names[i_iso]))

            plt.show()
        """
        
    
    return k_opt, delta_mass_opt


# *************************************************************************
# fmt: on


def f_multi(
    x, params, function: callable[[np.ndarray, float, float, float, float], np.ndarray]
) -> np.ndarray:
    """Function to compute the sum of multiple of a function."""
    model = params["baseline"] + np.sum(
        [
            function(
                x,
                params[f"A{idx_peak}"],
                params[f"mu{idx_peak}"],
                params[f"sigma{idx_peak}"],
                params[f"alpha{idx_peak}"],
            )
            for idx_peak in range(params["nb_peaks"].value)
        ],
        axis=0,
    )
    return model


# Constant
c_pv = np.sqrt(2 * np.log(2))


def f_pseudoVoigt(x, A, mu, sigma, alpha):
    """
    https://lmfit.github.io/lmfit-py/builtin_models.html#pseudovoigtmodel
    weighted sum of a Gaussian and Lorentzian distribution function
    that share values for amplitude (A), center (mu) and full width at half maximum fwhm
    (and so have constrained values of sigma and height (maximum peak height).
    A parameter fraction (alpha) controls the relative weight of the Gaussian and Lorentzian components
    x: np array
    A, mu, sigma, alpha : floats (cf. Gaussian function)
    Note: this function does not include the baseline.
    If any, it has to be added separately.
    """
    sigma_L = sigma * c_pv
    return (
        # Gaussian part
        (1 - alpha) * f_Gauss(x, A, mu, sigma)
        # Voigt part
        + alpha * A / np.pi * sigma_L / ((x - mu) ** 2 + sigma_L**2)
    )


def cost_f_pseudoVoigt(params, x, y):
    """
    Calculate the cost function to minimise
    This cost function is adapted to the number of expected peaks
    It returns the quantity to be minimised using later the minimise function from limfit
    """
    return f_multi(x, params, function=f_pseudoVoigt) - y


def fit_pseudoVoigt(
    x: np.ndarray,
    y: np.ndarray,
    mu_guess: np.ndarray,
    apex_guess: np.ndarray,
    alpha_guess: float,
    n_per_bin: float,
    sigma_tofidx_slope: float,
    sigma_tofidx_b: float,
    graph: bool,
    mode: str,
    area_test_threshold: float | None = None,
):
    """This function computes a (or several) pseudo-Voigt fit(s) of the data.

    Pseudo-Voigt profile is parameterized by mu, sigma, A
    :param x: np array of floating point numbers, x-coordinates
    :param y: np array of floating point numbers, y-coordinates
    :param mu_guess: np array of guess of mu parameter, float
    :param apex_guess: np array of value of y when x=mu, float, max of peak
    :param n_per_bin: number of points x in between two integer values (sampling rate for x)

    :param sigma_tofidx_slope: slope of the sigma parameter as a function of mass
    :param sigma_tofidx_b: intercept of the sigma parameter as a function of mass

    :returns:
        * mu_opt, optimised centre of peak
        * A_opt, optimised area
        * sigma_opt,
        * alpha_opt,
        * baseline_opt, optimised baseline value.
    """

    logger = logging.getLogger("alpinac.fit_pseudoVoigt")
    # Set the defautl value of the area test threshold
    if area_test_threshold is None:
        area_test_threshold = 2.6
        if mode == "MassCal":
            area_test_threshold = 1.3

    # Initial guess for the parameters

    # Find baseline by taking the median of the lowest third of the data
    y_sorted = np.sort(y)
    y_median_test = np.median(y_sorted[: int(len(y) / 3)])
    baseline_guess = max(0.0, y_median_test)

    # Sigma = standard deviation / width of the pseudo voigt
    sigma_guess = sigma_tofidx_b + sigma_tofidx_slope * mu_guess

    # A = Amplitude of the pseudo voigt
    A_guess = np.divide(
        apex_guess - baseline_guess,
        (1.0 - alpha_guess) / (sigma_guess * np.sqrt(2 * np.pi))
        + alpha_guess / (math.pi * sigma_guess * np.sqrt(2 * np.log(2))),
    )
    # Alpha = mixing parameter between Gaussian and Lorentzian
    alpha_guess = np.full_like(mu_guess, alpha_guess)

    # plot first guess peak position and apex:
    if graph == True:
        fig, ax1 = plt.subplots(figsize=(8, 4))
        fig.suptitle("Pseudo-Voigt fit over ToF index domain", fontsize=12)
        ax1.set_xlabel("ToF index [index]")
        ax1.set_ylabel("Intensity [V]", color="b")
        ax1.tick_params("y", colors="b")
        ax1.plot(
            mu_guess,
            apex_guess,
            "o",
            color="xkcd:peach",
            markersize=12,
            label="apex guess",
        )
        ax1.plot(
            [min(x), max(x)],
            [baseline_guess, baseline_guess],
            "r-.",
            label="baseline guess",
        )
        # to plot the fit with good resolution to visualise something:
        x_plot = np.linspace(
            min(x), max(x), num=int((max(x) - min(x)) / (n_per_bin / 20.0))
        )
        ax1.plot(x, y, "--bo")  #'b.'

    if mode == "MassCal":
        sigma_guess_vary = True
        alpha_guess_vary = True
    else:
        # change to True for improved precision (slower)
        sigma_guess_vary, alpha_guess_vary = False, False

    while True:
        # Add parameters for lmfit to be opitimized
        params = Parameters()
        params.add(
            "baseline",
            value=baseline_guess,
            min=max(0.0, min(y) - 3 * min(y)),
            max=max(3, baseline_guess * 3, y_median_test * 3),
        )
        # params.add('baseline', value=max(1, baseline_guess), min = max(0, baseline_guess/3), max = max(3, baseline_guess*3)) #
        # params.add('baseline', value=baseline_guess, min = max(0.0, min(y)-3.0*min(y)), max = baseline_guess+y_mean/10.0) #
        # baseline cannot be above max I
        params.add("nb_peaks", value=len(mu_guess), vary=False)

        mu_guess_min = mu_guess - n_per_bin * 2.0
        mu_guess_max = mu_guess + n_per_bin * 2.0

        A_guess_min = A_guess * 0.1
        A_guess_max = A_guess * 10.0

        if mode == "MassCal":
            sigma_guess_min = sigma_guess - 0.4
            sigma_guess_max = sigma_guess * 2.0

        else:
            sigma_guess_min = sigma_guess - 0.2
            sigma_guess_max = sigma_guess + 0.2

        for idx_peak in range(len(mu_guess)):
            # parameters per peak:
            params.add(
                f"mu{idx_peak}",
                value=mu_guess[idx_peak],
                min=mu_guess_min[idx_peak],
                max=mu_guess_max[idx_peak],
            )
            params.add(
                f"sigma{idx_peak}",
                value=sigma_guess[idx_peak],
                min=sigma_guess_min[idx_peak],
                max=min(3.0, sigma_guess_max[idx_peak]),
                vary=sigma_guess_vary,
            )
            params.add(
                f"alpha{idx_peak}",
                value=alpha_guess[idx_peak],
                min=0.2,
                max=1.0,
                vary=alpha_guess_vary,
            )
            params.add(
                f"A{idx_peak}",
                value=max(float(1.0), A_guess[idx_peak]),
                min=max(1, A_guess_min[idx_peak]),
                max=max(2, A_guess_max[idx_peak]),
            )
        # ***end of make parameters***

        logger.debug(f"Starting pseudo-voigt fitting with n_peaks: {len(mu_guess)}")

        if graph == True:
            ax1.plot(
                x_plot,
                f_multi(x_plot, params, function=f_pseudoVoigt),
                "r--",
                label="initial guess",
            )

        minner = Minimizer(
            cost_f_pseudoVoigt, params, fcn_args=(x, y)
        )  # , method='leastsq'
        peak_optimised = minner.minimize(method="leastsq")
        # logger.debug(f"optimization done: {peak_optimised.params.pretty_repr()}")

        # retrieve optimised parameters
        # peak_optimised_params = peak_optimised.params

        l = len(mu_guess)
        param_values = lambda param: np.array(
            [peak_optimised.params[f"{param}{idx_peak}"].value for idx_peak in range(l)]
        )
        A_opt = param_values("A")
        mu_opt = param_values("mu")
        sigma_opt = param_values("sigma")
        alpha_opt = param_values("alpha")

        baseline_opt = peak_optimised.params["baseline"].value
        max_opt = (
            baseline_opt
            + (1.0 - alpha_opt) * A_opt / (sigma_opt * np.sqrt(2 * np.pi))
            + alpha_opt * A_opt / (np.pi * sigma_opt * c_pv)
        )

        # Test wich peaks signals are large enough to keep
        mask_keep = max_opt >= area_test_threshold * y_median_test

        # Prepare the next iteration
        A_guess = A_opt[mask_keep]
        mu_guess = mu_opt[mask_keep]
        sigma_guess = sigma_opt[mask_keep]
        alpha_guess = alpha_opt[mask_keep]
        baseline_guess = baseline_opt

        if np.all(mask_keep):
            break

        if sum(mask_keep) == 0:
            # Case no peak found
            return [], [], [], [], baseline_guess

    # ***The optimisation is finished***

    if graph == True:
        ax2 = ax1.twinx()
        ax2.plot(x, peak_optimised.residual * (-1), "c.")
        ax2.set_ylabel("residuals", color="c")
        ax2.tick_params("y", colors="c")

        y_opt_sum_plot = [baseline_opt] * len(x_plot)  # add baseline only once!
        # y_opt_sum_plot = [0.0]*len(x_plot)
        max_opt = [None] * len(mu_guess)

        for idx_peak in range(len(mu_guess)):
            y_opt_plot = f_pseudoVoigt(
                x_plot,
                A_opt[idx_peak],
                mu_opt[idx_peak],
                sigma_opt[idx_peak],
                alpha_opt[idx_peak],
            )
            ax1.plot(x_plot, y_opt_plot, "g--")
            y_opt_sum_plot += y_opt_plot

        # y_opt_sum_plot = np.sum(, axis = 0)
        ax1.plot(
            [min(x), max(x)],
            [baseline_opt, baseline_opt],
            "r--",
            label="baseline fitted",
        )

        ax1.plot(x_plot, y_opt_sum_plot, "k-")
        ax1.plot(mu_opt, max_opt, "*", color="xkcd:deep red", markersize=12)
        ax1.legend()
        # plt.savefig('Gauss_fit_on_mass_smallA.png', bbox_inches='tight', transparent = True, dpi = 300)

        fig.tight_layout()
        plt.show()

    return mu_opt, A_opt, sigma_opt, alpha_opt, baseline_opt


def f_chromato(
    x: np.ndarray, H: float, mu: float, sigma: float, alpha: float
) -> np.ndarray:
    """Fit a chromatographic function to the data.

    Equation from Pap and Papai, Journal of Chromatography, 2001
    Title: Application of a new mathematical function for describing chromatographic peaks.

    x: np.array, retention time (index (int) or seconds (float)?)
    H: float, peak height (value at centre of peak)
    mu: float, position of centre of peak, same unit as x
    sigma: float, standard deviation of the peak.
    alpha: float, parameter for the asymetry of the peak in RT domain, 0<a< 2.

    Returns:
    chromato, np.array, has 1 dimension of lenght of x.
    """
    if alpha <= 0.01:
        # Gaussian
        A = H * sigma * np.sqrt(2 * np.pi)
        return f_Gauss(x, A, mu, sigma)

    chromato = np.zeros_like(x)
    alpha_2 = alpha * alpha

    v = 2.0 * alpha * (x - mu) / (sigma * (4.0 - alpha_2))
    mask_valid = v > -0.99
    v_valid = 2.0 * alpha * (x[mask_valid] - mu) / (sigma * (4.0 - alpha_2))
    chromato[mask_valid] = H * np.exp(
        (4.0 / alpha_2 - 1.0) * (np.log(1.0 + v_valid) - v_valid)
    )

    return chromato


def f_chromato_area(A: float, sigma: float, alpha: float):
    """This function calculate the area corresponding to a chromatographic peak.

    See :py:func:`f_chromato` for the description of the function.

    It takes into accunt tailing of the peak (alpha factor), see f_chromato.
    Warning! In practice, calculation of the area fails for alpha < 0.2.
    We use Gaussian equation instead in this case.
    If alpha<0.2, the shape can be well approximated by the Gaussian function anyway.
    """

    if alpha > 0.2:
        # use equation with tailing
        w = float(4.0) / alpha**2
        area = (
            gamma(w) * A * alpha * sigma / (2.0 * np.exp((w - 1) * (np.log(w - 1) - 1)))
        )
    else:
        # use Gaussian approximation to avoid computing error
        area = A * (sigma * np.sqrt(2 * np.pi))
        # height = area/(sigma * np.sqrt(2*np.pi))

    return area


def cost_f_chromato(params, x, y):
    """Calculate the cost function to minimise a chromatographic fit."""
    return f_multi(x, params, function=f_chromato) - y


def f_2d_peak(
    rt: np.ndarray,
    mz: np.ndarray,
    center_rt: float,
    center_mz: float,
    amplitude: float,
    sigma_rt: float,
    sigma_mz: float,
    asymetry_rt: float,
    alpha_voigt: float,
    background: float = 0.0,
) -> np.ndarray:
    """Peak in RT/MZ dimensions.

    Chromatographic fit in rt domain  and pseudo voigt in mz.

    The rt and mz axis should have the same size, or broadcastable dimensions.
    (ex: (n_rt, 1), (1, n_mz) which returns an array (n_rt, n_mz))


    For the input parameters, see :py:func:`f_chromato` and :py:func:`f_pseudo_voigt` .
    """
    # We can assume the amplitude of the pseudovoigt is given the chromato ?
    chromato = f_chromato(rt, amplitude, center_rt, sigma_rt, asymetry_rt)

    # Chomatography is then applyed to the amplitude of the pseudo voigt
    intensity = f_pseudoVoigt(mz, chromato, center_mz, sigma_mz, alpha_voigt)

    return intensity + background


def compute_rt_sigma_guess(sigma_guess_slope, sigma_guess_exp_k, mu_guess):
    """
    Compute the guess value for peak broadness in time domain.
    This is an exponential function of the retention time.
    Parameters have been empirically determined, by fitting results of
    sigma (from fitted peaks when using a quite large tolerance around sigma),
    vs rt.
    The parameters sigma_guess_slope, sigma_guess_exp_k
    depend on the chromatographic method used!
    Using a different temperature ramp will modify them.

    mu_guess: float or np.array, rt where sigma_guess is computed
    sigma_guess_slope: float
    sigma_guess_exp_k: float
    """
    rt_sigma_guess = sigma_guess_slope * np.exp(sigma_guess_exp_k * mu_guess)

    if mu_guess <= 1000:
        rt_sigma_guess = 2.5

    return rt_sigma_guess


def fit_chromato(
    x: np.ndarray,
    y: np.ndarray,
    mu_guess: np.ndarray,
    apex_guess: np.ndarray,
    baseline_guess: float,
    n_per_bin,
    conf_int,
    broadness,
    sigma_guess_value,
    rt_window_min: float,
    rt_window_max: float,
    tof_series,
    mass_series,
    ratio_to_linear_fit: float,
    rt_interval_sec: float | None = None,
    graph_show: bool = False,
    graph_savefile: Path | None = None,
):
    """This function computes one (or several) chromatographic fit(s) to the data.

    The chromato functin is parameterized by mu, sigma, A, alpha.
    Compared to Gaussian, there is one more parameter: alpha.

    :param x: list of floating point numbers, x-coordinates
    :param y: list of floating point numbers, y-coordinates
    :param mu_guess: list of guess of mu parameter (position of centre on x axis), float
    :param apex_guess: list of value of y when x=mu (apex of peak, on y axis), float
    :param alpha: tailing of the chromatographic peak. 0 > alpha > 2.
    So far, it seems difficult to constrain value of alpha, for example finding a correlation with total area of peak or elution time.
    :param n_per_bin: number of points x in between two integer values (sampling rate for x)
    (on the rt domain, this is s_per_bin)
    :param area_threshold: minimum value of optimised area, float
    :param ratio_to_linear_fit: minimum fit improvement for Gauss/Chromato compared to linear fit.
    This was chosen compared to simply using a fit residual threshold, because there is
    a lot of noise close to the LOD.
    :arg rt_interval_sec: The possible interval around the peak center

    returns optimised values for:
    mu_opt
    A_opt
    sigma_opt
    alpha_opt
    area_opt, calculated from optimised parameters.
    area_opt_u, std dev of optimised area.

    In addition, for each fitted chromatographic peak, calculate:
    - the average centre of tof, using tof_series
    - the average mass centre, using mass_series
    - LOD
    - SN (signal/noise, this is apex_opt/LOD)
    """
    logger = logging.getLogger("alpinac.fit_chromato")

    len_x = len(x)
    len_mu_guess = len(mu_guess)

    # Whether to create the plot
    graph = graph_show or graph_savefile is not None

    # fit a linear function, to then compare fit with chromato, to test presence of real peak
    _, _, linear_fit, std_dev_linear_fit = fit_linear_np(x, y)

    # assuming same baseline for all peaks in a small window:
    # x should cover a RT range large enough around the expected peak centre in RT
    # so that minimum can be found where no mass is expected
    # i.e.  guess RT + / - 4* sigma of a peak

    baseline_guess = max(0, baseline_guess)  # , min(y)*0.8
    # we have now points that corresponds to signal above baseline
    # so the minimum is the closest to the real baseline

    # sigma_guess = compute_rt_sigma_guess(sigma_guess_slope, sigma_guess_exp_k, mu_guess)
    sigma_guess = np.zeros(len_mu_guess) + sigma_guess_value
    # TODO check if these value I changed are making things bad (before 0.6, 1.6)
    sigma_guess_min = sigma_guess * 0.2
    sigma_guess_max = sigma_guess * 2
    H_guess = apex_guess - baseline_guess

    alpha_guess = np.full(len_mu_guess, 0.1)
    alpha_guess_min = np.full(len_mu_guess, 0.001)
    alpha_guess_max = np.full(len_mu_guess, 1.6)

    # plot first guess peak position and apex:
    if graph:
        fig, ax1 = plt.subplots(figsize=(8, 4))
        # fig.suptitle('Chromatographic fit over RT', fontsize=12)
        fig.suptitle(
            f"Chromatographic fit over RT from m/z = [{np.min(mass_series):.3f}, {np.max(mass_series):.3f}]",
            fontsize=12,
        )
        ax1.set_xlabel("Retention time [s]")
        ax1.set_ylabel("Intensity [TOF-index * V]", color="b")
        ax1.tick_params("y", colors="b")
        ax1.plot(
            mu_guess,
            apex_guess,
            "o",
            color="xkcd:peach",
            markersize=13,
            label="apex_guess",
        )
        ax1.plot(x, y, "b.", label="data")

        # to plot the fit with good resolution to visualise something:
        x_plot = np.linspace(
            min(x), max(x), num=int((max(x) - min(x)) / (n_per_bin / 4.0))
        )
        y_guessed_sum_plot = np.full(len(x_plot), baseline_guess)
        # Add all the peaks that will be tried to found
        for i in range(len_mu_guess):
            y_guessed_sum_plot += f_chromato(
                x_plot, H_guess[i], mu_guess[i], sigma_guess[i], alpha_guess[i]
            )

        ax1.plot(
            x_plot,
            y_guessed_sum_plot,
            "--",
            color="xkcd:peach",
            label="chromato_fit_guessed",
        )
        ax1.plot(
            [min(x), max(x)],
            [baseline_guess, baseline_guess],
            "--",
            color="xkcd:peach",
            label="baseline guess",
        )

    fails = False

    while True:
        # ***make parameters***
        params = Parameters()
        params.add(
            "baseline",
            value=max(float(1.0), baseline_guess),
            min=max(float(0.0), baseline_guess / 3),
            max=max(float(3.0), baseline_guess * 10),
        )  #
        # baseline cannot be above max I
        params.add("nb_peaks", value=len_mu_guess, vary=False)

        if rt_interval_sec is None:
            # Guess the interval
            rt_interval_sec = 3 * n_per_bin
        mu_guess_min = mu_guess - rt_interval_sec
        mu_guess_max = mu_guess + rt_interval_sec

        H_guess_min = H_guess / 10.0
        H_guess_max = H_guess * 10

        for idx_peak in range(len_mu_guess):
            params.add(
                f"mu{idx_peak}",
                value=mu_guess[idx_peak],
                min=mu_guess_min[idx_peak],
                max=mu_guess_max[idx_peak],
            )  # abcisse cannot be negative
            params.add(
                f"sigma{idx_peak}",
                value=sigma_guess[idx_peak],
                min=max(0.5, sigma_guess_min[idx_peak]),
                max=max(0.8, sigma_guess_max[idx_peak]),
            )
            params.add(
                f"A{idx_peak}",
                value=max(2.0, H_guess[idx_peak]),
                min=max(1.0, H_guess_min[idx_peak]),
                max=max(3.0, H_guess_max[idx_peak]),
            )
            params.add(
                f"alpha{idx_peak}",
                value=alpha_guess[idx_peak],
                min=alpha_guess_min[idx_peak],
                max=alpha_guess_max[idx_peak],
            )

        logger.info(f"Starting optimization with n_peaks: {len_mu_guess}")
        logger.debug(f"{params.pretty_repr()}")
        # fit, here with leastsq model
        minner = Minimizer(cost_f_chromato, params, fcn_args=(x, y))
        peak_optimised = minner.minimize(method="leastsq")

        # retrieve optimised parameters: peak_optimised.params
        logger.debug(f"optimization done: {peak_optimised.params.pretty_repr()}")

        # Statistical test: is each fitted peak statistically significant?
        baseline_opt = peak_optimised.params["baseline"].value
        t_test_baseline = 0.0  # np.zeros(len(mu_guess))

        H_opt = np.zeros(len_mu_guess)
        mu_opt = np.zeros(len_mu_guess)
        sigma_opt = np.zeros(len_mu_guess)
        alpha_opt = np.zeros(len_mu_guess)  # chromato

        apex_opt_u = np.zeros(len_mu_guess)
        y_opt = np.zeros((len_mu_guess, len_x))
        y_opt_sum = np.zeros(len_x)

        t_test_value = np.zeros(len_mu_guess)
        t_test_apex = np.zeros(len_mu_guess)

        # np_pts_min = np.zeros(len(mu_guess))

        # ***t-test***
        for idx_peak in range(len_mu_guess):
            H_opt[idx_peak] = peak_optimised.params[f"A{idx_peak}"].value
            mu_opt[idx_peak] = peak_optimised.params[f"mu{idx_peak}"].value
            sigma_opt[idx_peak] = peak_optimised.params[f"sigma{idx_peak}"].value
            alpha_opt[idx_peak] = peak_optimised.params[f"alpha{idx_peak}"].value
            # calculate fit of specific peak:
            y_opt[idx_peak] = f_chromato(
                x,
                H_opt[idx_peak],
                mu_opt[idx_peak],
                sigma_opt[idx_peak],
                alpha_opt[idx_peak],
            )

        # extract all y_opt before doing the statistical tests!
        y_opt_sum_no_baseline = np.sum(y_opt, axis=0)  # + baseline_opt
        y_opt_sum = y_opt_sum_no_baseline + baseline_opt

        # Residuals
        res = y - y_opt_sum
        # Calculate standard deviation of the fit
        std_dev_f = np.sqrt(np.sum(res**2) / float(len(y) - 1))

        # baseline variance weighted by inverse of y distance of fit to baseline:
        # make sure weights are non-zeros:
        b_w = 1.0 / np.clip(y_opt_sum_no_baseline, 1, None) ** 2

        y_opt_baseline_u = np.sqrt(
            np.sum(b_w * (res) ** 2) / np.sum(b_w) * len_x / (len_x - 1)
        )

        t_test_baseline = Student_t.ppf(conf_int, len(y) - 1) * y_opt_baseline_u

        for idx_peak in range(len_mu_guess):
            # here add calculation of std between fit and (measured data minus all other fit)
            # we remove from the measured data the modelled signal due to all other peaks:
            extracted_y = res + y_opt[idx_peak]

            # Look at the points included in this peak.
            # indexes of x around mu_opt +/- broadness:
            sigma = sigma_opt[idx_peak]
            mu = mu_opt[idx_peak]
            mask_x_in_this_peak = np.abs(x - mu) <= broadness * sigma

            mask_left = mask_x_in_this_peak & (x <= mu)
            mask_right = mask_x_in_this_peak & (x >= mu)

            number_of_x_points = sum(mask_x_in_this_peak)
            np_pts_left = sum(mask_left)
            np_pts_right = sum(mask_right)

            if mu - rt_window_min > 0:  # mu_opt is not outside left of the scan window
                np_pts_left_min = round(
                    max(
                        1,
                        min(
                            broadness * sigma,
                            mu - rt_window_min,
                        )
                        / n_per_bin
                        * 0.4,
                    )
                )

            else:
                np_pts_left_min = 0
            if rt_window_max - mu > 0:
                np_pts_right_min = round(
                    max(
                        1,
                        min(
                            broadness * sigma,
                            rt_window_max - mu,
                        )
                        / n_per_bin
                        * 0.4,
                    )
                )

            else:
                np_pts_right_min = 0

            summed_signal = np.sum(y_opt[idx_peak][mask_x_in_this_peak])
            if (
                summed_signal > 0
                and np_pts_left >= np_pts_left_min
                and np_pts_right >= np_pts_right_min
            ):
                # calculate standard deviation of fit to y, weighted by y value:

                apex_opt_u[idx_peak] = math.sqrt(
                    np.sum(
                        (
                            y_opt[idx_peak][mask_x_in_this_peak]
                            - extracted_y[mask_x_in_this_peak]
                        )
                        ** 2
                        * y_opt[idx_peak][mask_x_in_this_peak]
                    )
                    / summed_signal
                )
            else:
                # peak of just 1 points, to eliminate
                apex_opt_u[idx_peak] = H_opt[idx_peak]

            t_test_value[idx_peak] = Student_t.ppf(
                conf_int, max(number_of_x_points - 1, 1)
            )  # added min value of 1 to avoid raised error if nan
            t_test_apex[idx_peak] = t_test_value[idx_peak] * apex_opt_u[idx_peak]

        # here remove peaks where height - std dev > threshold
        # And update guess values to be used for next loop

        if std_dev_f / std_dev_linear_fit > ratio_to_linear_fit:
            fails = True
            break

        mask_passes_H_test = (H_opt - t_test_apex - t_test_baseline >= 0) & (
            H_opt - 2 * t_test_baseline >= 0
        )

        if np.all(mask_passes_H_test):
            break

        H_guess = H_opt[mask_passes_H_test]

        mu_guess = mu_opt[mask_passes_H_test]
        sigma_guess = sigma_opt[mask_passes_H_test]
        alpha_guess = alpha_opt[mask_passes_H_test]
        sigma_guess_min = sigma_guess_min[mask_passes_H_test]
        sigma_guess_max = sigma_guess_max[mask_passes_H_test]

        baseline_guess = baseline_opt

        len_mu_guess = sum(mask_passes_H_test)

        if len_mu_guess == 0:
            fails = True
            break

    # ***The optimisation is finished***

    if len_mu_guess != 0:

        mu_opt_stderr = np.zeros(len_mu_guess)
        area_opt = np.zeros(len_mu_guess)
        SN_opt = np.zeros(len_mu_guess)
        tof_centre_opt = np.zeros(len_mu_guess)
        tof_centre_opt_u = np.zeros(len_mu_guess)
        mass_centre_opt = np.zeros(len_mu_guess)
        mass_centre_opt_u = np.zeros(len_mu_guess)

        min_pts_for_LOD = np.min(sigma_guess * 3 / n_per_bin)
        LOD_H = np.zeros(len_mu_guess)
        LOD = np.zeros(len_mu_guess)

    for idx_peak in range(len_mu_guess):
        mu_opt_stderr[idx_peak] = peak_optimised.params[f"mu{idx_peak}"].stderr
        area_opt[idx_peak] = f_chromato_area(
            H_opt[idx_peak], sigma_opt[idx_peak], alpha_opt[idx_peak]
        )
        # we need to add a factor 2 here to get the position of the minimum peak
        # with the left tailing touching the std dev of y.
        # because we want 5% (or so) cases of taking noise as a real peak.
        LOD_H[idx_peak] = max(
            1, 2.0 * Student_t.ppf(conf_int, min_pts_for_LOD - 1) * y_opt_baseline_u
        )
        SN_opt[idx_peak] = H_opt[idx_peak] / LOD_H[idx_peak]

        # now calculate LOD as equivalent area with same peak shape as integrated peak
        LOD[idx_peak] = f_chromato_area(
            LOD_H[idx_peak], sigma_opt[idx_peak], alpha_opt[idx_peak]
        )

        # calculate centre of tof and centre of mass for each individual peak:
        # weight values using y_opt calculated for each peak
        y_opt_peak_sum = np.sum(y_opt[idx_peak])
        tof_centre_opt[idx_peak] = np.sum(tof_series * y_opt[idx_peak]) / y_opt_peak_sum
        tof_centre_opt_u[idx_peak] = np.sqrt(
            np.sum((tof_series - tof_centre_opt[idx_peak]) ** 2 * y_opt[idx_peak])
            / (y_opt_peak_sum * (len_x - 1) / len_x)
        )
        mass_centre_opt[idx_peak] = (
            np.sum(mass_series * y_opt[idx_peak]) / y_opt_peak_sum
        )
        mass_centre_opt_u[idx_peak] = np.sqrt(
            np.sum((mass_series - mass_centre_opt[idx_peak]) ** 2 * y_opt[idx_peak])
            / (y_opt_peak_sum * (len_x - 1) / len_x)
        )

    if len_mu_guess > 0 and graph == True:
        ax2 = ax1.twinx()
        ax2.plot(x, peak_optimised.residual, "c.")
        ax2.set_ylabel("residuals", color="c")
        ax2.tick_params("y", colors="c")

        # to plot the fit with good resolution to visualise something:
        x_plot = np.linspace(
            min(x), max(x), num=int((max(x) - min(x)) / (n_per_bin / 4.0))
        )
        y_opt_sum_plot = np.zeros(len(x_plot))
        for idx_peak in range(len_mu_guess):
            y_opt_plot = f_chromato(
                x_plot,
                H_opt[idx_peak],
                mu_opt[idx_peak],
                sigma_opt[idx_peak],
                alpha_opt[idx_peak],
            )  # + baseline_opt #add baseline only once!
            ax1.plot(x_plot, y_opt_plot, "g--")
            y_opt_sum_plot += y_opt_plot
        ax1.plot(x_plot, y_opt_sum_plot + baseline_opt, "k-")
        ax1.plot(mu_opt, H_opt, "*", color="xkcd:deep red", markersize=11)
        ax1.plot(
            [min(x), max(x)], [baseline_opt, baseline_opt], "--", color="xkcd:slate"
        )
        logger.info("Std dev of fit: " + "{:.4f}".format(std_dev_f))
        logger.info(
            "std dev peak fit / srd dev linear: "
            + "{:.4f}".format(std_dev_f / std_dev_linear_fit)
        )
        if fails:
            ax1.plot(
                x,
                linear_fit,
                "r--",
                label=f"linear fit is a better fit at {ratio_to_linear_fit}",
            )

    if graph == True:
        ax1.legend()

    if graph_savefile is not None:
        # Assign automatic name to the file
        if graph_savefile.is_dir():
            graph_savefile = graph_savefile / f"{np.mean(mass_series):.4f}.png"
        fig.savefig(graph_savefile)
        plt.close(fig)
    if graph_show:
        plt.show()

    if fails:
        return [], [], [], [], [], [], [], [], [], [], [], []

    return (
        mu_opt,
        mu_opt_stderr,
        area_opt,
        apex_opt_u,
        sigma_opt,
        alpha_opt,
        LOD,
        SN_opt,
        tof_centre_opt,
        tof_centre_opt_u,
        mass_centre_opt,
        mass_centre_opt_u,
    )


# fmt: off
#*************************************************************************
    
def f_Gauss(x, A, mu, sigma):
    """
    Calculate Gaussian function of known parameters A, mu, sigma, for a given x
    x: np array containing floats: abcisses where we want the Gaussian value
    A: parameter of the Gaussian function, float
    mu: parameter of the Gaussian function (centre of peak), float
    sigma: parameter of the Gaussian function, float
    """
    #do not include baseline here otherwise it will be added 
    #each time the gaussian function is summed up!
    return A / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(x-mu)**2/(2*sigma)**2)
        
def cost_f_Gauss(params, x, y):
    """
    Calculate the cost function to minimise
    This cost function is adapted to the number of expected peaks
    It returns the quantity to be minimised using later the minimise function from limfit
    """
    #if model == 'Gaussian':
    nb_Gauss_params = int(3) #3 parameters to define the Gauss function
    model=params['baseline'] #+str(idx_peak)
    #this cost function should adapt itself to number of found peaks
    for idx_peak in range(int((len(params)-1)/nb_Gauss_params)): #need to be integer value; -1 to remove 1 line for baseline
        mu = params['mu'+str(idx_peak)]
        sigma = params['sigma'+str(idx_peak)]
        A = params['A'+str(idx_peak)]
        model += f_Gauss(x, A, mu, sigma)
    
    #using a weight = math.sqrt(data) to give more weight to detection of large peaks:
    #diff_model_data = model - data
    #if there are more points on the side of the peak 
    #as on the peak, multiplying by y may heuu no
    
    #Cost functions that do not work:
    #-------------------------
    #cost = (model - y) * y -> underweight points at baseline
    #baseline is not captured correctly -> area not correct
    #-------------------------
    #definition of an uncertaint value
    #uncertainty at baseline should represent detection noise
    #but not more
    weight_list = []
    U_max = 1.2
    U_min = 1
    for val in y:
        #weight = 1.0
        if val != 0:
            #we want U to increase towards baseline value but not above a threshold U of 1:
            U = min(U_max, abs(params['baseline']/val))
            #however we don't want too small U values when y is large (will disturb baseline estimate)
            if U < U_min: U = U_min
        else: U = U_max
        weight_list.append(1/U)
    #elif model == 'tofidx_to_mass':
        
        
    cost = (model - y) #* weight_list
    return cost #**2 / len(x) 

def fit_Gauss_make_params(x, y, mu_guess, apex_guess, baseline_guess, n_per_bin):
    #make parameters for Gauss fit
    #x_mean = np.sum(x)/len(x)
    
    #y_mean=np.sum(y)/len(y)
    params = Parameters()
    #params.add('baseline', value=max(1, baseline_guess), min = max(0, min(y)-3*min(y)), max = max(3, baseline_guess*3, min(y)+y_mean/10)) #
    params.add('baseline', value=max(1, baseline_guess), min = max(0, baseline_guess/3), max = max(3, baseline_guess*3)) #
    #baseline cannot be above max I
    #logging.info('baseline guess: ' + str(baseline_guess))
    
    mu_guess_min = mu_guess-n_per_bin*3
    mu_guess_max = mu_guess+n_per_bin*3
    
    #sigma_guess = max(0.5, 1.0 + 0.00357488*(mu_guess-1900))
    sigma_guess = 0.0416569*np.exp(0.0017*mu_guess)
    sigma_guess_min = sigma_guess*float(0.7)
    sigma_guess_max = sigma_guess+1.5
    
    A_guess = (apex_guess-baseline_guess) * sigma_guess * 2.0
    A_guess_min = A_guess/2.0
    A_guess_max = A_guess*10
            
    for idx_peak in range(len(mu_guess)):
        #parameters per peak:
        #len(params) is a multiple of 4
        params.add('mu'+str(idx_peak), value = mu_guess[idx_peak], min = mu_guess_min[idx_peak], max = mu_guess_max[idx_peak]) #abcisse cannot be negative
        #params.add('sigma'+str(idx_peak), value = peak_value_final[idx_peak]/2, min = 0.0) #assumed proportionality between max intensity and sigma
        #params.add('sigma'+str(idx_peak), value = 0.65+1/48000*mu_guess[idx_peak], min = 0.65, max = (0.65+1/48000*mu_guess[idx_peak])*2)
        #logging.info(str(mu_guess[idx_peak]) + ', min: ' + str(mu_guess[idx_peak]-n_per_bin*3) + ', max: ' + str(mu_guess[idx_peak]+n_per_bin*3))
        #sigma_guess = 0.6
        #params.add('sigma'+str(idx_peak), value = sigma_guess, min = 0.3, max = 1.0)

        params.add('sigma'+str(idx_peak), value = sigma_guess[idx_peak], min = sigma_guess_min[idx_peak], max = sigma_guess_max[idx_peak])
        
        #A_guess = apex_guess[idx_peak]/(sigma_guess*math.sqrt(2*math.pi))
        #A_guess = (apex_guess[idx_peak]-baseline_guess) * sigma_guess * 2.0
        params.add('A'+str(idx_peak), value = max(2, A_guess[idx_peak]) , min = max(1, A_guess_min[idx_peak]), max = max(3, A_guess_max[idx_peak]))
        #params.add('A'+str(idx_peak), value = max(2, A_guess[idx_peak]) , min = max(1, A_guess/2.0), max = max(3, A_guess*10)) #, max = A_guess*3.0
        #params.add('A'+str(idx_peak), value = apex_guess[idx_peak]/(PW*math.sqrt(2*math.pi)) , min = 0.0)
        #logging.info(str(max(2, A_guess)) + ', min: ' + str(max(1, A_guess/2.0)) + ', max: ' + str(max(3, A_guess*10)))
       
    #logging.info('Guessed parameters: ')
    #params.pretty_logging.info()
    #fit, here with leastsq model
    minner = Minimizer(cost_f_Gauss, params, fcn_args=(x, y)) #, method='leastsq'
    peak_optimised = minner.minimize(method = 'leastsq')
    
    #resulting optimised parameters are callable with:
    #peak_optimised.params
    return params, peak_optimised

def fit_Gauss(x, y, mu_guess, apex_guess, baseline_guess_0, n_per_bin, area_threshold, ratio_to_linear_fit, graph):
    """This function computes a (or several) Gaussian fit(s) of the data. 
    Gaussian is parameterized by mu, sigma, A
    :param x: list of floating point numbers, x-coordinates
    :param y: list of floating point numbers, y-coordinates
    :param mu_guess: list of guess of mu parameter (mean) for Gaussian, float
    :param apex_guess: list of value of y when x=mu, float, max of Gaussian (at peak)
    :param n_per_bin: number of points x in between two integer values (sampling rate for x)
    :param area_threshold: minimum value of area (of Gaussian), float
    :param sigma_threshold: minimum value of sigma, float
    
    returns 
    """

    #assuming same baseline for all peaks in a small window:
    #x should cover a mass range large enough around unit mass (or same in tof_idx)
    #so that minimum can be found where no mass is expected
    #i.e. unit mass +/- 0.3 m/z or more
    #baseline_guess = min(y)
    #y_sort = [y[i] for i in range(len(y))]
    #y_sort.sort()
    #baseline_guess = np.median([y_sort[i] for i in range(int(len(y_sort)/3))]) 
    baseline_guess = max(0, min(y)*0.8, baseline_guess_0)
    #we have now points that corresponds to signal above baseline
    #so the minimum is the closest to the real baseline

    params, peak_optimised = fit_Gauss_make_params(x, y, mu_guess, apex_guess, baseline_guess, n_per_bin)
    #peak_optimised_params = peak_optimised.params

    if graph == True:
        logging.info('Guessed parameters: ')
        params.pretty_logging.info()
        fig, ax1 = plt.subplots(figsize=(8,4))
        fig.suptitle('Gauss fit over RT', fontsize=12)
        ax1.set_xlabel('Retention time [s]')
        ax1.set_ylabel('Intensity [TOF-index * V]', color='b')
        ax1.tick_params('y', colors='b')
        ax1.plot(x, y, 'b.')
        ax1.plot(mu_guess, apex_guess, 'o', color='xkcd:peach')
   
    
    A_opt = np.zeros(len(mu_guess))
    for idx_peak in range(len(mu_guess)):
        A_opt[idx_peak] = peak_optimised.params['A'+str(idx_peak)].value

    count_delete = (np.where(A_opt < area_threshold, 1, 0))
    count_delete_sum = int(np.sum(count_delete))
        
    while len(mu_guess)-count_delete_sum > 0 and count_delete_sum > 0:
        mu_guess = mu_guess[A_opt > area_threshold]
        apex_guess = apex_guess[A_opt > area_threshold]

        params, peak_optimised=fit_Gauss_make_params(x, y, mu_guess, apex_guess, baseline_guess, n_per_bin)
        peak_optimised.params = peak_optimised.params
        A_opt = np.zeros(len(mu_guess))
        for idx_peak in range(len(mu_guess)):
            A_opt[idx_peak] = peak_optimised.params['A'+str(idx_peak)].value
        count_delete = np.where(A_opt < area_threshold, 1, 0)
        count_delete_sum = int(np.sum(count_delete))


    """
    count_delete =0
    for idx_peak in range(len(mu_guess)-1, -1, -1):
        #delete too small peaks (may be just noise)
        #delete summits outside of RT window (not sure, what if we are on the tail of another peak?)
        if peak_optimised.params['A'+str(idx_peak)].value < area_threshold:
        #or peak_optimised.params['mu'+str(idx_peak)].value < min(x) \
        #or peak_optimised.params['mu'+str(idx_peak)].value > max(x):
            count_delete += 1
            mu_guess.remove(mu_guess[idx_peak])
            apex_guess.remove(apex_guess[idx_peak])
    while len(mu_guess)>0 and count_delete > 0:
        params, peak_optimised=fit_Gauss_make_params(x, y, mu_guess, apex_guess, baseline_guess, n_per_bin)
        peak_optimised_params = peak_optimised.params
        count_delete =0
        for idx_peak in range(len(mu_guess)-1, -1, -1):
            if peak_optimised.params['A'+str(idx_peak)].value < area_threshold:
            #or peak_optimised.params['mu'+str(idx_peak)].value < min(x) \
            #or peak_optimised.params['mu'+str(idx_peak)].value > max(x):     
                count_delete += 1
                mu_guess.remove(mu_guess[idx_peak])
                apex_guess.remove(apex_guess[idx_peak])
    """
 
    
    #fit a linear function, to then compare fit with Gauss, to test presence of real peak
    #fit with linear function y = a* x + b
    linear_fit_slope, linear_fit_b, linear_fit, std_dev_linear_fit = fit_linear_np(x, y)

    mu_opt = []
    mu_opt_stderr = []
    A_opt= []
    sigma_opt = []
    baseline_opt = []
    max_opt = []
    
    baseline_opt.append(peak_optimised.params['baseline'].value)
    final = baseline_opt


    if graph == True:
        logging.info('Optimised parameters:')
        peak_optimised.params.pretty_logging.info()
        ax2 = ax1.twinx()
        ax2.plot(x, peak_optimised.residual, 'c.')
        ax2.set_ylabel('residuals', color='c')
        ax2.tick_params('y', colors='c')

        #to plot the fit with good resolution to visualise something:
        x_plot = []
        x_plot.append(min(x))
        x_plot_step = min(x)
        step = n_per_bin/4.0
        while x_plot_step <= max(x):
            x_plot.append(x_plot_step+step)
            x_plot_step += step
        final_plot = baseline_opt

    for idx_peak in range(len(mu_guess)- count_delete_sum):
        A_opt.append(peak_optimised.params['A'+str(idx_peak)].value)
        mu_opt.append(peak_optimised.params['mu'+str(idx_peak)].value)
        mu_opt_stderr.append(peak_optimised.params['mu'+str(idx_peak)].stderr)
        sigma_opt.append(peak_optimised.params['sigma'+str(idx_peak)].value)
        max_opt.append(A_opt[idx_peak]/(sigma_opt[idx_peak]*math.sqrt(2*math.pi))+baseline_opt) #when x = mu
        y_opt = f_Gauss(x, A_opt[idx_peak], mu_opt[idx_peak], sigma_opt[idx_peak]) + baseline_opt #add baseline only once!
        final +=y_opt - baseline_opt
        if graph == True:
            y_opt_plot=f_Gauss(x_plot, A_opt[idx_peak], mu_opt[idx_peak], sigma_opt[idx_peak]) + baseline_opt #add baseline only once!
            ax1.plot(x_plot, y_opt_plot, 'g--')
            final_plot += y_opt_plot - baseline_opt
    
    if len(mu_opt)>0 and graph == True: 
        ax1.plot(x_plot, final_plot, 'k-')
        ax1.plot(mu_opt,max_opt, '*', color='xkcd:deep red')
    
    #if count_delete>0:
    if len(mu_opt)>0:
        #or better calculate standard deviation and divide by total area
        std_dev_Gauss = np.sqrt(np.sum((final- y)**2) / float(len(y)-1))
        std_dev_Gauss_rel= std_dev_Gauss /np.sum(A_opt)
        #noise_signal = sum([abs((final[i]- y[i]) / (final[i]- baseline_opt))  for i in range(len(y))])/len(y)
        
        if graph == True:
            #logging.info('Std dev of linear fit: ' + '{:.4f}'.format(std_dev_linear_fit))
            logging.info('Std dev of fit: ' + '{:.4f}'.format(std_dev_Gauss) + '; rel. to area: ' + '{:.4f}'.format(std_dev_Gauss_rel))
            logging.info('std dev Gauss / srd dev linear: ' + '{:.4f}'.format(std_dev_Gauss / std_dev_linear_fit ) )

        #test if Gaussian peak really improve the fit compared to linear fit
        #if not, this is not a peak, just noise (typically for N2, O2)
        #in case of real peak std_dev_Gauss/std_dev_linear_fit < 0.5
        if std_dev_Gauss/std_dev_linear_fit > ratio_to_linear_fit: 
            mu_opt = []
            mu_opt_stderr = []
            A_opt= []
            sigma_opt = []
            baseline_opt = []
            max_opt = []
            final = []
            if graph == True:
                ax1.plot(x, linear_fit, 'r--')
    if graph == True:
        #plt.savefig('Gauss_fit_on_mass_smallA.png', bbox_inches='tight', transparent = True, dpi = 300)
        fig.tight_layout()
        plt.show()
    
    return mu_opt, mu_opt_stderr, max_opt, A_opt, sigma_opt, baseline_opt, final, peak_optimised.residual

#******************************************************************************

def find_match(array_in, value, idx_value_in_tuple):
    """
    Aurore Guillevic 20190721
    ex: idx_value_in_tuple = 1 to look for mass in mass_cal_list
    Returns an array with all the array_in[i][j] if
    array_in[i][j][idx_in_tuple] == value
    if no match, return None
    """
    ans = [None]*len(array_in)
    for i in range(len(array_in)):
        j=0
        while j<len(array_in[i]) and array_in[i][j][idx_value_in_tuple] != value:
            j+=1
        if j<len(array_in[i]): #we have found a match because we went out of the while loop
            ans[i] = array_in[i][j]
    #Some ans[i] may be None
    return ans

def find_match_without_none(array_in, value, idx_value_in_tuple):
    """
    Aurore Guillevic 20190721
    ex: idx_value_in_tuple = 1 to look for mass in mass_cal_list
    Returns an array with all the array_in[i][j] if
    array_in[i][j][idx_in_tuple] == value
    if no match, return None
    """
    ans = []*len(array_in)
    i1=0
    for i in range(len(array_in)):
        j=0
        while j<len(array_in[i]) and array_in[i][j][idx_value_in_tuple] != value:
            j+=1
        if j<len(array_in[i]): #we have found a match because we went out of the while loop
            ans[i1] = array_in[i][j]
            i1 += 1
    #at this stage we may have empty cells at the end, we want to remove them from ans
    if i1<len(array_in):
        ans = ans[:i1]
    return ans


def make_histogram(results_list, delta_tofidx_discrete, h_index):
    """Make histograms of data.
    """      
        
    # sort results according to the center of mass peak (second item in tuple)
    results_list.sort(key=operator.itemgetter(h_index))        
            
    # histogram of nb of peak per TOF-index (or mass) bin

    #create empty histogram of right size, add +5 for slope detection algo later
    #histo_mass = [0 for i in range( int((results_list[len(results_list)-1][1] - results_list[0][1])/delta_mass_discrete) +20)]
    histo = [0 for i in range( int((results_list[len(results_list)-1][h_index] - results_list[0][h_index])/delta_tofidx_discrete) +20)]
    i = 0
    j = 10 #artificially leave 10 first values as zero to allow slope detection algo later on
    histo_x_a = results_list[0][h_index]
    #mass_b = mass_a + delta_mass_discrete
    histo_x_b = histo_x_a + delta_tofidx_discrete
    while i < len(results_list): # and j < len(histo_mass)
        while i < len(results_list) and results_list[i][h_index] < histo_x_b:
            histo[j] += 1
            i += 1
        j += 1
        histo_x_a = histo_x_b
        #mass_b += delta_mass_discrete
        histo_x_b += delta_tofidx_discrete
    
    #below we have i-10 because above we have j=10; we start filling the histo at j=10
    histo_x_coord = [results_list[0][h_index] + delta_tofidx_discrete*(i-10) for i in range(len(histo))]
    
    return histo_x_coord, histo


def find_bins_np(x, delta_bin, PWh, PTh, h_nb_per_bin, graph, mode, remove_long_0_areas: bool = False):
    """Group the points together.
    
    Returns an array the centers of the groups.
    """
    
    logger = logging.getLogger(__name__)

    left, right = min(x) - PWh, max(x) + PWh 
    hist_bin_edges = np.arange(left, right, delta_bin)
    logger.debug(f'hist_bin_edges: {hist_bin_edges}')
    
    hist, hist_bin_edges = np.histogram(x, bins = hist_bin_edges)
    
    logger.debug(f"hist: {hist}")
    np_hist_bin_centres = hist_bin_edges[:-1] + delta_bin / 2
    # histogram of nb of peak per TOF-index (or mass) bin
    if remove_long_0_areas:
        indices = np.where(hist > 0)[0] 
        mask = np.zeros_like(hist, dtype=bool)
        clip_range = lambda x: np.clip(x, 0, len(hist) - 1)
        for i in range(-3, 4): 
            mask[clip_range(indices + i)] = True

        hist = hist[mask]
        np_hist_bin_centres = np_hist_bin_centres[mask]



    #we now have an histogram of nb of peaks per mass bin
    #we apply again the slope detection algo
    histo_bin_centres = peak_detect(
        np_hist_bin_centres,
        hist, 
        PWh, PTh, h_nb_per_bin, 
        graph = graph, mode = mode
    )
    
    if len(histo_bin_centres) == 0:
        return []
    
    bin_centres = histo_bin_centres[:,0]
    return bin_centres

def find_nearest(
    array: np.ndarray, values: np.ndarray, return_distance: bool = False
) -> np.ndarray | tuple[np.ndarray, np.ndarray]:
    """Find the nearest value in an array.
    
    :param array: np.array, sorted array of values to search in
    :param values: np.array, values to search for in the array
    :param return_distance: bool, whether to return the distance to the nearest value

    :return: np.array, indices of the nearest values in the array
        or tuple of np.array, indices and distances to the nearest values
    """
    # Check that it is sorted 
    if not np.all(np.diff(array) >= 0):
        raise ValueError("Input array must be sorted.")
    
    right_idx = np.searchsorted(array, values, side="left")
    left_idx = right_idx - 1

    clip_range = lambda x: np.clip(x, 0, len(array) - 1)

    left_idx = clip_range(left_idx)
    right_idx = clip_range(right_idx)
    
    # Check if values are out of bounds
    dist_left = np.abs(array[left_idx] - values)
    dist_right = np.abs(array[right_idx] - values)

    # Choose the closest value
    result = np.where(dist_left < dist_right, left_idx, right_idx)
    if return_distance:
        return result, np.where(dist_left < dist_right, dist_left, dist_right)
    return result

class MakeBinsModes(Enum):
    """Modes for the make_bins_np function.
    
    * WITH_SLOPE: the algorithm will try to remove points that are too far
        from a linear fit of the data in the bin
    * NO_SLOPE: the algorithm will only remove points that are too far from the median
    * ROLLING: the algorithm will remove points that are too far from the rolling median
        Assumes that the points with the same x values have a similar distance between 
        each other.
    """
    WITH_SLOPE = 'with_slope'
    NO_SLOPE = 'no_slope'
    ROLLING = 'rolling'    


def minimun_bin_of_mode(mode: MakeBinsModes) -> int:
    """Return the minimum number of points per bin for a given mode.
    
    :param mode: MakeBinsModes, mode to get the minimum number of points for
    
    :return: int, minimum number of points per bin
    """
    match mode:
        case MakeBinsModes.WITH_SLOPE:
            return 2
        case MakeBinsModes.NO_SLOPE | MakeBinsModes.ROLLING:
            return 1
        
    raise ValueError(f'mode should one of {MakeBinsModes.__members__.keys()}')


def make_bins_np(
        data_x: np.ndarray,
        data_y: np.ndarray,
        histo_centers: np.ndarray,
        std_dev_p_max: float,
        nb_bin_min: int = 1,
        s_per_bin: float = 1.0,
        graph: bool = False,
        mode: MakeBinsModes = MakeBinsModes.WITH_SLOPE,
        plot_mode: str = 'rt_domain',
        save_graph_path: Path | None = None,
    ) -> tuple[np.ndarray, int, list[int]]:
    """
    Distribute data into the identified groups (histo_centre_list).
    Filter: eliminate data too far away from the centre.
    These data, not assigned to a centre, are then assigned an idx_bin of -1.
    
    Inputs:
    :param data_x: x values of the data
    :param data_y: y values of the data
    :param histo_centres: list of centre of bins of points
    :param std_dev_p_max: maximum standard deviation of position of points within a bin
    :param nb_bin_min: minimum number of points per bin. If smaller, bin index is set to -1 (unused).
    :param s_per_bin: size of a bin in seconds. used only if plot_mode is 'mass_domain',
        optional, default is 1.0
    :param graph: bool, whether to plot the results, optional, default is False
    :param mode: see :py:class:`MakeBinsModes`, optional, default is 'with_slope'
    :param plot_mode: str, 'rt_domain' or 'mass_domain', optional, default is 'rt_domain'
    :param save_graph_path: Path, path to save the graph, optional, default is None
    

    :return: tuple of:
        bin_value_list: np.ndarray, has size of data_x, contain the corresponding bin value.
        no_bins_final: number of different valid bins (not -1).
        bin_list: list of index values for all various bins.
    """

    assert len(data_x) == len(data_y), 'data_x and data_y should have the same length'

    bin_list = []
    bin_value_list = np.zeros(len(data_x), dtype = int)


    
    #***Assign bin index according to closest centre of bins***
    #for each bin, eliminate already all points that are away of the centre by more than n * std_dev_p_max
    max_distance = std_dev_p_max * 4.0

    closest_index, distances_to_closest = find_nearest(histo_centers, data_y, return_distance=True)

    closest_index = np.where(distances_to_closest <= max_distance, closest_index, -1)
    
    no_peak_min = max(minimun_bin_of_mode(mode), nb_bin_min)
    original_mode = mode

    data_y_original = np.array(data_y).copy()
    data_y = data_y_original.copy()

    if mode == MakeBinsModes.ROLLING:
        unique_x = np.unique(data_x)


        for x in unique_x:
            mask = (data_x == x) & (closest_index != -1)
            if mask.sum() == 0:
                continue
            y = data_y[mask]
            offsets = y - histo_centers[closest_index[mask]]
            # Duplicated indexes, we keep the one with the smallest offset
            counts = np.bincount(closest_index[mask], minlength=len(histo_centers))
            indexes_duplicated = np.argwhere(counts > 1).flatten()
            mask_duplicated = np.isin(closest_index[mask], indexes_duplicated) 
            mask_single = ~mask_duplicated
            if np.sum(mask_single) == 0:
                # Cannot calculate an offset
                continue
            mean_offset = np.median(offsets[mask_single])
            # Keep points closest to the center + offset
            data_y[mask] = y - mean_offset

        # Now we have the offsets, we can calculate the closest index
        # but with the offsetec data
        mode = MakeBinsModes.NO_SLOPE
                

    # For each center, we make an additional fit to have a better groupping
    for i, _ in enumerate(histo_centers):

        limit = std_dev_p_max
        #logging.info('len bin indexes: ' + str(len(bin_indexes)))

        mask_this_center = closest_index == i

        # We proceed by removing points that are too far from the linear fit one by one
                
        while (
            # First condition: enough points in this group
            np.sum(mask_still_valid := (closest_index == i)) >= no_peak_min
        ): 
            
            x = data_x[mask_still_valid]
            y = data_y[mask_still_valid]


            if mode == MakeBinsModes.NO_SLOPE:
                # Fit a median line
                poly = lambda x: np.full_like(x, np.median(y))
                
            elif mode == MakeBinsModes.WITH_SLOPE:
                poly = np.polynomial.polynomial.Polynomial.fit(x, y, 1)
            
            else:
                raise ValueError(f'Unknown mode: {mode}')

            max_diff = np.max(np.abs(y - poly(x)))
            # Limit to use to remove points that are too far from the linear fit
            limit = max(std_dev_p_max, max_diff)


            # Take all data again, in case throuing out a bad point brings ok data back to the line
            mask_remove = np.abs(data_y[mask_this_center] - poly(data_x[mask_this_center])) > limit
            closest_index[mask_this_center] = np.where(mask_remove, -1, i)

            # Points deleted during this iteration
            deleted_points = np.sum(mask_still_valid) - np.sum(~mask_remove)

            # Conditions to stop
            if deleted_points <= 0:
                # No points deleted this iteration
                break

            if limit == std_dev_p_max:
                # Limit was already the minimum allowed
                break
        
        mask_this_center = closest_index == i
        if np.sum(mask_this_center) < no_peak_min:
            bin_value_list[mask_this_center] = -1
        else:
            # Add the bin to the valid list
            bin_value_list[mask_this_center] = i
            bin_list.append(i)
    
    if graph == True:

        fig, ax1 = plt.subplots(figsize=(10,5))
        ax1.ticklabel_format(axis = 'both', style = 'plain', useMathText=False)
        ax1.grid(True, color='xkcd:grey', linestyle='--', linewidth=0.5)

        title = 'Fragments grouped by co-elution' if plot_mode == 'rt_domain' else 'TOF-indexes grouped by same fragment'
        title += f" (method={original_mode.value})"

        if plot_mode == 'rt_domain': #group fragments by co-elution
            fig.suptitle(title, fontsize=14)
            ax1.set_xlabel('Retention time [s]', fontsize=14)
            ax1.set_ylabel('Mass of fragment [m/z]', fontsize=14)

            x, y = data_y, data_x
        
        elif plot_mode == 'mass_domain': #mass calibration: group by tof-index
            fig.suptitle(title, fontsize=12)
            ax1.set_xlabel('Retention time [s]')
            ax1.set_ylabel('Centre of TOF peak [TOF index]')
            # Put the y ticks at each unit value 
            #ticks = np.arange(int(min(data_y)), int(max(data_y)) + 2, 1)
            #ax1.set_yticks(ticks, ticks)

            x, y = data_x*s_per_bin, data_y
        else:
            raise ValueError('plot_mode should be either `rt_domain` or `mass_domain`')
        
        ax1.plot(x, y, '.', color = 'xkcd:slate', label="Non-grouped data")
        if original_mode == MakeBinsModes.ROLLING:
            ax1.plot(x, data_y_original, 'x', color = 'xkcd:slate', alpha=0.5, label="Non-grouped data (original no offset)")
        
        #plot each bucket with a different color
        color_iter = iter(color1_names)
        for i, center in enumerate(histo_centers):
            mask_this_center = closest_index == i

            color = next(color_iter)

            ax1.plot(
                x[mask_this_center], y[mask_this_center], '.',
                color = color, label = f'Group {i}, {center=}'
            ) 
            if original_mode == MakeBinsModes.ROLLING:
                ax1.plot(
                    x[mask_this_center], data_y_original[mask_this_center], 'x',
                    color = color,
                    alpha = 0.5,
                    label = f'Group {i}, {center=} (original no offset)'
                ) 
            line_method = ax1.axhline if plot_mode == 'mass_domain' else ax1.axvline
            line_method(center, color=color, linestyle='--', linewidth=0.5)    
        ax1.legend()

        if save_graph_path is not None:
            fig.savefig(save_graph_path, bbox_inches='tight', transparent=True, dpi=300)
        else:
            plt.show()
              
    return bin_value_list, len(bin_list), bin_list

   
def make_buckets(results_list, histo_centre_list, std_dev_p_max, nb_bin_min, h_index, h_index_x, s_per_bin, graph, mode):
    """
    Distribute data into the identified groups (histo_centre_list).
    Filter: eliminate data too far away from the centre.
    """
    #bucket_peak_rm = []
    if graph == True:
        #normal plot routine
        fig, ax1 = plt.subplots(figsize=(10,5))
        ax1.ticklabel_format(axis = 'both', style = 'plain', useMathText=False)
        ax1.grid(True, color='xkcd:grey', linestyle='--', linewidth=0.5)

        if h_index == 0 and h_index_x == 1: #group fragments by co-elution
            fig.suptitle('Fragments grouped by co-elution', fontsize=14)
            plt.xlabel('Retention time [s]', fontsize=14)
            plt.ylabel('Mass of fragment [m/z]', fontsize=14)
            rt_mean = (min([line[h_index] for line in results_list]) + max([line[h_index] for line in results_list]))/2

            for line in results_list:
                #mass = ((line[h_index_x] +3729.0) / 2828.0)**(1/0.4999)
                ax1.plot(line[h_index], line[6], '.', color = 'xkcd:slate')
        
        elif h_index == 1 and h_index_x == 0: #mass calibration: group by tof-index
            fig.suptitle('TOF-indexes grouped by same fragment', fontsize=12)
            plt.xlabel('Retention time [RT index or s]')
            plt.ylabel('Centre of TOF peak [TOF index]')
            for line in results_list:
                ax1.plot(line[h_index_x], line[h_index], '.', color = 'xkcd:slate')  


        else: #if h_index == 1 and h_index_x == 0:
            fig.suptitle('Masses grouped by same fragment', fontsize=12)
            plt.xlabel('Retention time [RT index or s]')
            plt.ylabel('Centre of mass peak [m/z]')
            for line in results_list:
                ax1.plot(line[h_index_x], line[h_index], '.', color = 'xkcd:slate')  



    #logging.info('make buckets')
    
    if len(histo_centre_list) ==1: #one identified peak only, makes only one bucket
        bucket_peak = []
        bucket_peak.append(results_list)
    
    elif len(histo_centre_list) > 1:
        bucket_peak = []
        i = 0
        for histo_idx_peak in range(len(histo_centre_list)-1):
            histo_peak_0 = histo_centre_list[histo_idx_peak] # float numbers, this one is the first peak
            histo_peak_1 = histo_centre_list[histo_idx_peak+1] # float numbers, this one is the second peak
            i0 = i
            while i < len(results_list) and abs(results_list[i][h_index] - histo_peak_0) < abs(results_list[i][h_index] - histo_peak_1):
                i += 1
            bucket_peak.append(results_list[i0:i])
        if i < len(results_list): #results after the last peak centre
            bucket_peak.append(results_list[i:])

    #now we want to eliminate the points that are too far from the assigned centres
    #the new list bucket_mass_peak_rm contains the masses that are close enough to the assigned centre
    #bucket_peak_rm = bucket_peak.copy()
    if mode == 'no_slope':
        no_peak_min = max(nb_bin_min, 1)
    elif mode == 'with_slope':
        no_peak_min = max(nb_bin_min, 2) #need two points at least for a linear fit

    for i in range(len(bucket_peak)):
        #logging.info('Bucket number: ' + str(i) + '; len of entire bucket: ' + str(len(bucket_peak[i])))
        
        #if len(bucket_peak[i])<max(nb_bin_min, 2): #do not evaluate data
        #    #do not throw away all, 
        #    #when looking at fragment close to LOD, this may be one or two real fragments.
        #    bucket_peak_rm.append([bucket_peak[i][j] for j in range(len(bucket_peak[i]))])

        #elif len(bucket_peak[i])>=max(nb_bin_min, 2): #need two points to calculate linear fit
        #if len(bucket_peak_rm[i]) >= max(nb_bin_min, 2): 
        count_delete = 1
        limit = std_dev_p_max
        len_bucket_peak_i = len(bucket_peak[i])
        #bucket_peak_rm.append([bucket_peak[i][j] for j in range(len(bucket_peak[i]))])
        
        while len_bucket_peak_i >= no_peak_min and (count_delete > 0  or limit > std_dev_p_max):
            
            y = np.array([float(bucket_peak[i][j][h_index]) for j in range(len(bucket_peak[i]))])
            
            if mode == 'no_slope':
                linear_fit_slope = 0
                linear_fit_b = np.median(y)
                std_dev_linear_fit = np.sqrt(np.sum((y - linear_fit_b)**2) / (len(y)-1))
                linear_fit = [linear_fit_b]*len(y)
                
            elif mode == 'with_slope':
                x = np.array([float(bucket_peak[i][j][h_index_x]) for j in range(len(bucket_peak[i]))])
                linear_fit_slope, linear_fit_b, linear_fit, std_dev_linear_fit = fit_linear_np(x, y)
            
            
            max_diff = max([abs(y[j] - linear_fit[j]) for j in range(len(y))])
            #eliminate mass values that are too far away from avrg_p:
            #for idx in range(len(bucket_mass_peak[i]),-1,-1):
            #    if bmp_ij[1] < (avrg_p - 3 *std_dev_p) or bmp_ij[1] > (avrg_p + 3 *std_dev_p):
            #        bucket_mass_peak[i].remove(bmp_ij)
            #        #delete_count += 1
            len_bucket_old = len(bucket_peak[i])
            #new_limit = max(2.0, std_dev_linear_fit)
            limit = max(std_dev_p_max, max_diff)
            #logging.info('max diff: ' + str(max_diff) + '; limit: ' + str(limit))
            #maybe the best woud be to remove the items one by one
            #bucket_peak_rm[i] = [bucket_peak_rm[i][j] for j in range(len(bucket_peak_rm[i])) if abs(bucket_peak_rm[i][j][1] - median_p) < std_dev_p_tofidx ]
            bucket_peak[i] = [bucket_peak[i][j] for j in range(len(bucket_peak[i])) if abs(bucket_peak[i][j][h_index] - linear_fit[j]) < limit ]
            
            count_delete = len_bucket_old - len(bucket_peak[i])
            len_bucket_peak_i = len(bucket_peak[i])
            #logging.info('count delete: ' + str(count_delete))
            #logging.info(len(bucket_mass_peak_rm[i]))
    #each [i] in bucket_mass_peak contains (rt_idx, centre of mass peak) belonging to same mass over an entire run
    #mass_centre_optimised_list = []
    
    bucket_peak_final = []
    #bucket_peak_final_single = []
    for i in range(len(bucket_peak)):
        if len(bucket_peak[i]) >= nb_bin_min:
            bucket_peak_final.append(bucket_peak[i])
        #else:
        #    bucket_peak_final_single.append(bucket_peak_rm[i])
    
    
    if graph == True:
        color_idx = 0
        #plot each bucket with a different color
        if h_index == 0 and h_index_x == 1:
            
            for idx in range(len(bucket_peak_final)):
                #if len(bucket_peak[idx]) >= nb_bin_min:
                for line in bucket_peak_final[idx]:
                    #mass = ((line[h_index_x] +3729.0) / 2828.0)**(1/0.4999)
                    ax1.plot(line[h_index], line[h_index_x], '.', color = str(color1_names[color_idx])) # 'xkcd:blue'
                color_idx += 1
                if color_idx > len(color1_names):
                    color_idx = 0


        else: #if h_index == 1 and h_index_x == 0:
            for idx in range(len(bucket_peak_final)):
                #if len(bucket_peak[idx]) >= nb_bin_min:
                for line in bucket_peak_final[idx]:
                    ax1.plot(line[h_index_x], line[h_index], '.', color = str(color1_names[color_idx]))
                color_idx += 1
                if color_idx > len(color1_names):
                    color_idx = 0
            
            #ax1.set_xlim(1600, 1800)
            #plt.savefig('grouped_peaks_vs_RT_'+ '{:.2f}'.format(rt_mean) + '.png', bbox_inches='tight', transparent = True, dpi = 300)
        #fig.tight_layout()
        plt.show()
        #******
   
    #at this stage we still may have some bucket_peak_rm with less than 3 peaks
            
    return bucket_peak_final #bucket_peak_final, bucket_peak_final_single

def split_into_windows(
    rt_idx_start_bin: int,
    rt_idx_stop_bin: int,
    s_per_bin: float,
    len_time_axis: int,
    # Constants
    rt_window: float = const_pd.rt_window,
    rt_window_edge: float = const_pd.rt_window_edge
) -> list[list[int, int]]:
    """
    
    Parameters
    ----------
    rt_idx_start_bin : int
        Index of start time.
    rt_idx_stop_bin : int
        Index of stop time.
    const_pd.rt_window : float
        maximum duration of a window.
    const_pd.rt_window_edge : float
        duration of an 'edge' window to add before and after when spliting in two.
    s_per_bin : float
        corresponding duration of one rt index.
    len_time_axis : float
        max index of the time axis.

    Returns
    -------
    List of intevals of the windows. 

    """
    if rt_idx_stop_bin - rt_idx_start_bin > 1.2 * (rt_window + 2 * rt_window_edge)/s_per_bin:
          #split windows:
          nb_windows = int(round((rt_idx_stop_bin - rt_idx_start_bin) / (rt_window/s_per_bin)))
          #logging.info(nb_windows)
          #exact number of time indexes per window:
          window_idx_duration = int(round(rt_idx_stop_bin - rt_idx_start_bin)/nb_windows)
          #logging.info(window_idx_duration)
          
          edge_idx_duration = math.ceil(rt_window_edge/s_per_bin)
          
          #window edges:
          #the first window may have no edge to the left
          start_edge_idx = max(0, rt_idx_start_bin-edge_idx_duration)
          #last window may have no edge to the right
          stop_edge_idx = min(len_time_axis-1, rt_idx_stop_bin + edge_idx_duration)
          
          windows_start_stop = [None]*nb_windows
          #windows_start_stop[0] = [start_edge_idx, start_edge_idx + window_idx_duration + 2*edge_idx_duration]
          for i_w in range(nb_windows):
              windows_start_stop[i_w] = [start_edge_idx  + window_idx_duration* i_w , start_edge_idx + window_idx_duration* (i_w+1) + 2* edge_idx_duration]
              #logging.info(str(windows_start_stop[i_w][0] + edge_idx_duration) + ' ' + str(windows_start_stop[i_w][1]- edge_idx_duration))
          windows_start_stop[-1][1] = stop_edge_idx
          
    else: #make one window
          windows_start_stop = [[rt_idx_start_bin, rt_idx_stop_bin]]
          
    return windows_start_stop

def tof_peak_extraction(data, list_exact_masses, mode):
    """
    mode: mass calibration
    mode: screening
    list_exact_masses: is used in mc mode only; otherwise is calculated from the data
    """
    return None
    

def extract_chomatograms(
    tof_data: np.ndarray,
    mass_axis: np.ndarray,
    compounds: list[str] | None = None,
    d_m: float = 0.008,
    masses: Iterable[int | float] = range(2, 300),
    background_quantile: float = 0.1,
    baseline_method: str | None = None,
    smoothing_kernel: np.ndarray | None = None,
    baseline_kwargs: dict[str, Any] = {},
    return_non_corrected: bool = False,
) -> dict[int, np.ndarray] | tuple[dict[int, np.ndarray], dict[int, np.ndarray]]:
    """Extract chomatograms from the tof data. 

    :arg tof_data: 2d array of tof data. shape = (n_rt, n_mz)
    :arg mass_axis: 1d array of the mass from the tof data. shape = (n_mz) 
    :arg compounds: A list of compound from which to extract the chromatograms.
    :arg d_m: The delta of mass to use to do the chromatogram. 
        Usually the chormatogram is obtained by summing over that range.
    :arg masses: If you don't want to define by compounds are given, you can specifiy the range of 
        masses to extract.
    :arg background_quantile: The quantile to use to define the background.
        Using 0.5 is equivalent to using the median.
    :arg baseline_method: The method to use to define the baseline.
        This should be an algorithm from the `pybaseline` package.
        If this is set, the background_quantile is ignored.
    :arg return_non_corrected: If True, return the non corrected chromatogram
        as an additional dict
    """


    if compounds:
        # Calculate the masses of the compounds we need to find

        compounds_masses = [
            formula_to_mass(compound)
            for compound in compounds
        ]
    else:
        # Use default integer mass range
        compounds_masses = list(masses)
        compounds = list(masses)

    
    out_dict = {}
    out_dict_non_corrected = {}
    
    for compound, mass in zip(compounds, compounds_masses):
        mask_valid_masses = (mass_axis > mass - d_m) & (mass_axis < mass + d_m)
        if np.sum(mask_valid_masses) == 0:
            raise ValueError(f"Mass {mass} is not in the data with the desired range {d_m=}")
        this_mass_data = tof_data[:, mask_valid_masses]

        mass_chromato = this_mass_data.sum(axis=1)
        if return_non_corrected:
            out_dict_non_corrected[compound] = mass_chromato.copy()


        if smoothing_kernel is not None:
            mass_chromato = np.convolve(mass_chromato, smoothing_kernel, mode='same')

        # Remove background
        if baseline_method is not None:
            from pybaselines import Baseline

            baseline_fitter  = Baseline()
            baseline_func = getattr(baseline_fitter, baseline_method)
            background = baseline_func(mass_chromato, **baseline_kwargs)[0]
        else:
            background = np.quantile(mass_chromato, background_quantile)
        nan_backgrounds = np.isnan(background)
        if np.any(nan_backgrounds):
            logger.warning(f"Background has nans for mass {mass}")
            # Set to 0
            background[nan_backgrounds] = 0
        mass_chromato -= background
        mass_chromato = np.clip(mass_chromato, 0, None)

        out_dict[compound] = mass_chromato
    
    if return_non_corrected:
        return out_dict, out_dict_non_corrected
    
    return out_dict
