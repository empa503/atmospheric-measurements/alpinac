"""Periodic table of elements that can be in the air.

The exact mass and the isotopic relative abundances are from IUPAC, see ref.:
Meija, J., Coplen, T.B., Berglund, M., Brand, W.A., Bievre,
P.D., Greoning, M., Holden, N.E., Irrgeher, J., Loss,
R.D., Walczyk, T., Prohaska, T.:
Atomic weights of the elements 2013 (IUPAC technical report).
Pure andApplied Chemistry 88(3), 265-291 (2016).
doi:10.1515/pac-2015-0305
"""

import logging
import re  # for formula_to_mass
from pathlib import Path
import pandas as pd
import copy

# Define a fragment as being a list containing the number of each atom
# the list has the same size as the periodic table from periodic_table
# Tis list is created using function get_fragment_from_string_formula
FragmentAsAtomCounts = list[int]


labels = ("element", "mass", "unit_mass", "valence_index", "occurence", "atomic_number")


def get_periodic_table_and_other_lists():
    """
    Creates a lists containing information about some isotopes.

    The list periodic_table contains the following information:
    element, mass, unit_mass, valence_index, occurence, atomic_number
    The list ordered_elements_string_formula_sub contains the indexes
    of the elements in periodic_table for formatting the formula in
    a nice-looking way.
    The list idx_mono_valent contains the indexes of the substances
    with valence equal to 1.
    The list idx_multi_valent contains the indexes of the substances
    with valence greater than 1.
    The list idx_zero_valent contains the indexes of the substances
    with valence smaller or equal than 0.
    """

    p = Path(__file__).with_suffix(".csv")
    df = pd.read_csv(p, index_col=0)
    df = df.replace(pd.NA, "")
    periodic_table = [
        dict(zip(labels, rows[: len(labels)])) for rows in df.itertuples(False)
    ]
    # Create tuple for the current row
    ordered_elements_string_formula_sub = df.sort_values(
        by="order_for_nice_looking"
    ).index.to_list()
    idx_mono_valent = df[df["valence_index"] == 1].index.to_list()
    idx_multi_valent = df[df["valence_index"] > 1].index.to_list()
    idx_zero_valent = df[df["valence_index"] <= 0].index.to_list()
    return (
        periodic_table,
        ordered_elements_string_formula_sub,
        idx_mono_valent,
        idx_multi_valent,
        idx_zero_valent,
    )


(
    periodic_table,
    ordered_elements_string_formula_sub,
    idx_mono_valent,
    idx_multi_valent,
    idx_zero_valent,
) = get_periodic_table_and_other_lists()

# define constants
# mass of a lost electron during ionisation process
electron_mass = float(0.00054858)
# mass of H+:
PRT_H = float(1.00782503) - electron_mass
len_frag = len(periodic_table)
# abundance
abundance = [elem["occurence"] for elem in periodic_table]
# mass
mass = [elem["mass"] for elem in periodic_table]
# label (string code) of the element
element = [elem["element"] for elem in periodic_table]
# valence
valence = [elem["valence_index"] for elem in periodic_table]
atomic_number = [elem["atomic_number"] for elem in periodic_table]
unit_mass = [elem["unit_mass"] for elem in periodic_table]
# correspondance between string code and index
dict_element_idx = {e: i for (i, e) in enumerate(element)}
# correspondance between string code and mass
dict_element_mass = {e: mass[i] for (i, e) in enumerate(element)}


def get_dict_abundant_rare():
    d = {}
    # adding to the dictionary the isotopes: the key is the index
    # of the must abundant isotope of one element and the values are
    # all the index of all the less abundant isotope of the same element.
    for i in range(len(periodic_table)):
        isotopes = [
            j
            for j, row in enumerate(periodic_table)
            if is_different_isotope(row["element"], periodic_table[i]["element"])
        ]
        if len(isotopes) > 0:
            d[i] = isotopes
    for j in range(55):
        # adding to the dictionary the elements without isotopes
        isotopes = [
            i for i, indexes_isotopes in d.items() if j in indexes_isotopes or j == i
        ]
        if not isotopes:
            d[j] = []

    # sorting the dictionary from the index of the element with the
    # isotope with the least mass to the element with the isotope with
    # the greatest mass
    ordered_keys = list(d.keys())
    ordered_keys.sort(
        key=lambda j: min([periodic_table[i]["mass"] for i in d[j] + [j]])
    )
    d = {i: d[i] for i in ordered_keys}
    # sorting each list in the dictionary from the index of the isotope with
    # the most abundant to the index of the isotope with the least abundant
    for i in d:
        d[i].sort(key=lambda j: periodic_table[j]["occurence"], reverse=True)
    return d


def is_different_isotope(atom: str, potential_isotop: str):
    """
    Check that the two given atoms are different, but still isotopes.
    """
    # in periodic_table, the most abundant isotop does not contain brakets nor
    # number. For example, if we remove from "[13C]" the brakets and "C", we get
    # a number.
    are_isotops = atom.strip("[]").replace(potential_isotop, "").isnumeric()
    are_different_atoms = potential_isotop != atom
    return are_isotops and are_different_atoms


# correspondance of indices: given the abundant one, which ones are rare
dict_abundant_rare = get_dict_abundant_rare()
# correspondance of indices of rare elements to their abundant
dict_rare_abundant = {
    isotop: element
    for element, isotopes in dict_abundant_rare.items()
    for isotop in isotopes
}

# the indices of abundant isotopes. Mono-isotopic atoms of abundance 1 are also given (F (11), P (18), I (47)).

idx_rare_isotopes_i = [
    isotope for isotopes in dict_abundant_rare.values() for isotope in isotopes
]
idx_abun_isotopes = [element for element in dict_abundant_rare] + list(
    range(max(dict_abundant_rare.keys()) + 1, len(periodic_table))
)


idx_rare_isotopes = copy.deepcopy(idx_rare_isotopes_i)
idx_rare_isotopes.sort()

dict_element_to_adduct = {
    j: i
    for i, abduct in enumerate(periodic_table)
    for j, element in enumerate(periodic_table)
    if "ad." in abduct["element"]
    and element["element"] in abduct["element"]
    and element["element"] != abduct["element"]
}

dict_adduct_to_element = {
    adduct: element for element, adduct in dict_element_to_adduct.items()
}

ionisation_EI_string = "EI"
ionisation_EI = int(0)
ionisation_CI_string = "CI"
ionisation_CI = int(1)

dict_ionisation = {
    ionisation_EI: ionisation_EI_string,
    ionisation_CI: ionisation_CI_string,
}


def get_string_formula(vec_frag: FragmentAsAtomCounts) -> str:
    """Standardized string encoding of chemical formula

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: a string encoding the chemical molecule
    """
    s = ""
    for i in range(len(vec_frag)):
        vi = vec_frag[i]
        if vi == 1:
            s += element[i]
        elif vi > 1:
            s += element[i] + str(vi)
    return s


def get_string_formula_sub(vec_frag: FragmentAsAtomCounts) -> str:
    """Standardized string encoding of chemical formula

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: a string encoding the chemical molecule with \\Sub and \\Sup

    The order of chemical elements is modified as well, to try to reflect order recommended by IUPAC.
    Multivalent atoms first, and then alphabetically (more or less).
    """
    s = ""
    # for i in range(len(vec_frag)):
    for i in ordered_elements_string_formula_sub:
        vi = vec_frag[i]
        if vi == 1:
            s += element[i]
        elif vi > 1:
            s += element[i] + r"\Sub{" + str(vi) + "}"
    return s


def get_string_formula_chem(vec_frag: FragmentAsAtomCounts) -> str:
    """Standardized string encoding of chemical formula
    compatible with \\chem{} notation in latex,
    required by copernicus publications.

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: a string encoding the chemical molecule with \\chem

    The order of chemical elements is modified as well, to try to reflect order recommended by IUPAC.
    Multivalent atoms first, and then alphabetically (more or less).
    """
    s = r"\chem{"
    for i in ordered_elements_string_formula_sub:
        vi = vec_frag[i]
        # is rare isotope?
        if i in idx_rare_isotopes:
            table_line = periodic_table[
                dict_rare_abundant[i]
            ]  # take element code of abundant one (e.g. Cl instead of 37Cl)
            el_abund = table_line["element"]
            iso_no = periodic_table[i][
                "unit_mass"
            ]  # take number that should go in front of element
            el = "^{" + str(iso_no) + "}" + el_abund  # write the latex code
        else:
            el = element[i]

        if vi == 1:
            s += el
        elif vi > 1:
            s += el + "_{" + str(vi) + "}"
    s += "}"
    return s


def get_string_formula_chem_ionised(vec_frag: FragmentAsAtomCounts) -> str:
    """Standardized string encoding of chemical formula
    compatible with \\chem{} notation in latex,
    required by copernicus publications.

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: a string encoding the chemical molecule with \\chem

    The order of chemical elements is modified as well, to try to reflect order recommended by IUPAC.
    Multivalent atoms first, and then alphabetically (more or less).
    """
    s = r"\chem{"
    for i in ordered_elements_string_formula_sub:
        vi = vec_frag[i]
        # is rare isotope?
        if i in idx_rare_isotopes:
            table_line = periodic_table[dict_rare_abundant[i]]
            el_abund = table_line["element"]
            iso_no = periodic_table[i]["unit_mass"]
            el = "[^{" + str(iso_no) + "}" + el_abund + "]"
        else:
            el = element[i]

        if vi == 1:
            s += el
        elif vi > 1:
            s += el + "_{" + str(vi) + "}"
    s += "^+}"
    return s


def get_ionization_value(fragment: str) -> int:
    """Return the ionization value of the fragment

    INPUT:
    - ``fragment``: a string formula with possible isotopologues (with brakets)

    OUPTUT: the ionization value of the fragment
    """
    plus_count = fragment.count("+")  # number of '+' in the formula
    if plus_count == 1 and fragment[-1] != "+":
        # read the number after the '+'
        _, number = fragment.split("+")
        plus_count = int(number)

    return plus_count


def get_mass_not_ionised(vec_frag: FragmentAsAtomCounts) -> float:
    """Exact mass of the fragment, not correction for ionisation.
    This is specifically used for the knpasack step.

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: the exact mass of `vec_frag`
    """
    return float(
        sum(
            [
                float(vec_frag[i]) * mass[i]
                for i in range(len(vec_frag))
                if vec_frag[i] > 0
            ]
        )
    )


def get_mass_single_ionised(vec_frag: FragmentAsAtomCounts) -> float:
    """Exact mass of a single-ionised fragment, correct for loss of one electron

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`

    OUPTUT: the exact mass of `vec_frag`
    """
    return get_mass_not_ionised(vec_frag) - electron_mass


def get_mass_ionized(vec_frag: FragmentAsAtomCounts, ionization_number: int) -> float:
    """Exact mass of a fragment, correct for loss of electrons

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`
    - ``ionization_number``: the number of electrons lost

    OUPTUT: the exact mass of `vec_frag`
    """
    return (get_mass_not_ionised(vec_frag) - ionization_number * electron_mass) / max(
        ionization_number, 1
    )


def get_mass_at_indices(vec_frag: FragmentAsAtomCounts, indices: list) -> float:
    """Exact mass of the fragment

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`
    - ``indices``: a list of all the indices to consider for computing the mass

    OUPTUT: the exact mass of `vec_frag` considering only the given indices
    """
    return float(sum([float(vec_frag[i]) * mass[i] for i in indices]))


def get_mass_at_indices_single_ionised(
    vec_frag: FragmentAsAtomCounts, indices: list
) -> float:
    """Exact mass of the fragment

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`
    - ``indices``: a list of all the indices to consider for computing the mass

    OUPTUT: the exact mass of `vec_frag` considering only the given indices
    """
    return float(sum([float(vec_frag[i]) * mass[i] for i in indices]) - electron_mass)


def get_mass_at_indices_double_ionised(
    vec_frag: FragmentAsAtomCounts, indices: list
) -> float:
    """Exact mass of the fragment

    INPUT:
    - ``vec_frag``: a list of positive integers of length `len_frag`
    - ``indices``: a list of all the indices to consider for computing the mass

    OUPTUT: the exact mass of `vec_frag` considering only the given indices
    """
    return float(
        sum([float(vec_frag[i]) * mass[i] for i in indices])
        - electron_mass * float(2.0)
    ) / float(2.0)


def get_DBE_value(fragment: FragmentAsAtomCounts) -> float:
    """Double-Bond-Equivalent (DBE) formula

    INPUT:
    - ``fragment``: a list of positive integers of length `len_frag`

    OUPTUT: the DBE value of the molecule `vec_frag`

    cf. p. 254 in Mass spectrometry, a textbook by Juergen H. Gross, 2007, eq. (6.9)
    'double bound equivalent': calculate number of rings or double bounds in a formula
    """
    free_bond = float(2.0)
    for i in range(len(fragment)):
        free_bond += fragment[i] * (valence[i] - 2)
    # to actually calculate and return DBE:
    free_bond /= float(2.0)
    return free_bond


def get_DBE_value_at_indices(fragment: FragmentAsAtomCounts, indices: list) -> float:
    """Double-Bond-Equivalent (DBE) formula

    INPUT:
    - ``fragment``: a list of positive integers of length `len_frag`
    - ``indices``: a list of all the indices to consider for computing the DBE

    OUPTUT: the DBE value of the molecule `vec_frag` at indices

    cf. p. 254 in Mass spectrometry, a textbook by Juergen H. Gross, 2007, eq. (6.9)
    'double bound equivalent': calculate number of rings or double bounds in a formula
    """
    free_bond = float(2.0)
    for i in indices:
        free_bond += fragment[i] * (valence[i] - 2)
    # to actually calculate and return DBE:
    free_bond /= float(2.0)
    return free_bond


def get_DBE_value2(fragment: FragmentAsAtomCounts) -> int:
    """Double-Bond-Equivalent (DBE) formula (times 2)

    INPUT:
    - ``fragment``: a list of positive integers of length `len_frag`

    OUPTUT: 2*(the DBE value of the molecule `vec_frag`)

    cf. p. 254 in Mass spectrometry, a textbook by Juergen H. Gross, 2007, eq. (6.9)
    'double bound equivalent': calculate number of rings or double bounds in a formula
    """
    return 2 + sum([fragment[i] * (valence[i] - 2) for i in range(len(fragment))])


def get_DBE_value2_at_indices(fragment: FragmentAsAtomCounts, indices: list) -> int:
    """Double-Bond-Equivalent (DBE) formula (times 2)

    INPUT:
    - ``fragment``: a list of positive integers of length `len_frag`
    - ``indices``: a list of all the indices to consider for computing the DBE

    OUPTUT: 2*(the DBE value of the molecule `vec_frag`)

    cf. p. 254 in Mass spectrometry, a textbook by Juergen H. Gross, 2007, eq. (6.9)
    'double bound equivalent': calculate number of rings or double bounds in a formula
    """
    return 2 + sum([fragment[i] * (valence[i] - 2) for i in indices])


# an old comment:
#    This function does two calculations to estimate
#    if the candidate formula is chemically possible or not.
#    If the formula is chemically possible,
#    it calculates the DBE value (float number) and returns it.
#    If the formula is impossible, it returns (-1) which causes later on
#    the candidate formula to be eliminated.
#    First test: (i) use all multivalent (V>1) atoms to form a chain
#    and calculate number of remaining free slots to bind another atom.
#    (ii) Calculate number of monovalent atoms (V = 1) and then assign DBE = -1
#    if number of monovalent > free slots for atoms (i.e., all monovelents
#    cannot be bound to a unique chain).
#    Second test: DBE: cf. p. 254 in Mass spectrometry, a textbook by Juergen H. Gross, 2007, eq. (6.9)
#    'double bound equivalent': calculate number of rings or double bounds in a formula
#    Note 20190821: using DBE < 0 as criteria for elimination of the candidate is not enough
#    because e.g. in H2F2, DBE > 0 but chemically this is impossible.
#
#    For compound identification (Gross p. 254):
#    the DBE algorithm produces integers for odd-electron ions and molecules
#    and non-integers for even-electron ions (that have to rounded to the next lower integer)
#    The molecular ion (if present) has to be an odd-electron ion, with integer DBE value.


def get_total_valence(fragment: FragmentAsAtomCounts) -> int:
    """
    Compute the total valence of a given fragment.
    This is used to evaluate if a fragment can be a molecular ion or not.

    Parameters
    ----------
    fragment : list
        a list of positive integers of length `len_frag`.

    Returns
    -------
    int
        Value of the total valence of the fragment.

    """
    total_valence = sum([valence[i] * fragment[i] for i in range(len(fragment))])
    return total_valence


def get_max_valence(fragment: FragmentAsAtomCounts) -> int:
    """
    Compute the maximum valence of a given fragment,
    or the valence of the chemical element having maximum valence.
    This is used to evaluate if a fragment can be a molecular ion or not.

    Parameters
    ----------
    fragment : list
        a list of positive integers of length `len_frag`.

    Returns
    -------
    int
        Value of the maximum valence of the fragment.

    """
    max_valence = max([valence[i] for i in range(len(fragment)) if fragment[i] > 0])
    return max_valence


def test_SENIOR_rule_ii(fragment: FragmentAsAtomCounts) -> bool:
    """test the SENIOR theorem, item (ii),
    as described in Kind and Fiehn 2007:
        The sum of valences is greater than or equal to twice the
        maximum valence.
    This prevents fragments such as CFCl to be considered as
    valid molecular ion."""

    test_SENIOR_rule = sum(
        [valence[i] * fragment[i] for i in range(len(fragment))]
    ) >= 2 * max([valence[i] for i in range(len(fragment)) if fragment[i] > 0])
    return test_SENIOR_rule


def get_fragment_from_string_formula(string: str) -> FragmentAsAtomCounts:
    """The standardised string formula of a fragment"""
    # Remove everything that is a '+' and after the '+'
    string = string.split("+")[0]
    # a code is a upper case letter + maybe a lower case letter + a number
    i = 0
    elements = []
    vec = [0] * len_frag
    while i < len(string):
        c = string[i]
        i += 1
        if c == "[":
            # get number
            c += string[i]
            i += 1
            while string[i].isnumeric():
                c += string[i]
                i += 1
            # get upper case letter
            c += string[i]
            i += 1
            while string[i].islower():
                c += string[i]
                i += 1
            # should be ']'
            assert string[i] == "]"
            c += string[i]
            i += 1
        else:
            while i < len(string) and string[i].islower():
                c += string[i]
                i += 1
        n = ""
        while i < len(string) and string[i].isnumeric():
            n += string[i]
            i += 1
        if len(n) > 0:
            I = int(n)
        else:
            I = 1
        elements.append((c, I))
        vec[dict_element_idx[c]] += I
    # logging.info(elements)
    return vec


def read_formula_pattern(formula: str) -> list:
    """
    This function:
        handles parenthesis if any, for example (CF3)3 becomes CF3CF3CF3, using "pattern",
        then identify atom + number, as many times as needed, using "pattern2".
        It basically cuts the string into element pieces.

    INPUT:
    - ``formula``: a string formula with possible isotopologues (with brakets)

    OUTPUT: a list of atoms
    """
    pattern = r"(\()([\[\]\w]*)(\))(\d*)"
    all_matches = re.findall(pattern, formula)

    while all_matches:
        for match in all_matches:
            count = match[3]
            text = ""
            if count == "":
                count = 1
            else:
                count = int(match[3])
            while count >= 1:
                text = text + match[1]
                count -= 1
            formula = formula.replace("(" + match[1] + ")" + match[3], text)

    # at this point, we end up with no parenthesis, but still brackets from isotopologues.
    # we cannot remove the brackets right now otherwise we end up mixing up isotope with numbers.
    # pattern2 = "\[?(\d*[A-Z]{1}[a-z]?)\]?(\d*)"
    pattern2 = r"(\[?\d*[A-Z]{1}[a-z]?\]?)(\d*)"  # do not strip the brackets here!
    all_matches2 = re.findall(pattern2, formula)

    return all_matches2


def nice_formula_from_frag_formula(text_formula):
    """


    Parameters
    ----------
    text_formula : TYPE string.
        format: chemical formula written from a fragment. No parenthesis. Isotopes may be present.

    Returns
    -------
    nice_text: tring. Formula written in a Latex compatible format,
    with subscript and super script as should be.
    Use this as text string as nice label on graph with Mathplotlib.

    """

    pattern = r"(\[?)(\d{0,3})([A-Z]{1}[a-z]{0,1})(\]?)(\d*)"
    text = re.findall(pattern, text_formula)
    nice_text = ""
    for match in text:
        if match[0] == "[":
            # this is an isotope notation as [37Cl]
            # for match in all_matches:
            nice_text += match[0] + "$^{" + match[1] + "}$" + match[2] + match[3]
        else:
            nice_text += match[2]  # +
        if match[4] != "":
            nice_text += "$_{" + match[4] + "}$"

    text_formula = nice_text

    return nice_text


def formula_to_mass(chem_formula: str, ionized: bool = False) -> float:
    """return the exact, ionized mass (minus one electron)

    INPUT:
    - ``chem_formula``: a chemical formula (string) corresponding to the
      formula of a fragment
    - ``ionized``: a boolean indicating if the formula is ionized or not
        By default assume single ionization

    OUTPUT: the mass of the chemical formula
    """
    frag_as_list = get_fragment_from_string_formula(chem_formula)
    ionization_level = get_ionization_value(chem_formula)
    if ionization_level == 0 and ionized:
        ionization_level = 1

    return get_mass_ionized(frag_as_list, ionization_level)


def formula_to_frag(chem_formula: str) -> FragmentAsAtomCounts:
    """return the fragment (a list of positive integers)

    INPUT:
    - ``chem_formula``: a chemical formula (string) corresponding to the
      formula of a fragment

    OUTPUT: the fragment as a list of integers
    """
    all_matches = read_formula_pattern(chem_formula)
    frag = [0] * len_frag
    for match in all_matches:
        # Search symbol
        symbol = match[0]
        # Search numbers
        number = match[1]
        logging.debug(f"{symbol},{number}")
        if number == "":
            number = 1
        else:
            number = int(match[1])
        frag[dict_element_idx[symbol]] += number
    return frag


def formula_to_e_list(chem_formula: str) -> list[str]:
    """return the list of present elements (atoms)

    INPUT:
    - ``chem_formula``: a chemical formula (string) corresponding to the
      formula of a fragment

    OUTPUT:
    This function is based on the read_formula_pattern function
    """
    e_list = []
    all_matches = read_formula_pattern(chem_formula)
    for match in all_matches:
        # Search symbol
        symbol = match[0]
        # logging.info(symbol)
        # if symbol not in e_list: ????? TOCHECK MYRIAM
        if symbol != [e_list[i] for i in range(len(e_list))]:
            e_list.append(symbol)
    return e_list
