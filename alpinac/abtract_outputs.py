"""Ouput saving for the algorithm."""
from __future__ import annotations

from abc import abstractmethod
from datetime import datetime
import logging
from pathlib import Path
import pathlib
import sys
from typing import TYPE_CHECKING

import alpinac

if TYPE_CHECKING:
    import matplotlib.pyplot as plt


class OutputObject:
    """Any object that can be saved.

    You will have to implement the save method.
    """

    logger: logging.Logger

    def __init__(self) -> None:
        self.logger = logging.getLogger(f"alpinac.OutputObject.{type(self).__name__}")

    @abstractmethod
    def save(self, dir: Path):
        """Save as a file in the current dictionary."""
        ...

    # @abstractmethod
    # def print(self):
    #    """Print the output in a terminal."""
    #    ...


class OutputSaverLog(OutputObject):
    """Log for the output saver"""

    creation_datetime: datetime
    saving_datetime: datetime

    def __init__(self) -> None:
        super().__init__()
        self.creation_datetime = datetime.now()

    def save(self, dir: Path):
        self.saving_datetime = datetime.now()
        with open(dir / "outputsaver.log", "w") as text_file:
            text_file.write(
                "\n".join(
                    [
                        "Log file for the output saver",
                        f"{self.creation_datetime = }",
                        f"{self.saving_datetime = }",
                    ]
                )
            )


class PackagesVersionSaver(OutputObject):
    """Save the version of all python packages."""

    def save(self, dir: Path):
        p = dir.parent / "packages.VERSION"
        if p.is_file():
            return
        with open(p, "w") as text_file:
            import pkg_resources

            for p in pkg_resources.working_set:

                text_file.write(
                    "\t".join(
                        [
                            p.project_name,
                            p.version,
                            p.egg_name(),
                            # p.location,
                        ]
                    )
                )
                text_file.write("\n")


class PyVersionSaver(OutputObject):
    """Save the version of python"""

    def save(self, dir: Path):
        p = dir.parent / "python.VERSION"
        if p.is_file():
            return
        with open(p, "w") as text_file:
            text_file.write(sys.version)


class AlpinacVersionSaver(OutputObject):
    """Save the version of alpinac. Git information if repo is used."""

    def save(self, dir: Path):
        p = dir.parent / "alpinac.VERSION"
        if p.is_file():
            return
        with open(p, "w") as text_file:
            print(alpinac.__path__, file=text_file)
            git_dir = pathlib.Path(*alpinac.__path__) / ".." / ".git"
            with (git_dir / "HEAD").open("r") as head:
                ref = head.readline().split(" ")[-1].strip()

            with (git_dir / ref).open("r") as git_hash:
                print("last commit", git_hash.readline().strip(), file=text_file)


class OutputSaver(OutputObject):
    """Abstract output saver.

    You can add any otput object to the objects list.
    Then call the save function to save in the requested directory.
    """

    objects: list[OutputObject]

    log: OutputSaverLog

    def __init__(self) -> None:
        super().__init__()
        self.objects = []
        self.log = OutputSaverLog()
        self.objects.append(self.log)

        self.objects.extend(
            [
                PackagesVersionSaver(),
                PyVersionSaver(),
                AlpinacVersionSaver(),
            ]
        )

    def _add_attribute_to_objects(self, attribute_name: str):
        # Add the attributes of the saver to its objects to saved
        # Check that the attribute exist and that it is not already in objects
        if (
            hasattr(self, attribute_name)
            and not getattr(self, attribute_name) in self.objects
        ):
            self.objects.append(getattr(self, attribute_name))

    def save(self, dir: Path | str):
        """Save the outputs to files."""
        dir = Path(dir)

        if not dir.exists():
            dir.mkdir(parents=True)
        if dir.is_file():
            raise TypeError(f"{dir} is file not directory.")

        for object in self.objects:
            try:
                object.save(dir)
            except Exception as e:
                self.logger.exception(f"Could not save {object} becase of {e}.")
        self.logger.info(f"saved to {dir}")


class PlotOutput(OutputObject):
    """Output for a plot."""

    fig: plt.Figure

    def __init__(self, fig: plt.Figure, plot_name: str) -> None:
        super().__init__()
        self.plot_name = plot_name
        self.fig = fig

    def save(self, dir: Path, format: str = ".png"):
        fig_file = dir / f"{self.plot_name}{format}"
        if self.fig is None:
            self.logger.warning(f"Cannot save plot {self.plot_name}.")
            return
        self.fig.savefig(fig_file)
        self.logger.info(f"saved plot {self.plot_name} to {dir}")


if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.INFO)
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    ax.plot([1,2,3], [1,2,3])
    vs = PlotOutput(fig, "test_plot" )
    vs.save(Path(r"tests\outputsaver"))
