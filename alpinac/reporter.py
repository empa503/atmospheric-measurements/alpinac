"""
A module used to run multiple files with Alpinac.

Together with the usual output that Alpinac provides, this
module generates a DataFrame with the main information for
each file. The DataFrame can be written to a csv and xlsx
files if wanted. The xlsx file has some conditional
formatting (e.g. some cell's background goes from red to green
depending on how good the result is, if there were some
problems with the execution of Alpinac there will be some
color highlighting them).

The sheet "main_result" contains all cases with at least
5 peaks or that had a problem.
The sheet "result_by_group" contains the following information
per group: averaged grade, averaged grade divided by maximal
grade, the number of graded files (i.e. the number of files
which did not had any Error or Exception from alpinac), the
number of files that we run, and the percentage of files
which scored a maximal grade.
In the sheet "result_by_file" we can find for each file one
compound which "represent" the file. If the expected formula
is given, we take the compound which has the most likely
formula with the smallest difference of atoms with the
expected formula. Otherwise, we take the compound with the
most likely formula.
For each group, we find a sheet containing all results of
the group.

Methods:
    generate
        Creates a json file listing the cases that we want to
        investigate.
    investigate
        Runs the cases from a json file, reads the Alpinac
        output for each cases, writes a csv and an xlsx
        file if wanted and returns a DataFrame with the
        most important information from the freshly run
        files.
    run
        Runs Alpinac's `make_identification_from_file` for
        the cases in the given json file.
    read
        Reads the output of Alpinac of the cases in the 
        given json file and creates a DataFrame.
"""

import argparse
import glob
import json
import logging
import os
import re
import sys
from functools import reduce
from multiprocessing import Pool
from pathlib import Path

import numpy as np
import openpyxl
import pandas as pd

import alpinac.periodic_table as chem_data
from alpinac.cli.utils import (
    add_verbose_debug_arguments,
    process_verbose_debug_arguments,
)
from alpinac.mode_identification.main_from_file import make_identification_from_file

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger("alpinac.reporter")
# only log really bad events
logger.setLevel(logging.INFO)
# you should install openpyxl and func_timeout with pip

columns = [  # Columns's names of the csv file and
    "file",  # DataFrame that are generated.
    "compound",
    "mean_RT",
    "is_expected_formula_present",
    "expected_formula",
    "likely_mol_ion_formula",
    "likelihood",
    "perc_explained_signal",
    "fraction_identified_fragments",
    "mol_ion_DBE",
    "ionization_mode",
    "mol_ion_formulae",
    "mol_ion_DBEs",
    "mol_ion_likelihoods",
    "ionization_modes",
    "step1_knapsack",
    "step2_directedgraph",
    "step3_initialize_isotopologues",
    "step5to8_optimization",
    "total",
    "difference_number_of_atoms",
    "most_likely_fragment_formula",
    "fragment_likelihood",
    "fragment_ionization_mode",
    "most_likely_fragment_DBE",
    "most_likely_fragment_molecular_ion",
    "fragment_formulae",
    "fragment_likelihoods",
    "fragment_DBEs",
    "fragment_molecular_ions",
    "fragment_ionization_modes",
    "group",
]


def generate(
    file_pattern_to_folder_path: dict[str, os.PathLike],
    reporter_input_path: os.PathLike,
):
    """Generates a json file used by reporter.investigate.

    The final json contains the information of the compound,
    taken from the  file substances.csv, identification
    kwagrs (to run Alpinac), the expected formula, if to
    run file, if to force run (if true, each time that
    investigate is called, Alpinac will identify), the
    group and the path to the file to run.

    Args:
        file_pattern_to_folder_path (dict[str, os.PathLike]):
            Dictionary with patterns to match files as keys and
            paths to the directories where the files are stored as
            values of the dictionary. The patterns are the same as
            for `Path.glob`.
            If we expect a substance to be present, we can
            put `_{compound}_` or `_{compound}.` in the pattern
            and put in the files' name a valid compound name or alias
            found in `substances.csv`. In this way, in the final
            report there will be the expected molecular formula
            of the substance and it will be possible to see how
            good our guess was.
            If `_{group}_` or `_{group}.` is present in the pattern,
            then the files matching the pattern will be grouped
            together in the final xlsx file. It is also possible
            to have `{group}\\filePattern : path\\to\\folder` as pair.
            If there is a folder `group1`, the above pair would group
            together all files in the directory `path\\to\\folder\\group1`.
            If the placeholder `{group}` is not present, then the
            result of the files matching the pattern will be grouped
            on a generic group `no_group`.

        reporter_input_path (os.PathLike):
            Path to the final json file.

    Raises:
        ValueError:
            If an empty dictionary is passed.
        FileExistsError:
            If the path to the reporter input exists.
        FileNotFoundError:
            If one of the folder is empty or no file in the folder
            matched the given patterns.
        NotADirectoryError:
            If one path in the dictionary is not a directory.

    Examples:
        The following are examples of accepted patterns,
        with corresponding examples of file names matching
        to the pattern:

        * The file name `fragments_HFC-41_jungfrau.txt`
          matches to to the pattern
          ``*_{compound}_{group}.txt``
          with ``compound = HFC-41``,`` group = jungfrau``;
        * the file name `frags_20ppmOfError_HFC-112.csv`,
          matches to to the pattern
          ``frags_{group}_{compound}.{suffix}``
          with ``compound = HFC-112``, ``group = 20ppmOfError``,
          ``suffix = csv``;
        * the file names `frags_EDI2003_PFC-116_1233432.txt`,
          `frags_EDI2003_PFC-116_1233432.csv`,
          matches to to the pattern
          ``frags_{group}_{compound}_{random_nr}.{suffix}``
          with ``compound = PFC-116``, ``group = EDI2003``, ``suffix = txt``
          and ``suffix = csv``, ``random_nr = 1233432``;
        * the file name `frags_PFC-114_42.csv`
          in the folder `NAME_OF_THE_FOLDER` matches to to
          the pattern
          ``{group}\\frags_{compound}_{random_nr}.{suffix}``
          with ``compound = PFC-114``, ``group = NAME_OF_THE_FOLDER``,
          ``suffix = csv``, ``random_nr = 42``;
        * the file name `frags_quandoBiondaAurora_3290.txt`,
          matches to to the pattern
          ``frags_{random}_{random_nr}.{suffix}``
          with ``random = quandoBiondaAurora``, ``random_nr = 3290``,
          ``suffix = txt``.
    """
    logger.info("generate")
    placeholder_regex = re.compile(r"\{([^\}]*)\}")
    reporter_input_path = Path(reporter_input_path)
    file_pattern_to_folder_path = {
        pattern: Path(path) for pattern, path in file_pattern_to_folder_path.items()
    }

    if len(file_pattern_to_folder_path) == 0:
        raise ValueError(
            "An empty dictionary of file patterns to folder paths was passed."
        )
    if reporter_input_path.is_file():
        raise FileExistsError(
            f"There is already a json file with path {reporter_input_path}."
        )
    for folder in file_pattern_to_folder_path.values():
        if not folder.is_dir():
            raise NotADirectoryError(f"The path {folder} is not a directory.")

    folder_info = []
    # for each folder, contains a list of information about each file (as dictionary)
    for pattern, path in file_pattern_to_folder_path.items():
        file_info = {
            "original_pattern": pattern,
            "pattern_re": placeholder_regex.sub(
                "([0-9A-Za-z\\-,]+)", _replace_pattern_suffix(pattern)
            )
            .replace(".", "\\.")
            .replace("*", ".*"),
            "pattern_fn": placeholder_regex.sub("*", pattern.split("\\").pop())
            .replace("txt", "*")
            .replace("csv", "*")
            .replace("jx", "*"),
            # take the pattern for the files' names and replace the suffix with *
            "placeholders": placeholder_regex.findall(pattern),
            "path": Path(path),
        }
        file_info["folders"] = [file_info["path"]]
        folder_pattern = placeholder_regex.sub(
            "*", file_info["original_pattern"].split("\\")[0]
        )

        if not "group" in pattern.split("\\").pop() and "group" in pattern:
            # the group is not in the name of the files (maybe
            # it is in the folder or it is not present in the pattern)
            file_info["folders"] = [
                folder_path
                for folder_path in file_info["path"].glob(folder_pattern)
                if folder_path.is_dir()
            ]

        for folder in file_info["folders"]:
            values_in_file_regex = re.compile(file_info["pattern_re"])
            matched_paths = [
                p
                for p in folder.glob(file_info["pattern_fn"])
                if values_in_file_regex.match(p.name)
            ]
            # sometimes the glob method matches files that we don't want
            # with the above we remove these files
            files_info = [_format_file_info(file_info, path) for path in matched_paths]
            folder_info.append(files_info)

    name_of_compounds = {
        data["compound"].split(".")[
            0
        ]  # sometimes we have "compound.something", in this way we take only compound
        for files_info in folder_info
        for data in files_info
        if "compound" in data.keys()
    }
    additional_information_of_compound = _get_additional_info_of_compound(
        name_of_compounds
    )

    cases_to_report = _format_cases_to_report(
        folder_info,
        additional_information_of_compound,
    )
    with open(
        reporter_input_path,
        "w",
    ) as outfile:
        json.dump(cases_to_report, outfile, indent=4)
    logger.info("Finished to generate")


def _replace_pattern_suffix(pattern: str):
    """
    Replace the suffixes txt, csv and jx of a pattern
    with a regex which matches with txt, csv and jx.

    """
    file_pattern = pattern.split("\\").pop()
    suffix_re = re.compile("(txt)|(csv)|jx")
    file_pattern = file_pattern[:-3] + suffix_re.sub("[txtcsvj]+", file_pattern[-3:])
    return file_pattern


def investigate(
    reporter_input_path: os.PathLike,
    reportage_path: os.PathLike = "empty_path",
    timeout_time: int = 600,
    write: bool = True,
    processes: int = 4,
):
    """
    Create a reportage for the cases in a json file, using
    Alpinac's method `make_identification_from_file`.

    If the paths in the json file are not not valid (i.e. there
    is no file in with such paths), they will be ignored.
    The files will be identified using Alpinac if they are
    not already identified. The information for the report are
    taken from the output folders of Alpinac.

    By default, the DataFrame is written to a csv and an xlsx
    files with the same path as the json file. If the parameter
    write is False, the DataFrame is not written.

    Args:
        reporter_input_path (os.PathLike):
            Path to the json file with the cases, which are going
            to be reported.
        reportage_path (os.PathLike, optional):
            Path to where we want to write the reportage.csv file
            (the xlsx file is written in the same folder). By
            default the folder is the same as the json file with
            path `reporter_input_path`.
        timeout_time (int, default 600):
            Time limit in seconds for a compound to be run. If the
            identification of a compound exceeds this limit, the
            identification is stopped and the next compound will be
            identified.
        write (bool, default True):
            Write csv and xlsx if true.
        processes (int, default 4):
            Number of worker processes.

    Returns:
        pd.DataFrame
            DataFrame containing the most important information
            from the Alpinac's result.
    """
    reporter_input_path = Path(reporter_input_path)
    if not (reporter_input_path.is_file() and reporter_input_path.suffix == ".json"):
        raise ValueError(f"There is no json file with path {reporter_input_path}")

    reportage_path = Path(reportage_path)
    if reportage_path == Path("empty_path"):
        reportage_path = reporter_input_path.with_name("reportage.csv")

    run(reporter_input_path, reportage_path, timeout_time, processes)

    df = read(reporter_input_path, reportage_path)

    if len(df) > 0:
        header = not reportage_path.is_file()
        # if the the file exists, then we don't need the header
        df.to_csv(reportage_path, mode="a", header=header)

    _to_xlsx(reportage_path, write)
    logger.info("Finished to investigate")
    if reportage_path.is_file():
        logger.info(
            f"You can find your xlsx file at {reportage_path.with_suffix('.xlsx')}"
        )
    return df


def run(
    input_path: os.PathLike,
    reportage_path: os.PathLike = "empty_path",
    timeout_time: int = 600,
    processes: int = 4,
):
    """
    Identify files with paths in the json file.

    Only if the file name is not present in the csv (file with
    path `reportage_path`) or it is forced to run, the file
    will be identified.

    Args:
        input_path (os.PathLike):
            Path to the json file with the cases to be run.
        reportage_path (os.PathLike, optional):
            Path to where we want to write the `reportage.csv` file
            (the xlsx file is written in the same folder). By
            default the folder is the same as the json file with
            path `reporter_input_path`.
        timeout_time (int, default 600):
            Time limit in seconds for a file to be run. If the
            identification of a file exceeds this limit, the
            identification is stopped and the next file will be
            identified.
        processes (int, default 4):
            Number of worker processes.
    """
    input_path = Path(input_path)
    reportage_path = Path(reportage_path)
    if reportage_path == Path("empty_path"):
        reportage_path = input_path.with_name("reportage.csv")
    cases_to_identify = _get_cases_to_identify(input_path, reportage_path)
    _identify(cases_to_identify, timeout_time, processes)


def read(reporter_input_path: os.PathLike, reportage_path: os.PathLike):
    """
    Generate a DataFrame from the Alpinac output files.

    All cases present in the json file with path
    `reporter_input_path` that were forced to be run or have
    not been saved in the reportage with path
    `reportage_path` are inserted in the DataFrame.

    Args:
        reporter_input_path (os.PathLike):
            Path to the json file with the cases, which are going
            to be reported.
        reportage_path (os.PathLike):
            Path to the csv file.

    Returns:
        pd.DataFrame
            DataFrame containing the most important information
            from the freshly Alpinac-run result.
    """
    logger.info("read")
    df = pd.DataFrame(columns=columns)
    cases_to_read = _get_cases_to_read(reporter_input_path, reportage_path)

    for data in cases_to_read:
        alpinac_result_path = Path(data["file"]).with_suffix("")
        compounds_result_paths = list(
            alpinac_result_path.glob("Compound_*")
        )  # find all compound's folders
        expected_formula = data["expected_formula"]
        file_name = Path(data["file"]).name

        if len(compounds_result_paths) == 0:
            logger.info(f"Empty folder and no compounds: {alpinac_result_path}")
            df.loc[len(df)] = (
                [
                    file_name,
                    "NaN",
                    "NaN",
                    "not_found",
                    expected_formula,
                    "empty_folder_no_compounds",
                ]
                + ["NaN"] * (len(df.columns) - 7)
                + [data["group"]]
            )
        for p in compounds_result_paths:
            compound_nr = p.stem.removeprefix("Compound_")
            if _is_folder_empty(p):  # is there any folder?
                logger.info(f"Empty folder: {p}")
                df.loc[len(df)] = (
                    [
                        file_name,
                        compound_nr,
                        "NaN",
                        "not_found",
                        expected_formula,
                        "empty_folder",
                    ]
                    + ["NaN"] * (len(df.columns) - 7)
                    + [data["group"]]
                )
                continue
            if _did_alpinac_take_too_long(
                p
            ):  # was the process taking too much time to be run?
                logger.info(f"Took too much time: {p}")
                df.loc[len(df)] = (
                    [
                        file_name,
                        compound_nr,
                        "NaN",
                        "not_found",
                        expected_formula,
                        "identification_took_too_much",
                    ]
                    + ["NaN"] * 13
                    + [
                        (p / "took_too_much.txt").read_text().split().pop()
                    ]  # the timeout (in seconds) is at the end of file took_too_much.txt
                    + ["NaN"] * (len(df.columns) - 21)
                    + [data["group"]]
                )
                continue
            if _are_files_not_present(p):  # are the file that we need present?
                logger.info(
                    f"There was an error during the identification for compound {compound_nr}: {p}"
                )
                df.loc[len(df)] = (
                    [
                        file_name,
                        compound_nr,
                        "NaN",
                        "not_found",
                        expected_formula,
                        "alpinac_error",
                    ]
                    + ["NaN"] * (len(df.columns) - 7)
                    + [data["group"]]
                )
                continue
            peaks = pd.read_csv(p / "peaks.txt", sep=r"\s+")
            most_likely_mol_ions = _read_most_likely_mol_ions(p)
            most_likely_mol_ions["spectrum_id"] = most_likely_mol_ions[
                "spectrum_id"
            ].apply(
                _translate_spectrum_id
            )  # in the file we have 0/1 instead of CI/EI

            (
                most_likely_mol_ion_formula,
                most_likely_mol_ion_DBE,
                most_likely_mol_ion_likelihood,
            ) = most_likely_mol_ions[["formula", "DBE", "likelihood"]].iloc[0]
            most_likely_mol_ion_ionization_mode = (
                _get_most_likely_mol_ion_ionization_mode(
                    most_likely_mol_ions, most_likely_mol_ion_formula
                )
            )
            mol_ions_data = _format_data_about_mol_ions(
                most_likely_mol_ions, most_likely_mol_ion_formula
            )

            (
                largest_detected_fragment_series,
                all_fragments,
            ) = _transform_mygu_to_dfs(p)
            merged_largest_detected_fragment_series = (
                largest_detected_fragment_series.merge(
                    all_fragments, left_on="formula", right_on="formula"
                )
            )
            grouped = merged_largest_detected_fragment_series.groupby(by="formula")
            largest_detected_fragment_series["ionization_mode"] = [
                f'({"/".join(group[1]["ionization_mode"].to_list())})'
                for group in grouped
            ] + ["NaN"] * (len(largest_detected_fragment_series) - grouped.ngroups)

            (
                most_likely_fragment_information  # list storing "formula", "likelihood", "ionization_mode", "DBE", "molecular_ion" for the most likely fragment
            ) = largest_detected_fragment_series.iloc[0][
                ["formula", "likelihood", "ionization_mode", "DBE", "molecular_ion"]
            ].to_list()

            fragments_data = _format_less_likely_fragments(
                largest_detected_fragment_series
            )

            mean_RT = all_fragments["rt"].mean()

            perc_explained_signal = _get_percentage_explained_signal(peaks)

            identified_fragments = all_fragments[
                all_fragments["formula"] != "unidentified"
            ]

            if len(identified_fragments) == 0:
                most_likely_fragment_information[0] = (
                    "unidentified"  # most_likely_fragment_formula is set to unidentified if there are no identified fragments
                )
                most_likely_mol_ion_formula = "unidentified"

            fraction_identified_fragments = _get_fraction_identified_fragments(
                peaks, identified_fragments
            )
            times = _get_how_long_it_took_to_run_steps(p)
            difference_number_of_atoms = _count_different_atoms(
                expected_formula, most_likely_mol_ion_formula
            )
            df.loc[len(df)] = (
                [
                    file_name,
                    compound_nr,
                    mean_RT,
                    _is_expected_formula_present(
                        expected_formula, most_likely_mol_ions
                    ),
                    expected_formula,
                    most_likely_mol_ion_formula,
                    most_likely_mol_ion_likelihood,
                    perc_explained_signal,
                    fraction_identified_fragments,
                    most_likely_mol_ion_DBE,
                    most_likely_mol_ion_ionization_mode,
                ]
                + mol_ions_data
                + times
                + [
                    difference_number_of_atoms,
                ]
                + most_likely_fragment_information
                + fragments_data
                + [
                    data["group"],
                ]
            )
    logger.info("Finished to read")
    return df


def _get_percentage_explained_signal(peaks: pd.DataFrame):
    """
    Calculate the percentage of the intensity of the
    identified peaks.

    Parameters
    ----------
    peaks : pd.DataFrame
        File "peaks.txt" as a DataFrame.

    Returns
    -------
    float
        Percentage of the intensity of the identified peaks
        2 digit rounded.
    """
    return round(
        100
        * sum(peaks[peaks["formulae"] != "unidentified"]["I"].to_list())
        / sum(peaks["I"].to_list()),
        2,
    )


def _get_fraction_identified_fragments(
    peaks: pd.DataFrame, identified_fragments: pd.DataFrame
):
    """
    Calculate and format the fraction of identified fragments'
    peaks to a string.

    Parameters
    ----------
    peaks : pd.DataFrame
        File "peaks.txt" as a DataFrame.
    identified_fragments : pd.DataFrame
        All identified fragments of the file "peaks.txt" as
        a DataFrame.

    Returns
    -------
    str
        Fraction of identified fragments' peaks.
    """
    number_of_identified_peaks = identified_fragments.groupby(
        by=["meas_mass", "tot_intensity"]
    ).ngroups

    number_of_meas_mass = len(peaks)
    fraction_identified_fragments = (
        f"{number_of_identified_peaks}/{number_of_meas_mass}"
    )

    return fraction_identified_fragments


def _get_most_likely_mol_ion_ionization_mode(
    most_likely_mol_ions: pd.DataFrame, most_likely_mol_ion_formula: pd.DataFrame
):
    """
    Format the ionization mode of the most likely formula.

    Sometimes there are more than one ionization mode, in
    this case each of them are separated by a backslash `/`.

    Parameters
    ----------
    most_likely_mol_ions : pd.DataFrame
        File "most_likely_mol_ions.txt" as a DataFrame.
    most_likely_mol_ion_formula : pd.DataFrame
        The formula in "most_likely_mol_ions.txt" with the
        greater likelihood.

    Returns
    -------
    str
        Ionization mode of the most likely molecular ion.
    """
    most_likely_mol_ion_ionization_mode = "/".join(
        most_likely_mol_ions[
            most_likely_mol_ions["formula"] == most_likely_mol_ion_formula
        ]["spectrum_id"].values
    )
    return most_likely_mol_ion_ionization_mode


def _format_less_likely_fragments(largest_detected_fragment_series: pd.DataFrame):
    """
    Format the data for the fragments that are not the most
    likely.

    A list containing the formatted strings of the formulae,
    likelihoods, DBEs, molecular ions and ionization modes is
    returned.
    When there are multiple fragments, the formulae of the
    fragments are joined into a single string separated by a
    backslash `/`. The same is done for the likelihoods, DBEs,
    molecular ions and ionization modes.

    Parameters
    ----------
    largest_detected_fragment_series : pd.DataFrame
        The top part of the file "results_file_mygu.txt" as a
        DataFrame.

    Returns
    -------
    list[str]
        Formatted strings of the formulae, likelihoods, DBEs,
        molecular ions and ionization modes.
    """
    less_largest_detected_fragment_series = largest_detected_fragment_series.iloc[1:, :]
    fragment_formulae = "/".join(
        less_largest_detected_fragment_series["formula"].to_list()
    )
    fragment_likelihoods = "/".join(
        less_largest_detected_fragment_series["likelihood"].map(str).to_list()
    )
    fragment_DBEs = "/".join(
        less_largest_detected_fragment_series["DBE"].map(str).to_list()
    )
    fragment_molecular_ions = "/".join(
        less_largest_detected_fragment_series["molecular_ion"].map(str).to_list()
    )
    fragment_ionization_modes = "/".join(
        less_largest_detected_fragment_series["ionization_mode"].to_list()
    )
    fragments_data = [
        fragment_formulae,
        fragment_likelihoods,
        fragment_DBEs,
        fragment_molecular_ions,
        fragment_ionization_modes,
    ]

    return fragments_data


def _did_alpinac_take_too_long(p: Path):
    """
    Check if the file "took_too_much.txt" is present in the
    given folder's path.
    """
    return (p / "took_too_much.txt").is_file()


def _is_folder_empty(p: Path):
    """
    Check if the folder is empty.
    """
    with os.scandir(p) as scanned_folders:
        folders = list(
            scanned_folders
        )  # if I don't use the with statement, I get "ResourceWarning: unclosed scandir iterator"
    return not any(folders)


def _are_files_not_present(p: Path):
    """
    Check if the folder contains all required files for the
    reportage.

    These are `peaks.txt`, `results_file_mygu.txt` and
    `most_likely_mol_ions.txt`.
    """
    return not (
        (p / "peaks.txt").is_file()
        and (p / "results_file_mygu.txt").is_file()
        and (p / "most_likely_mol_ions.txt").is_file()
    )


def _transform_mygu_to_dfs(mygu_path: Path):
    """
    Generate a tuple of DataFrames from `results_file_mygu.txt`.

    Two DataFrames are generated: one containing the largest
    detected fragment series and one containing all fragments.
    Until there are at least 2 largest detected fragment, rows
    whose cells are `NaN` are added to largest detected
    fragment series.

    Parameters
    ----------
    mygu_path : Path
        Path to the folder containing `results_file_mygu.txt`.

    Returns
    -------
    Tuple of pd.DataFrames
        Largest detected fragment series in
        `results_file_mygu.txt` as one DataFrame and all the
        fragments in `results_file_mygu.txt` as an other
        DataFrame.
    """
    results_mygu_path = mygu_path.joinpath("results_file_mygu.txt")
    with results_mygu_path.open() as f:
        lines_of_mygu_result = f.readlines()
    (
        first_delineator_index,
        second_delineator_index,
    ) = _find_delineator_lines_indexes(lines_of_mygu_result)
    largest_detected_fragment_series = pd.read_csv(
        results_mygu_path,
        sep=r"\s+",
        skiprows=[
            row_index
            for row_index in range(len(lines_of_mygu_result))
            if row_index <= first_delineator_index
            or second_delineator_index <= row_index
        ],
        names=["formula", "likelihood", "DBE", "molecular_ion"],
    )

    while len(largest_detected_fragment_series) < 2:
        largest_detected_fragment_series.loc[len(largest_detected_fragment_series)] = [
            "NaN"
        ] * len(largest_detected_fragment_series.columns)

    largest_detected_fragment_series["formula"] = largest_detected_fragment_series[
        "formula"
    ].map(lambda formula: formula.removesuffix(":"))

    all_fragments = pd.read_csv(
        results_mygu_path,
        sep=r"\s+",
        skiprows=list(range(second_delineator_index + 2)),
        names=[
            "perc_signal",
            "rt",
            "tot_intensity",
            "perc_tot_intensity",
            "meas_mass",
            "exact_mass",
            "exact_mass_u",
            "mass_diff",
            "formula",
            "DBE",
            "intensity",
            "intensity_fraction",
            "ionization_mode",
            "max_allowed_adduct",
            "spectra_name",
        ],
    )

    return largest_detected_fragment_series, all_fragments


def _find_delineator_lines_indexes(lines_result_path: list):
    """
    Find the lines' index which separates the largest detected
    fragment series and the rest of the file
    `results_file_mygu.txt`.

    Parameters
    ----------
    lines_result_path : list
        `results_file_mygu.txt` as a list of string.

    Returns
    -------
    Tuple of integers
        Indexes of the rows/lines which separates
        `results_file_mygu.txt`.
    """
    first_delineator = (
        "Formula                  Likelihood indicator"
        " (%) DBE                      Molecular ion?\n"
    )
    second_delineator = (
        "##########################################"
        "################################################\n"
    )
    first_delineator_index = lines_result_path.index(first_delineator)
    second_delineator_index = lines_result_path.index(
        second_delineator, first_delineator_index
    )

    return first_delineator_index, second_delineator_index


def _format_data_about_mol_ions(
    most_likely_mol_ions: pd.DataFrame, most_likely_mol_ion_formula: str
):
    """
    Format less likely molecular ion's data into a list of
    strings.

    Parameters
    ----------
    most_likely_mol_ions : pd.DataFrame
        The file `most_likely_mol_ions.txt` as a DataFrame.
    most_likely_mol_ion_formula : str
        Formula of the most likely molecular ion.

    Returns
    -------
    list[str]
        Strings of formulae, DBEs, likelihoods and ionization
        modes, divided by "/" and ordered from the most to the
        least likely. If Formula of the most likely molecular
        ion is `NaN`, a list of `NaN` strings is returned.
    """
    less_likely_mol_ions = most_likely_mol_ions.iloc[1:]
    mol_ion_formulae = "NaN"
    mol_ion_DBEs = "NaN"
    mol_ion_likelihoods = "NaN"
    mol_ion_ionization_modes = "NaN"

    if pd.notna(most_likely_mol_ion_formula):
        mol_ion_formulae = "/".join(less_likely_mol_ions["formula"].map(str).to_list())
        mol_ion_DBEs = "/".join(less_likely_mol_ions["DBE"].map(str).to_list())
        mol_ion_likelihoods = "/".join(
            less_likely_mol_ions["likelihood"].map(str).to_list()
        )
        mol_ion_ionization_modes = "/".join(
            less_likely_mol_ions["spectrum_id"].to_list()
        )

    return [
        mol_ion_formulae,
        mol_ion_DBEs,
        mol_ion_likelihoods,
        mol_ion_ionization_modes,
    ]


def _get_cases_to_read(reporter_input_path: str, reportage_path: str):
    """
    Generates a list of cases, whose Alpinac's output will be
    read.

    The paths from the json file (with or without regular
    expressions) will be match to existing paths of the Alpinac
    output.
    If the reportage path point to a existing csv file, the
    cases forced to run or not present in the csv file will
    be read. If the Alpinac folder of the case does not exist,
    the output will not be read.

    Parameters
    ----------
    reporter_input_path : str
        The path to the input json file, which stores the file
        paths to be reported.
    reportage_path : str
        Path to the csv file with the already run cases.

    Returns
    -------
    List
        Cases from the json to read the folders generated by Alpianc.
    """
    logger.info("_get_cases_to_read")
    files_in_reportage = []
    if Path(reportage_path).is_file():
        files_in_reportage = pd.read_csv(reportage_path)["file"].to_list()
    cases_to_read = json.load(open(reporter_input_path, "r"))
    cases_to_read = _match_regular_expressions_with_paths(cases_to_read)
    cases_to_read = [
        data
        for data in cases_to_read
        if Path(data["file"]).with_suffix("").is_dir()
        and (not Path(data["file"]).name in files_in_reportage or data["force_run"])
    ]
    logger.info("Finished to _get_cases_to_read")
    return cases_to_read


def _read_most_likely_mol_ions(folder_path: Path):
    """
    Generate a DataFrames from `most_likely_mol_ions.txt`.

    Until there are at least 2 molecular ions, rows whose cells
    are `NaN` are added to the DataFrame.

    Parameters
    ----------
    folder_path : Path
        Path to the folder containing
        `most_likely_mol_ions.txt`.

    Returns
    -------
    pd.DataFrame
        DataFrames from `most_likely_mol_ions.txt`, sorted by
        likelihood and with at least two row (which could
        contain only `NaN` cells).
    """
    most_likely_mol_ions = pd.read_csv(
        folder_path / "most_likely_mol_ions.txt",
        sep=r"\s+",
        header=None,
        skiprows=[0],
        names=[
            "spectrum_id",
            "max_adduct",
            "ranking",
            "formula",
            "DBE",
            "likelihood",
        ],
    )
    while len(most_likely_mol_ions) < 2:
        most_likely_mol_ions.loc[len(most_likely_mol_ions)] = [np.nan] * len(
            most_likely_mol_ions.columns
        )

    return most_likely_mol_ions.sort_values(
        "likelihood", ascending=False, ignore_index=True
    )


def _translate_spectrum_id(id):
    """
    Convert ionization type integer to string (CI or EI).

    Parameters
    ----------
    id : int
        Ionization type as a integer (0 or 1).

    Returns
    -------
    String
        Ionization type as a string (CI or EI) or a message
        string if id is not 0 nor 1.
    """
    if not id in {0, 1}:
        return "not_valid_ion_type"
    if id == chem_data.ionisation_CI:
        return "CI"
    return "EI"


def _is_expected_formula_present(
    expected_formula: str, most_likely_mol_ions: pd.DataFrame
):
    """
    Check at which row in `most_likely_mol_ions.txt`,
    sorted by likelihood, is the expected formula present.

    Parameters
    ----------
    expected_formula : str
        String of the expected formula.
    most_likely_mol_ions: pd.DataFrame
        String of the expected formula.

    Returns
    -------
    String
        A string of the form
        `found_at_{positions}({likelihoods})`, where
        `{positions}` are the indexes of the expected formula
        in the DataFrame most_likely_mol_ions separated by `/`
        and `{likelihoods}` are the likelihoods (also
        separated by `/`). If the expected formula is not
        present in most_likely_mol_ions, `not_found` is
        returned.
    """
    is_expected_formula_present = "not_found"

    if expected_formula in set(most_likely_mol_ions["formula"]):
        is_expected_formula_present = "found_at_{}({})".format(
            "/".join(
                most_likely_mol_ions[
                    most_likely_mol_ions["formula"] == expected_formula
                ]
                .index.map(str)
                .to_list()
            ),
            "/".join(
                most_likely_mol_ions[
                    most_likely_mol_ions["formula"] == expected_formula
                ]["likelihood"]
                .map(str)
                .to_list()
            ),
        )

    return is_expected_formula_present


def _get_how_long_it_took_to_run_steps(compound_path: Path):
    """
    Reads the times in `runtimes.log` of a compound.

    Parameters
    ----------
    compound_path : Path
        Path to the compound folder which has a `runtimes.log`.

    Returns
    -------
    List
        Strings of times to run step1, step2, step3, step3-5
        and total. If `runtimes.log` is not present one of
        step, then the value of tha step is `NaN`.
    """
    with compound_path.joinpath("runtimes.log").open() as f:
        runtime_log_lines = f.readlines()
    keys = [
        "step1_knapsack",
        "step2_directedgraph",
        "step3_initialize_isotopologues",
        "step5to8_optimization",
        "total",
    ]
    times = dict(zip(keys, ["NaN"] * len(keys)))  # setting all values to NaN

    times.update(
        dict([time.removesuffix("\n").split(": \t ") for time in runtime_log_lines[1:]])
    )
    return [
        times[key] for key in keys
    ]  # return an ordered list containing the times in order of the steps


def _count_different_atoms(expected_formula, most_likely_mol_ion_formula):
    """
    Calculate by how many atoms the expected formula and the
    most likely molecular ion formula differ.

    Parameters
    ----------
    expected_formula : str | pd.nan
        String of the expected formula.
    most_likely_mol_ion_formula : str | pd.nan
        String of the most likely molecular ion formula.

    Returns
    -------
    Int | str
        Number of atoms that the expected formula and the most
        likely molecular ion formula differ. If any of the two
        is `NaN`, a string "NaN" is returned.
    """
    difference_number_of_atoms = "NaN"
    if pd.notna(most_likely_mol_ion_formula) and pd.notna(expected_formula):
        difference_number_of_atoms = sum(
            [
                abs(
                    chem_data.formula_to_frag(most_likely_mol_ion_formula)[i]
                    - chem_data.formula_to_frag(expected_formula)[i]
                )
                for i in range(len(chem_data.formula_to_frag(expected_formula)))
            ]
        )

    return difference_number_of_atoms


def _grade_file_result(file_result: pd.DataFrame):
    """
    Grades the Alpinac result of a file.

    The grades mean the following:
    1:  The expected formula was not found;
    2:  The expected formula was found, but one of the
        likelihood is 0;
    3:  If the expected formula was found, but it was not the
        most likely;
    4:  If the expected formula was found and it is the most
        likely.

    Parameters
    ----------
    file_result : pd.DataFrame
        Alpinac results of a file.

    Returns
    -------
    int
        Grade between 1 and 4, where 1 means that the expected
        formula was not found and 4 means that the expected
        formula was found and it is the most likely.
    """
    messages_of_presence = file_result[
        "is_expected_formula_present"
    ]  # in this column you find either "not found" or "found_at_indexes(likelihoods)"
    cases_with_founded_exp_mol = messages_of_presence[
        (messages_of_presence != "not_found") & (messages_of_presence != "empty_folder")
    ].to_list()  # list containing cells in is_expected_formula_present of compound in which the expected molecular can be found.
    if len(cases_with_founded_exp_mol) == 0:
        return 1
    cases_with_founded_exp_mol = [
        string.removeprefix("found_at_").removesuffix(")").split("(")
        for string in cases_with_founded_exp_mol
    ]  # removes the prefix "found_at_" and separates the indexes and the likelihood into two strings

    if any(
        [
            "0.0" in positions_likelihoods[1].split("/")
            for positions_likelihoods in cases_with_founded_exp_mol
        ]
    ):  # checks if one of the likelihood is 0
        return 2
    if any(
        [
            "0" in positions_likelihoods[0].split("/")
            for positions_likelihoods in cases_with_founded_exp_mol
        ]
    ):  # checks if the expected formula is in the the was the most likely, i.e. its index was 0
        return 4
    return 3


def _to_xlsx(reportage_path: Path, write=True):
    """
    Read a csv file and write it to a formatted excel file.

    The csv is read to a DataFrame, a new `grade` column is
    created (each file has a grade between 1 and 4).

    The background of the most likely molecular ion formula is
    of the following colors:
        green to orange to red if Alpinac gave a result (the
        shade depends on the difference in number of atoms
        between the most likely molecular ion formula and the
        expected formula, where dark green means 0 atmos and
        dark red means more than 5 atmos);
        purple if no formula was identified;
        yellow if the identification took too much;
        blue if Alpinac raised some kind of error;
        black if the compound's folder is empty,
        the Alpinac result's folder is empty or if the
        expected formula was `NaN` (i.e. it was not given).
    The background of `is_expected_formula_present` goes from
    red to orange to green, where red means that the file has
    a 1 in the `grade` column and green means that the file has
    the maximal grade in the `grade` column.
    The background of `mol_ion_DBE` get green if they are natural
    numbers (positive and integer), otherwise red.
    The background of `fraction_identified_fragments` column is
    of color
        green if the numerator and denominator are equal;
        orange if the numerator differs from the denominator by
        -1;
        brown if the the numerator is greater than the
        denominator;
        red if the value is "NaN".


    Parameters
    ----------
    reportage_path : Path
        Path to the csv file which stores the Alpinac results.
    write : bool, default True
        True to write the file, False otherwise.
    """
    if not write:
        return
    logger.info("_to_xlsx")
    xlsx_path = reportage_path.with_suffix(".xlsx")

    df_csv = _read_csv(reportage_path)

    main_result_with_5_peaks = _get_compounds_with_enough_peaks(
        df_csv, min_amount_peaks=5
    )

    with pd.ExcelWriter(xlsx_path, engine="openpyxl", mode="w") as writer:
        _style_and_write_sheet(writer, "main_result", main_result_with_5_peaks)

    result_by_file = _get_result_by_file(df_csv)

    result_by_group = _get_result_by_group(
        df_csv,
        main_result_with_5_peaks,
    )
    with pd.ExcelWriter(xlsx_path, engine="openpyxl", mode="a") as writer:
        _style_and_write_result_by_group(writer, result_by_group)
        _style_and_write_sheet(writer, "result_by_file", result_by_file)
        for group, sheet in main_result_with_5_peaks.groupby("group"):
            _style_and_write_sheet(writer, group, sheet)

    _adjust_xlsx_columns_width(xlsx_path)
    logger.info("Finished _to_xlsx")


def _get_compounds_with_enough_peaks(df_csv: pd.DataFrame, min_amount_peaks: int):
    """
    Removes from the DataFrame all compound with less than the
    minimal amount of peaks.

    However, if the expected formula was found, the compound
    is kept.

    Parameters
    ----------
    df_csv : pd.DataFrame
        The csv file generated by Reporter as a DataFrame.
    min_amount_peaks : int
        Number of minimal peaks that the final DataFrame must
        have.

    Returns
    -------
    pd.DataFrame
        All interesting cases with at least the minimal amount
        of peaks.
    """
    return df_csv[
        df_csv["fraction_identified_fragments"].apply(
            lambda fraction_str: not pd.notna(fraction_str)
            or float(fraction_str.split("/")[1]) >= min_amount_peaks
        )
        | (df_csv["difference_number_of_atoms"] < 1)
    ]


def _read_csv(reportage_path: Path):
    """
    Read the csv file and add `grade` column.

    Parameters
    ----------
    reportage_path : Path
        The csv file generated by Reporter as a DataFrame.

    Returns
    -------
    pd.DataFrame
        The csv file with the added column `grade`.
    """
    df_csv = pd.read_csv(reportage_path, index_col=0)
    df_csv.reset_index(inplace=True)

    df_csv["grade"] = "NaN"
    unbiased_grades = df_csv.groupby(by="file")
    for file, file_result in unbiased_grades:
        df_csv.loc[df_csv["file"] == file, "grade"] = _grade_file_result(file_result)

    return df_csv


def _style_and_write_result_by_group(
    writer: pd.ExcelWriter, result_by_group: pd.DataFrame
):
    """
    Write and format the sheet result_by_group.

    Both the background of the columns "grade" and
    "grade/max_grade" goes from red to orange to green, where
    red means that the grade is 1 (the minimal grade) and green
    means that the grade is 4 (the maximal grade).
    The font is Arial.

    Parameters
    ----------
    writer : pd.ExcelWriter
        Context manager for appending the sheet to the existing
        excel file.
    result_by_group : pd.DataFrame
        Main results of Alpinac categorized by group. These are
        averaged grade, averaged grade divided by the maximal
        grade, number of graded files, number of run files,
        percentage of files with maximal grades.
    """
    result_by_group.style.applymap(
        lambda cell: " font-family: Arial, Helvetica, sans-serif;"
    ).background_gradient(
        cmap="RdYlGn_r",
        subset=["grade", "grade/max_grade"],
        gmap=result_by_group["grade"].apply(lambda grade: round(5 - grade, 2)),
        vmax=4,
        vmin=1,
    ).background_gradient(
        cmap="RdYlGn_r",
        subset=["nr_grades"],
        gmap=result_by_group["file"] - result_by_group["nr_grades"],
        vmax=4,
        vmin=0,
    ).to_excel(
        writer,
        sheet_name="result_by_group",
        header=[
            "averaged_grade",
            "grade/max_grade",
            "nr_grades",
            "run_files",
            "perc_files_with_max_grade",
        ],
        float_format="%.2f",
    )


def _get_result_by_file(df_csv: pd.DataFrame):
    """
    Construct the sheet `result_by_file` as a DataFrame.

    The compound whose most likely molecular ion formula has
    the least amount of atoms difference (with the expected
    formula) and the greatest likelihood represents the file
    in the final DataFrame.

    Parameters
    ----------
    df_csv : pd.DataFrame
        The csv file generated by Reporter as a DataFrame.

    Returns
    -------
    pd.DataFrame
        The sheet `result_by_file`, where there is a compound
        representing each file. There is no constrain on the
        amount of peaks.
    """
    grouped_by_file = df_csv.groupby("file")
    result_by_file = pd.DataFrame(columns=df_csv.columns)
    for file, df in grouped_by_file:
        result_by_file.loc[len(result_by_file)] = df.sort_values(
            by=["difference_number_of_atoms", "likelihood"],
            ignore_index=True,
            ascending=[True, False],
        ).loc[0]
    result_by_file = result_by_file.sort_values(by="index")
    return result_by_file


def _get_result_by_group(df_csv: pd.DataFrame, df_with_5_peaks: pd.DataFrame):
    """
    Construct the sheet `result_by_group` as a DataFrame.

    Parameters
    ----------
    df_csv : pd.DataFrame
        The csv file generated by Reporter as a DataFrame.
    df_with_5_peaks : pd.DataFrame
        The csv file generated by Reporter as a DataFrame,
        with only the compounds with at least 5 peaks.

    Returns
    -------
    pd.DataFrame
        The sheet `result_by_group`, where we can find for each
        group the averaged grade, the same averaged grade
        divided by the maximal grade, the number of graded
        files, the number of files, the percentage of files
        with maximal grades.
    """
    count_files = df_csv.groupby(["group"])["file"].nunique()
    unbiased_grades = pd.DataFrame(
        df_with_5_peaks.groupby(["file", "group"])["grade"].mean()
    )
    # for each file we need one grade. If we don't do this
    # the averaged grade will depend on the amount of compounds

    grouped_by_group = unbiased_grades.groupby("group")
    number_maximal_grade = (
        unbiased_grades[unbiased_grades["grade"] == 4.0]
        .groupby("group")
        .count()
        .rename(columns={"grade": "perc_files_with_max_grade"})
    )

    number_grades = grouped_by_group.count().rename(columns={"grade": "nr_grades"})
    data_series = [
        grouped_by_group["grade"].mean(),
        grouped_by_group["grade"].mean().rename("grade/max_grade") / 4,
        number_grades,
        count_files,
        number_maximal_grade,
    ]
    result_by_group = reduce(
        lambda left, right: pd.merge(left, right, on=["group"], how="outer"),
        data_series,
    )

    result_by_group["perc_files_with_max_grade"] = (
        result_by_group["perc_files_with_max_grade"]
        / count_files
        # dividing the number of maximal grade by the number of total files
    )
    return result_by_group


def _style_and_write_sheet(
    writer: pd.ExcelWriter, sheet_name: str, sheet_df: pd.DataFrame
):
    """
    Format and write the sheets displaying results from
    alpinac.

    The coloring is the same as described in `_to_xlsx`.

    Parameters
    ----------
    writer : pd.ExcelWriter
        Context manager for writing or appending the sheet to the
        excel file.
    sheet_name : str
        Name to give to the final sheet.
    sheet_df : pd.DataFrame
        Result of Alpinac, framed as in the csv file produced
        by reporter (it has the same columns and the cells are
        formatted as in the csv file).
    """
    sheet_df.style.background_gradient(
        cmap="RdYlGn_r",
        subset="likely_mol_ion_formula",
        gmap=sheet_df["difference_number_of_atoms"],
        vmax=6,
    ).map(
        # set every cell's font to arial family
        lambda cell: "font-family: Arial, Helvetica, sans-serif;",
    ).background_gradient(
        cmap="RdYlGn_r",
        subset="is_expected_formula_present",
        gmap=sheet_df["grade"].apply(lambda grade: round(5 - grade, 2)),
        # background_gradient does not allow to choose if red is the greatest element or the smallest element.
        # red is the greatest element and green the smallest. Therefore, the grade must be "reordered" to fit the western idea of green good red bad.
        vmax=4,
        vmin=1,
    ).map(
        _style_something_went_wrong,
        # i.e. it colors the cell yellow if Alpinac took too much,
        # purple if no peak was identified, brown if an error was
        # raised during the identification, blue if there was an
        # Alpinac error (error.log was present in the folder)
        props=[
            "color:black;background-color:yellow; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:purple; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:brown; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:blue; font-family: Arial, Helvetica, sans-serif;",
            "font-family: Arial, Helvetica, sans-serif;",
        ],
        subset="likely_mol_ion_formula",
    ).map(
        _style_DBE,
        props=[
            "color:white;background-color:darkgreen; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:darkred; font-family: Arial, Helvetica, sans-serif;",
        ],
        subset="mol_ion_DBE",
    ).map(
        _style_fraction_identified_fragments,
        props=[
            "color:black;background-color:darkgreen; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:orange; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:brown; font-family: Arial, Helvetica, sans-serif;",
            "color:white;background-color:darkred; font-family: Arial, Helvetica, sans-serif;",
        ],
        subset="fraction_identified_fragments",
    ).to_excel(
        writer,
        sheet_name=sheet_name,
        index=False,
    )


def _adjust_xlsx_columns_width(xlsx_path: Path):
    """
    Make the columns's width large enough to fit all the cells.

    If the column's width is too wide, it is shorten, otherwise
    it is stretched. All columns of all sheets of the file are
    adjusted.

    Parameters
    ----------
    xlsx_path : Path
        Path to the xlsx whose columns need to be adjusted.
    """
    wb = openpyxl.load_workbook(filename=xlsx_path)
    for worksheet in wb.worksheets:
        for col in worksheet.columns:
            max_length = 0
            column = col[0].column_letter  # Get the column name
            for cell in col:
                try:  # Necessary to avoid error on empty cells
                    if len(str(cell.value)) > max_length:
                        max_length = len(str(cell.value))
                except:
                    pass
            adjusted_width = (max_length) * 1.2 + 2
            worksheet.column_dimensions[column].width = adjusted_width
    wb.save(xlsx_path)


def _style_fraction_identified_fragments(v, props=["", None, None, None]):
    """
    Choose a CSS style to give to the fraction of identified
    fragments.

    Parameters
    ----------
    v
        Value to be formatted.
    props : list
        CSS styles as strings, with at exactly 4 elements.

    Returns
    -------
    str
        CSS style as a string: if the value is `NaN` the forth
        style is returned, otherwise the difference between
        denominator and numerator is used as an index. If the
        index is greater than 3, we take 3 as an index.
        Similarly, if the index is less than -1, -1 is taken as
        an index.
    """
    if pd.isna(v):
        return props[3]
    numerator, denominator = v.split("/")
    difference = max(min(int(denominator) - int(numerator), 3), -1)
    return props[difference]


def _style_DBE(v, props=["", None]):
    """
    Choose a CSS style to give to the DBE.

    Parameters
    ----------
    v
        Value to be formatted.
    props : list
        CSS styles as strings, with at least 2 elements.

    Returns
    -------
    str
        CSS style as a string: if the value is a
        non-negative integer the first CSS style is
        returned, otherwise the second.
    """
    if (
        v % 1 == 0
    ) and v >= 0:  # if it is a non negative integer, then it is a good DBE
        return props[0]
    return props[1]


def _style_something_went_wrong(v, props=["", None, None, None]):
    """
    Choose a CSS style to give to the column of the must likely
    molecular ion's formula, if something went wrong.

    Parameters
    ----------
    v
        Value to be formatted.
    props : list
        CSS styles as strings, with at least 5 elements.

    Returns
    -------
    str
        CSS style as a string: if there is an error message
        instead of a formula, one of the first 4 CSS style is
        used, otherwise the last one.
    """
    if v == "identification_took_too_much":
        return props[0]
    if v == "unidentified":
        return props[1]
    if v == "error_raised_during_identification":
        return props[2]
    if v == "alpinac_error":
        return props[3]
    return props[4]


def _get_already_run_files(reportage_path: Path):
    """
    List already run files and generated a csv with path
    reportage_path if inexistent and we want to write it.

    Parameters
    ----------
    reportage_path : Path
        Path to the csv file generated by Reporter.

    Returns
    -------
    list
        Files's name that are present int the csv file.
    """
    already_run_files = []
    if reportage_path.is_file():
        already_run_files = pd.read_csv(reportage_path)["file"].values
    return already_run_files


def _identify(cases: list, timeout_time: int, processes: int):
    """
    Identify the cases with a time limit.

    If the identification of one compound takes more than
    timeout_time seconds, we skip to the next compound.

    Parameters
    ----------
    cases : list
        Cases which we want to identify.
    timeout_time : int
        Time limit in seconds for a compound to be run.
        If the identification of a compound exceeds this
        limit, the identification is stopped and the next
        compound will be identified.
    processes : int
        Number of worker processes.
    """
    logger.info("_identify")
    cases_ = [
        (timeout_time, index + 1, data, len(cases)) for index, data in enumerate(cases)
    ]
    with Pool(processes=processes) as pool:
        pool.map(_make_identification_from_file_adapter, cases_)
    logger.info("Finished to _identify")


def _make_identification_from_file_adapter(tpl):
    """
    Adapt the interface of `make_identification_from_file`
    to work with multiprocessing.

    Parameters
    ----------
    tpl : tuple
        Tuple containing the timeout time, the index of
        the case, the data of the case (to run alpinac,
        check `make_identification_from_file` for more
        info) and the number of cases.
    """
    timeout_time = tpl[0]
    index = tpl[1]
    data = tpl[2]
    number_of_cases = tpl[3]
    file_path = Path(data["file"])
    logger.info(
        f"identifying case number {index} out of {number_of_cases} cases: "
        + data["file"]
    )
    kwagrs = data["identification_kwagrs"]
    make_identification_from_file(file_path, timeout_time=timeout_time, **kwagrs)
    logger.info(f"Finished to identifying case number {index}: {file_path}")


def _get_cases_to_identify(input_path: Path, reportage_path: Path):
    """
    Generates a list of the cases to identify.

    Given the path to the json file, we keep the cases which
    are forced to be run or not already run (i.e. files that
    are present in the csv file with path reportage_path).
    If there is a regular expression as a file path, we
    generate cases with file path matching the regular
    expression.

    Parameters
    ----------
    input_path : Path
        Path to the json file with the cases to be run.
    reportage_path : TextIOWrapper
        Opened json file containing the information about the
        cases to run, see `create_report_from_json_file` for
        how to format the json file.

    Returns
    -------
    list
        List of the cases to identify, which are forced to
        run or not run.
    """
    logger.info("_get_cases_to_identify")
    already_run_files = _get_already_run_files(reportage_path)
    with input_path.open() as json_file:
        cases_to_identify = json.load(json_file)
    cases_to_identify = _match_regular_expressions_with_paths(cases_to_identify)
    cases_to_identify = [
        data
        for data in cases_to_identify
        if (
            not (Path(data["file"]).name in already_run_files)
            and not Path(data["file"]).with_suffix("").is_dir()
        )
        or data["force_run"]
    ]
    logger.info("Finished to _get_cases_to_identify")
    return cases_to_identify


def _match_regular_expressions_with_paths(cases_to_identify: list):
    """
    Replace the paths of a list of cases with paths of files
    matching the regular expression.

    Parameters
    ----------
    cases_to_identify : list
        List of cases to identify which may contain regular
        expressions as paths.

    Returns
    -------
    list
        List of the cases to identify, whose file paths match
        the regular expressions.
    """
    cases_with_path_matched_regular_expression = []
    for data in cases_to_identify:
        for path in glob.glob(data["file"]):
            data_cpy = data.copy()
            data_cpy["file"] = path
            cases_with_path_matched_regular_expression.append(data_cpy)
    return cases_with_path_matched_regular_expression


def _get_additional_info_of_compound(name_of_compounds: list):
    """
    Read the file `substances.csv` and keep all compounds
    whose name, or aliases, are contained in name_of_compounds.

    Parameters
    ----------
    name_of_compounds : list
        Names of compounds that can be found in `substances.csv`.
        If the name of the compound can't be found in `substances.csv`,
        it will be not present in the final json.

    Returns
    -------
    pd.DataFrame
        Information about the compounds in the file `substances.csv`
        which have name or aliases in name_of_compounds.
    """
    substances_path = Path(__file__).parents[0] / "validation" / "substances.csv"
    targets_additional_information = pd.read_csv(substances_path)
    targets_additional_information.loc[
        len(targets_additional_information), "Compound"
    ] = "unknown_compound"
    targets_additional_information["is_name_of_compound_in_aliases"] = (
        _check_if_compound_is_in_aliases(
            name_of_compounds, targets_additional_information
        )
    )
    additional_information_of_compound = targets_additional_information[
        targets_additional_information["is_name_of_compound_in_aliases"]
        | targets_additional_information["Compound"].isin(name_of_compounds)
    ]

    return additional_information_of_compound


def _format_cases_to_report(
    folders_info: list,
    additional_information_of_compound: pd.DataFrame,
):
    """
    List all information needed to investigate.

    Parameters
    ----------
    folders_info : list
        Information generated from the file names of
        the folders that we want to identify.
    additional_information_of_compound : pd.DataFrame
        Names of compounds that can be found in `substances.csv`.

    Returns
    -------
    pd.DataFrame
        Information to investigate. If the name or aliases in
        name_of_compounds are present in the file `substances.csv`,
        there will be additional information in the final json about
        the given compounds. Otherwise, there will be generic
        information that is sufficient to just run the alpinac
        for the given files and get an xlsx file which sums up
        the result of alpinac.
    """
    cases_to_report = []
    for compound_info in additional_information_of_compound.to_dict(orient="records"):
        compound_info["Aliases"] = (
            str(compound_info["Aliases"]).strip("]['").split("', '")
        )
        compound_name = compound_info["Compound"]
        for data in _match_compound_with_file_names(
            folders_info, compound_info["Aliases"], compound_name
        ):
            if not Path(data["file"]).suffix in {".jx", ".txt", ".csv"}:
                continue
            compound = compound_info.copy()
            compound_keys = compound.keys()
            if not "identification_kwagrs" in compound_keys:
                compound["identification_kwagrs"] = {
                    "target_elements": None,
                    "target_formula": None,
                    "show_plt_figures": False,
                    "output_dir": None,
                    "raise_error": False,
                    "compounds": [],
                }
            if not "expected_formula" in compound_keys:
                compound["expected_formula"] = compound_info["Sum formula"]
                if pd.notna(compound_info["Sum formula"]):
                    compound["expected_formula"] = chem_data.get_string_formula(
                        chem_data.formula_to_frag(compound_info["Sum formula"])
                    )  # order the atoms of the sum formula from smaller mass to bigger mass
            if not "force_run" in compound_keys:
                compound["force_run"] = False
            if not "group" in compound_keys:
                compound["group"] = data["group"]

            compound["file"] = str(data["file"])
            cases_to_report.append(compound)

    cases_without_duplicate = []
    cases_without_duplicate = [
        data for data in cases_to_report if data not in cases_without_duplicate
    ]  # sometimes I got duplicate, so just to be sure that it won't happen.
    return cases_without_duplicate


def _match_compound_with_file_names(
    folders_info: list[list[dict]], aliases: list[str], compound: str
):
    """
    List information about files which match the compound's name or
    aliases.

    Parameters
    ----------
    folders_info : list[list[dict]]
        Information generated from the files' names of
        the folders that we want to identify.
    aliases : list[str]
        List of aliases of the compound.
    compound : str
        Compound's name.

    Returns
    -------
    list
        Information about the files which match the given compound's
        name or aliases.
    """
    return [
        info
        for files_info in folders_info
        for info in files_info
        if (
            compound == info["compound"]
            or any([alias == info["compound"] for alias in aliases])
        )
    ]


def _check_if_compound_is_in_aliases(
    name_of_compounds: list[str], targets_additional_information: pd.DataFrame
):
    """
    Generates a series of booleans.

    Parameters
    ----------
    name_of_compounds : list[str]
        Compounds' names that we want to identify.
    targets_additional_information : pd.DataFrame
        Additional information from the file `substances.csv`
        about compound.

    Returns
    -------
    Series
        Series of booleans, True if the intersection of the aliases
        and the name of the compounds that we want to run is non-empty,
        otherwise False.
    """
    return targets_additional_information["Aliases"].apply(
        lambda aliases_str: len(
            set(str(aliases_str).strip("]['").split("', '")).intersection(
                name_of_compounds
            )
        )
        > 0
    )


def _format_file_info(file_info: dict, file_path: Path):
    """
    Create a dictionary with all the information generated from the
    file name.

    Parameters
    ----------
    file_info : dict
        Dictionary with keys `pattern`, `placeholders`, `path`
        and `folders`.
    file_path : Path
        Path to the file.

    Returns
    -------
    dict
        Updated dictionary with `group`, `file` and `compound`.
    """
    values_in_file_regex = re.compile(file_info["pattern_re"])
    updated_file_info = {}
    if len(file_info["original_pattern"].split("\\")) > 1:
        updated_file_info["group"] = file_path.parent.name
        if "group" in file_info["placeholders"]:
            file_info["placeholders"].remove("group")
    file_name_values = values_in_file_regex.findall(file_path.name)
    # file_name_values stores non-overlapping matches of the pattern
    # i.e. for the pattern "a*d*" and the string "abcdefgabcdefg" the
    # non-overlapping matches would be "bc" and "efg" and findall
    # would return [("bc","efg"), ("bc","efg")]
    if len(file_info["placeholders"]) > 1:
        file_name_values = file_name_values[0]

    updated_file_info.update(dict(zip(file_info["placeholders"], file_name_values)))
    updated_file_info["file"] = str(file_path)

    if not "compound" in updated_file_info.keys():
        updated_file_info["compound"] = "unknown_compound"

    if len(updated_file_info["compound"].split(".")) > 0:
        updated_file_info["compound"] = updated_file_info["compound"].split(".")[0]

    if not "group" in updated_file_info.keys():
        updated_file_info["group"] = "no_group"
    return updated_file_info


if __name__ == "__main__":

    # Create a parser
    parser = argparse.ArgumentParser(description="Generate a report from the files.")
    # Add the arguments
    parser.add_argument(
        "frags_dir", type=str, help="Path to the directory with the files."
    )
    # Optional argument
    parser.add_argument(
        "--reporter-filepath", type=str, help="Path to the reporter json file."
    )
    add_verbose_debug_arguments(parser)

    # Parse the arguments
    args = parser.parse_args()

    process_verbose_debug_arguments(args)
    # Required for the logger to work
    logger.setLevel(logging.INFO)

    frags_dir = Path(args.frags_dir)
    if not frags_dir.is_dir():
        raise FileNotFoundError(f"{frags_dir} is not a directory.")

    if args.reporter_filepath:
        reporter_filepath = Path(args.reporter_filepath)
    else:
        reporter_filepath = frags_dir.parent / "reporter_input.json"

    if not reporter_filepath.is_file():
        generate(
            {
                "*.txt": frags_dir,
            },
            reporter_filepath,
        )

    investigate(reporter_filepath)
