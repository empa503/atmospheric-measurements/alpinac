"""Functions and classes to handle compunds.

"""
from __future__ import annotations
from enum import Enum
import numpy as np
import math
import logging
from alpinac import const_identification

import alpinac.io_tools as io_tools
import alpinac.periodic_table as chem_data
from alpinac.utils_data_extraction import (
    f_pseudoVoigt,
    fit_linear_np,
)
from alpinac.abtract_outputs import OutputSaver

from typing import NewType

import matplotlib.pyplot as plt

MolecularFormula = NewType("MolecularFormula", str)


class IonizationType(Enum):
    EI = "EI"
    CI = "CI"


class Spectra:
    """Represent Information about a spectra."""

    ionisation: IonizationType

    def __repr__(self) -> str:
        return f"{type(self).__name__}"


class EI_Spectra(Spectra):
    """Represent Information about a EI spectra."""

    ionisation: IonizationType = IonizationType.EI


class CI_Spectra(Spectra):
    """Represent Information about a CI spectra."""

    adduct: MolecularFormula
    ionisation: IonizationType = IonizationType.CI

    def __init__(self, adduct: MolecularFormula) -> None:
        super().__init__()
        self.adduct = adduct

    def __repr__(self) -> str:
        return super().__repr__() + f"({self.adduct})"


class Compound:
    """A compound is what alpinac tries to identify.

    At the beggining of the identification,
    a compound is a group of measured peaks (or measured fragments)
    belonging to the same RT index.

    The most important measured data are the measured masses
    and their corresponding signal intensities.

    A compund can be composed of different spectra, with usually
    different ionisation modes.

    Sometimes coumpounds co-elute and therefore are likely
    to belong to the same chemical compound or they could also belong
    to several co-eluting chemical compounds.

    Attributes
    ----------

    logger: A logger object.

    fragment_index: The index of the compound in the inputs.
    data: The measured data of the compound. It the the one_batch_fragment
        argument of the __init__ method.
    unit_mass_resolution: If the masses are specified in unit of mass resolution.
        .. note:: Identification of unit masses was performed during the developpement of alpinac.
            Now it is not used anymore and is not fully supported/tested  in all
            the code.

    mass_u_k: Coverage factor for the mass uncertainty,
        mutiply this to the mass uncertainty.
        See equation (1) in Guillevic et al.
    mass_u_k_loose: A parameter that scales the  meas_mass_u_peak_width of a peak.
        Not clear what it represents exactly. TODO: explain.
    mass_axis_offset: The offset of the mass axis. Not clear what it represents. TODO: explain.
    alpha_pseudo_voigt: The mean of the alpha parameter of the pseudo-Voigt function.
        The alpha parameter is found in the data.
        The alpha parameter is the Lorenzian contribution to peak shape.
        see :ref:`pseudo-Voigt function <pseudo-voigt-function>`.

    sigma_m_slope: The slope of the linear fit of the mass (x) vs peakwidth (y).
        The peakwidth is the sigma of the peaks. (Assumes the sigma varies linearly with the mass)
    sigma_m_b: The intercept of the linear fit.

    meas_len: The number of measured peaks in the compound.
    meas_rt: The retention times of the center of each peaks.
    ionisation: The ionisation mode of the peaks. (EI or CI)
    meas_I: The signal intensity of the peaks (area of the peak).
    meas_LOD: The limit of detection of the peaks.
    meas_mass: The m/z ratio of the peaks. Unit: m/z.
    meas_mass_min: The minimum value of the m/z ration for the peaks
        (part of the interval of possible masses).
    meas_mass_max: The maximum value of the m/z ration for the peaks
        (part of the interval of possible masses).
    meas_mass_u: The uncertainty of the m/z values due to
        measurement noise. Unit: m/z.
    meas_mass_u_cal: The uncertainty of the m/z values due to
        the mass calibration. Unit:  m/z.
    meas_mass_U_combined: The combined uncertainty (mass_u + mass_u_cal)
        of the m/z values,
        multiplied by mass_u_k. Unit:  m/z.
    meas_mass_U_combined_ppm: The combined uncertainty of the m/z values. Unit: ppm.
    meas_mass_u_median: The median of the combined uncertainty of the m/z values. Unit: m/z.
    meas_mass_u_median_ppm: The median of the combined uncertainty of the m/z values. Unit:  ppm of m/z.
    meas_mass_u_peak_width: The Uncertainty due to peak width in the mass domain. Unit:  m/z.
        The peak width is used for that and is the sigma of the peaks


    average_rt: average retention time
    sum_I: sum of the signal intensity of the peaks
    meas_I_max: maximal intensity of all the peaks
    meas_I_min: minimal intensity of all the peaks

    spectras: A list of Spectra objects. Each Spectra object contains
        the information about a the type of spectra (EI or CI).
        And the information about the adducts for CI spectra.
    spectra_ids: A list of the index of the spectra in :attr:spectras: for
        each of the peaks.

    """

    logger: logging.Logger

    fragment_index: int | str
    data: list[
        list[float | str | int]
    ]  # list of lists, output of io_tools:get_data_from_frag_file
    unit_mass_resolution: bool

    mass_u_k: float
    mass_u_k_loose: float
    mass_axis_offset: float

    sigma_m_slope: float
    sigma_m_b: float

    meas_len: int
    meas_rt: list[float]
    ionisation: list[
        int
    ]  # TODO change this to an enum which is already used in the extraction and calibration modes
    meas_I: list[float]
    meas_LOD: list[float]
    meas_mass: list[float | int]
    meas_mass_min: list[float | int]
    meas_mass_max: list[float | int]

    # Uncertainties
    meas_mass_u: list[float]
    meas_mass_U_combined: list[float]
    meas_mass_U_combined_ppm: list[float]
    meas_mass_u_median: float
    meas_mass_u_median_ppm: float

    # Stats
    average_rt: float
    sum_I: float
    meas_I_max: float
    meas_I_min: float

    # For defining which spectra the peaks belong to
    spectras: list[Spectra]
    spectra_idx_list: list[int]

    def __init__(
        self,
        one_batch_fragment: list[list[float | str | int]],
        compound_number: int | str = "",
        LOD: float = np.inf,
        mass_u_k: float = const_identification.m_u_k,
        mass_u_k_loose: float = 3.0,  # arbitrary choice. Should it be larger than dict_k['m_u_k']? Not necessarily.
    ):
        """

        INPUT:
        - ``one_batch_fragment``: a list of lists, output of io_tools:get_data_from_frag_file
            * RT
            * mass
            * mass_u_ppm
            * mass_cal_u_ppm
            * area
            * area_u
            * peak_width
            * peak_alpha
            * compound_bin
            * LOD
            * Ionisation
            * Adduct
            * run_bin

        - ``compound_number``: a number ( bin number)
        - "str_ionisation": string that is a code to inform what type of ionisation was used

        """
        self.logger = logging.getLogger(f"alpinac.Compound_{compound_number}")
        self.logger.info(f"Creating compound from {one_batch_fragment}")
        self.fragment_index = compound_number
        self.data = one_batch_fragment

        # Data sorted by increasing mass:
        # needed to speed up looking for isotopologues
        self.data.sort(key=lambda x: x[io_tools.idx_m])

        self.meas_len = len(self.data)
        self.logger.info(f"Number of data peaks:  {self.meas_len}")

        # re-extract data (split by columns)
        # retention time of centre of peak, list
        self.meas_rt = [self.data[idx][io_tools.idx_rt] for idx in range(self.meas_len)]

        self.mass_u_k_loose = mass_u_k_loose

        # All measured masses must be integer to define the set as unit-mass resolution data.
        if (
            sum(
                [
                    self.data[idx][io_tools.idx_m].is_integer()
                    for idx in range(self.meas_len)
                ]
            )
            == self.meas_len
        ):
            self.logger.info("Unit mass resolution data (masses are integers).")
            self.unit_mass_resolution = True

            self.meas_mass = [
                self.data[idx][io_tools.idx_m] for idx in range(self.meas_len)
            ]

            self.meas_mass_u_cal = np.zeros(len(self.meas_mass))
            self.mass_u_k = 2.0
            self.mass_axis_offset = 0.0
            self.meas_mass_U_combined = [0.49995 for _ in range(self.meas_len)]
            self.meas_mass_u_peak_width = self.meas_mass_U_combined.copy()

            # meas_mass_u_median_ppm: not relevant, unit mass resolution data
            self.meas_mass_u_median = 0.0
            self.meas_mass_u_median_ppm = 0.0

        else:
            # Measured mass type: mass resolution better than 1 (e.g., TOF data)
            self.unit_mass_resolution = False

            # Measured mass
            # To prepare for step 1 knpasack, we assume that all masses have been ionised once
            # (each mass lost one electron)
            # so we add he the mass of one electron to each measured mass
            self.meas_mass = [
                self.data[idx][io_tools.idx_m] + chem_data.electron_mass
                for idx in range(self.meas_len)
            ]

            self.mass_axis_offset = 0.5

            # Definition of uncertainties for the measured masses:
            # there are 3 different uncertainties used for different things
            # (GUM = Guide for uncertainty of measurements, see:)
            # https://www.bipm.org/documents/20126/2071204/JCGM_100_2008_E.pdf/cb0ef43f-baa5-11cf-3f85-4dcd86f77bd6

            # 1) mass measurement uncertainty
            # or uncertainty due to measurement noise
            # (metrological term according to GUM: uncertainty type A)
            self.meas_mass_u = [
                self.data[idx][io_tools.idx_m_u] for idx in range(self.meas_len)
            ]

            # 2) uncertainty due to uncertainty of the mass calibration
            # (metrological term according to GUM: uncertainty type B)
            self.meas_mass_u_cal = [
                self.data[idx][io_tools.idx_m_cal_u] for idx in range(self.meas_len)
            ]
            self.logger.debug(f"{self.meas_mass_u_cal = }")

            # From these two uncertainties we compute the combined uncertainty
            # which is used as mass tolerance
            # to generate masses in the knapsack algorithm (alpinac step 1)
            self.mass_u_k = mass_u_k  ###2.5
            self.meas_mass_U_combined = [
                self.mass_u_k
                * math.sqrt(
                    self.meas_mass_u[idx_m] ** 2 + self.meas_mass_u_cal[idx_m] ** 2
                )
                for idx_m in range(self.meas_len)
            ]
            self.logger.debug(f"{self.meas_mass_U_combined = }")

            self.meas_mass_U_combined_ppm = 1e6 * np.array(self.meas_mass_U_combined) / np.array(self.meas_mass)

            # Calculate the medians of the uncertainties
            self.meas_mass_u_median = np.median(self.meas_mass_U_combined)
            self.meas_mass_u_median_ppm = np.median(self.meas_mass_U_combined_ppm)
            self.logger.debug(
                f"{self.meas_mass_u_median = }, {self.meas_mass_u_median_ppm = }"
            )

            # 3) Uncertainty due to peak width in the mass domain
            # The mass resolution of the detector cause a peak in the mass domain
            # to have a certain width which is not zero.
            # In class isotopologue, function find_idx_meas_mass_from_exact_mass,
            # to match the generated masses for minor isotopologues
            # to measured masses, we need as well a mass tolerance
            # which is larger than self.meas_mass_U_combined.
            # For example if a minor isotope is a minor contribution
            # to a measured mass peak, it could be under the edge of the measured peak.
            # So here we try to define the peak width in the mass domain
            # and use it as tolerance.

            # mass_sigma_peak = [self.sigma_m_b + self.sigma_m_slope*self.meas_mass_u[idx_m] for idx_m in range(self.meas_len)]
            mass_sigma_peak = [
                self.data[idx][io_tools.idx_peak_width] for idx in range(self.meas_len)
            ]
            self.meas_mass_u_peak_width = [
                self.mass_u_k_loose
                * (
                    mass_sigma_peak[idx_m]
                    + math.sqrt(
                        self.meas_mass_u[idx_m] ** 2 + self.meas_mass_u_cal[idx_m] ** 2
                    )
                )
                for idx_m in range(self.meas_len)
            ]

            # Get parameter to define the peak shape in the mass domain.
            # The peak shape is defined as pseudo-Voigt:
            # Gaussian peak with a Lorenzian contribution.
            # 1)Lorenzian contribution to peak shape: alpha parameter.
            self.alpha_pseudo_voigt = (
                sum(
                    [
                        self.data[idx][io_tools.idx_peak_alpha]
                        for idx in range(self.meas_len)
                    ]
                )
                / self.meas_len
            )

            # 2)Peak width: sigma_m_slope and sigma_m_b
            # Get parameters that define the mass peak width
            # as a linear function of the meaured mass.
            # These are needed later to compute the pseudo-Voigt function
            # to model the shape of the peak in the mass domain
            # (the width of the mass peak increases linearly with the measured mass).
            self.sigma_m_slope, self.sigma_m_b, _, _ = fit_linear_np(
                np.array(self.meas_mass),
                np.array(
                    [
                        self.data[idx][io_tools.idx_peak_width]
                        for idx in range(self.meas_len)
                    ]
                ),
            )

        # Compute the mass tolerance
        # used in the knapsack algorithm (alpinac step 1)
        self.meas_mass_min = [
            self.meas_mass[idx_m] - self.meas_mass_U_combined[idx_m]
            for idx_m in range(self.meas_len)
        ]
        self.meas_mass_max = [
            self.meas_mass[idx_m] + self.meas_mass_U_combined[idx_m]
            for idx_m in range(self.meas_len)
        ]

        # Ionisation type due to experimental measurement procedure (EI, CI, reagent type for CI)
        self.ionisation = [
            chem_data.ionisation_CI
            if self.data[idx][io_tools.idx_ionisation] == chem_data.ionisation_CI_string
            else chem_data.ionisation_EI
            for idx in range(self.meas_len)
        ]
        self.ionisation_str = [
            self.data[idx][io_tools.idx_ionisation] for idx in range(self.meas_len)
        ]

        self.logger.debug(f"{self.ionisation = }")
        self.logger.debug(f"{self.ionisation_str = }")

        # MYGU 20220414
        # to handle several spectra (obtained simultaneously using the same or different detectors
        # or consecutively using the same or different detectors)
        # of presumably the same compound
        # in one instance of the Compound class
        # These are the name of the acquired spectra.
        # All masses measured within the same spectrum
        # (same measurement run and same detector)
        # have the same name (and later, same index).
        # Note: it is not necessary an interger value, it can be anything.
        # there is one spectrum index for each measured mass.
        self.meas_mass_spectra_name = [
            self.data[idx][io_tools.idx_run_bin] for idx in range(self.meas_len)
        ]
        self.meas_mass_adduct_name = [
            self.data[idx][io_tools.idx_adduct] for idx in range(self.meas_len)
        ]
        self.logger.debug(f"{self.meas_mass_spectra_name = }")
        self.logger.debug(f"{self.meas_mass_adduct_name = }")

        # get the unique spectra names and their indices
        unique_names, self.spectra_ids = np.unique(
            self.meas_mass_spectra_name, return_inverse=True
        )

        # Convert the spectra data to a list of Spectra objects
        self.spectras = []
        for spectra_id, spectrum_name in enumerate(unique_names):
            self.logger.debug(f"{spectrum_name = }")
            matching_peaks = np.argwhere(self.spectra_ids == spectra_id).flatten()
            # all masses of the same spectra must have the same adduct and ionisation type
            ionisation_types = np.unique(
                [self.ionisation_str[idx] for idx in matching_peaks]
            )
            adducts = np.unique(
                [self.meas_mass_adduct_name[idx] for idx in matching_peaks]
            )

            # Raise errors if the types and adducts are not the same
            if len(ionisation_types) > 1:
                raise ValueError(
                    f"all masses of the same spectra must have the same ionisation type, but {ionisation_types = } for {spectrum_name = }"
                )
            if len(adducts) > 1:
                raise ValueError(
                    f"all masses of the same spectra must have the same adduct, but {adducts = } for {spectrum_name = }"
                )

            # Check the ionisation types
            match ionisation_types[0]:
                case "EI":
                    spectrum = EI_Spectra()
                    if adducts[0]:
                        raise ValueError(
                            f"adducts are not supported for EI spectra, but {adducts = } for {spectrum_name = }"
                        )
                case "CI":
                    spectrum = CI_Spectra(adduct=adducts[0])
                case _:
                    raise ValueError(
                        f"unknown ionisation type {ionisation_types[0] = } for {spectrum_name = }"
                    )

            self.spectras.append(spectrum)

        self.logger.debug(f"{self.spectras = }")

        # region code_to_remove becuase we want to treat spectra and adducts using the code above

        # Spectrum index: Masses belonging to the same spectrum have the same index.
        self.meas_mass_spec_id = [int(0)] * self.meas_len
        # Adduct index: masses with the same possible adduct have the same index.
        # Warning! Several distinct spectra may have the same adduct.
        self.meas_mass_adduct_id = [int(0)] * self.meas_len

        # now we need to know:
        # - how many distinct spectra?
        # - how many distinct adduct?
        # several distinct spectra may have the same adduct -> treat together for step 1
        # make list of indexes of spectra
        self.spectra_idx_list = []  # list of distinct spectrum index(es)
        self.spectra_name_list = []  # list of distinct spectrum name(s)
        self.dict_spectra_name_idx = {}
        self.spectra_adduct_frag = (
            []
        )  # for each spectra, max adduct formula as a fragment vector

        self.adduct_idx_list = []  # list of distinct adduct index(es)
        self.adduct_name_list = (
            []
        )  # list of distinct chemical formula for adduct(s), string
        self.adduct_maxfrag_list = (
            []
        )  # list of distinct fragment formula (vector) for adduct(s)
        self.dict_adduct_name_idx = {}  # dict adduct name -> adduct index
        self.dict_adduct_idx_frag = (
            {}
        )  # dict adduct index -> adduct fragment formula (vector)

        # self.spectra_max_adduct = []

        i_spec = -1
        i_adduct = -1
        for i_m in range(self.meas_len):

            # adduct
            adduct = self.meas_mass_adduct_name[i_m]
            if adduct == None or adduct == "None" or adduct == "none" or adduct == "":
                self.meas_mass_adduct_name[i_m] = None

            if self.meas_mass_adduct_name[i_m] not in self.adduct_name_list:
                i_adduct += 1
                self.adduct_name_list.append(self.meas_mass_adduct_name[i_m])
                self.adduct_idx_list.append(i_adduct)

                self.meas_mass_adduct_id[i_m] = i_adduct
                self.dict_adduct_name_idx[self.meas_mass_adduct_name[i_m]] = i_adduct

                if self.meas_mass_adduct_name[i_m] is None:
                    self.adduct_maxfrag_list.append(
                        chem_data.get_fragment_from_string_formula("")
                    )
                else:
                    adduct_maxfrag = chem_data.get_fragment_from_string_formula(
                        self.meas_mass_adduct_name[i_m]
                    )
                    adduct_max_frag_posDBE = [0 for i in range(len(adduct_maxfrag))]

                    for i in range(len(adduct_maxfrag)):
                        if adduct_maxfrag[i] > 0:
                            adduct_max_frag_posDBE[
                                chem_data.dict_element_to_adduct[i]
                            ] += adduct_maxfrag[i]
                            # logging.info("create target max formula: added adduct special  atom")
                    self.adduct_maxfrag_list.append(
                        adduct_max_frag_posDBE
                    )  # this is the fragment formula with special atoms. Needed for step 1 and step 10.
                    # self.adduct_maxfrag_list.append(chem_data.get_fragment_from_string_formula(self.meas_mass_adduct_name[i_m]))

                self.dict_adduct_idx_frag[i_adduct] = self.adduct_maxfrag_list[i_adduct]

            else:
                self.meas_mass_adduct_id[i_m] = self.dict_adduct_name_idx[
                    self.meas_mass_adduct_name[i_m]
                ]

            # spectra
            if self.meas_mass_spectra_name[i_m] not in self.spectra_name_list:
                i_spec += 1
                self.spectra_name_list.append(self.meas_mass_spectra_name[i_m])
                self.spectra_idx_list.append(i_spec)
                self.meas_mass_spec_id[i_m] = i_spec
                self.dict_spectra_name_idx[self.meas_mass_spectra_name[i_m]] = i_spec
                self.spectra_adduct_frag.append(self.adduct_maxfrag_list[i_adduct])

            else:
                self.meas_mass_spec_id[i_m] = self.dict_spectra_name_idx[
                    self.meas_mass_spectra_name[i_m]
                ]

        self.no_spectra = len(self.spectra_idx_list)  # number of spectra
        self.no_adduct = len(self.adduct_idx_list)  # number of spectra

        # Then create list of adduct index, that is
        # the index of the adduct in self.max_adduct_list
        # self.adducts = [self.data[idx][io_tools.idx_adduct] for idx in range(self.meas_len)]
        logging.info("def of self.adduct_maxfrag_list")
        logging.info(self.adduct_maxfrag_list)

        # how many different measurement type with various adduct
        # for one unknown substance?
        # one substance can be mesured with EI (no adduct)
        # and then with CI, and here with various reagent
        # so we may have CI ionisation associated to different adduct lists

        # endregion code_to_remove

        # measured intensity of peak
        # each peak integrated by pseudo-voigt fit in mass domain
        # and Gaussian with tailing in rt domain
        self.meas_I = [
            self.data[idx][io_tools.idx_area] for idx in range(self.meas_len)
        ]
        self.logger.debug(f"{self.meas_I=}")
        # self.meas_I_u = [self.data[idx][io_tools.idx_area_u] for idx in range(self.meas_len)]

        # LOD at the measured mass (depends on background noise, is mass dependent)
        self.meas_LOD = [
            self.data[idx][io_tools.idx_LOD] for idx in range(self.meas_len)
        ]
        self.logger.debug(f"{self.meas_LOD=}")

        self.average_rt = sum(
            [self.meas_rt[i] * self.meas_I[i] for i in range(self.meas_len)]
        ) / sum(self.meas_I)
        self.sum_I = sum(self.meas_I)
        self.meas_I_max = max(self.meas_I)
        self.meas_I_min = min(self.meas_I)

        # If the user specified a LOD, use it. Otherwise, use the minimum measured intensity.
        self.LOD = float(min(LOD, self.meas_I_min))

        # The following seems to be filled at runtime
        # contains total assigned signal after optimisation, per measured mass
        # TODO need to comment them in docstring
        self.assigned_I = [float(0)] * self.meas_len
        self.atom_list_likelihood = [None]
        self.atom_list_likelihood_idx_sorted = [None]

        self.unidentified_mass_idx_list = []
        self.identified_mass_idx_list = []

    def initialise_mass_profile(self):
        """Initialise mass profile.

        Used in graph optimise isotopologue.
        """
        self.meas_mass_profile_x = []
        self.delta_mass_u = float(0.0)
        self.meas_mass_profile_idx = (
            []
        )  # list of indexes of measured masses matching (within mass uncertainty) a candidate mass
        self.meas_mass_profile = []

    def do_meas_mass_profile_x(
        self, m_start: float, m_stop: float, mass_res: float
    ) -> None:
        """Initialize self.meas_mass_profile_x

        Write np.array with x abcisse values, which is the mass axis,
        where the intensities for each theoretical isotopologue
        and for the measured signal
        will be computed.

        INPUT:
        - ``m_start``: minimum mass abcissa
        - ``m_stop``: maximum mass abcissa
        - ``mass_res``: interval in between each mass point

        OUTPUT: None
        """
        self.meas_mass_profile_x = np.linspace(
            round(m_start) - self.mass_axis_offset,
            round(m_stop) + self.mass_axis_offset,
            round(
                mass_res
                * (
                    round(m_stop)
                    + self.mass_axis_offset
                    - (round(m_start) - self.mass_axis_offset)
                    + 1
                )
            ),
        )

    def do_meas_mass_profile(self, delta_mass, i_spec):
        """Compute measured mass profile at the pre-defined list of x-abcissa.
        First find idx of measured masses to use,
        which are within the mass range defined by min and max of x-abcissa.

        INPUT:
        - ``delta_mass``: potential mass axis offset, at initial step it is zero.
          Used to shift position of measured masses by a constant offset.
          delta_mass is optimised by the lmfit algo.

          MYGU 20220319: added input parameter: i_spec

        """
        # initialisation at zero
        self.meas_mass_profile = np.zeros(len(self.meas_mass_profile_x))
        # indexes of measured masses within window range:
        self.meas_mass_profile_idx = [
            idx
            for idx in range(self.meas_len)
            if self.meas_mass[idx] >= self.meas_mass_profile_x[0]
            and self.meas_mass[idx] <= self.meas_mass_profile_x[-1]
            and self.meas_mass_spec_id[idx] == i_spec
        ]
        # logging.info("self.meas_mass_profile_idx")
        # logging.info(self.meas_mass_profile_idx)
        # logging.info([self.meas_mass_u_cal[i] for i in self.meas_mass_profile_idx])

        # compute only if there is at least one matching measured mass
        if len(self.meas_mass_profile_idx) > 0:
            self.delta_mass_u = min(
                [self.meas_mass_u_cal[i] for i in self.meas_mass_profile_idx]
            )

            # delta_mass = ppm_mass
            for idx_m in range(
                len(self.meas_mass_profile_idx)
            ):  # and self.meas_mass[self.meas_mass_profile_idx[idx_m]] <= max(self.meas_mass_profile_x):
                # f_pseudoVoigt(x, A, mu, sigma, alpha)
                sigma = (
                    self.meas_mass[self.meas_mass_profile_idx[idx_m]]
                    * self.sigma_m_slope
                    + self.sigma_m_b
                )
                self.meas_mass_profile += f_pseudoVoigt(
                    self.meas_mass_profile_x,
                    self.meas_I[self.meas_mass_profile_idx[idx_m]],
                    self.meas_mass[self.meas_mass_profile_idx[idx_m]] + delta_mass,
                    sigma,
                    self.alpha_pseudo_voigt,
                )

    def do_meas_mass_stick_spectrum(
        self, delta_mass, i_spec, m_start: float = None, m_stop: float = None
    ):
        """Return measured mass profile in a desired rang

        INPUT:
        - ``delta_mass``: potential mass axis offset, at initial step it is zero.
          Used to shift position of measured masses by a constant offset.
          delta_mass is optimised by the lmfit algo.

          MYGU 20220319: added input parameter: idx_spectrum of each measured mass
          self.meas_mass_spec_id
        """
        # initialisation at zero
        # self.meas_mass_profile = np.zeros(len(self.meas_mass_profile_x))
        # indexes of measured masses belong to desired spectrum witho no i_spec:
        idx_i_spec = [
            idx for idx in range(self.meas_len) if self.meas_mass_spec_id[idx] == i_spec
        ]
        self.meas_mass_corr = self.meas_mass - self.mass_axis_offset

        # indexes of measured masses within window range:
        if m_start or m_stop:
            # if only one of borders were given, set the other to maximum
            if not m_start:
                m_start = 0
            if not m_stop:
                m_stop = (
                    max(self.meas_mass[idx_i_spec]) + 0.5
                )  # +0.5 to make sure after mass calibration highest value is taken
            self.meas_mass_profile_idx = [
                idx
                for idx in idx_i_spec
                if self.meas_mass[idx_i_spec] >= m_start
                and self.meas_mass[idx_i_spec] <= m_stop
            ]
        else:
            self.meas_mass_profile_idx = idx_i_spec
        # logging.info("self.meas_mass_profile_idx")
        # logging.info(self.meas_mass_profile_idx)
        # logging.info([self.meas_mass_u_cal[i] for i in self.meas_mass_profile_idx])

        # compute only if there is at least one matching measured mass
        if len(self.meas_mass_profile_idx) > 0:
            self.delta_mass_u = min(
                [self.meas_mass_u_cal[i] for i in self.meas_mass_profile_idx]
            )

            # loops over all peaks: delta_mass = ppm_mass
            for idx_m in range(
                len(self.meas_mass_profile_idx)
            ):  # and self.meas_mass[self.meas_mass_profile_idx[idx_m]] <= max(self.meas_mass_profile_x):
                # f_pseudoVoigt(x, A, mu, sigma, alpha)
                sigma = (
                    self.meas_mass[self.meas_mass_profile_idx[idx_m]]
                    * self.sigma_m_slope
                    + self.sigma_m_b
                )
                self.meas_mass_profile += f_pseudoVoigt(
                    self.meas_mass_profile_x,
                    self.meas_I[self.meas_mass_profile_idx[idx_m]],
                    self.meas_mass[self.meas_mass_profile_idx[idx_m]] + delta_mass,
                    sigma,
                    self.alpha_pseudo_voigt,
                )

    def do_iso_profiles(self, iso_list_idx: list, isotopologues_list: list):
        """
        Generate a list, containing for each row a numpy array
        corresponding to the sum profile of one isotopologue series.
        All isotopologue series are in the same mass window.

        INPUT:
        -``iso_list_idx``:
        -``isotopologues_list``:
        """
        # per isotopologue we need:
        # centre of mass (exact mass)
        # sigma (cf. mass cal)
        # Arel, determined knowing relative intensity
        # alpha, mass cal
        # we set a factor k to link Arel to measured intensity
        # at beginning k is calculated using Arel of abundant = 1 and I of matched frag abund.
        no_iso = len(iso_list_idx)
        self.iso_profiles = [None] * no_iso
        for i_no_iso in range(no_iso):
            iso_profile = np.zeros(len(self.meas_mass_profile_x))
            # for i_iso in range(len(iso_list_idx[i_no_iso])):
            # we take one isotopologue series
            # and sum up the profile
            # at the required mass values
            # logging.info('i_no_iso ' + str(i_no_iso))
            isotopologue = isotopologues_list[iso_list_idx[i_no_iso]]
            for i_iso_series in range(isotopologue.iso_list_len):
                # f_pseudoVoigt(x, A, mu, sigma, alpha)
                iso_mass = chem_data.get_mass_single_ionised(
                    isotopologue.iso_list[i_iso_series]
                )
                # iso_mass = isotopologue.iso_list_mass[i_iso_series]
                sigma = iso_mass * self.sigma_m_slope + self.sigma_m_b
                iso_profile += f_pseudoVoigt(
                    self.meas_mass_profile_x,
                    isotopologue.iso_list_I_rel[i_iso_series],
                    iso_mass,
                    sigma,
                    self.alpha_pseudo_voigt,
                )
            self.iso_profiles[i_no_iso] = iso_profile.copy()

    def comp_list_identified_unidentified_mass(self, G):
        """
        Parameters
        ----------
        G : TYPE: networkx graph. Each node contains an instance of Isotopologues.
            DESCRIPTION.

        Returns
        -------
        List of indexes of identified masses.
        List of indexes of non-identified masses.
        Calculates total assigned signal for each measured mass. Total may be > measured.

        """
        # used 20200730
        self.identified_mass_idx_list = []
        self.unidentified_mass_idx_list = []
        self.assigned_I = [float(0)] * self.meas_len
        for i_node in G.nodes():
            for idx_m in range(len(G.nodes[i_node]["g_iso"].meas_mass_idx)):
                if G.nodes[i_node]["g_iso"].meas_mass_idx[idx_m] is not None:
                    self.assigned_I[idx_m] += min(
                        G.nodes[i_node]["g_iso"].iso_list_I_rel[idx_m]
                        * G.nodes[i_node]["g_iso"].k_guess,
                        self.meas_I[idx_m],
                    )
                    if (
                        G.nodes[i_node]["g_iso"].meas_mass_idx[idx_m]
                        not in self.identified_mass_idx_list
                    ):
                        self.identified_mass_idx_list.append(
                            G.nodes[i_node]["g_iso"].meas_mass_idx[idx_m]
                        )

        for idx_m in range(self.meas_len):
            if idx_m not in self.identified_mass_idx_list:
                self.unidentified_mass_idx_list.append(idx_m)


class CompoundNIST(Compound):
    """A class dedicated to NIST data that inherits from Compound.

    .. warning:: This class is not tested anymore.
        It is kept if someone wants to implement it again for integer masses
        spectra.
    """

    def __init__(self, one_batch_fragment: list, compound_number: int, LOD: float):
        # call superclass constructor
        super().__init__(one_batch_fragment, compound_number, LOD)

    def do_meas_mass_profile_x(self, m_start: float, m_stop: float, ppm_mass) -> None:
        """Initialize self.meas_mass_profile_x

        TODO Myriam what does this function do?: Creates high resolution x-axis on which discretized intensity is computed

        INPUT:
        - ``m_start``:
        - ``m_stop``:
        - ``ppm_mass``: TODO Myriam int or float?

        OUTPUT: None
        """
        mass_res = float(1.0)
        super().do_meas_mass_profile_x(m_start, m_stop, mass_res)

    def do_meas_mass_profile(self, delta_mass, i_spec):
        """Do measured mass profile

        INPUT:
        - ``delta_mass``: potential mass axis offset, at initial step it is zero.
          Used to shift position of measured masses by a constant offset.
          delta_mass is optimised by the lmfit algo.
          For unit-mass resolution data delta_mass is zero and therefore unused.
        """

        # indexes of measured masses within window range:
        self.meas_mass_profile_idx = [
            idx
            for idx in range(self.meas_len)
            if self.meas_mass[idx] >= self.meas_mass_profile_x[0]
            and self.meas_mass[idx] <= self.meas_mass_profile_x[-1]
        ]
        self.delta_mass_u = float(0.0)
        self.meas_mass_profile = np.zeros(len(self.meas_mass_profile_x))

        # for idx_m in range(len(self.meas_mass_profile_idx)):
        #    #here could do a while loop
        #    for idx_x in range(len(self.meas_mass_profile_x)):
        #        if self.meas_mass_profile_x[idx_x] == self.meas_mass[self.meas_mass_profile_idx[idx_m]]:
        #            self.meas_mass_profile[idx_x] += self.meas_I[self.meas_mass_profile_idx[idx_m]]

        idx_start = 0
        for idx_profile in range(len(self.meas_mass_profile_x)):
            idx = 0  # idx_start
            found = False
            while idx < len(self.meas_mass) and found == False:
                if round(self.meas_mass_profile_x[idx_profile]) == round(
                    self.meas_mass[idx]
                ):
                    self.meas_mass_profile[idx_profile] = self.meas_I[idx]
                    found = True
                    idx_start = idx
                else:
                    idx += 1

        # logging.info(str(self.meas_mass_profile_x)  + "  .meas_mass_profile  " + str(self.meas_mass_profile))

    def do_iso_profiles(self, iso_list_idx: list, isotopologues_list: list):
        """
        Generate a list, containing for each row a numpy array
        corresponding to the sum profile of one isotopologue series.
        All isotopologue series are in the same mass window.
        Uni mass spectrum: signal of all same round(mass) added up together.

        INPUT:
        -``iso_list_idx``:
        -``isotopologues_list``:
        """
        no_iso = len(iso_list_idx)
        self.iso_profiles = [None] * no_iso
        for i_no_iso in range(no_iso):
            iso_profile = np.zeros(len(self.meas_mass_profile_x))
            isotopologue = isotopologues_list[iso_list_idx[i_no_iso]]
            for i_iso_series in range(isotopologue.iso_list_len):
                iso_mass = chem_data.get_mass_single_ionised(
                    isotopologue.iso_list[i_iso_series]
                )
                # logging.info("iso_mass:  " + str(iso_mass))
                test_m = False
                idx_m = 0
                while idx_m < len(self.meas_mass_profile_x) and test_m == False:
                    if round(iso_mass) == self.meas_mass_profile_x[idx_m]:
                        iso_profile[idx_m] += isotopologue.iso_list_I_rel[i_iso_series]
                        test_m = True
                    idx_m += 1
            self.iso_profiles[i_no_iso] = iso_profile.copy()

        # logging.info(".iso_profiles  " + str(self.iso_profiles))


def plot_compund_peaks(compound: Compound, ax: plt.Axes | None = None) -> plt.Axes:
    """Plot a compound."""

    if ax is None:
        fig, ax = plt.subplots()
    ax.stem(compound.meas_mass, compound.meas_I, linefmt="C0-", markerfmt="C0o")
    ax.set_xlabel("m/z")
    ax.set_ylabel("Intensity")

    # Set the y axis to log scale
    ax.set_yscale("log")

    # Add the uncertainties of the masses
    for mass_min, mass_max in zip(compound.meas_mass_min, compound.meas_mass_max):
        ax.axvspan(mass_min, mass_max, alpha=0.2, color="C0")
    
    # Add a small text label with the exact mass at the top of the stems
    for mass, intensity in zip(compound.meas_mass, compound.meas_I):
        ax.text(mass, intensity, f"{mass:.4f}", ha="center", va="bottom")

    return ax