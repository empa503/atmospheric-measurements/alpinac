.. ALPINAC documentation master file, created by
   sphinx-quickstart on Wed Jun  8 09:59:30 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ALPINAC
=======

ALgorithmic Process for Identification of Non-targeted Atmospheric Compounds


Origninally presented in [Guillevic2021]_ , ALPINAC is a Python package
dedicated to the identification of new molecules.

The github repository is found at
https://gitlab.com/empa503/atmospheric-measurements/alpinac 




.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart 
   userguide
   api 
   data_extraction/index
   licence
   publications
   contact






Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
