Mass Calibration 
================

The raw TOF data gives the mass axis in terms of time-of-flight index. 
It has no unit and probably simply correspond to the detector cell number. 
But as cells are equally spaces, there is a linear relationship between the
time-of-flight index and the mass.

To convert this axis to mass, we need to calibrate it.


Theory 
------

Due to the nature of the instrument, the relationship betwee the distance inside 
the tof tube and the mass is quadratic.
Example of derivation is a usual exercise in physics and can be found on  
`wikipedia  <https://en.wikipedia.org/wiki/Time-of-flight_mass_spectrometry>`_ .

This leaves us with the following relationship:


.. math::

    m = a \cdot t^2 + b 

where
where :math:`m` is the mass, :math:`t` is the time of flight (or tof index in our case) 
:math:`a`` and :math:`b`` are the calibration constant parameters. 

The calibration is done by measuring the time of flight of a set of known fragments
and fitting the above equation to the data. 

In real case, this equation is a bit simple and can be improved by adding more 
parameters.

alpinac provides few different equations which can be determined by the 
`mass_cal_mode_index` parameter in the
`:py:class:~alpinac.mass_calibration.make_mass_calibration` function. 

Equations
---------

alpinac provides few different equations with more or less parameters.

The more parameters, the more complex the equation and the more flexible it is.
But it also means that more data is needed to determine the parameters and 
chances of overfitting are higher.

0 - Basic 
^^^^^^^^^

.. math:: tofid = p_1 \sqrt{m} + p_2 $ 


2 - Power parameter 
^^^^^^^^^^^^^^^^^^^

This one is similar to the basic one but the mass is not following a perfect square root.

.. math:: tofid = p_1 \cdot m^{p_3} + p_2 $


4 - quadratic correction term 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

More complex equation which has a correction term for the mass. 

.. math:: tofid = p_1 \sqrt{m} + p_2 + p_3  m^2 + p_4 m $




Mass drift 
----------

As the instrument is not perfect, the calibration can drift over time, mostly due to
temperature changes that can affect the length of the tof tube. 

To correct for this drift, the calibration can be performed at various time points
during the measurement.

Figure below shows the mass drift of the fragments used for calibration in GC-TOF-MS

.. image:: ./mass_drift.png
    :width: 600
    :align: center


