Non-targeted High Resolution Peak Extraction 
=============================== 

alpinac provides extraction routine of high resolution GC-MS peak. 

Non-targeted peak extraction is the process of extracting peaks from a 
GC-MS dataset without prior knowledge of the compounds or the masses we want to extract.

A GC-MS peak is defined by an `intensity` at a given `retention time` 
and a `mass to charge `values.
We can write: :math:`peak = (i, rt, m_z)`

In non targeted peak extraction, we want to extract all the peaks from the dataset.

Extraction Steps 
----------------

the alpinac extraction algorithm implemented in function
:py:func:`alpinac.mode_extraction.mode_extraction_nontarget.nontarget_peak_extraction`
works the following way:

1. Find peaks in the mass domain at all retention times 
2. Fit the mass peaks using a Pseudo-Voigt function to get high resolution `m/z` values 
3. Cluster the peaks in mz groups in different mz domains. 
4. Fit chromatographic functions on these chromatograms from these clusters.
5. Now we have the three peaks parameters, we want to assign them to compounds.
6. Cluster based on retention time to identify compounds. 
7. Export the peaks.


Parameters 
----------


Additional parameters can be given, such as the shape of the peak in 
different dimensions.

1. `RT`: Retention time in seconds 
2. `mass`: Measured mass, m/z
3. `mass_u_ppm`: Uncertainty of the measured mass (measurement noise), ppm, 1 sigma
4. `mass_cal_u_ppm`: Uncertainty of the mass calibration, ppm, 1 sigma
5. `area`: Measured signal intensity
6. `area_u`: Uncertainty of the measured intensity  
7. `peak_width`: Width of the mass peak, 1 sigma
8. `peak_alpha`: Fraction of Lorenzian peak shape, use zero if your peak is Gaussian.
9. `compound_bin`: Index of time bin, all co-eluting masses should have the same value.
10. `LOD`: Limit of detection for the measured mass  
11. `SN`: Ratio signal/noise 
12. `Ionisation`: Ionisation type, can be EI or CI
13. `Adduct`: The molecule that is used as adduct, required for CI ionization
14. `Spectrum_id`: Id of the belonging spectrum, (if more than one spectrum is analyzed)

