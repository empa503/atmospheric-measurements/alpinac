Data Extraction 
===============

To extract the data for alpinac, you need to follow the specific pipeline.


1. High Resolution mass calibration 
2. High Resolution peak extraction


By High resolution, we mean locating the peak with a precision of 10 to 20 ppm. 
To achieve this we need to fit peak functions to the data.
This allow is to obtain sub-tof-bin resolution. (so higher precison that the tof binning)


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mass_calibration
   extraction
