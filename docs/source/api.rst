
API 
===

.. automodule:: alpinac 
    :members:


.. autofunction:: alpinac.mode_identification.main.make_identification


.. autoclass:: alpinac.compound.Compound


Output Objects 
--------------

.. automodule:: alpinac.mode_identification.output_saver 
    :members:


Reporter
--------

.. automodule:: alpinac.reporter
    :members:



Data Extraction
--------------- 

.. automodule:: alpinac.mode_extraction.peaks
    :members: