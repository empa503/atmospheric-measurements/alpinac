Publications
============


This is a list of all the publications related to ALPINAC

.. 
    This file contains all the references to publications
    Please try to respect the exisiting format when adding new publications

    To cite the publicaitons form the doc you can simply write [Guillevic2021]_
    
.. [Guillevic2021]
    `hal-03176025 <https://hal.inria.fr/hal-03176025>`_,
    `arxiv 2103.13807 <https://arxiv.org/abs/2103.13807>`_.

    Automated fragment identification for electron ionisation mass spectrometry: application to atmospheric measurements of halocarbons


    *Myriam Guillevic, Aurore Guillevic, Martin Vollmer, Paul Schlauri, Matthias Hill, Lukas Emmenegger, Stefan Reimann.*

