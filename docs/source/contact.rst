
Contact
=======


`ALPINAC official repository <https://gitlab.com/empa503/atmospheric-measurements/alpinac/>`_
is managed by the
`Laboratory for Air Pollution / Environmental Technology <https://www.empa.ch/web/empa/air-pollution-/-environmental-technology>`_
of 
`Empa (Eidgenössische Materialprüfungs- und Forschungsanstalt) <https://www.empa.ch/>`_

The prefered way of contacting us is through the gitlab page: 
https://gitlab.com/empa503/atmospheric-measurements/alpinac/ .