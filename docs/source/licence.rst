
License
=======
ALPINAC source code is protected under a GNU GPLv3 licence.

Information about the licence can be found `here <https://www.gnu.org/licenses/quick-guide-gplv3>`_

The licence can be seen below, or is made available on `the official website <https://www.gnu.org/licenses/gpl-3.0.html>`_ 



.. include:: ../../LICENCE
   :literal: