# Documentation of ALPINAC

The ALPINAC documentation is based on [sphinx](https://www.sphinx-doc.org/en/master/index.html)

The files are written 
The API is automatically generated from [autodoc](https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#module-sphinx.ext.autodoc)


### Build the documentation
To build the documentation, follow the next steps

0. Install the requirements (see below)
1. Open a terminal in the `docs/` folder of alpinac (where this readme is)
2. Type `make html` on linux and `.\make.bat html` on windows to generate a web version of the doc.
3. The html files are genereated in `docs/build/html`
4. You can open the `docs/build/html/index.html` file in your web browser to be able to see the doc.

### Modify the documentation
The documentation is written in `reStructuredText`.

You can find [here](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) basic information in how to write in this language.

The `.rst` files can be found in the `docs/source` folder and 
the main file is `index.rst`.

If you want to see your modifications, you need to rebuild the documentation.


### Requirements
You will need to have python, sphinx and alpinac installed.

1. install alpinac as an editable version
go to the main alpinac directory (where the file setup.cfg is located)
and run :
    ```
    pip install -e .
    ```
2. install sphinx
    ```
    pip install sphinx
    ```

## Chapters of the documentation

This is there to explain what should go in each chapter.

### Quickstart 
Should be an example of how to run a simple example of the code.

### User Guides
Theses can be detailled examples of what the user can do with alpinac, 
or how alpinac works, explanation of the algorithms or anything else.

### API
This should contain all the functions that we want programmers or 
users of alpinac to call.

### Publications

This is for regroupping publications and other citations used in the 
doc.

