"""Script to get the data from https://zenodo.org/record/4471217#.Y7hHZ9WZM2x.

It then convert the data into alpinac inputs files.
"""
import os
import itertools
from pathlib import Path
import wget
from datetime import datetime

keys = [
    "RT",
    "mass",
    "mass_u_ppm",
    "mass_cal_u_ppm",
    "area",
    "area_u",
    "peak_width",
    "peak_alpha",
    "compound_bin",
    "LOD",
    "SN",
    "Ionisation",
]


def download_msp(dir: Path):
    link = "https://zenodo.org/record/4471217/files/RECETOX_GC-EI_MS_20201028.msp?download=1"
    filename = Path(dir) / "RECETOX_GC-EI-MS_20201028.msp"
    
    if not filename.parent.exists():
        filename.parent.mkdir(parents=True)

    if not filename.is_file():
        print("Downloading the file...")
        wget.download(link, str(filename))

    return filename


def parse_file(file: Path):
    """the file is a simple text file. Each return to line that is empty is a new fragment."""
    all_cmp={}
    out_dir = file.with_suffix("")
    if not out_dir.exists():
        out_dir.mkdir(parents=True)
    with open(file, "r") as f:
        lines = f.readlines()
    # make a counter
    it = itertools.count()
    new_cmp = lambda: {
        "name": f"Number_{next(it)}",
        "formula": f"_",
    } | {key: [] for key in keys}
    cmp = new_cmp()

    rt = 0
    line_id = 0
    while line_id < len(lines):
        line = lines[line_id]
        if line.startswith("NAME:"):
            name = line[6:-1]
            # clean the name for file 
            name = name.replace(" ", "_")
            name = name.replace("/", "_")
            name = name.replace("(", "_")
            name = name.replace(")", "_")
            name = name.replace(",", "_")
            name = name.replace(":", "_")
            name = name.replace(";", "_")
            name = name.replace("=", "_")
            name = name.replace("+", "_")
            name = name.replace("-", "_")
            name = name.replace("?", "_")
            name = name.replace("!", "_")
            name = name.replace("°", "_")

            cmp["name"] = name
            print(name)
        elif line.startswith("RETENTIONTIME:"):
            rt = line[14:-1]
        elif line.startswith("FORMULA:"):
            cmp["formula"] = line[9:-1]
        elif line.startswith("Num Peaks:"):
            # The next lines until blanks contain peak info
            while line != "\n":
                line_id += 1
                line = lines[line_id]
                if line != "\n":
                    # Split over tab and space
                    line_parts = line.split("\t")
                    #print(line_parts)
                    if len(line_parts) <= 1:
                        print("Error in line", line_id, 'of compound', cmp["name"], line)
                        continue
                    cmp["RT"].append(rt)
                    mass = float(line_parts[0])
                    intensity = float(line_parts[1])
                    cmp["mass"].append(mass)
                    # Take the smallest given digit for mass as error
                    number_of_digits = 5
                    abs_error = 10 ** (-number_of_digits)
                    error = abs_error /mass *  1e6  # To ppm
                    cmp["mass_u_ppm"].append(error)
                    cmp["mass_cal_u_ppm"].append(error)
                    cmp["area"].append(intensity)
                    cmp["area_u"].append(0)
                    cmp["peak_width"].append(0)
                    cmp["peak_alpha"].append(0)
                    cmp["compound_bin"].append(0)
                    cmp["SN"].append(0)
                    cmp["Ionisation"].append("EI")
            # Calculate some global properties arbitrarily
            #  TODO change that if it does not work ?  
            cmp['LOD'] = [a * 0.1 for a in cmp['area']]
            # Half gauss half lorentz
            cmp['peak_alpha'] = [0.5 for a in cmp['area']]
            cmp['peak_width'] = [m * 1e-6 for m in cmp['mass']]

            # Save the cmp
            save_cmp_dict(out_dir, cmp)
            all_cmp[name] = cmp
            cmp = new_cmp()


        line_id += 1

    return all_cmp


def save_cmp_dict(dir, cmp):
    """SAve to alpinac format"""
    with open(dir / f"{cmp['name']}__{cmp['formula']}.txt", "w") as f:
        f.write('\t'.join(keys).expandtabs(15) + "\n")
        for i in range(len(cmp["RT"])):

            line = "\t".join([str(cmp[key][i]) for key in keys]).expandtabs(15)
            f.write(line)
            f.write("\n")


if __name__ == "__main__":
    filename = download_msp("data/recetox_exposome")

    all_cmd = parse_file(filename)
    print(all_cmd.keys())
