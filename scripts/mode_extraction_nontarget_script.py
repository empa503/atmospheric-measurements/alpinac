"""Script for perfroming the extraction."""

import logging
from alpinac.mode_extraction.mode_extraction_nontarget import nontarget_peak_extraction



if __name__ == '__main__':
    logging.basicConfig()
    # logging.getLogger('alpinac').setLevel(logging.DEBUG)

    # nontarget_peak_extraction(
    #     path_file="path_to_h5_file.h5",
    #     rt_start_extract=900.00,
    #     rt_stop_extract=1100.0,
    #     #mass_start=mass_start,
    #     #mass_stop=mass_stop,
    #     mass_list=[35, 36],
    # )


    nontarget_peak_extraction(
        path_file='path_to_h5_file.h5',
        rt_start_extract=900.00,
        rt_stop_extract=1100.0,
        #mass_start=mass_start,
        #mass_stop=mass_stop,
        mass_list=[35, 36],
    )

