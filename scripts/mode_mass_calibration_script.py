"""Script for performing the mass calibration required for the peak extraction scripts."""
import logging
from datetime import datetime
from alpinac.mass_calibration.mode_mass_calibration import make_mass_calibration
from alpinac.io_tools_hdf5 import h5_files_from_to

logging.basicConfig()
#logging.getLogger('alpinac').setLevel(logging.DEBUG)




files = h5_files_from_to(
    directory=r"H:\TOF-DATA",
    from_dt=datetime(year=2022, month=1, day=1, hour=0),
    to_dt=datetime(year=2023, month=1, day=3),
)

dict_spikes = {}
# The fragments to use for the calibration
dict_spikes['0.0'] =[
    "CF3", 
    "C2F3",  "C2F4", 
    "C3F4", 
    "C4F5", 
    "C5F5", 
    "CF2","C3F3","CF", "C2F5", 
    "C3F5",
    # "C5NF10",
    "C3F6", "C3F7", "C4F7",
    "C4F9", 
    "C5F9", 
    "C6F9", 
    "C7F11", 
    "C8F9", 
    #"C6NF12", "C7NF14", "C8FN14", "C8NF16"
]  


for file in files:

    if file.with_suffix('.mc.txt').is_file():
        # Already exists, no need ot remake it 
        print("passing ", file, " because calibration already exists")
        continue

    print("processing", file)
    make_mass_calibration(
        path_file=file,
        #segments=(6,10),
        #segments=(1,2),
        #segments=(3,4),
        mass_cal_mode_index=2,
        dict_spikes=dict_spikes,
        #spike_duration=5,

    )  