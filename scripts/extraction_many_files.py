"""Script for perfroming extraction on many files using multiprocessing."""

import logging
from datetime import datetime
from multiprocessing import Pool

from alpinac.io_tools_hdf5 import h5_files_from_to
from alpinac.mode_extraction.mode_extraction_nontarget import nontarget_peak_extraction


def multi_proc_extract(file):
    print("processing", file)
    nontarget_peak_extraction(
        path_file=file,
        rt_start_extract=900.00,
        rt_stop_extract=1100.0,
        #mass_start=mass_start,
        #mass_stop=mass_stop,
        mass_list=[35, 36],
    )

if __name__ == '__main__':
    logging.basicConfig()
    # logging.getLogger('alpinac').setLevel(logging.DEBUG)

    # Will check all the files in this directory
    files = h5_files_from_to(
        directory=r"H:\TOF-DATA",
        from_dt=datetime(year=2022, month=1, day=1, hour=0),
        to_dt=datetime(year=2023, month=1, day=3),
    )

    # parallelize the extraction
    n_process = 2
    with Pool(n_process) as pool:

        pool.map(multi_proc_extract, files)
