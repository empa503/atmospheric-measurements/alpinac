"""Plot the peak from a fragment file."""

from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

files = list(Path(r"H:\TOF-DATA").glob("*.frag.1640s1670s.txt"))

fig, axes = plt.subplots(
    nrows=len(files),
    sharex=True,
    sharey=True,
    figsize=(10, 10),
    squeeze=True,
    gridspec_kw={
        "hspace": 0.0,
        "wspace": 0.0,
    },
)


for f, ax in zip(files, axes):
    df = pd.read_csv(
        f,
        # unknown number of spaces
        sep="\s+",
    )

    # errors_in_mass = 1e-6 * (df["mass_u_ppm"] + df["mass_cal_u_ppm"])

    data = df["area"]
    # Show the points with a colormap depending on the area

    points = ax.scatter(
        df["RT"],
        df["mass"],
        c=df["area"],
        norm=plt.Normalize(0, 5000),
    )

    ax.set_ylabel(f"{f.stem[:16]}\n mass")

axes[-1].set_xlabel("RT (s)")

    
# Add the color bar
cbar = fig.colorbar(points, ax=axes.ravel().tolist())

plt.show()
