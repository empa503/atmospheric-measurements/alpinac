# -*- coding: utf-8 -*-
"""Extraction code of chromatograms for specific masses.
"""

# %%

from enum import Enum, auto
import itertools
import time
import math
import numpy as np
import pandas as pd


import logging

from pathlib import Path

# import importlib

from multiprocessing import Pool, cpu_count, Process, Queue  # for parallel computing


from alpinac.io_tools_hdf5 import hdf5Metadata
from alpinac.periodic_table import (
    get_fragment_from_string_formula,
    get_mass_single_ionised,
)
from alpinac import const_peak_detection as const_pd
from alpinac.utils_data_extraction import f_mass_to_tofidx, f_tofidx_to_mass, fit_pseudoVoigt, peak_detect


class ExtractionMethod(Enum):
    FITTING = auto()
    NAIVE = auto()
    NAIVE_BACKGROUND = auto()

extraction_method = ExtractionMethod.NAIVE_BACKGROUND

logger = logging.getLogger(f"alpinac.{__name__}")
logging.basicConfig(level=logging.INFO)
logging.getLogger("alpinac").setLevel(logging.INFO)

# This will search tofid +- tofidx_domain_range
tofidx_domain_range: int = 6
mandatory_ranges = {
    ExtractionMethod.NAIVE: 0,
    ExtractionMethod.NAIVE_BACKGROUND: 2,
}
if extraction_method in mandatory_ranges:
    tofidx_domain_range = mandatory_ranges[extraction_method]



time_range = (1200, 2000)
segments = (0, 1)

# Make all the possible combinations of H, C and F containing from 0 to 4 of each 
# (H, C, F, HC, CF, HF, H2C, HFC, H2F, C2F, H3C, H3F, C3F, H4C, H4F, C4F, ...)
atoms = ['H', 'C', 'F', 'Cl', 'Br']

def generate_comp_list(atoms: list[str], max_atom_count: int) -> list[str]:
    comps = [""]
    for a in atoms:
        new_comps = [f"{c}{a}{i}" for i in range(1, max_atom_count + 1) for c in comps]
        comps.extend(new_comps)

#comps = generate_comp_list(atoms, 4)
comps = ['CF', 'CF3', 'C3H5', 'Br', '[81Br]', 'BrCH2', 'ClC2H2']

compound_masses: dict[str, float] = {
    cmp: mass
    for cmp in comps
    if  (
        (mass := get_mass_single_ionised(get_fragment_from_string_formula(cmp)))
        > 10.
    )
}

target_compounds: dict[str, tuple[float, float]] = {
    c: time_range for c in compound_masses
}

toffile_path = r"C:\Users\coli\Documents\EI_CI_2023\data\0713_std_comparision_EICI_vs_EI\230213.1817.std.7.h5"

# %%

toffile = Path(toffile_path).name
toffile_name = Path(toffile_path).stem
# logger.info(toffile_name)
toffile_suffix = Path(toffile_path).suffix

cl_run = hdf5Metadata(toffile_path, mode="r")


# **************************************
# ***LOAD MASS CALIBRATION PARAMETERS***
# **************************************
mass_cal = cl_run.mass_calibration_data["EI"]


t0 = time.time()

tofidx_spectrums = {}
results_list = []

# parallel computing: here separate the keys in group (make a list of keys to call?)
# and ship to the various CPUs...

for cmp, time_range in target_compounds.items():
    mass_suspect = compound_masses[cmp]
    # find indexes of candidate masses to screen in time domain:
    time_average = (time_range[0] + time_range[1]) / 2
    tofidx_float = f_mass_to_tofidx(
        mass_suspect, mass_cal.get_parameters_for_rt(time_average)
    )
    tofidx_int = int(round(tofidx_float))

    logger.info("********************************************")
    logger.info(
        f"{cmp}: Candidate unit mass:{mass_suspect:.4f}, TOF:{tofidx_float:.2f}"
    )

    tofid_start = tofidx_int - tofidx_domain_range
    tof_id_stop = tofidx_int + tofidx_domain_range + 1
    tofidx_series = np.arange(tofid_start, tof_id_stop, dtype=float)

    rt_idx_start = int(round(time_range[0] / cl_run.s_per_bin))
    rt_idx_stop = int(round(time_range[1] / cl_run.s_per_bin))
    rt_idx_list = np.arange(rt_idx_start, rt_idx_stop + 1, dtype=int)

    logger.info(
        f"Looking for fragments from {time_range[0]:.1f} to {time_range[1]:.1f} s"
    )

    

    tof_data = cl_run.import_hdf5_data(
        *time_range, *segments, tofid_start, tof_id_stop  # rt time, seconds
    )

    assert tof_data.shape[0] == len(
        rt_idx_list
    ), f"Expected {len(rt_idx_list)} time points, got {tof_data.shape[0]}"

    if extraction_method in [ExtractionMethod.NAIVE, ExtractionMethod.NAIVE_BACKGROUND]:
        if extraction_method == ExtractionMethod.NAIVE: 
            data = tof_data.reshape(-1)
        elif extraction_method == ExtractionMethod.NAIVE_BACKGROUND:
            data = (
                 tof_data[:,2] 
                 -tof_data[:,0] - tof_data[:,4] 
                + 0.5 * (tof_data[:,1] + tof_data[:,3])
            )
        
        results_list.append(
            pd.Series(
                data=data,
                index=pd.Index(
                    rt_idx_list * cl_run.s_per_bin,
                ),
                name=cmp,
            )
        )
        continue
        


    for scan_idx, idx_rt in enumerate(rt_idx_list):  # take one time slice
        # Get the mass spectra
        i_series = tof_data[scan_idx]
        i_series_sum = np.sum(i_series)

        if i_series_sum <= 10:
            # Too low signal, skip
            continue
        peak_detected_tofidx = peak_detect(
            tofidx_series,
            i_series,
            const_pd.WTtofidx,
            const_pd.PTtof,
            const_pd.tof_per_bin,
            graph=False,
        )

        if len(peak_detected_tofidx) == 0:
            continue
        mu_opt, A_opt, sigma_opt, alpha_opt, baseline_opt_pv = fit_pseudoVoigt(
            tofidx_series,
            i_series,
            peak_detected_tofidx[:, 0],
            peak_detected_tofidx[:, 1],
            mass_cal.alpha_pseudo_voigt,
            const_pd.tof_per_bin,
            const_pd.A_min_tofidx,
            mass_cal.sigma_tofidx_slope,
            mass_cal.sigma_tofidx_b,
            graph=False,
            mode="extraction",
        )

        for idx_peak in range(len(mu_opt)):
            
            mass = f_tofidx_to_mass(
                mu_opt[idx_peak], mass_cal.get_parameters_for_rt(idx_rt)
            )
            rt = idx_rt * cl_run.s_per_bin
            results_list.append(
                (
                    rt,
                    mass,
                    A_opt[idx_peak],
                    sigma_opt[idx_peak],
                    baseline_opt_pv,
                )
            )

if extraction_method == ExtractionMethod.FITTING:

    df_extraction = pd.DataFrame(results_list, columns=["rt", "mz", "A", "sigma", "baseline"])
    results_list = []
    for cmp, mass in compound_masses.items():
        mass_tol_ppm = 10 
        mass_tol = mass * mass_tol_ppm / 1e6

        mask_masses = (df_extraction['mz'] > mass - mass_tol) & (df_extraction['mz'] < mass + mass_tol)
        df_extraction[mask_masses]

        results_list.append(
            pd.Series(
                data=df_extraction[mask_masses]['A'].values,
                index=pd.Index(
                    df_extraction[mask_masses]['rt'].values,
                ),
                name=cmp,
            )
        )




df_results = pd.DataFrame(results_list).T

t1 = time.time()

logger.info(f"Data exported in {t1 - t0} s")

#%%

df_gcwerks_peaks = pd.read_csv(
    r"C:\Users\coli\Documents\EI_CI_2023\data\0918_coli_test_assign_peaks\test_peaks.csv",
    index_col=0,
)

# %%
%matplotlib qt
import matplotlib.pyplot as plt

fig, ax = plt.subplots(figsize=(10, 10))
masses_of_compounds = {v: k for k, v in compound_masses.items()}
sorted_masses = sorted(masses_of_compounds.keys())
for  mass in sorted_masses:
    cmp = masses_of_compounds[mass]
    serie = df_results[cmp]

    # Normalize the data
    serie /= serie.abs().max()
    ax.plot(serie.index, serie.values, label=f"{mass:.2f} {cmp}")

# Same as text color (depending on the style)
v_line_color = "white"
for rt, name in zip(df_gcwerks_peaks.index, df_gcwerks_peaks['name']):
    ax.axvline(rt, color=v_line_color, linestyle='--', alpha=0.5)
    ax.text(rt, 0.0, name, rotation=90, va='bottom', ha='center')


ax.set_ylim(-1, 1)

ax.legend()

# %%
