# -*- coding: utf-8 -*-

from cmath import nan
from enum import unique
import logging
import time
import matplotlib.pyplot as plt
#from pathlib import Path
import alpinac.const_path as const_path

#from MyClassRunParameters import PathParameters
#import periodic_table as chem_data
from alpinac.mode_identification.main_from_file import make_identification_from_file
from alpinac.utils_data_extraction import FittingMethod
import numpy as np

#cl_path = PathParameters()
#**************************************
#LOAD EMPA TOF RESULTS

#200714.2200.cal6L.7.frag.1551
#200714.2200.cal6L.7.frag.1509_Oin_Thorin_test2


#toffile_list = list(const_path.fragments.glob('[1,2]*.frag.val_*.txt'))
#toffile_list1 = list(const_path.fragments.glob('[1,2]*.frag.val_*.txt'))
#toffile_list = toffile_list0 + toffile_list1

toffile_list0 = list(const_path.fragments.glob('[1,2]*.frag.train_*.txt'))
toffile_list1 = list(const_path.fragments.glob('[1,2]*.frag.val_*.txt'))
toffile_list = toffile_list0 + toffile_list1

toffile_list = [(toffile_list0 + toffile_list1)[4]]

#toffile_list = list(const_path.fragments.glob('200602.1550.std.7.frag.train_C2H6.txt'))

#toffile_list = list(const_path.fragments.glob('190425.1731.tank.3.frag.val_HFC152a.txt'))
#toffile_list = list(const_path.fragments.glob('210617.0529.std.9.frag.1378s1385s.known.CFC113.txt'))

#toffile_list = list(const_path.fragments.glob('210623.1439.tank.3.frag.1339s1345s.known.HFC4310mee.txt'))
#toffile_list = list(const_path.fragments.glob('210621.1548.tank.3.frag.1702s1712s.known.PCBTF.txt'))

#toffile_list = list(const_path.fragments_as_known.glob('*.txt'))

#toffile_list = list(const_path.fragments.glob('200602.1550.std.7.frag.train_CFC113.txt'))
#toffile_list = list(const_path.fragments.glob('200602.1550.std.7.frag.train_C2Cl4.txt'))

#toffile_list = list(const_path.fragments.glob('200224.0907.tank.5.frag.val_TCHFB.txt'))

#for file in toffile_list:
#    logging.info(file.name)
#toffile_list = list(const_path.fragments.glob('210623.2029.tank.11.frag.search_flurane_1386s.txt'))

#toffile_list = list(const_path.fragments.glob('200714.2200.cal6L.7.frag.train_NF3_.txt'))


#toffile_list = list(const_path.fragments.glob('200128.1735.tank.5.frag.1490s_MrMiralago.txt'))
#toffile_list = list(const_path.fragments.glob('211105.0535.std.9.frag.1411s1425s.txt'))

#COS: 463-58-1-Mass.jdx
#toffile_list = list(const_path.NIST_EI.glob('123-91-1-Mass.jdx'))
#toffile_list = list(const_path.NIST_EI.glob('test_spectra_Katharina_Mass.jdx'))
toffile_list = list(const_path.NIST_EI.glob('*.jdx'))
#toffile_list = [toffile_list[17]]
fitting_method = FittingMethod.discrete
do_eval = True

if fitting_method == FittingMethod.discrete and do_eval == True:
    start_time = time.process_time()
    if len(toffile_list) == 0:
        raise ValueError('Error mode_identification_script.py line 28.\nNo file found. Check that path is correct and that filename is correct.')

    #write results of no nodes and runtime in results file
    method_performance_results = []
    method_performance_file = const_path.fragments / 'method_performance.txt'

    list_likelihood_true_frag = []
    list_likelihood_false_frag = []
    list_likelihood_true_att = []
    list_likelihood_false_att = []
    list_ranking_true_frag = []
    list_ranking_false_frag = []
    list_ranking_true_att = []
    list_ranking_false_att = []

    for path_file in toffile_list:
        #logging.info("Processing file {}".format(str(path_file)))
        filename = path_file.name

        # should be removed afterwards
        if 'train_' in filename or 'val_' in filename or 'known_' in filename:
            do_val = True
        else:
            do_val = False
            
        
        # go to one parent directory to find the directory "mass_spectra" where to save figures:
        fig_path = path_file.parents[1]/"mass_spectra"
        formulas_path = path_file.parents[1]/"formulas"

        chosen_list_frag_idx = [0] # index of chosen compound, zero by default chosen_list_frag_idx = None
        min_frag_idx = None #15
        target_elements = None #"HCFCl" #"CHFO" #list of chemical elements potentially present
        target_formula = None #"HC4OF7" #"CCl2F2" #formula of molecular ion, if present
        # choose one of:
        #make_identification(run_type, path_file, fig_path, formulas_path, fragment_indices=chosen_list_frag_idx, fragfile_name=fragfile_name)
                

        #method_performance_results_i, fragment_results, validation_dict_results = \
        make_identification_from_file(
            path_file = path_file,
            fragment_indices =chosen_list_frag_idx,
            min_fragment_index=min_frag_idx,
            target_elements = target_elements,
            target_formula = target_formula,
            show_plt_figures = False,
            output_dir = path_file.parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/discrete/",path_file.parts[-1].split(".")[-2]),
            fitting_method = fitting_method
            )
        
    #    for key, out in output.items():
    #        print(out)
    #        out.save('data/test_outputs/test_main')
        
    print("time for discrete spectra optimization")
    print(time.process_time() - start_time)
elif fitting_method == FittingMethod.continuous and do_eval == True:
    start_time = time.process_time()
    if len(toffile_list) == 0:
        raise ValueError('Error mode_identification_script.py line 28.\nNo file found. Check that path is correct and that filename is correct.')

    #write results of no nodes and runtime in results file
    method_performance_results = []
    method_performance_file = const_path.fragments / 'method_performance.txt'

    list_likelihood_true_frag = []
    list_likelihood_false_frag = []
    list_likelihood_true_att = []
    list_likelihood_false_att = []
    list_ranking_true_frag = []
    list_ranking_false_frag = []
    list_ranking_true_att = []
    list_ranking_false_att = []

    for path_file in toffile_list:
        #logging.info("Processing file {}".format(str(path_file)))
        filename = path_file.name

        if 'train_' in filename or 'val_' in filename or 'known_' in filename:
            do_val = True
        else:
            do_val = False
            
        
        # go to one parent directory to find the directory "mass_spectra" where to save figures:
        fig_path = path_file.parents[1]/"mass_spectra"
        formulas_path = path_file.parents[1]/"formulas"

        chosen_list_frag_idx = [0] # index of chosen compound, zero by default
        min_frag_idx = None #15
        target_elements = None #"HCFCl" #"CHFO" #list of chemical elements potentially present
        target_formula = None #"HC4OF7" #"CCl2F2" #formula of molecular ion, if present
        # choose one of:
        #make_identification(run_type, path_file, fig_path, formulas_path, fragment_indices=chosen_list_frag_idx, fragfile_name=fragfile_name)
                

        #method_performance_results_i, fragment_results, validation_dict_results = \
        make_identification_from_file(
            path_file = path_file,
            fragment_indices =chosen_list_frag_idx,
            min_fragment_index=min_frag_idx,
            target_elements = target_elements,
            target_formula = target_formula,
            show_plt_figures = False,
            output_dir = path_file.parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/continuous/",path_file.parts[-1].split(".")[-2]),
            fitting_method = fitting_method
            )
        
    #    for key, out in output.items():
    #        print(out)
    #        out.save('data/test_outputs/test_main')
        
    print("time for continuous spectra optimization")
    print(time.process_time() - start_time)
else:
    print("nothing to do")

# comparison, get process time of continuous spectra evaluation
output_dir = toffile_list[0].parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/continuous/")
runtimes_cont_files = list(output_dir.glob('*/Compound*/runtimes.log'))
opt_time_cont = [np.nan]*len(runtimes_cont_files)
tot_time_cont = [np.nan]*len(runtimes_cont_files)
for i in range(len(runtimes_cont_files)):
    f = open(runtimes_cont_files[i], 'r')
    while True:
        a = f.readline()
        if a=='': break
        a = a.split(":")
        if len(a)>1:
            var = a[0]
            val = a[1].strip()
            if var == "step5to8_optimization":
                opt_time_cont[i] = float(val)
            if var == "total":
                tot_time_cont[i] = float(val)
        


# get process time of discrete spectra evaluation
output_dir = toffile_list[0].parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/discrete/")
runtimes_disc_files = list(output_dir.glob('*/Compound*/runtimes.log'))
opt_time_disc = [np.nan]*len(runtimes_disc_files)
tot_time_disc = [np.nan]*len(runtimes_disc_files)
for i in range(len(runtimes_disc_files)):
    f = open(runtimes_disc_files[i], 'r')
    while True:
        a = f.readline()
        if a=='': break
        a = a.split(":")
        if len(a)>1:
            var = a[0]
            val = a[1].strip()
            if var == "step5to8_optimization":
                opt_time_disc[i] = float(val)
            if var == "total":
                tot_time_disc[i] = float(val)
            
# Draw comparison of timings
def f(x):
   return x
def hline(x, const_y):
    return [const_y]*len(x)
x = np.linspace(0, np.nanmax(opt_time_cont), 10)

fig, ax = plt.subplots(figsize=(1, 1))
ax.set_xlabel("Time for continuos fitting / ms")
ax.set_ylabel("Time for discrete fitting / ms")
plt.plot(opt_time_cont, tot_time_disc,">", color = 'lightgrey', mfc='none')
plt.plot(opt_time_cont, opt_time_disc,"x")
plt.plot(x, f(x), color='red')
plt.plot(x, hline(x, 0), color='grey')

fig, ax = plt.subplots(figsize=(1, 1))
ax.set_xlabel("Substance index")
ax.set_ylabel("Optimization time ratio for discrete/continuos fitting / %")
plt.plot(np.array(opt_time_disc)/np.array(opt_time_cont),"x")
plt.plot(x, hline(x, 1), color='red')

opt_time_disc[44]

#get 
#toffile_list1 = list(output_dir.glob('[1,2]*.frag.val_*.txt'))
#toffile_list = toffile_list0 + toffile_list1


#for path_file in toffile_list:
        #logging.info("Processing file {}".format(str(path_file)))
#        filename = path_file.name



#continuous
output_dir = toffile_list[0].parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/continuous/")
runtimes_cont_files = list(output_dir.glob('*/Compound*/most_likely_mol_ions.txt'))
len(runtimes_cont_files )
rank = []
form = []
dbe = []
score = []
ind_cont = []
for i in range(len(runtimes_cont_files)):
    f = open(runtimes_cont_files[i], 'r')
    while True:
        a = f.readline()
        if a=='': break
        a = a.split()
        #ignore line if text (e.g. header)
        if a[0].strip().isdigit():
            rank.append(int(a[0].strip()))
            form.append(a[1].strip())
            dbe.append(float(a[2].strip()))
            score.append(float(a[3].strip()))
            #assign
            ind_cont.append(i)

#discrete
output_dir = toffile_list[0].parents[0].parents[0].parents[0].joinpath("test_fitting_method_performance/discrete/")
runtimes_disc_files = list(output_dir.glob('*/Compound*/most_likely_mol_ions.txt'))
len(runtimes_disc_files)
rank_disc = []
form_disc = []
dbe_disc = []
score_disc = []
ind_disc = []
for i in range(len(runtimes_disc_files)):
    f = open(runtimes_disc_files[i], 'r')
    while True:
        a = f.readline()
        if a=='': break
        a = a.split()
        #ignore line if text (e.g. header)
        if a[0].strip().isdigit():
            rank_disc.append(int(a[0].strip()))
            form_disc.append(a[1].strip())
            dbe_disc.append(float(a[2].strip()))
            score_disc.append(float(a[3].strip()))
            #assign
            ind_disc.append(i)

#delete results which are only found in one data set
i = 0
while i < min(len(ind_disc),len(ind_cont)):
    if ind_disc[i] < ind_cont[i]:
        print(f"remove {i} from disc")
        rank_disc.pop(i)
        ind_disc.pop(i)
        form_disc.pop(i)
        dbe_disc.pop(i)
        score_disc.pop(i)
    elif ind_disc[i] > ind_cont[i]:
        print(f"remove {i} from cont")
        rank.pop(i)
        ind_cont.pop(i)
        form.pop(i)
        dbe.pop(i)
        score.pop(i)
    else:
        i = i+1
    





len(ind_cont)
len(ind_disc)
len(score)
len(score_disc)


same_score = [i for i in ind_cont if not score_disc[i] == score[i]]
different_score = [i for i in ind_cont if score_disc[i] == score[i]]
np.array(score_disc) - np.array(score)
len(np.array(score_disc))
len(np.array(score))

# for i = 27 there is a difference
ind[26]
runtimes_disc_files[ind_disc[26]]
