# -*- coding: utf-8 -*-
"""
Created on Mon May 17 15:56:21 2021

@author: mygu

aim: convert "alpinac" fragment file format 
to MOLGEN-MS compatible format.

2022-12-23: coli
By cleaning the compund classes this will be unusable.
One interested should be careful when replacing parts of the code.
"""
import logging
from pathlib import Path
from alpinac.io_tools import make_dict_of_paths, get_data_from_frag_file
from alpinac.compound import CompoundTofwerkTOF

import alpinac.const_identification as const_id
from alpinac.validation.validation_utils import find_validation_idx_from_database
from alpinac.validation.mol_validation import mol_validation
import alpinac.periodic_table as chem_data

dict_path = make_dict_of_paths()

toffile_list = list(dict_path['fragments'].glob('[1,2]*.frag.val_*.txt'))

for path_file in toffile_list:
    #logging.info("Processing file {}".format(str(path_file)))
    toffile_name = path_file.stem # the filename without extension (.txt)
    logging.info("toffile_name = {}".format(toffile_name))
    if '.frag.' in toffile_name:
        fragfile_name = toffile_name.split('.')[5]
        logging.info(fragfile_name)
        
        idx_val = find_validation_idx_from_database(fragfile_name, mol_validation)
        
    batch_fragments = get_data_from_frag_file(path_file)
    
    # get the keys of the dictionary that are the fragment indices:
    list_frag_idx = [i for i in batch_fragments if i != -1]
    #list_frag_idx = [i for i in fragment_indices if i in list_frag_idx]
    for compound_number in list_frag_idx:
        #logging.info("==== Initialise Compound")
        cl_comp = CompoundTofwerkTOF(batch_fragments[compound_number], compound_number, const_id.m_u_k, const_id.LOD)
        #const_id_min_detect_mass = min(min(cl_comp.meas_mass), const_id.min_detect_mass)
        mol_sum_formula = mol_validation[idx_val]["Sum formula"]
        mol_frag = chem_data.get_fragment_from_string_formula(mol_sum_formula)
        str_sum_formula = chem_data.get_string_formula(chem_data.get_fragment_from_string_formula(mol_sum_formula))
        comp_name = mol_validation[idx_val]["Aliases"][0]


        no_different_atoms = sum([1 for val in mol_frag if val>0])
        
        #cl_comp.meas_mass
        #cl_comp.meas_I
        
        #order by increasing mass
        #write nb of peaks
        
        #write file with format compatible with TRANSPEC-file, "file.TRA"
        
        molgen_filename = str(comp_name) + ".TRA"
        
        #out_formula_file_name = toffile_name + '.comp.' + '{:.2f}'.format(rt_average) + '_' + str(compound_number) + '.txt'
        path_molgen = Path("D:/HALOSEARCH/MOLGEN-MS/MolgenMS/MolgenMS/tra")
        out_file = path_molgen / molgen_filename
        #with out_file.open('w') as f:
        #    f.write('Run file: ' + toffile_name + '\n')

        #out_formula_file_name = 'formula_' + str(fragment_index) + '_' + str(filename)
        #out_formula_file_name = toffile_name + '.comp.' + str(cl_comp.fragment_index) + '.' + '{:.2f}'.format(cl_comp.average_rt) + '.txt'
        #out_formula_path = cl_path.dict_path['formulas']

        #file = out_formula_path / out_formula_file_name
        #with out_file.open('w') as out_formula_file:
        #with out_file.open('a') as f:
        with out_file.open('w') as f:
            #out_formula_file.write('#############################################\n')
            #out_formula_file.write('Largest detected fragment series:'  +  '\n')
            #out_formula_file.write('Formula' + '\t' + 'Likelihood indicator (%)' + '\t' + 'DBE' +  '\n')
            
            
            
            f.write("/IDENT     1    0" + "\n")
            f.write("                        101" + "\n")
            #f.write("/FORMUL    1    3" + "\n") # + str(int(no_different_atoms)))
            #str_molgen_compatible = []
            
            #f.write(" C   12    6 H    1   12 O   16    2" + "\n")
            #f.write("/NAME      1   30" + "\n")
            #f.write(" N-PENTANOIC ACID, METHYL ESTER" + "\n")
            f.write("/PEAKS     "+ str(int(cl_comp.meas_len))+ "   " + str(int(cl_comp.meas_len)) + "\n")
            for i_p in range(cl_comp.meas_len):
                #f.write("{:.1f}".format(cl_comp.meas_mass[i_p]) + "  " + "{:.2f}".format(100.0*cl_comp.meas_I[i_p]/cl_comp.meas_I_max) + "\n")
                f.write("{:.1f}".format(int(round(cl_comp.meas_mass[i_p]))) + "  " + "{:.2f}".format(100.0*cl_comp.meas_I[i_p]/cl_comp.meas_I_max) + "\n")
                
            f.write("/EOR       0    0")
