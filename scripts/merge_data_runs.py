"""Very simple script to sum the data from different runs.

This works only if the data is perfectly superimposable, which is a strong
assumption.
"""
#%% 

import h5py
from pathlib import Path
import shutil
import logging

logger = logging.getLogger("alpinac.merge_data_runs")

files_dir = Path(r"/files/directory")

files  = [
    '230228.0143.air.1.h5', 
    '230228.0013.air.1.h5', 
    '230227.1943.air.1.h5',
    '230228.0313.std.7.h5',
]
files = [files_dir / f for f in files]


outfile = files_dir / "summed_data.h5"

# %%
# Read the data, check here the sahpe of the data before summing
dim0 = None
for file in files:
    #h5py_file = h5py.File(file, "r")
    with h5py.File(file, "r") as h5py_file:
        print(file.stem, h5py_file['FullSpectra/TofData'])
        # Get the dimension 
        this_dim0 = h5py_file['FullSpectra/TofData'].shape[0]
        if dim0 is None:
            dim0 = this_dim0
        else:
            if dim0 != this_dim0:
                logger.error(f"{file.stem} has a different shape than the other files.")
            if this_dim0 < dim0:
                raise ValueError(
                    "Please sort your files by increasing size of dim0."
                )
                
                


#%%%
data = None
for file in files:
    #h5py_file = h5py.File(file, "r")
    with h5py.File(file, "r") as h5py_file:
        print(h5py_file['FullSpectra/TofData'])
        # Get the data 
        if data is None:
            data = h5py_file['FullSpectra/TofData'][()]
        else:
            if dim0 != this_dim0:
                indexer = slice(None, data.shape[0])
            else:
                indexer = slice(None)
            data += h5py_file['FullSpectra/TofData'][indexer]
# %%


# copy a file as the output file
shutil.copyfile(files[0], outfile)

# Write the summed data to the output file
with h5py.File(outfile, "a") as h5py_file:
    h5py_file['FullSpectra/TofData'][()] = data



# %%
