"""Script for running the identification on peaks files."""
import logging

#from MyClassRunParameters import PathParameters
#import periodic_table as chem_data
from alpinac.mode_identification.main_from_file import make_identification_from_file


#logging.info("Processing file {}".format(str(path_file)))




#logging.basicConfig(level=logging.DEBUG, handlers=[logging.StreamHandler()])
logging.getLogger('alpinac').setLevel(logging.DEBUG)
    

#method_performance_results_i, fragment_results, validation_dict_results = \
output = make_identification_from_file(
    path_file = r"C:\Users\coli\Documents\EI_CI_2023\data\0601_coli_validates_alpinac_on_know_substances\validation_extracted\for_blendi\frags_minmassu10ppm_HCFC-141b.csv",
    compounds=[2],
    )
