#%%
from alpinac.utils_data_extraction import f_pseudoVoigt, f_chromato, f_2d_peak
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import simpson


#%%
if __name__ == "__main__":
    rt = np.linspace(0, 10, num=1000)

    A = 20
    center = 5
    alpha = 0
    y = f_pseudoVoigt(rt, A, center, 0.2, alpha)
    #%%
    simpson(y, rt)
    #%%
    plt.plot(rt, y)
    plt.hlines(A, rt[0], rt[-1], label="Amplitude")
    plt.vlines(center, 0, A, label="Center")
    plt.legend()
    plt.show()
    # %%

    H = 20
    alpha = 0.7
    sigma = 2
    mz = np.linspace(0, 10, num=100)
    y = f_chromato(mz, H, center, sigma, alpha)
    plt.plot(mz, y)
    plt.hlines(H , mz[0], mz[-1], label="H ")
    plt.vlines(center, 0, H , label="Center")
    plt.legend()
    plt.show()
    # %%

    # Fit now in 2d 

    array = f_2d_peak(
        rt=rt.reshape((1,-1)),
        mz=mz.reshape((-1,1)),
        center_rt = 4,
        center_mz = 7,
        amplitude=20,
        sigma_rt=2,
        sigma_mz=1,
        asymetry_rt=0.7,
        alpha_voigt=1.,
        background=0,
    )
    # %%


    plt.imshow(
        array,
        # Make the plot look square even if distorted
        aspect="auto",
        # add the real axes as ticks
        extent=[rt[0], rt[-1], mz[-1], mz[0]],
    )
    plt.colorbar()
    plt.ylabel("m/z")
    plt.xlabel("rt")
    plt.show()
# %%
