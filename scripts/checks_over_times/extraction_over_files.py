import logging
from alpinac.mode_extraction.mode_extraction_nontarget import nontarget_peak_extraction, IonizationMethod, Plots
from alpinac.mode_identification.main_from_file import make_identification_from_file


logging.basicConfig()
logging.getLogger("alpinac").setLevel(logging.INFO)

out_file = nontarget_peak_extraction(
    path_file = #"E:\\TOF-Data\\2022\\22*.h5",  
    #E:\\TOF-Data\\2022\\221006.1503.tank.11.h5",
    "E:\\TOF-Data\\2024\\2410*.h5",
    rt_start_extract = 1640,                              
    rt_stop_extract = 1645,                                                         
    mass_list = [133.0083],
    save_plots=[Plots.CHROMATOGRAPHY_FITTING, ],
    outfile_name = f"export_2410_HFC-245cb_sus3.txt",
    mz_domain_range=0.1,
    outfile_path = r"C:\Users\lab134\Documents\Data\Alpinac\ExtractionManyFiles"
    #rt_sigma_guess_scale=5 
    )
