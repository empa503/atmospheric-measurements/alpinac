
#%%
# Create a netcdf file for mzmime

# Once you import the data, you will need to run a mass detection 
# then you can run a spectral deconvolution
from pathlib import Path
import math
import matplotlib.pyplot as plt
import numpy as np

from alpinac.io_tools_hdf5 import hdf5Metadata
from alpinac.utils_data_extraction import (
    interpol_mass_cal,
    f_mass_to_tofidx,
    f_tofidx_to_mass,
)
import xarray as xr 
#%%
mass_int = (20, 200)
interval_rt = (1200, 2220)
# Limit of detection for the color plot
lod  = 10 
file = r"path/to/file.h5"

# From start_to_end (end is not included)
seg_range = (0,1)

path_file = Path(file)

cl_run = hdf5Metadata(path_file, mode="r")
mass_calibration_data = cl_run.mass_calibration_data['EI']

mass_calibration_params = interpol_mass_cal(
    (interval_rt[0] + interval_rt[1]) / 2,
    mass_calibration_data.mass_cal_parameters,
    mass_calibration_data.mass_cal_mode,
)

mz_domain_range = 0.5
tofidx_int_start = int(
    f_mass_to_tofidx(mass_int[0] - mz_domain_range, mass_calibration_params)
)
tofidx_int_stopp = math.ceil(
    f_mass_to_tofidx(mass_int[1] + mz_domain_range, mass_calibration_params)
)
# we make sure we are not outside tofidx domain.
tofidx_int_start = max(0, tofidx_int_start)
tofidx_int_stopp = min(tofidx_int_stopp, cl_run.len_mass_axis - 1)


tof_data = cl_run.import_hdf5_data(
    *interval_rt,  # rt time, seconds
    *seg_range,
    tofidx_int_start,
    tofidx_int_stopp + 1,
)

rt_axis = np.linspace(*interval_rt, num=tof_data.shape[0])
tofidx_series = np.arange(tofidx_int_start, tofidx_int_stopp + 1, dtype=float)
mass_axis = f_tofidx_to_mass(tofidx_series, mass_calibration_params)
#%%

# Teh data has to be flatted 
intensity_values = tof_data.reshape(-1)
n_bins = len(intensity_values)

# The mass of each bin has to be given 
mass_values = np.tile(mass_axis, tof_data.shape[0])

# Scan index tells where a new scan starts
# its size is the number of scans
scan_index = np.arange(tof_data.shape[0]) * tof_data.shape[1]
scan_acquisition_time = rt_axis
#%%

ds = xr.Dataset(
    {
        "intensity_values": ('n_bins', intensity_values, {'scale_factor': 1.}),
        "mass_values": ('n_bins', mass_values, {'scale_factor': 1.}),
        "scan_index": ('n_scans', scan_index),
        "scan_acquisition_time": ('n_scans', scan_acquisition_time * 60),
    },
)

ds.to_netcdf(path_file.parent / f"{path_file.stem}.gcms.nc")
# %%
