"""Script to run the knapsack algorithm on a frag file.

It prints out the results of the knapsack for all the different fragments and 
compouunds in the file.
"""
from pathlib import Path
import numpy as np
import pandas as pd
import alpinac.periodic_table as chem_data
from alpinac.compound import Compound
from alpinac.io_tools import get_data_from_frag_file
from alpinac.utils_identification import define_knapsack_target, knapsack_double


frag_file = Path("path/to/frag/file")

batch_frag = get_data_from_frag_file(frag_file)


all_fragments = np.asarray(sum(batch_frag.values(), []))
idx_list_kn, max_no_each_atom = define_knapsack_target(
    mass_max=all_fragments[:, 1].astype(float).max(),
    target_atoms="HBCNOFSiPSClBrI",
    target_formula=None,
)

columns = [
    "mass",
    "mass_u",
    "knapsack_solutions",
]

# target_mass = 24.0
for comp_bin in batch_frag:
    frag_data = batch_frag[comp_bin]
    cl_comp = Compound(frag_data, comp_bin)

    df = pd.DataFrame(columns=columns)
    for idx_m in range(cl_comp.meas_len):
        number_solutions, valid_DBE_solutions, list_solutions = knapsack_double(
            cl_comp.meas_mass[idx_m],
            cl_comp.meas_mass_max[idx_m],
            cl_comp.meas_mass_min[idx_m],
            idx_list_kn,
            max_no_each_atom,
        )

        df.loc[idx_m] = [
            cl_comp.meas_mass[idx_m],
            cl_comp.meas_mass_u[idx_m],
            [chem_data.get_string_formula(sol) for sol in list_solutions],
        ]
    #    ctr_target_mass += count_frags_with_target_mass(list_solutions, target_mass)
    print(30 * "*")
    print(f"compound {comp_bin}")
    print(30 * "*")
    for i in range(cl_comp.meas_len):
        print(f'frag {i}: mass = {df["mass"][i]} +- {df["mass_u"][i] } m/z')
        print(f'knapsack solutions: {df["knapsack_solutions"][i]}')
