"""Validation of the data."""
import logging
import matplotlib.pyplot as plt


from alpinac.validation.validation_utils import validation, find_frag_file

from alpinac.validation.mol_validation import mol_validation
import alpinac.periodic_table as chem_data

if __name__ == "__main__":
    
    logging.info("molecules of validation ({})".format(len(mol_validation)))
    # for each substance of the validation set (~38 substances)
    for mol_tuple in [mol_validation[30], mol_validation[34]]:
        #for mol_tuple in mol_validation:
        smiles = mol_tuple["SMILES code"]
        mol_name = mol_tuple["Sum formula"]
        cmp_name = mol_tuple["Compound"]
        str_sum_formula = chem_data.get_string_formula(chem_data.get_fragment_from_string_formula(mol_name))
        filename = find_frag_file(cmp_name)

        validation(smiles, mol_name, filename, draw_graph=True) # CCl4:30, C2Cl4:34
    #logging.info("molecules of tests ({})".format(len(mol_test)))
    #validation(mol_test)
    #logging.info("molecules of validation HFO ({})".format(len(mol_validation_HFO)))
    #validation(mol_validation_HFO)
    #logging.info("molecules of validation anaesthetics ({})".format(len(mol_validation_anaesthetics)))
    #validation(mol_validation_anaesthetics)


