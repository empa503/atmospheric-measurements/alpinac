from alpinac.mass_calibration.utils import (
    f_tofidx_to_mass,
    f_mass_to_tofidx,
    MassCalParams,
)
import numpy as np

# Independent varaibles for testing that function works
# They do not correspond ;)
test_tofids = np.linspace(1, 40000)  # Avoid 0
test_masses = np.linspace(10, 400)


def from_idx_to_mass_and_back(mass_cal_params: MassCalParams):
    masses = f_tofidx_to_mass(test_tofids, mass_cal_params)
    tofids_back = f_mass_to_tofidx(masses, mass_cal_params)
    assert np.allclose(test_tofids, tofids_back)


def test_f_tofidx_to_mass():
    from_idx_to_mass_and_back(
        MassCalParams(mode=0, p1=3000.0, p2=-200.0, p3=0.0049),
    )


def test_f_tofidx_to_mass():
    from_idx_to_mass_and_back(MassCalParams(mode=1, p1=3000.0, p2=-200.0, p3=0.0049))


def test_f_tofidx_to_mass():
    from_idx_to_mass_and_back(MassCalParams(mode=2, p1=3000.0, p2=-200.0, p3=0.0049))


# Currently fails, becaause of numerical issues
# def test_f_tofidx_to_mass_method_3():
#    from_idx_to_mass_and_back(
#        MassCalParams(mode=3, p1=3000.0, p2=-200.0, p3=-0.00001, p4=23.0)
#    )


def test_f_tofidx_to_mass_method_4():
    from_idx_to_mass_and_back(
        MassCalParams(
            mode=4,
            p1=3000.0,
            p2=-200.0,
            p3=-0.00001,
            p4=23.0,
            p5=0,
        )
    )


def test_f_tofidx_to_mass_method_5():
    from_idx_to_mass_and_back(MassCalParams(mode=5, p1=3000.0, p2=-200.0, p3=0.0049))
