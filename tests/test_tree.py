# -*- coding: utf-8 -*-
"""
Test that the graph is generated correctly for a given list of fragments.
"""
import unittest
import matplotlib.pyplot as plt
import networkx as nx

import alpinac.periodic_table as chem_data
from alpinac.periodic_table import get_string_formula, get_fragment_from_string_formula

from alpinac.plot_figures import plot_forest_of_frag_with_pydot, plot_graph_with_pydot

from alpinac.utils_graph import (
    add_nodes_and_edges_to_graph_from_list_fragments,
    set_str_formula_and_attribute_for_pydot,
    node_edge_labels_colors_for_pydot,
)
from alpinac.utils_graph import (
    compute_all_subfragments_of_nodes,
    compute_all_subfragments_of_nodes_with_lists,
)


class TestUtilsIdentification(unittest.TestCase):
    def test_tree(self, plot: bool = False):
        list_frag_str = [
            "CF",
            "Cl",
            "HCl",
            "CCl",
            "OS",
            "FCl",
            "CFCl",
            "H3S2",
            "HCFCl",
            "CCl2",
            "FS2",
            "OSCl",
            "HCCl2",
            "OCl2",
            "CFCl2",
            "H3S2Cl",
            "F2S2",
            "HCNBr",
            "C3Cl2",
            "CCl3",
            "FS2Cl",
            "OSCl2",
            "HCCl3",
            "C2S3",
            "CSBr",
            "C2OS3",
            "HC3S2Cl",
            "H2S2Cl2",
            "H2O2SCl2",
            "HNFSCl2",
            "HC2SBr",
            "HC2O2Br",
            "C2NFBr",
            "CFCl3",
            "CNOS3",
            "H4S2Cl2",
            "HF2S2Cl",
            "HOFSCl2",
            "HCNSBr",
            "C3SCl2",
            "H3C2SBr",
            "HCNO2Br",
            "C2OFBr",
            "CN2FBr",
        ]

        list_frag = [
            get_fragment_from_string_formula(frag_str) for frag_str in list_frag_str
        ]
        list_frag.reverse()

        sample_expected_color_values = {
            137.92234019480003,
            137.9103703842,
            137.9170769419,
            137.91259842760002,
            137.9136522397,
            66.96706797569999,
            65.96670729,
            53.96670729,
            35.9761291619,
            34.96830413,
            30.99785458,
        }
        sample_expected_max_nodes = {
            "CN2FBr",
            "HCNO2Br",
            "H3C2SBr",
            "HCNSBr",
            "HF2S2Cl",
            "H4S2Cl2",
            "CFCl3",
            "C2NFBr",
            "HNFSCl2",
            "H2O2SCl2",
            "HC3S2Cl",
            "C2OS3",
            "HCCl3",
        }

        # generate graph
        G3 = nx.DiGraph(maximal_nodes=[])
        add_nodes_and_edges_to_graph_from_list_fragments(G3, list_frag)

        max_nodes = {str(G3.nodes[n]["node_lite"]) for n in G3.graph["maximal_nodes"]}

        self.assertEqual(
            max_nodes, max_nodes | sample_expected_max_nodes
        )  # assert that max_nodes contains the elements sample_expected_max_nodes

        min_m = 0.0
        compute_all_subfragments_of_nodes_with_lists(G3, min_measured_mass=min_m)
        compute_all_subfragments_of_nodes(G3, min_measured_mass=min_m, check=True)

        for node_id in G3.nodes():
            node = G3.nodes[node_id]["node_lite"]
            with self.subTest(str(node)):
                self.assertEqual(
                    node.no_all_subfragments, node.no_all_subfragments_check
                )

        node_labels, color_values, edge_labels = node_edge_labels_colors_for_pydot(G3)
        node_labels_set = set(node_labels.values())

        self.assertEqual(
            node_labels_set, node_labels_set | set(list_frag_str)
        )  # assert that node_labels_set contains the elements list_frag_str
        nodes_indexes = list(G3.nodes)
        sample_expected_edge_labels = self.__get_sample_expected_edges(nodes_indexes)

        self.assertEqual(edge_labels, edge_labels | sample_expected_edge_labels)
        self.assertEqual(
            set(color_values), set(color_values) | sample_expected_color_values
        )  # assert that color_values contains the elements sample_expected_color_values

        if plot:
            plot_graph_with_pydot(G3, node_labels, edge_labels, color_values)

    def __get_sample_expected_edges(self, nodes_indexes: list):
        """
        Generate a sample of expected edges with expected labels.

        Since the indexing of the nodes may change during the tests, this help
        method reindex the expected edge with the correct node indexed found
        in nodes_indexes.

        Parameters
        ----------
        nodes_indexes : list
            The correct indexes of the nodes in the graph.

        Returns
        -------
        dict
            Dictionary of the reindexed sample of expected edges with their labels.
        """
        sample_expected_edge_labels = {
            (0, 43): "N2Br",
            (1, 43): "COBr",
            (2, 26): "O2",
            (3, 13): "H2",
            (10, 29): "Cl",
            (11, 43): "CNBr",
            (13, 19): "HC",
            (14, 38): "HNSCl",
            (28, 36): "Cl",
            (28, 41): "H2S2",
            (29, 34): "F",
            (29, 37): "Cl",
            (30, 42): "OCl",
        }
        reindex_sample_expected_edge_labels: dict = {}
        for edge in sample_expected_edge_labels:
            reindex_sample_expected_edge_labels[
                (nodes_indexes[edge[0]], nodes_indexes[edge[1]])
            ] = sample_expected_edge_labels[edge]
        return reindex_sample_expected_edge_labels


if __name__ == "__main__":
    unittest.main()
