"""End to end test on the mode identification."""

import time
import unittest
from pathlib import Path
from unittest.mock import Mock, call, patch

import numpy as np

from alpinac.compound import Compound
from alpinac.mode_identification.compound_identification import \
    compound_identification
from alpinac.mode_identification.output_saver import (
    IdentificationOutput, IdentificationTookTooLongOutput)
from alpinac.utils_data_extraction import FitModes, FittingMethod


def _mocked_compound_identification(
    cl_comp,
    target_elements,
    target_formula,
    min_measured_mass,
    show_plt_figures,
    do_validation,
    expected_compound_name,
    max_opt_signal,
    fitting_method,
):
    """
    Mocks the method `compound_identification`.

    The method does nothing (because we don't really need to know
    what `compound_identification` does), but we want to
    test what happens if `compound_identification` would
    take too long. For this reason, if `target_elements` is `wait`,
    the method sleeps for 4 seconds.
    """
    if target_elements == "wait":
        time.sleep(2)

    return IdentificationOutput()


class TestEndToEnd(unittest.TestCase):
    @patch(
        "alpinac.mode_identification.compound_identification.compound_identification"
    )
    def test_compound_identification_limited_time(self, mocked_compound_identification):
        """
        Checks that `compound_identification` calls
        `compound_identification` with the right parameters.
        """
        mocked_compound_identification.side_effect = _mocked_compound_identification
        target_elements = "HO"
        target_formula = "H2O"
        min_measured_mass = 4.2

        with patch("alpinac.compound.Compound") as compound:
            compound_identification(
                compound,
                target_elements,
                target_formula,
                min_measured_mass,
                compound_identification_function=mocked_compound_identification,
            )
            mocked_compound_identification.assert_called_with(
                compound,
                target_elements,
                target_formula,
                min_measured_mass,
                False,
                False,
                None,
                95.0,
                FittingMethod.continuous,
            )

    @patch(
        "alpinac.mode_identification.compound_identification.compound_identification"
    )
    def test_compound_identification_crossing_time_limit(
        self, mocked_compound_identification
    ):
        """
        Checks that `compound_identification` stops when
        `compound_identification` takes too long and that it returns the
        right output (which is an instance of the class
        `IdentificationTookTooLongOutput`).
        """
        mocked_compound_identification.side_effect = _mocked_compound_identification
        mocked_compound_identification.__name__ = "_mocked_compound_identification"
        target_elements = "wait"
        timeout_time = 1
        target_formula = "H2O"
        min_measured_mass = 4.2
        with patch("alpinac.compound.Compound") as compound:
            output = compound_identification(
                compound,
                target_elements,
                target_formula,
                min_measured_mass,
                timeout_time=timeout_time,
                compound_identification_function=mocked_compound_identification,
            )
        self.assertTrue(isinstance(output, IdentificationTookTooLongOutput))


if __name__ == "__main__":
    unittest.main()
