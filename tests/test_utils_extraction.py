import pytest
from alpinac.utils_data_extraction import find_nearest
import numpy as np


def test_find_nearest():

    references = np.array([1, 1.5, 2, 3, 4, 5])

    indexes, distances = find_nearest(
        references, [1.2, 0.7, 3.2, 4.4, 4.6, 5.5, 1.3], return_distance=True
    )

    np.testing.assert_array_equal(indexes, [0, 0, 3, 4, 5, 5, 1])
    np.testing.assert_array_almost_equal(distances, [0.2, 0.3, 0.2, 0.4, 0.4, 0.5, 0.2])


def test_find_nearest_not_sorted_raises():

    references = np.array([1, 2, 1.5])

    with pytest.raises(ValueError):
        find_nearest(references, [0, 1])
