"""
Simple tests to check the basic uses of the main methods in `reporter.py`.
"""
import logging
from pathlib import Path
import unittest
from unittest.mock import Mock, call, patch
import json
import pandas as pd

from alpinac.reporter import (
    _identify,
    _make_identification_from_file_adapter,
    generate,
    investigate,
    read,
    run,
)


class TestReporter(unittest.TestCase):
    def setUp(self) -> None:
        self.folder = Path(__file__).parent / "test_reporter"
        self.reporter_input_path = self.folder / "reporter_input.json"
        self.reportage_path = self.folder / "reportage.csv"
        logging.basicConfig(level=logging.ERROR)
        logger = logging.getLogger("alpinac.reporter")
        logger.setLevel(logging.ERROR)

    @patch("alpinac.reporter._get_cases_to_read")
    def test_read_problematic_cases(self, mocked_get_cases_to_read):
        """
        Checks the DataFrame generated from `read` in the cases where
        alpinac's `make_identification_from_file` took too long,
        the folder of a compound's result was empty, there were
        no compounds' folders or one of the required output's files
        were not present in the compound's folder.

        In the docker it was breaking because of the paths, so it is
        skipped if the operating system is not windows. 
        """
        # creating empty folders to check that reporter creates the right messages
        empty_folder_and_no_compounds = (
            self.folder / "test_read" / "test_empty_folder_and_no_compounds"
        )
        empty_folder_and_no_compounds.mkdir(parents=True, exist_ok=True)
        empty_compound = self.folder / "test_read" / "test_empty_folder" / "Compound_1"
        empty_compound.mkdir(parents=True, exist_ok=True)

        with self.reporter_input_path.open() as f:
            cases = json.load(f)

        for case in cases:
            # adjust the paths (the json file has partial paths, because the complete
            # paths depend on the computer. Moreover, in the json we have windows 
            # paths, which don't work in linux. Hence, the split)
            case["file"] = self.folder.joinpath(*case["file"].split("\\"))

        expected = pd.read_csv(
            self.folder / "test_read" / "expected_df.csv",
            index_col=0,
            dtype=object,
            na_filter=False,
        )
        # by default read_csv tries to fit the type of the dataframe, but the dataframe generated from
        # read has only object types.

        mocked_get_cases_to_read.return_value = cases

        result = read(self.reporter_input_path, self.reportage_path)
        
        result = result.sort_values(by="compound", ignore_index=True)
        expected = expected.sort_values(by="compound", ignore_index=True)

        pd.testing.assert_frame_equal(result, expected)

    @patch("alpinac.reporter._get_cases_to_identify")
    @patch("alpinac.reporter._identify")
    def test_run(self, mocked_identify, mocked_get_cases_to_identify):
        """
        Checks that the `run` method just calls the methods `_cases_to_identify`
        and `_identify` with the expected arguments.
        """
        cases_to_identify = unittest.mock.Mock()
        mocked_get_cases_to_identify.return_value = cases_to_identify
        exected_processes = 4
        run(self.reporter_input_path)

        mocked_get_cases_to_identify.assert_called_once_with(
            self.reporter_input_path, self.reportage_path
        )
        mocked_identify.assert_called_once_with(
            cases_to_identify, 600, exected_processes
        )

    @patch("alpinac.reporter.run")
    @patch("alpinac.reporter.read")
    @patch("alpinac.reporter._to_xlsx")
    def test_investigate(self, mocked_to_xlsx, mocked_read, mocked_run):
        """
        Checks that the `investigate` method just calls the methods
        `run`, `read` and `_to_xlsx` with the expected arguments
        and the DataFrame of the final csv file is empty.
        """
        read_cases = pd.DataFrame()
        mocked_read.return_value = read_cases
        expected_timeout = 600
        expected_processes = 4
        result = investigate(self.reporter_input_path)

        mocked_run.assert_called_once_with(
            self.reporter_input_path,
            self.reportage_path,
            expected_timeout,
            expected_processes,
        )
        mocked_read.assert_called_once_with(
            self.reporter_input_path, self.reportage_path
        )
        mocked_to_xlsx.assert_called_once_with(self.reportage_path, True)
        self.assertEqual(result.to_json(), read_cases.to_json())
        self.assertEqual(len(pd.read_csv(self.reportage_path)), 0)

    def test_generate(self):
        """
        Checks that the `generate` method creates the expected json file.
        """
        test_generate_folder = self.folder / "test_generate"
        grouped_by_folder = test_generate_folder / "files_grouped_by_folder"
        grouped_by_file_name = test_generate_folder / "files_grouped_by_name"
        not_grouped_files = test_generate_folder / "files_not_grouped"
        funny_named_files = test_generate_folder / "files_funny_named"
        report_input_path = test_generate_folder / "reporter_input.json"
        substances_df = pd.read_csv(
            self.folder.parents[1] / "alpinac" / "validation" / "substances.csv"
        )
        report_input_path.unlink(True)
        possible_compound_aliases = sum(
            substances_df["Aliases"].apply(
                lambda aliases: str(aliases).strip("]['").split("', '")
            ),
            [],
        )
        # the aliases column is either a string of a list (e.g. "['H2O']")
        # or nan. Thus we use strip and split. Since we get a series of
        # list[str], sum makes unifies the list into one list[str]
        possible_compound_names = substances_df["Compound"].to_list() + ["unknown_compound"]

        generate(
            {
                "{group}\\{something}_{compound}_{random}.txt": grouped_by_folder,
                "{group}\\frags_{random}_{compound}.csv": grouped_by_folder,
                "frags_{group}_{compound}.csv": grouped_by_file_name,
                "frags_{group}_{compound}_{random}.csv": grouped_by_file_name,
                "frag_{group}_{compound}_{random}.csv": grouped_by_file_name,
                "frags_{something}_{compound}_{random}.csv": not_grouped_files,
                "{random_nr}.{another_random_nr}.tank.3.frag.val_{compound}.csv": not_grouped_files,
                "{random_nr}.{another_random_nr}.{random_str}.7.frag.val_.csv": funny_named_files,
                "230401.1747.air.3.frag.{random_nr}.csv": funny_named_files,
                "fra@{random_str}@sda189u12@{compound}.csv": funny_named_files,
                "frag!{group}!{compound}.csv": funny_named_files,
                "frag4{group}4{compound}.csv": funny_named_files,
                "fragEmpa{group}Empa{compound}.csv": funny_named_files,
                "FUNNY%{group}%{compound}.csv": funny_named_files,
                "{group}ç{pattern}ç{compound}.csv": funny_named_files,
                "{group}+{pattern}+{compound}.csv": funny_named_files,
            },
            report_input_path,
        )

        with report_input_path.open() as report_json:
            report_input = json.load(report_json)
        files_paths = (
            list(grouped_by_file_name.glob("*"))
            + list(not_grouped_files.glob("*"))
            + list(funny_named_files.glob("*"))
        )
        for folder in grouped_by_folder.glob("*"):
            files_paths = files_paths + list(folder.glob("*"))

        for data in report_input:
            with self.subTest(msg=str(data)):
                file_path = Path(data["file"])

                self.assertTrue(file_path.is_file())
                self.assertTrue(file_path in files_paths)
                self.assertTrue(
                    data["Compound"] in possible_compound_aliases
                    or data["Compound"] in possible_compound_names
                )

        report_input_path.unlink(missing_ok=False)  # delete the generated json

    def test_identify(self):
        """
        Checks that if the method `_identify` is called, then
        `_func_timeout_adapter` is also called by a `Pool` with the
        expected parameters.
        """
        kwargs = {
            "target_elements": None,
            "target_formula": None,
            "show_plt_figures": False,
            "output_dir": None,
            "raise_error": False,
            "compounds": [],
        }
        processes = 32
        time_out = 2
        cases = [
            {
                "file": "path1",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "path2",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "path3",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "wait",
                "identification_kwagrs": kwargs,
            },
        ]
        expected_call_args_list = [
            call(
                _make_identification_from_file_adapter,
                [
                    (time_out, index + 1, case, len(cases))
                    for index, case in enumerate(cases)
                ],
            )
        ]

        with patch("alpinac.reporter.Pool") as mock_pool_init:
            mock_pool_instance = mock_pool_init.return_value.__enter__.return_value
            _identify(cases, time_out, processes)
            self.assertEqual(
                mock_pool_instance.map.call_args_list, expected_call_args_list
            )

    @patch("alpinac.reporter.make_identification_from_file")
    def test_make_identification_from_file_adapter(
        self, mocked_make_identification_from_file
    ):
        """
        Checks that if the method `_make_identification_from_file_adapter`
        is called, then `make_identification_from_file` is also called
        with the expected parameters. Moreover, if
        `make_identification_from_file` takes too long, the method
        `_identification_took_too_much` should be called (with the
        expected parameters).
        """
        kwargs = {
            "target_elements": None,
            "target_formula": None,
            "show_plt_figures": False,
            "output_dir": None,
            "raise_error": False,
            "compounds": [],
        }
        timeout_time = 2
        cases = [
            {
                "file": "path1",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "path2",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "path3",
                "identification_kwagrs": kwargs,
            },
            {
                "file": "wait",
                "identification_kwagrs": kwargs,
            },
        ]
        expected_call_args_list = [
            call(
                Path(case["file"]),
                timeout_time=timeout_time,
                **case["identification_kwagrs"],
            )
            for case in cases
        ]

        for index, case in enumerate(cases):
            _make_identification_from_file_adapter(
                (timeout_time, index, case, len(cases))
            )

        self.assertEqual(
            mocked_make_identification_from_file.call_args_list, expected_call_args_list
        )


if __name__ == "__main__":
    unittest.main()
