"""Test of the utils_identification module."""

from alpinac.utils_identification import (
    get_all_isotopic_patterns_one_element,
    generate_all_isotopolog_fragments,
    generate_all_isotopolog_fragments_with_relative_proba,
    p_iso_rare_compute_wrt_pure_abundant,
    p_iso_rare_compute,
    enumerate_all_valid_subfragments,
)
import unittest
import alpinac.periodic_table as chem_data
from alpinac.validation.mol_validation import mol_validation


def is_subfragment(fragment: list, sub_fragment: list) -> bool:
    i = 0
    while i < chem_data.len_frag and fragment[i] >= sub_fragment[i]:
        i += 1
    return i >= chem_data.len_frag


class TestUtilsIdentification(unittest.TestCase):
    def test_get_all_isotopic_patterns_one_element(self):
        """
        Translated old test into a unittest.

        Generates a sample of isotopic patterns with 
        get_all_isotopic_patterns_one_element and check that the iso and
        the list of patterns coincide.
        """
        sample_indexes = [(0, 5), (15, 4), (22, 5), (34, 1), (56, 5)]
        expected_iso_and_isotopic_patterns = {
            "H": (
                2,
                {
                    "H5",
                    "H4[2H]",
                    "H3[2H]2",
                    "H2[2H]3",
                    "H[2H]4",
                    "[2H]5",
                },
            ),
            "Si": (
                3,
                {
                    "Si4",
                    "Si3[29Si]",
                    "Si3[30Si]",
                    "Si2[29Si]2",
                    "Si2[29Si][30Si]",
                    "Si2[30Si]2",
                    "Si[29Si]3",
                    "Si[29Si]2[30Si]",
                    "Si[29Si][30Si]2",
                    "Si[30Si]3",
                    "[29Si]4",
                    "[29Si]3[30Si]",
                    "[29Si]2[30Si]2",
                    "[29Si][30Si]3",
                    "[30Si]4",
                },
            ),
            "Cl": (
                2,
                {
                    "Cl5",
                    "Cl4[37Cl]",
                    "Cl3[37Cl]2",
                    "Cl2[37Cl]3",
                    "Cl[37Cl]4",
                    "[37Cl]5",
                },
            ),
            "Kr": (
                6,
                {
                    "Kr",
                    "[86Kr]",
                    "[82Kr]",
                    "[83Kr]",
                    "[80Kr]",
                    "[78Kr]",
                },
            ),
            "Re": (
                2,
                {
                    "Re5",
                    "[185Re]Re4",
                    "[185Re]2Re3",
                    "[185Re]3Re2",
                    "[185Re]4Re",
                    "[185Re]5",
                },
            ),
        }
        for idx_abundant_atom, n in sample_indexes:
            with self.subTest():
                element = chem_data.element[idx_abundant_atom]
                res = get_all_isotopic_patterns_one_element(idx_abundant_atom, n)
                iso = 1 + len(chem_data.dict_abundant_rare[idx_abundant_atom])
                res_str = {chem_data.get_string_formula(frag) for frag in res}
                self.assertEqual(
                    (iso, res_str), expected_iso_and_isotopic_patterns[element]
                )

    def test_small_difference_between_p_iso_rare_compute_wrt_pure_abundant_and_p_iso_rare_compute(
        self,
    ):
        """
        Translated old test into a unittest.

        Previous name: test_p_iso_rare_compute_wrt_pure_abundant
        """
        for idx_abundant_atom in chem_data.idx_abun_isotopes[:10]:
            with self.subTest(
                msg=f"idx_abundant_atom={chem_data.element[idx_abundant_atom]}"
            ):
                E = chem_data.element[idx_abundant_atom]
                for i in range(
                    1, min(8, len(chem_data.dict_abundant_rare[idx_abundant_atom]) + 5)
                ):
                    res = get_all_isotopic_patterns_one_element(idx_abundant_atom, i)
                    iso = 1 + len(chem_data.dict_abundant_rare[idx_abundant_atom])
                    frag_abun = res[0]
                    self.assertLessEqual(iso, len(res), msg=str(E))
                    for j in range(1, len(res)):
                        frag_rare = res[j]

                        p_iso_rare = p_iso_rare_compute(frag_abun, frag_rare)

                        p_iso_wrt_pure_abundant = p_iso_rare_compute_wrt_pure_abundant(
                            frag_abun, frag_rare
                        )

                        self.assertAlmostEqual(
                            p_iso_rare, p_iso_wrt_pure_abundant, places=7
                        )

    def test_generate_all_isotopolog_fragments_with_relative_proba_and_threshold(self):
        """
        Translated old test into a unittest
        """
        key = "Sum formula"
        test_vector = [v[key] for v in mol_validation]
        for formula in test_vector:
            with self.subTest(msg=formula):
                frag_abundant = chem_data.get_fragment_from_string_formula(formula)
                res1, probas1 = generate_all_isotopolog_fragments_with_relative_proba(
                    frag_abundant, min_relative_proba=float(0.0)
                )
                threshold = probas1[len(probas1) // 2]
                res2, probas2 = generate_all_isotopolog_fragments_with_relative_proba(
                    frag_abundant, min_relative_proba=threshold
                )
                self.assertEqual(res1[: len(probas1) // 2 + 1], res2)

                self.assertIn(chem_data.get_fragment_from_string_formula(formula), res1)

                self.assertIn(chem_data.get_fragment_from_string_formula(formula), res2)

                self.assertEqual(probas1[: len(probas1) // 2 + 1], probas2)

    def test_fragment_is_in_result_of_generate_all_isotopolog_fragments_with_relative_proba(
        self,
    ):
        """
        Translated old test into a unittest.

        Previous name: test_minimal
        """
        frag_abundant = chem_data.get_fragment_from_string_formula("SF")
        res1, probas1 = generate_all_isotopolog_fragments_with_relative_proba(
            frag_abundant
        )
        self.assertIn(frag_abundant, res1)

    def test_generate_all_isotopolog_fragments_with_relative_proba(self):
        """
        Translated old test into a unittest

        The method generate_all_isotopolog_fragments_with_relative_proba (with or without
        min_relative_proba=float(0.0)) should return the same isotopologue fragments as
        generate_all_isotopolog_fragments. They can be in different orders. For this
        reason the results will be converted into sets of tuples, before we test them
        """
        key = "Sum formula"
        test_vector = [v[key] for v in mol_validation]
        for fragment_str in test_vector:
            with self.subTest(msg=str(fragment_str)):
                frag_abundant = chem_data.get_fragment_from_string_formula(fragment_str)

                res1, probas1 = generate_all_isotopolog_fragments_with_relative_proba(
                    frag_abundant
                )

                res2, probas2 = generate_all_isotopolog_fragments_with_relative_proba(
                    frag_abundant, min_relative_proba=float(0.0)
                )

                res3 = generate_all_isotopolog_fragments(
                    frag_abundant, ignore_original=False
                )
                probas3 = [p_iso_rare_compute(frag_abundant, frag) for frag in res3]

                # generate_all_isotopolog_fragments_with_relative_proba generate same fragments
                # even if min_relative_proba is set to float(0.0)
                self.assertSetEqual(
                    {tuple(frag) for frag in res1}, {tuple(frag) for frag in res2}
                )
                self.assertEqual(len(res1), len(res2))

                # both generate_all_isotopolog_fragments_with_relative_proba and
                # generate_all_isotopolog_fragments generate same fragments
                self.assertSetEqual(
                    {tuple(frag) for frag in res2}, {tuple(frag) for frag in res3}
                )
                self.assertEqual(len(res2), len(res3))

                # frag_abundant must be contained in all 3 lists
                self.assertIn(
                    frag_abundant,
                    res1,
                    msg=str([chem_data.get_string_formula(frag) for frag in res1]),
                )
                self.assertIn(
                    frag_abundant,
                    res2,
                    msg=str([chem_data.get_string_formula(frag) for frag in res2]),
                )
                self.assertIn(
                    frag_abundant,
                    res3,
                    msg=str([chem_data.get_string_formula(frag) for frag in res3]),
                )

                # sort probabilities and checks that they are very close
                probas1.sort(reverse=True)
                probas2.sort(reverse=True)
                probas3.sort(reverse=True)

                for i in range(len(probas1)):
                    self.assertAlmostEqual(probas1[i], probas2[i], places=7)
                    self.assertAlmostEqual(probas2[i], probas3[i], places=7)

    def test_sample_of_isotopologues_is_generated_by_generate_all_isotopolog_fragments(
        self,
    ):
        test_molecules = ["NF3", "CF4", "C2H6", "C2F6", "SF6"]
        sample_of_expected_isotopologues = {
            "NF3": {"[15N]F3"},
            "CF4": {"[13C]F4"},
            "C2H6": {
                "H5[2H]C2",
                "H4[2H]2C2",
                "H3[2H]3C2",
                "H2[2H]4C2",
                "H[2H]5C2",
                "[2H]6C2",
                "H6C[13C]",
            },
            "C2F6": {"C[13C]F6", "[13C]2F6"},
            "SF6": {"F6[34S]", "F6[33S]", "F6[36S]"},
        }
        for mol in test_molecules:
            with self.subTest(msg=mol):
                fragment = chem_data.get_fragment_from_string_formula(mol)
                res = generate_all_isotopolog_fragments(fragment)
                self.assertTrue(
                    {
                        chem_data.get_string_formula(iso_frag) for iso_frag in res
                    }.issuperset(sample_of_expected_isotopologues[mol])
                )

    def test_enumerate_all_valid_subfragments(self):
        """
        Translated old test into a unittest
        """
        key = "Sum formula"
        test_vector = [v[key] for v in mol_validation]
        min_mass = float(24.0)
        for formula in test_vector:
            with self.subTest(msg=formula):
                frag_abundant = chem_data.get_fragment_from_string_formula(formula)
                str_frag = chem_data.get_string_formula(frag_abundant)

                (
                    number_solutions_min_mass,
                    solutions_min_mass,
                ) = enumerate_all_valid_subfragments(frag_abundant, min_mass)

                number_solutions, solutions = enumerate_all_valid_subfragments(
                    frag_abundant
                )

                # the solutions must be subfragments of frag_abundant
                for sol in solutions:
                    self.assertTrue(
                        is_subfragment(frag_abundant, sol),
                        msg=f"expected: {str_frag}\n result: {chem_data.get_string_formula(sol)}",
                    )

                # the solutions must have mass greater than min_mass
                for sol in solutions_min_mass:
                    self.assertGreaterEqual(
                        chem_data.get_mass_not_ionised(sol), min_mass
                    )

                self.assertEqual(len(solutions), number_solutions)

                expected_number_sols = 1
                for i in [
                    index
                    for index in range(chem_data.len_frag)
                    if frag_abundant[index] > 0
                ]:
                    expected_number_sols *= frag_abundant[i] + 1

                self.assertLessEqual(len(solutions), expected_number_sols)
                self.assertIn(frag_abundant, solutions)



if __name__ == "__main__":
    unittest.main()
