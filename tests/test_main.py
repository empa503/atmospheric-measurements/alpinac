"""End to End Tests for the main function."""
import numpy as np



from alpinac.mode_identification.main import make_identification


def test_main():
    """Simple test that the main function runs properly with simple arguments."""

    output = make_identification(
        mass=[
            28.0303309,
            29.0378048,
            30.0458923,
            27.0223337,
            31.0494122,
            26.014517,
        ],
        mass_uncertainty=[
            6.06,
            2.77,
            1.88,
            1.7,
            10.58,
            2.59,
        ],
        mass_cal_uncertainty=[
            2.52,
            3,
            3.45,
            2.6,
            3.87,
            2.7,
        ],
        area=[
            799697.83,
            209165.62,
            266008.67,
            213036.96,
            7055.63,
            92527.35,
        ],

    )

if __name__ == "__main__":
    test_main()