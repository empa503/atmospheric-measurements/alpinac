# -*- coding: utf-8 -*-
"""
Test that some variables of the periodic_table are generated from the csv 
file `periodic_table.csv` as we expect them to be generated.
N.B.: If you change the file `periodic_table.csv`, you have to adapt this 
tests.
"""
import unittest

from alpinac import periodic_table


class TestPeriodicTable(unittest.TestCase):
    def setUp(self) -> None:
        self.periodic_table = periodic_table.periodic_table

    def test_dict_abundant_rare(self):
        expected_dic = {
            0: [1],
            3: [2],
            4: [5],
            6: [7],
            8: [10, 9],
            11: [],
            12: [14, 13],
            15: [16, 17],
            18: [],
            19: [21, 20, 23],
            22: [25],
            27: [24, 26],
            34: [35, 32, 33, 30, 28],
            29: [31],
            41: [39, 38, 36, 42, 40, 37],
            43: [44],
            47: [],
            52: [49, 51, 53, 54, 50, 48, 45, 46],
            56: [55],
        }
        result = periodic_table.dict_abundant_rare
        self.assertEqual(expected_dic, result)

    def test_idx_mono_valent(self):
        expected_lst = [i for i in range(len(self.periodic_table)) if self.periodic_table[i]["valence_index"] == 1]
        result = periodic_table.idx_mono_valent
        self.assertEqual(expected_lst, result)

    def test_idx_multi_valent(self):
        expected_lst = [i for i in range(len(self.periodic_table)) if self.periodic_table[i]["valence_index"] > 1]
        result = periodic_table.idx_multi_valent
        self.assertEqual(expected_lst, result)

    def test_idx_zero_valent(self):
        expected_lst = [i for i in range(len(self.periodic_table)) if self.periodic_table[i]["valence_index"] <= 0]
        result = periodic_table.idx_zero_valent
        self.assertEqual(expected_lst, result)
        

    def test_dict_rare_abundant(self):
        expected_dic = {
            1: 0,
            2: 3,
            5: 4,
            7: 6,
            10: 8,
            9: 8,
            14: 12,
            13: 12,
            16: 15,
            17: 15,
            21: 19,
            20: 19,
            23: 19,
            25: 22,
            24: 27,
            26: 27,
            35: 34,
            32: 34,
            33: 34,
            30: 34,
            28: 34,
            31: 29,
            39: 41,
            38: 41,
            36: 41,
            42: 41,
            40: 41,
            37: 41,
            44: 43,
            49: 52,
            51: 52,
            53: 52,
            54: 52,
            50: 52,
            48: 52,
            45: 52,
            46: 52,
            55: 56,
        }

        result = periodic_table.dict_rare_abundant
        self.assertEqual(expected_dic, result)

    def test_idx_rare_isotopes_i(self):
        expected_lst = [
            1,
            2,
            5,
            7,
            10,
            9,
            14,
            13,
            16,
            17,
            21,
            20,
            23,
            25,
            24,
            26,
            35,
            32,
            33,
            30,
            28,
            31,
            39,
            38,
            36,
            42,
            40,
            37,
            44,
            49,
            51,
            53,
            54,
            50,
            48,
            45,
            46,
            55,
        ]

        result = periodic_table.idx_rare_isotopes_i
        self.assertEqual(
            expected_lst,
            result,
            msg=f"dictionary (key=expected,value=result): {dict(zip(expected_lst,result))}",
        )

    def test_idx_abun_isotopes(self):
        expected_lst = [
            0,
            3,
            4,
            6,
            8,
            11,
            12,
            15,
            18,
            19,
            22,
            27,
            34,
            29,
            41,
            43,
            52,
            47,
            56,
            57,
            58,
            59,
            60,
        ]

        result = periodic_table.idx_abun_isotopes
        self.assertEqual(expected_lst, result, msg=str(dict(zip(expected_lst, result))))

    def test_idx_rare_isotopes(self):
        expected_lst = [
            1,
            2,
            5,
            7,
            9,
            10,
            13,
            14,
            16,
            17,
            20,
            21,
            23,
            24,
            25,
            26,
            28,
            30,
            31,
            32,
            33,
            35,
            36,
            37,
            38,
            39,
            40,
            42,
            44,
            45,
            46,
            48,
            49,
            50,
            51,
            53,
            54,
            55,
        ]

        result = periodic_table.idx_rare_isotopes
        self.assertEqual(expected_lst, result)

    def test_ordered_elements_string_formula_sub(self):
        """
        At the moment this fails because the element 83Kr, with index 33 in the periodic table,
        is in the wrong place (in my list it is together with the other isotopes of Kr and in the old
        list it is after the Re isotopes).
        Except for 83Kr, we have that the isotopes are grouped together (and ordered in base of the mass)
        and then atoms not isotopes (e.g. for 13C and 76Kr we have 13C->76Kr) are ordered following the following rule:
        C->N->O->S->Si->P->H->B->F->Cl->Br->I->Ne->Ar->Kr->Mo->Ag->Xe->Re.
        """
        expected_lst = [
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            19,
            20,
            21,
            23,
            15,
            16,
            17,
            18,
            0,
            1,
            2,
            3,
            11,
            22,
            25,
            29,
            31,
            47,
            12,
            13,
            14,
            24,
            26,
            27,
            28,  
            30,
            32,  
            33, 
            34,
            35,  
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,  
            57,  
            58,
            59,
            60,  
        ]

        result = periodic_table.ordered_elements_string_formula_sub
        difference = {exp: res for exp, res in zip(expected_lst, result) if exp != res}
        self.assertEqual(
            expected_lst,
            result,
            msg=f"different elements (key=expected,value=result): {difference}",
        )

if __name__ == "__main__":
    unittest.main()
