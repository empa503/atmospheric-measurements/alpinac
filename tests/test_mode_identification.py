"""End to end test on the mode identification."""
from pathlib import Path
import unittest

import numpy as np

from alpinac.mode_identification.main import make_identification
from alpinac.mode_identification.main_from_file import make_identification_from_file


class TestEndToEnd(unittest.TestCase):
    def test_C2H6(self):
        mz = [
            28.0303309,
            29.0378048,
            30.0458923,
            27.0223337,
            31.0494122,
            26.014517,
        ]
        m_u = 10 * np.array(
            [
                6.06,
                2.77,
                1.88,
                1.7,
                10.58,
                2.59,
            ]
        )
        m_cal_u = [
            2.52,
            3,
            3.45,
            2.6,
            3.87,
            2.7,
        ]
        area = [
            799697.83,
            209165.62,
            266008.67,
            213036.96,
            7055.63,
            92527.35,
        ]

        output = make_identification(
            mz,
            mass_uncertainty=m_u,
            mass_cal_uncertainty=m_cal_u,
            area=area,
            # target_elements="CHCl",
            # show_plt_figures=True,
        )

        # Make sure we find the good substance
        self.assertListEqual(
            output[0].reconstruction_results.mol_ion_formulae, ["H6C2", "H5CN"]
        )

    def test_make_identification_from_file_with_sample_files(self):
        files = [
            "200602.1550.std.7.frag.train_PFCc318.txt",
            "200602.1550.std.7.frag.train_CFC11.txt",
            "190424.0810.tank.3.frag.val_HFC41.txt",
            "200602.1550.std.7.frag.train_HCFC141b.txt",
        ]
        data_folder_path = (
            Path(__file__).parents[1] / "data" / "nontarget_screening" / "fragments"
        )
        expected_mol_ion_formulae = ["C3F6", "CFCl3", "H3CF", "H3C2F2Cl"]
        for i in range(len(files)):
            with self.subTest(msg=files[i]):
                self.__test_file(
                    files[i], data_folder_path, expected_mol_ion_formulae[i]
                )

    def __test_file(self, file, data_folder_path, expected_mol_ion_formula="none"):
        output = make_identification_from_file(
            data_folder_path / file,
        )
        if expected_mol_ion_formula != "none":
            self.assertIn(
                expected_mol_ion_formula,
                output[0].reconstruction_results.mol_ion_formulae,
            )
    def test_target_element_Xe(self):
        """
        When we tried to put the xenon in the target elements, make_identification and
        make_identification_from_file would fail. With this test, we check that that 
        does not happen. 
        It failed because `utils_identification.define_knapsack_target` returned
        a list without the index of the Xenon, but a list containing the maximal
        amount of Xenon. 
        """
        p = Path(__file__).parent / "test_mode_identification" / "frags_Xe.csv"
        target_elements= "CClHXe"
        try:
            make_identification_from_file(
                p, target_elements= target_elements
            )
        except Exception as e:
            self.fail(f"The following exception arraised while trying to identify the xenon: {str(e)}")
        


if __name__ == "__main__":
    unittest.main()
