# -*- coding: utf-8 -*-
"""
Test knapsack methods in utils_identification.

For every method used in step 1 and their inner methods, there will be at least 
one test case. The more complex a method is, the more it will be tested.
"""
import logging
import math
from pathlib import Path
import unittest

import numpy as np
import pandas as pd
import alpinac

import alpinac.periodic_table as chem_data
from alpinac.compound import Compound
from alpinac.utils_identification import define_knapsack_target
from alpinac.utils_identification import knapsack_double
from alpinac.utils_identification import unbounded_knapsack_rec_aux
from alpinac.io_tools import get_data_from_frag_file, get_data_from_jdx_file


logger = logging.getLogger("alpinac.tests.knapsack")


def count_monovalent_fragments(list_fragments):
    """
    Count how many fragments in list_fragments are made only of mono-valent atoms.
    """
    ctr = 0
    for frag in list_fragments:
        if (
            sum([frag[i] for i in chem_data.idx_multi_valent]) == 0
            and sum([frag[i] for i in chem_data.idx_mono_valent]) > 0
        ):
            ctr += 1
    return ctr


def count_frags_with_target_mass(list_fragments: list, target_mass: float):
    """
    Count how many fragments in list_fragments have the same mass as the target_mass.
    """
    ctr = 0  # count the number of fragments made of mono-valent atoms only
    for frag in list_fragments:
        if chem_data.get_mass_single_ionised(frag) == target_mass:
            ctr += 1
    return ctr


class TestKnapsack(unittest.TestCase):
    def assert_small_difference_between_target_mass_and_solutions(
        self, target_mass: float, solutions: list
    ):
        """
        Previously named: print_str_sum_formulas_solutions.
        """
        for sol in solutions:
            mass_single_io = chem_data.get_mass_single_ionised(sol)
            self.assertAlmostEqual(target_mass, mass_single_io, delta=0.01)

    def test_knapsack_double(self):
        """
        Tests the knapsack_double method for a small list of measurements and a list
        of abundant atoms with positive valence (they can build a molecule).
        """
        meas_mass = [43.014469580000004, 84.09146768]
        meas_mass_u = [0.00283504753311, 0.0018802729510759997]
        meas_mass_u_cal = [0.00026104, 0.00035282]
        u_k = 4.0

        idx_list_kn = [
            idx for idx in chem_data.idx_abun_isotopes if chem_data.valence[idx] > 0
        ]
        # write in idx_list_kn the indices of abundant atoms (most abundant of the isotopes)
        # that can build a molecule (valence > 0)

        all_solutions = []
        for idx_m in range(len(meas_mass)):
            mass_measured = meas_mass[idx_m]
            U = u_k * math.sqrt(meas_mass_u[idx_m] ** 2 + meas_mass_u_cal[idx_m] ** 2)
            m_max = mass_measured + U
            m_min = mass_measured - U
            number_solutions, valid_DBE_solutions, list_solutions = knapsack_double(
                mass_measured,
                m_max,
                m_min,
                idx_list_kn,
                max_no_each_atom=None,
                verbose=False,
            )
            all_solutions.append(list_solutions)

        for n_i in range(len(all_solutions)):
            self.assert_small_difference_between_target_mass_and_solutions(
                meas_mass[n_i], all_solutions[n_i]
            )

    def test_knapsack_double_NIST_data(self):
        """
        Translated old test into a unittest.

        Test vectors and tests for knapsack on NIST data.
        --> the initial molecule is known, the (corrected) mass spectrum is known,
        we want to label each fragment, to know exactly how the molecule splits.
        """
        # hexa-bromo-benzene: C6Br6
        # toffile_list = list(dict_path['NIST_EI'].glob("87-82-1-Mass.jdx"))
        # cyclo-hexane: H12C6
        path_file = Path(*alpinac.__path__, "../data/nontarget_screening/NIST_EI/110-82-7-Mass.jdx")

        # Batch fragment is a dict containing as values list of peak information
        self.__test_nist_file(path_file)

    def __test_nist_file(self, path_file):
        """
        Checks that the masses of the solutions of knapsack_double are close
        enough to the measured masses.
        """
        batch_fragments, nist_metadata = get_data_from_jdx_file(path_file)
        batch_fragment = batch_fragments[0]

        # ********************************************************
        # CASE OF KNOWN FORMULA: WORKING FROM THE NIST EI DATABASE
        # ********************************************************
        idx_list_kn, max_no_each_atom = define_knapsack_target(
            mass_max=batch_fragment[-1][1],
            # target_formula=target_formula,
        )

        cl_comp = Compound(batch_fragment)

        # NIST data: use integer mass of atoms to run the knapsack.
        # Note: this is needed since the NIST database contains spectra already corrected for mass defect
        # (I don't understand why but this is how it is)
        all_solutions = []
        total_solutions = 0
        total_DBE_solutions = 0
        for idx_m in range(cl_comp.meas_len):
            number_solutions, valid_DBE_solutions, list_solutions = knapsack_double(
                cl_comp.meas_mass[idx_m],
                cl_comp.meas_mass_max[idx_m],
                cl_comp.meas_mass_min[idx_m],
                idx_list_kn,
                max_no_each_atom,
            )
            all_solutions.append(list_solutions)
            total_solutions += number_solutions
            total_DBE_solutions += valid_DBE_solutions

        self.assertEqual(total_DBE_solutions, total_solutions)

        for i in range(cl_comp.meas_len):
            for sol in all_solutions[i]:
                with self.subTest(
                    msg=f"{cl_comp.meas_mass[i]:5.1f} m/z :{(chem_data.get_string_formula(sol),chem_data.get_mass_not_ionised(sol))}"
                ):
                    self.assertAlmostEqual(
                        cl_comp.meas_mass[i],
                        chem_data.get_mass_not_ionised(sol),
                        places=0,
                    )

    def test_unbounded_knapsack_rec_aux_does_nothing_if_id_kn_is_negative(self):
        """
        Checks that the method unbounded_knapsack_rec_aux does nothing
        if id_kn is negative.
        """
        ctr, ctr_DBE = 0, 0
        mass_measured = 7.0
        mass_max = 7.0
        mass_min = 6.0
        vec_fragment = list(range(10))
        id_kn = -1
        idx_list_kn = []
        results_lst_negative_id_kn = []

        self.assertTrue(id_kn < 0)
        self.assertFalse(mass_max < 0)
        self.assertFalse(mass_min > 0 and mass_max < chem_data.mass[0])
        self.assertEqual(results_lst_negative_id_kn, [])

        result_negative_id_kn = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_negative_id_kn,
        )

        # for id_kn < 0 don't add solutions to list of results
        self.assertEqual(result_negative_id_kn, (ctr, ctr_DBE))
        self.assertEqual(results_lst_negative_id_kn, [])

    def test_unbounded_knapsack_rec_aux_does_nothing_if_mass_min_is_positive_and_mass_max_is_less_than_chem_data_mass0(
        self,
    ):
        """
        Checks that the method unbounded_knapsack_rec_aux does nothing
        if (mass_min is positive and mass_max < chem_data.mass[0]).
        """
        ctr, ctr_DBE = 0, 0
        mass_measured = 7.0
        mass_max = chem_data.mass[0] - 1
        mass_min = 6.0
        vec_fragment = list(range(10))
        id_kn = 0
        idx_list_kn = []
        results_lst_positive_mass_min_and_mass_max_less_than_m0 = []

        self.assertFalse(id_kn < 0)
        self.assertFalse(mass_max < 0)
        self.assertTrue(mass_min > 0 and mass_max < chem_data.mass[0])
        self.assertEqual(results_lst_positive_mass_min_and_mass_max_less_than_m0, [])

        result_positive_mass_min_and_mass_max_less_than_m0 = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_positive_mass_min_and_mass_max_less_than_m0,
        )

        # for mass_max < chem_data.mass[0] don't add solutions to list of results
        self.assertEqual(
            result_positive_mass_min_and_mass_max_less_than_m0, (ctr, ctr_DBE)
        )
        self.assertEqual(results_lst_positive_mass_min_and_mass_max_less_than_m0, [])

    def test_unbounded_knapsack_rec_aux_does_nothing_if_mass_max_is_negative(self):
        """
        Checks that the method unbounded_knapsack_rec_aux does nothing
        if mass_max is negative.
        """
        ctr, ctr_DBE = 0, 0
        mass_measured = 7.0
        mass_max = -7.0
        mass_min = -6.0
        vec_fragment = list(range(10))
        id_kn = 0
        idx_list_kn = []
        results_lst_negative_mass_max = []

        self.assertFalse(id_kn < 0)
        self.assertTrue(mass_max < 0)
        self.assertFalse(mass_min > 0 and mass_max < chem_data.mass[0])
        self.assertEqual(results_lst_negative_mass_max, [])

        result_negative_mass_max = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_negative_mass_max,
        )

        # for mass_max < 0 don't add solutions to list of results (mass_min is
        # also negative, to not trigger mass_min > 0 and mass_max < chem_data.mass[0])
        self.assertEqual(result_negative_mass_max, (ctr, ctr_DBE))
        self.assertEqual(results_lst_negative_mass_max, [])

    def test_unbounded_knapsack_rec_aux_base_case_with_limited_atoms(self):
        """
        Test unbounded_knapsack_rec_aux with a vector and limited amounts of atoms,
        only one component of the vector changes.

        We start with an empty list and a vector [0,1,...,7,8,9] and we expect to have
        a list containing only [0,1,...,6,1,8,9].
        """
        mass_measured = 13.0
        mass_max = 123.0
        mass_min = 3.0
        vec_fragment = list(range(10))
        id_kn = 0
        idx_list_kn = [7]  # element of vec_fragment with index 7 changes
        results_lst_limited_number_of_atoms = []
        max_no_each_atom = [
            1
        ]  # limit to one atom the element of vec_fragment with index 7

        self.assertFalse(
            id_kn < 0 or mass_max < 0 or (mass_min > 0 and mass_max < chem_data.mass[0])
        )
        self.assertTrue(id_kn == 0)
        self.assertEqual(results_lst_limited_number_of_atoms, [])

        result_with_limited_number_of_atoms = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_limited_number_of_atoms,
            max_no_each_atom,
        )

        self.assertEqual(result_with_limited_number_of_atoms, (1, 1))
        self.assertEqual(
            results_lst_limited_number_of_atoms, [[0, 1, 2, 3, 4, 5, 6, 1, 8, 9]]
        )

    def test_unbounded_knapsack_rec_aux_base_case_without_limited_atoms(self):
        """
        Test unbounded_knapsack_rec_aux with a vector and an "infinite" amounts of atoms,
        only one component of the vector changes.

        We start with an empty list and a vector [0,1,...,7,8,9] and we expect to have
        a list containing only vector of the form [0,1,...,6,index,8,9] for index=8,...,1.
        """
        mass_measured = 13.0
        mass_max = 123.0
        mass_min = 3.0
        vec_fragment = list(range(10))
        id_kn = 0
        results_lst_without_atom_limits = []
        idx_list_kn = [7]  # element of vec_fragment with index 7 changes

        self.assertFalse(
            id_kn < 0 or mass_max < 0 or (mass_min > 0 and mass_max < chem_data.mass[0])
        )
        self.assertTrue(id_kn == 0)
        self.assertEqual(results_lst_without_atom_limits, [])

        results_lst_without_atoms_limits = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_without_atom_limits,
        )

        self.assertEqual(results_lst_without_atoms_limits, (8, 8))
        self.assertEqual(
            results_lst_without_atom_limits,
            [[0, 1, 2, 3, 4, 5, 6, change, 8, 9] for change in range(8, 0, -1)],
        )

    def test_unbounded_knapsack_rec_aux_two_components_of_the_vector_change_with_limited_atoms(
        self,
    ):
        """
        Test unbounded_knapsack_rec_aux with a vector and an "infinite" amounts of atoms,
        two components of the vector change.

        We start with an empty list and a vector [0,1,...,7,8,9] and we expect to have
        a list containing only vector of the form [0,1,index1,3,...,6,index2,8,9] for
        index1=1,0 and index2=4,...,0.
        """
        mass_measured = 13.0
        mass_max = 123.0
        mass_min = 3.0
        vec_fragment = list(range(10))
        id_kn = 1
        idx_list_kn = [2, 8]  # elements of vec_fragment with index 2 and 8 change
        results_lst_limited_number_of_atoms = []
        max_no_each_atom = [
            1,
            4,
        ]  # limit to one atom (respectively 4 atoms) the element of vec_fragment with index 2 (respectively with index 8)

        self.assertEqual(results_lst_limited_number_of_atoms, [])

        results_with_limited_atoms = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_limited_number_of_atoms,
            max_no_each_atom,
        )

        self.assertEqual(results_with_limited_atoms, (9, 9))

        expected_lst = [
            [0, 1, change_1, 3, 4, 5, 6, 7, change_2, 9]
            for change_2 in range(4, -1, -1)
            for change_1 in range(1, -1, -1)
        ]
        expected_lst.pop()

        self.assertEqual(results_lst_limited_number_of_atoms, expected_lst)

    def test_unbounded_knapsack_rec_aux_two_components_of_the_vector_change_without_limited_atoms(
        self,
    ):
        """
        Test unbounded_knapsack_rec_aux with a vector and an "infinite" amounts of atoms,
        two components of the vector change.

        We start with an empty list and a vector [0,1,...,7,8,9] and we expect to have
        a list containing only vector of the form [0,1,index1,3,...,6,index2,8,9], where
        index1 and index2 are two non negative integers. Since the amount of results is too
        big, only the presence of a sample of expected result is checked.
        """
        mass_measured = 13.0
        mass_max = 123.0
        mass_min = 3.0
        vec_fragment = list(range(10))
        id_kn = 1
        idx_list_kn = [2, 8]  # elements of vec_fragment with index 2 and 8 change
        results_lst_without_atoms_limits = []

        expected_sample_vectors = {
            (0, 1, 1, 3, 4, 5, 6, 7, 0, 9),
            (0, 1, 5, 3, 4, 5, 6, 7, 4, 9),
            (0, 1, 6, 3, 4, 5, 6, 7, 3, 9),
            (0, 1, 8, 3, 4, 5, 6, 7, 0, 9),
            (0, 1, 9, 3, 4, 5, 6, 7, 1, 9),
        }

        results_without_atoms_limits = unbounded_knapsack_rec_aux(
            mass_measured,
            mass_max,
            mass_min,
            vec_fragment,
            id_kn,
            idx_list_kn,
            results_lst_without_atoms_limits,
        )

        self.assertEqual(results_without_atoms_limits, (57, 57))
        self.assertEqual(
            {tuple(result) for result in results_lst_without_atoms_limits},
            {tuple(result) for result in results_lst_without_atoms_limits}
            | expected_sample_vectors,
        )

    def test_knapsack_double_with_a_file(self):
        """
        Tests the method knapsack_double using the data from 230401.1747.air.3.frag.1463s1470s.txt.

        We take the data from 230401.1747.air.3.frag.1463s1470s.txt, we define the target (assuming
        that mass_max is the maximal mass in the data) and we use knapsack_double for each mass from the data.
        We expect to find a molecule for some peaks
        """
        batch_frag = get_data_from_frag_file(
            Path(__file__).parents[1] / "data" / "test_input_files" / "230401.1747.air.3.frag.1463s1470s.txt"
        )

        expected_tuple_of_mass_formula = [
            (100.9333113, "CFCl2"),
            (100.9364716, "CFCl2"),
            (84.9656838, "CF2Cl"),
            (65.9643661, "CFCl"),
            (49.99500758, "CF2"),
            (46.96759838, "CCl"),
            (34.96721678, "Cl"),
        ]
        columns = [
            "mass",
            "mass_u",
            "knapsack_solutions",
        ]
        all_fragments = np.asarray(sum(batch_frag.values(), []))
        idx_list_kn, max_no_each_atom = define_knapsack_target(
            mass_max=all_fragments[:, 1].astype(float).max(),
            target_atoms="HBCNOFSiPSClBrI",
            target_formula=None,
        )

        df = pd.DataFrame(columns=columns)

        for comp_bin in batch_frag:
            frag_data = batch_frag[comp_bin]
            cl_comp = Compound(frag_data, comp_bin)
            # by construction, idx_list_kn contains atoms ordered by increasing mass.
            all_solutions = []
            no_monovalent_fragments = [0] * cl_comp.meas_len

            for idx_m in range(cl_comp.meas_len):
                number_solutions, valid_DBE_solutions, list_solutions = knapsack_double(
                    cl_comp.meas_mass[idx_m],
                    cl_comp.meas_mass_max[idx_m],
                    cl_comp.meas_mass_min[idx_m],
                    idx_list_kn,
                    max_no_each_atom,
                )

                all_solutions += list_solutions
                no_monovalent_fragments[idx_m] += count_monovalent_fragments(list_solutions)
                df.loc[idx_m] = [
                    cl_comp.meas_mass[idx_m],
                    cl_comp.meas_mass_u[idx_m],
                    [chem_data.get_string_formula(sol) for sol in list_solutions],
                ]

        self.assertEqual(
            sum(no_monovalent_fragments), 1, msg=str(no_monovalent_fragments)
        )
        self.assertEqual(len(all_solutions), 96)
        for pair in expected_tuple_of_mass_formula:
            with self.subTest(msg=str(pair)):
                solutions = df[np.isclose(df["mass"], pair[0])][
                    "knapsack_solutions"
                ].values.tolist()[0]
                self.assertTrue(len(solutions) != 0)
                self.assertIn(pair[1], solutions)


if __name__ == "__main__":
    unittest.main()
