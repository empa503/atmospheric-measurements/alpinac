# ALPINAC

ALgorithmic Process for Identification of Non-targeted Atmospheric Compounds

## Pre-logging.info
**Automated fragment identification for electron ionisation mass spectrometry:
application to atmospheric measurements of halocarbons**,
Myriam Guillevic, Aurore Guillevic, Martin Vollmer, Paul Schlauri, Matthias
Hill, Lukas Emmenegger, Stefan Reimann.
[hal-03176025](https://hal.inria.fr/hal-03176025),
[arxiv 2103.13807](https://arxiv.org/abs/2103.13807).

## Releases
- 06.2021 First release with LGPL licence, Copyright EMPA and Inria, 2019 - 2021.
- 06.2022 Packaging pre-release on EMPA gitlab.
- 08.2022 API for both files and arrays inputs.
- 04.2023 Official documentation.



## Installation

### Downloading the source from git

Currently hosted on EMPA gitlab.

Clone the halosearch directory.

```
git clone git@gitlab.com:empa503/atmospheric-measurements/alpinac.git
```

### With pip (All platforms)

1. Go to the halosearch directory (where this readme is located)
2. run `pip install -e .`


## Quick-Start

The main function for using alpinaq is `mode_identification`.

It can be called in two different ways.

### Command line

When installing with pip, the command `alpinac` becomes available.

```
alpinac <filename> [--target-elements <elements> --target-formula <formula>  --verbose-knapsack]
```

For more options, you can run:
 
```
alpinac -h
```

The first compulsory argument is the filename, preceeded by the directory.

Additional arguments can be provided with:
- `--target-elements`: chemical elements potentially present, e.g. CHFClBr. The order has no importance.

- `--target-formula`: the chemical formula of the molecular ion. ALPINAC will use only this formula or smaller fragments. For example with CCl4 given, ALPINAC will use only C, Cl, CCl, CCl2, CCl3, CCl4 and Cl2 (and all possible isotopologues).


Command-line
examples are provided in
[`mode_identification_script_train.sh`](./scripts/mode_identification_script_train.sh)
and [`mode_identification_script_val.sh`](./scripts/mode_identification_script_val.sh).


### From Python script
The function `make_identification` must be imported from alpinac using

```
from alpinac.mode_identification.main import make_identification
```

This function can do the identification with pre-loaded spectrum data.

If you prefer reading the files from python, 
the file 
[`mode_identification_script.py`](./scripts/mode_identification_script.py) 
show an example of how to integrate the function in a Python script.
It is configured to call `make_identification_from_file` for each file of the training set, using default parameters.

### Reporter 

If you want to run many files together and be able to visualize the results, 
you can run the reporter.

```
python -m alpinac.reporter path/to/folder/containing/input_files
```

To get more information on the reporter, you can run:
```
python -m alpinac.reporter -h
```








